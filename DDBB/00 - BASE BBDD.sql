
CREATE TABLE [dbo].[parameters](
	[activitiesenabled_revsthirtyminutes] [bit] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[parameters] ADD  CONSTRAINT [DF_parameters_activitiesenabled_revsthirtyminutes]  DEFAULT ((0)) FOR [activitiesenabled_revsthirtyminutes]
GO

--CREAMOS UN 1� registro de parametros
INSERT INTO parameters (activitiesenabled_revsthirtyminutes) VALUES (0);
GO

--CREAR TABLA USUARIOS
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number] [int] NOT NULL,
	[dni] [varchar](20) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[lastname] [varchar](150) NOT NULL,
	[association] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[cp] [varchar](10) NOT NULL,
	[birthday] [date] NOT NULL,
	[shirt] [varchar](10) NOT NULL,
	[lunch] [bit] NOT NULL,
	[sleep] [bit] NOT NULL,
	[tutorpermission] [bit] NOT NULL,
	[comments] [text] NOT NULL,
	[createdate] [datetime] NOT NULL,
	[modifydate] [datetime] NOT NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_number]  DEFAULT ((0)) FOR [number]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_shirt]  DEFAULT ('NO') FOR [shirt]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_lunch]  DEFAULT ((0)) FOR [lunch]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_sleep]  DEFAULT ((0)) FOR [sleep]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_tutorpermission]  DEFAULT ((0)) FOR [tutorpermission]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_createdate]  DEFAULT (getdate()) FOR [createdate]
GO

ALTER TABLE [dbo].[users] ADD  CONSTRAINT [DF_users_modifydate]  DEFAULT (getdate()) FOR [modifydate]
GO

--CREAR TRIGGER USUARIOS
CREATE TRIGGER trg_users_Update
ON dbo.users
AFTER UPDATE
AS
    UPDATE dbo.users
    SET modifydate = GETDATE()
    WHERE ID IN (SELECT DISTINCT ID FROM INSERTED)
GO



--CREAR TABLA USUARIOS WEB
CREATE TABLE [dbo].[web_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dni] [varchar](20) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[lastname] [varchar](150) NOT NULL,
	[association] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[cp] [varchar](10) NOT NULL,
	[birthday] [date] NOT NULL,
	[shirt] [varchar](10) NOT NULL,
	[lunch] [bit] NOT NULL,
	[sleep] [bit] NOT NULL,
	[tutorpermission] [bit] NOT NULL,
	[comments] [text] NOT NULL,
	[createdate] [datetime] NOT NULL,
 CONSTRAINT [PK_web_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[web_users] ADD  CONSTRAINT [DF_web_users_shirt]  DEFAULT ('NO') FOR [shirt]
GO

ALTER TABLE [dbo].[web_users] ADD  CONSTRAINT [DF_web_users_lunch]  DEFAULT ((0)) FOR [lunch]
GO

ALTER TABLE [dbo].[web_users] ADD  CONSTRAINT [DF_web_users_sleep]  DEFAULT ((0)) FOR [sleep]
GO

ALTER TABLE [dbo].[web_users] ADD  CONSTRAINT [DF_web_users_tutorpermission]  DEFAULT ((0)) FOR [tutorpermission]
GO

ALTER TABLE [dbo].[web_users] ADD  CONSTRAINT [DF_web_users_createdate]  DEFAULT (getdate()) FOR [createdate]
GO


--CREAR TABLA Actividades
CREATE TABLE [dbo].[activities](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[organizer] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[association] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[activitydate] [datetime] NOT NULL,
	[place] [varchar](100) NOT NULL,
	[type] [varchar](100) NOT NULL,
	[participants] [varchar](100) NOT NULL,
	[maxparticipants] [int] NOT NULL,
	[enabled] [bit] NOT NULL,
	[summary] [text] NOT NULL,
	[comments] [text] NOT NULL,
	[needs] [text] NOT NULL,
	[createdate] [datetime] NOT NULL,
	[modifydate] [datetime] NOT NULL,
 CONSTRAINT [PK_activities] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[activities] ADD  CONSTRAINT [DF_activities_maxparticipants]  DEFAULT ((0)) FOR [maxparticipants]
GO

ALTER TABLE [dbo].[activities] ADD  CONSTRAINT [DF_activities_enabled]  DEFAULT ((1)) FOR [enabled]
GO

ALTER TABLE [dbo].[activities] ADD  CONSTRAINT [DF_activities_createdate]  DEFAULT (getdate()) FOR [createdate]
GO

ALTER TABLE [dbo].[activities] ADD  CONSTRAINT [DF_activities_modifydate]  DEFAULT (getdate()) FOR [modifydate]
GO

--CREAR TRIGGER Actividades
CREATE TRIGGER trg_Actividades_Update
ON dbo.activities
AFTER UPDATE
AS
    UPDATE dbo.activities
    SET modifydate = GETDATE()
    WHERE ID IN (SELECT DISTINCT ID FROM INSERTED)
GO

--CREAR TABLA activity_users
CREATE TABLE [dbo].[activity_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[activity_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[inscriptiondate] [datetime] NOT NULL,
	[reserve] [bit] NOT NULL,
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_activity_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[activity_users] ADD  CONSTRAINT [DF_activity_users_inscriptiondate]  DEFAULT (getdate()) FOR [inscriptiondate]
GO

ALTER TABLE [dbo].[activity_users] ADD  CONSTRAINT [DF_activity_users_reserve]  DEFAULT ((0)) FOR [reserve]
GO

ALTER TABLE [dbo].[activity_users]  WITH CHECK ADD  CONSTRAINT [FK_activity_users_activities] FOREIGN KEY([id])
REFERENCES [dbo].[activities] ([id])
GO

ALTER TABLE [dbo].[activity_users] CHECK CONSTRAINT [FK_activity_users_activities]
GO

ALTER TABLE [dbo].[activity_users]  WITH CHECK ADD  CONSTRAINT [FK_activity_users_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO

ALTER TABLE [dbo].[activity_users] CHECK CONSTRAINT [FK_activity_users_users]
GO

--CREAR TABLA games
CREATE TABLE [dbo].[games](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](200) NOT NULL,
	[editorial] [varchar](200) NOT NULL,
	[family] [int] NOT NULL,
	[type] [int] NOT NULL,
	[years] [int] NOT NULL,
	[player_min] [int] NOT NULL,
	[player_max] [int] NOT NULL,
	[difficulty] [int] NOT NULL,
	[expansion] [int] NOT NULL,
	[duration] [int] NOT NULL,
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_games] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_family]  DEFAULT ((0)) FOR [family]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_type]  DEFAULT ((0)) FOR [type]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_years]  DEFAULT ((0)) FOR [years]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_player_min]  DEFAULT ((0)) FOR [player_min]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_player_max]  DEFAULT ((0)) FOR [player_max]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_difficulty]  DEFAULT ((0)) FOR [difficulty]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_expansion]  DEFAULT ((0)) FOR [expansion]
GO

ALTER TABLE [dbo].[games] ADD  CONSTRAINT [DF_games_duration]  DEFAULT ((0)) FOR [duration]
GO


--CREATE TABLA associations
CREATE TABLE [dbo].[associations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](200) NOT NULL,
	[contact] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_associations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


--CREAR TABLA localgames
CREATE TABLE [dbo].[localgames](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[game_id] [int] NOT NULL,
	[association_id] [int] NOT NULL,
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_localgames] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[localgames]  WITH CHECK ADD  CONSTRAINT [FK_localgames_associations] FOREIGN KEY([association_id])
REFERENCES [dbo].[associations] ([id])
GO

ALTER TABLE [dbo].[localgames] CHECK CONSTRAINT [FK_localgames_associations]
GO

ALTER TABLE [dbo].[localgames]  WITH CHECK ADD  CONSTRAINT [FK_localgames_games] FOREIGN KEY([game_id])
REFERENCES [dbo].[games] ([id])
GO

ALTER TABLE [dbo].[localgames] CHECK CONSTRAINT [FK_localgames_games]
GO


--crear tabla games_lending
CREATE TABLE [dbo].[games_lending](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[localgame_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[modifydate] [datetime] NOT NULL,
	[comments] [text] NOT NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PK_games_lending] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[games_lending] ADD  CONSTRAINT [DF_games_lending_modifydate]  DEFAULT (getdate()) FOR [modifydate]
GO

ALTER TABLE [dbo].[games_lending] ADD  CONSTRAINT [DF_games_lending_status]  DEFAULT ((0)) FOR [status]
GO

ALTER TABLE [dbo].[games_lending]  WITH CHECK ADD  CONSTRAINT [FK_games_lending_localgames] FOREIGN KEY([id])
REFERENCES [dbo].[localgames] ([id])
GO

ALTER TABLE [dbo].[games_lending] CHECK CONSTRAINT [FK_games_lending_localgames]
GO

ALTER TABLE [dbo].[games_lending]  WITH CHECK ADD  CONSTRAINT [FK_games_lending_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO

ALTER TABLE [dbo].[games_lending] CHECK CONSTRAINT [FK_games_lending_users]
GO


--CREATE TABLE [TPV_ProductoTipo]
CREATE TABLE [dbo].[TPV_ProductoTipo](
	[tipoID] [int] IDENTITY(1,1) NOT NULL,
	[tipoName] [text] NOT NULL,
 CONSTRAINT [PK_TPV_ProductoTipo] PRIMARY KEY CLUSTERED 
(
	[tipoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
--Resetea el indice autonumerico a 0, para coger el primer valor el 1
DBCC CHECKIDENT ('TPV_ProductoTipo', RESEED, 1);  
GO
DELETE FROM [dbo].[TPV_ProductoTipo];
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Bebida');
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Comida');
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Ropa');
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Otros');
GO

--CREATE TABLE TPV_Productos
CREATE TABLE [dbo].[TPV_Productos](
	[productoID] [int] IDENTITY(1,1) NOT NULL,
	[productoCB] [text] NOT NULL,
	[productoName] [text] NOT NULL,
	[productoTipo] [int] NOT NULL,
	[productoPrecioSinIva] [float] NOT NULL,
	[productoIVA] [int] NOT NULL,
	[productoPrecio] [float] NOT NULL,
	[productoCantidad] [int] NOT NULL,
 CONSTRAINT [PK_TPV_Productos] PRIMARY KEY CLUSTERED 
(
	[productoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[TPV_Productos] ADD  CONSTRAINT [DF_TPV_Productos_productoPrecioSinIva]  DEFAULT ((0)) FOR [productoPrecioSinIva]
GO

ALTER TABLE [dbo].[TPV_Productos] ADD  CONSTRAINT [DF_TPV_Productos_productoIVA]  DEFAULT ((0)) FOR [productoIVA]
GO

ALTER TABLE [dbo].[TPV_Productos] ADD  CONSTRAINT [DF_TPV_Productos_productoPrecio]  DEFAULT ((0)) FOR [productoPrecio]
GO

ALTER TABLE [dbo].[TPV_Productos] ADD  CONSTRAINT [DF_TPV_Productos_productoCantidad]  DEFAULT ((0)) FOR [productoCantidad]
GO

ALTER TABLE [dbo].[TPV_Productos]  WITH CHECK ADD  CONSTRAINT [FK_TPV_Productos_TPV_ProductoTipo] FOREIGN KEY([productoTipo])
REFERENCES [dbo].[TPV_ProductoTipo] ([tipoID])
GO

ALTER TABLE [dbo].[TPV_Productos] CHECK CONSTRAINT [FK_TPV_Productos_TPV_ProductoTipo]
GO



GO
DBCC CHECKIDENT ('TPV_Productos', RESEED, 1);  
GO
Delete from TPV_Productos;
INSERT INTO [dbo].[TPV_Productos] (productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad) 
VALUES 
('5449000000996','Cocacola',1,0.91,10,1.0,100),
('5449000131805', 'Cocacola Zero', 1, 0.91, 10, 1.0, 100),
('5449000006004', 'Fanta Limon', 1, 0.91, 10, 1.0, 100),
('8410732050011', 'Camiseta XL', 3, 8.26, 21, 10.0, 20),
('5449000121059', 'Agua Mineral', 1, 0.54, 10, 0.6, 100),
('0000000000000', 'Ticket Cesta', 4, 1.0, 10, 1.0, 1000),
('0000000000001', 'Comida', 4, 18.18, 10, 20.0, 50),
('8410022108219', 'Donuts Glace 1€', 2, 0.91, 10, 1.0, 44),
('8410022108226', 'Donuts Bombón 1€', 2, 0.91, 10, 1.0, 44),
('8410022109094', 'Donettes Clasicos 1€', 2, 0.91, 10, 1.0, 20),
('8410022109117', 'Donettes Rayados 1€', 2, 0.91, 10, 1.0, 20),
('8410022012035', 'Palmera Bollo Rellena 1€', 2, 0.91, 10, 1.0, 30),
('8410022110717', 'Bollycao Clasicos 1€', 2, 0.91, 10, 1.0, 14),
('8410022110724', 'Bollycao Leche 1€', 2, 0.91, 10, 1.0, 14),
('8410022012110', 'Caña Crema 1€', 2, 0.91, 10, 1.0, 35),
('0000000000003', 'Palmera Hojaldre', 2, 0.91, 10, 1.0, 35),
('8412600028629', 'Takis Fuego', 2, 0.91, 10, 1.0, 72),
('8412600028605', 'Takis Queso', 2, 1.36, 10, 1.5, 36),
('8412600028599', 'Takis Extrahot', 2, 1.36, 10, 1.5, 36),
('8412600029015', 'Takis Burguer', 2, 1.36, 10, 1.5, 36),
('8412600025925', 'Cacahuetes Miel', 2, 0.91, 10, 1.0, 72),
('8412600028377', 'Cacahuetes Barbacoa', 2, 1.36, 10, 1.5, 24),
('8412600028384', 'Cacahuetes Chili', 2, 1.36, 10, 1.5, 24),
('0000000000004', 'Camiseta S', 3, 8.26, 21, 10.0, 50),
('0000000000005', 'Camiseta M', 3, 8.26, 21, 10.0, 50),
('0000000000006', 'Camiseta L', 3, 8.26, 21, 10.0, 50),
('0000000000007', 'Camiseta XXL', 3, 8.26, 21, 10.0, 50),
('5449000011527', 'FANTA NARANJA', 1, 0.91, 10, 1.0, 100),
('5449000012913', 'Nestea', 1, 0.91, 10, 1.0, 100),
('5449000205520', 'Sprite', 1, 0.91, 10, 1.0, 100),
('8410099573675', 'Weikis', 2, 0.91, 10, 1.0, 100),
('5449000058560', 'Aquarius Límon', 1, 0.91, 10, 1.0, 100),
('5060166690380', 'MONSTER', 1, 1.81, 10, 2.0, 100),
('5449000033819', 'Aquarius Naranja', 1, 0.91, 10, 1.0, 100),
('0000000000008', 'CAMISETA AÑOS ANTERIORES', 3, 1.81, 10, 2.0, 1000),
('54491090','Minute Maid Piña', 1, 0.72, 10, 0.8, 100),
('40822716','Minute Maid Melocoton', 1, 0.72, 10, 0.8, 1000);
GO

--Create Tabla TPV_Pedidos
CREATE TABLE [dbo].[TPV_Pedidos](
	[pedidoID] [int] IDENTITY(1,1) NOT NULL,
	[pedidoFecha] [datetime] NOT NULL,
	[pedidoTotal] [float] NOT NULL,
	[pedidoEntregado] [float] NOT NULL,
	[pedidoDevuelto] [float] NOT NULL,
 CONSTRAINT [PK_TPV_Pedidos] PRIMARY KEY CLUSTERED 
(
	[pedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TPV_Pedidos] ADD  CONSTRAINT [DF_TPV_Pedidos_pedidoFecha]  DEFAULT (getdate()) FOR [pedidoFecha]
GO

ALTER TABLE [dbo].[TPV_Pedidos] ADD  CONSTRAINT [DF_TPV_Pedidos_pedidoTotal]  DEFAULT ((0)) FOR [pedidoTotal]
GO

ALTER TABLE [dbo].[TPV_Pedidos] ADD  CONSTRAINT [DF_TPV_Pedidos_pedidoEntregado]  DEFAULT ((0)) FOR [pedidoEntregado]
GO

ALTER TABLE [dbo].[TPV_Pedidos] ADD  CONSTRAINT [DF_TPV_Pedidos_pedidoDevuelto]  DEFAULT ((0)) FOR [pedidoDevuelto]
GO

--Create Tabla TPV_PedidoDetalle
CREATE TABLE [dbo].[TPV_PedidosDetalle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pedidoID] [int] NOT NULL,
	[productoID] [int] NOT NULL,
	[pdCantidad] [int] NOT NULL,
 CONSTRAINT [PK_TPV_PedidosDetalle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TPV_PedidosDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Pedidos] FOREIGN KEY([pedidoID])
REFERENCES [dbo].[TPV_Pedidos] ([pedidoID])
GO

ALTER TABLE [dbo].[TPV_PedidosDetalle] CHECK CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Pedidos]
GO

ALTER TABLE [dbo].[TPV_PedidosDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Productos] FOREIGN KEY([productoID])
REFERENCES [dbo].[TPV_Productos] ([productoID])
GO

ALTER TABLE [dbo].[TPV_PedidosDetalle] CHECK CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Productos]
GO


--CREATE TABLE TPV_ControlCaja
CREATE TABLE [dbo].[TPV_ControlCaja](
	[cajaMovID] [int] IDENTITY(1,1) NOT NULL,
	[cajaMovFecha] [datetime] NOT NULL,
	[cajaPersona] [text] NOT NULL,
	[cajaIngreso] [float] NOT NULL,
	[cajaExtracto] [float] NOT NULL,
	[cajaConcepto] [text] NOT NULL,
	[cajaObservaciones] [text] NOT NULL,
	[cajaDineroActual] [float] NOT NULL,
	[cajaDescuadre] [float] NOT NULL,
 CONSTRAINT [PK_TPV_ControlCaja] PRIMARY KEY CLUSTERED 
(
	[cajaMovID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[TPV_ControlCaja] ADD  CONSTRAINT [DF_TPV_ControlCaja_cajaIngreso]  DEFAULT ((0)) FOR [cajaIngreso]
GO

ALTER TABLE [dbo].[TPV_ControlCaja] ADD  CONSTRAINT [DF_TPV_ControlCaja_cajaExtracto]  DEFAULT ((0)) FOR [cajaExtracto]
GO

ALTER TABLE [dbo].[TPV_ControlCaja] ADD  CONSTRAINT [DF_TPV_ControlCaja_cajaDineroActual]  DEFAULT ((0)) FOR [cajaDineroActual]
GO

ALTER TABLE [dbo].[TPV_ControlCaja] ADD  CONSTRAINT [DF_TPV_ControlCaja_cajaDescuadre]  DEFAULT ((0)) FOR [cajaDescuadre]
GO


