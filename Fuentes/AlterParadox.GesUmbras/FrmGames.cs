﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using AlterParadox.GesUmbras.UserService;
using AlterParadox.Core;

namespace AlterParadox.GesUmbras
{
    public partial class FrmGames : Form
    {
        DataTable gridGames;
#pragma warning disable CS0649 // El campo 'FrmGames.juegos' nunca se asigna y siempre tendrá el valor predeterminado null
        List<LocalGame> juegos;
#pragma warning restore CS0649 // El campo 'FrmGames.juegos' nunca se asigna y siempre tendrá el valor predeterminado null

        public FrmGames()
        {
            InitializeComponent();
            search_all();
        }


        private void search_all() {
            //juegos = LocalGame.getAll(null);
            rellenarGrid(juegos);
        }

        private void search_prestados() {
            //juegos = LocalGame.getPrestados();
            rellenarGrid(juegos);
        }

        private void search_noPrestados() {
            //juegos = LocalGame.getNOPrestados();
            rellenarGrid(juegos);
        }

        private void rellenarGrid( List<LocalGame> j) {
            gridGames = new DataTable();
            gridGames.Columns.Add(new DataColumn("Nombre Juego"));
            gridGames.Columns.Add(new DataColumn("Asociacion"));
            gridGames.Columns.Add(new DataColumn("Prestado"));
            gridGames.Columns.Add(new DataColumn("Inscrito"));
            gridGames.Columns.Add(new DataColumn("Nombre"));
            gridGames.Columns.Add(new DataColumn("Observaciones"));
            gridGames.Columns.Add(new DataColumn("Id"));
            string prestado;
            foreach (LocalGame g in j)
            {
                if ( true)
                {
                    prestado = "NO";
                }
                else
                {
                    prestado = "SÍ";
                }
                string nombre = "";
                if ( true) {
                    nombre = "";
                }
                else {
                    User user = null;

                    try
                    {
                        using (UserServiceClient svc = new UserServiceClient())
                        {
                            //Esto esta mal. Hay que incluir la posibilidad de mandar el numero y no solo la ID
                            //Cambiar el nombre Request por Usuario
                            user = svc.GetUser(new GetUserRequest() { IdUser = 2 }).Usuario;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }

                    nombre = user.Name;
                }
                gridGames.Rows.Add(new Object[] { g.TheGame.Name, g.Association, prestado, 2, nombre, g.Comments, g.Id });
            }

            dataGridJuegos.DataSource = gridGames;
            try { 
            dataGridJuegos.Columns[0].Width = 250;
            }catch(Exception e)
            {
                Console.Write(e.Message);
            }
        }


        private void viewAllButton_Click(object sender, EventArgs e)
        {
            search_all();
        }

        private void viewPrestButton_Click(object sender, EventArgs e)
        {
            search_prestados();
        }

        private void viewNoPrestButton_Click(object sender, EventArgs e)
        {
            search_noPrestados();
        }

        //Devolver por inscrito
        private void inscritoSearchButton_Click(object sender, EventArgs e)
        {
            if (numInscrito.Value <= 0)
            {
                MessageBox.Show("Debe poner numero de Inscrito", "Error Inscrito", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                LocalGame j= null; //= LocalGame.getByInscrito((int)numInscrito.Value);
                if (j == null)
                {
                    MessageBox.Show("El inscrito no tiene juego cogido o no existe", "Error Inscrito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //Form prestamo = new FrmGamePrestar("Inscrito", j);
                    Form prestamo = new FrmGamePrestar();
                    prestamo.ShowDialog();
                    numInscrito.Value = 0;
                    //search_all();
                    search_prestados();
                }
            }
        }

        //prestar / devolver por nombre
        private void nameSearchButton_Click(object sender, EventArgs e)
        {
            searchByName();
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            if (gameNameInsertTB.Text != "" && gameAssocInsertTB.Text != "")
            {
                LocalGame g = new LocalGame();
                g.TheGame.Name = gameNameInsertTB.Text;
                g.Association.Name = gameAssocInsertTB.Text;
                //g.gameInscrito = 0;
                //g.gamePrestado = 0;
                g.Comments = gameObsInsertTB.Text;
                //LocalGame.Insert(g);
                gameObsInsertTB.Text = "";
                gameAssocInsertTB.Text = "";
                gameNameInsertTB.Text = "";
                search_all();
            }
            else {
                MessageBox.Show("Debe rellenar nombre y asociación", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void numInscrito_Click(object sender, EventArgs e)
        {
            numInscrito.Select(0, 4);
        }

        private void nameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                searchByName();
            }
        }

        private void searchByName()
        {
            if (nameTextBox.Text == "")
            {
                MessageBox.Show("Debe escribir un Nombre", "Error Nombre", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                List<LocalGame> j = null;// = LocalGame.getByName(nameTextBox.Text);
                List<LocalGame> final = new List<LocalGame>();
                if (j == null)
                {
                    MessageBox.Show("El juego no existe", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    foreach (LocalGame g in j)
                    {
                        if (true)
                        {
                            final.Add(g);
                        }
                    }
                    if (final.Count == 0)
                    {
                        MessageBox.Show("Ningun juego de este titulo esta libre", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        //Form prestamo = new FrmGamePrestar("Nombre", final[0]);
                        Form prestamo = new FrmGamePrestar();
                        prestamo.ShowDialog();
                        //search_all();
                        search_prestados();
                    }
                }
            }
        }

        private void searchById(int id)
        {
            if (id == 0)
            {
                MessageBox.Show("Error no conocido.");
            }
            else
            {
                LocalGame j = null;//= LocalGame.getById(id);

                List<LocalGame> final = new List<LocalGame>();
                if (j == null)
                {
                    MessageBox.Show("El juego no existe", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    if (true)
                    {
                        final.Add(j);
                    }
                    
                    if (final.Count == 0)
                    {
                        MessageBox.Show("Ningun juego de este titulo esta libre", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        //Form prestamo = new FrmGamePrestar("Nombre", final[0]);
                        Form prestamo = new FrmGamePrestar();
                        prestamo.ShowDialog();
                        //search_all();
                        search_prestados();
                    }
                }
            }
        }

        private void filtrarButton_Click(object sender, EventArgs e)
        {
            if (nameTextBox.Text == "")
            {
                MessageBox.Show("Debe escribir un Nombre", "Error Nombre", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                List<LocalGame> j = null;// = LocalGame.getByName(nameTextBox.Text);
                if (j == null)
                {
                    MessageBox.Show("El juego no existe", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    rellenarGrid(j);
                }
            }
        }

        private void nameTextBox_Click(object sender, EventArgs e)
        {
            numInscrito.Select(0, nameTextBox.Text.Length);
        }

        private void dataGridJuegos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                searchById(Convert.ToInt32("" + dataGridJuegos.Rows[e.RowIndex].Cells[6].Value));
            }
        }

        private void nameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (nameTextBox.Text == "")
            {
                MessageBox.Show("Debe escribir un Nombre", "Error Nombre", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                List<LocalGame> j = null;//= Game.getByName(nameTextBox.Text);
                if (j == null)
                {
                    MessageBox.Show("El juego no existe", "Error Juego", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    rellenarGrid(j);
                }
            }
        }

        private void FrmGames_Load(object sender, EventArgs e)
        {

            foreach(string association in OtherThings.associationList())
            {
                txtAsociacion.Items.Add(association);
            }
            
        }

        private void cmdExport_Click(object sender, EventArgs e)
        {
            string path = "gamesinassociation_" + G.UnixDate(DateTime.Now) + ".txt";
            if (File.Exists(path))
            {
                // Note that no lock is put on the
                // file and the possibility exists
                // that another process could do
                // something with it between
                // the calls to Exists and Delete.
                File.Delete(path);
            }

            using (var tw = new StreamWriter(path, true))
            {
                tw.WriteLine("Asociación: " + this.txtAsociacion.Text);
                tw.WriteLine("");
                tw.WriteLine("Juegos de la asociación:");
                int i = 1;
                foreach (LocalGame game in LocalGame.getAll(null))
                {
                    if (""+game.Association.Id == this.txtAsociacion.Text)
                    {
                        tw.WriteLine(i + "> " + game.TheGame.Name + " ()");
                        i = i + 1;
                    }
                }

                tw.Close();
            }
            Process proc = new Process();
            proc.StartInfo.FileName = path;
            proc.StartInfo.UseShellExecute = true;
            proc.Start();
        }
    }
}
