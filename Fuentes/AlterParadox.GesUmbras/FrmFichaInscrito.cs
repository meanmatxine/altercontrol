﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.UserService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmFichaInscrito : Form
    {
        public List<UserResumen> Inscritos;
        public bool Nuevo;
        public FrmFichaInscrito()
        {
            InitializeComponent();
        }

        private void FrmFichaInscrito_Load(object sender, EventArgs e)
        {
            if (Inscritos == null)
                return;

            if (Nuevo)
                this.BackColor = Color.IndianRed;


            var tutor = Inscritos.FirstOrDefault(x => !x.HumanoDependiente); //Siempre debería haber solo 1
            if(tutor != null)
            {
                DNI.Text = tutor.Dni;
                NameUser.Text = string.Format("{0} {1}", tutor.Name, tutor.LastName);
                Number.Text = "" + tutor.Number;
            }
            

            //Luego actualizo la lista y se la añado al grid

            GridUsers.AutoGenerateColumns = false;

            GridUsers.DataSource = Inscritos
                .Where(x =>x.HumanoDependiente).OrderBy(x => x.Number).ToList();

        }

        private void FrmFichaInscrito_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}
