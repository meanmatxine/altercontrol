﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.ActivityService;
using AlterParadox.GesUmbras.UserService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmActivities : Form
    {
        //LOCAL FUNCTIONS AND VARS
        Activity activity;
        public FrmActivities()
        {
            InitializeComponent();
        }

        private void L_refresh_Activities()
        {
            var filtro = txtFilter.Text;
            GridActivities.AutoGenerateColumns = false;
            List<Activity> listaActividades = new List<Activity>();
            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    listaActividades = svc.GetActivities().ListaActividades.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
            var lista = listaActividades
                .Where(x =>
                    G.Contains(string.Format("{0}-{1}-{2}",x.Name,x.Organizer,x.Association), filtro)
                ).ToList();

            GridActivities.DataSource = lista.Where(x => (!chkAll.Checked ? x.ActivityDate.CompareTo(DateTime.Now) > 0 : true)).ToList();
        }

        
        private void L_refresh_ActivityUsers()
        {
            if (string.IsNullOrEmpty(txtId.Text))
            {
                GridActivityUsers.DataSource = null;
                return;
            }
                

            GridActivityUsers.AutoGenerateColumns = false;
            List<ActivityUser> listaActividadUsuarios = new List<ActivityUser>();
            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    listaActividadUsuarios = svc.GetActivityUsers(new GetActivityUsersRequest() { IdActivity = Convert.ToInt32(txtId.Text) }).ListaActividadUsuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            GridActivityUsers.DataSource = listaActividadUsuarios.ToList();

            L_Load_TotalParticipants();
            textBox1.Text = "";
            if (textBox1.Enabled) { 
                textBox1.Focus();
            }
            else
            {
                txtName.Focus();
            }
        }
        private void L_New()
        {
            activity = new Activity();
            txtId.Text = string.Empty;
            txtName.Text = string.Empty;
            txtDate.Text = new DateTime(2018, 08, 16).ToShortDateString();
            txtHour.Text = "11:00";
            txtType.Text = "Actividad";
            txtNumParticipants.Text = "0";
            txtParticipantsComment.Text = string.Empty;
            txtPlace.Text = string.Empty;
            Duracion.Text = string.Empty;
            txtOthers.Text = string.Empty;
            txtNeeds.Text = string.Empty;
            txtComments.Text = string.Empty;

            txtOrganizer.Text = string.Empty;
            txtWinner.Text = string.Empty;
            //txtAssociation.Text = "";
            //txtEmail.Text = "";
            //txtPhone.Text = "";
            L_refresh_ActivityUsers();

            txtName.Focus();
            
        }
        private void L_Load()
        {

            txtId.Text =  activity.Id.ToString();
            txtName.Text = activity.Name;
            txtDate.Text =  activity.ActivityDate.ToShortDateString();
            txtHour.Text =  activity.ActivityDate.ToShortTimeString();
            txtType.Text = activity.Type;
            txtNumParticipants.Value =  activity.MaxParticipants;
            txtMinParticipants.Value =  activity.MinParticipants;
            txtParticipantsComment.Text = activity.Participants;
            txtPlace.Text =  activity.Place;
            Duracion.Value =  activity.Duration;
            txtOthers.Text =  activity.Summary;
            txtNeeds.Text =  activity.Needs;
            txtComments.Text =  activity.Comments;

            txtOrganizer.Text =  activity.Organizer;
            txtWinner.Text = activity.Winner;

            lblDisable.Visible = !activity.IsAbleToJoin;
            textBox1.Enabled = activity.IsAbleToJoin;

            L_refresh_ActivityUsers();

            //txtAssociation.Text = "" + activity.Association;
            //txtEmail.Text = "" + activity.Email;
            //txtPhone.Text = "" + activity.Phone;
            //txtDni.Focus();
            //Focus on txtBox to register a user in an activity
        }

        private void L_Save()
        {
            //Save
            activity = new Activity();
            if (!string.IsNullOrEmpty(txtId.Text))
                activity.Id = Convert.ToInt32(txtId.Text);

            activity.Name = "" + txtName.Text;
            activity.ActivityDate =  new DateTime(txtDate.Value.Year, txtDate.Value.Month, txtDate.Value.Day,  txtHour.Value.Hour, txtHour.Value.Minute,00);
            activity.Type = "" + txtType.Text;
            activity.MaxParticipants = decimal.ToInt32(txtNumParticipants.Value);
            activity.MinParticipants = decimal.ToInt32(txtMinParticipants.Value);
            activity.Participants =""+ txtParticipantsComment.Text;
            activity.Place = "" + txtPlace.Text;
            activity.Duration = (int)Duracion.Value;
            activity.Summary = "" + txtOthers.Text;
            activity.Needs = "" + txtNeeds.Text;
            activity.Comments = "" + txtComments.Text;

            activity.Organizer = "" + txtOrganizer.Text;
            activity.Winner = "" + txtWinner.Text ;

            activity.Association = string.Empty;
            activity.Email = "" + string.Empty;
            activity.Phone = "" + string.Empty;

            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    //SAVEUSER DEBERIA DEVOLVER O EL ID DEL USUARIO NUEVO O LOS DATOS DEL USUARIO
                    var actividadRespuesta = svc.SaveActivity(new SaveActivityRequest() { Actividad = activity });

                    if (!actividadRespuesta.EsCorrecto) { 
                        MessageBox.Show("Error guardando la actividad. Verifique los datos.");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            L_refresh_Activities();
            L_refresh_ActivityUsers();
            L_New();
        }
        private void L_Delete()
        {
            if (activity.Id != 0)
            {
                try
                {
                    using (ActivityServiceClient svc = new ActivityServiceClient())
                    {
                        svc.DeleteActivity(new DeleteActivityRequest() { IdActivity = (int)activity.Id });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }
            L_refresh_Activities();
            L_New();
        }

        private void L_Search()
        {

        }

        //FORM
        private void FrmActivities_Load(object sender, EventArgs e)
        {
            this.mnuDelete.Visible = G.admin;
            this.txtId.Visible = G.admin;
            this.Show();
            L_refresh_Activities();
            L_New();
            txtFilter.Focus();
        }

        private void mnuNew_Click(object sender, EventArgs e)
        {
            L_New();
        }

        private void mnuSave_Click(object sender, EventArgs e)
        {
            L_Save();
        }

        private void mnuSearch_Click(object sender, EventArgs e)
        {
            L_Search();
        }

        private void mnuDelete_Click(object sender, EventArgs e)
        {
            L_Delete();
        }

        private void GridActivities_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                activity = (Activity)GridActivities.Rows[e.RowIndex].DataBoundItem;
                L_Load();
            }
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_refresh_Activities();
        }

        private void cmdCleanFilter_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
            txtFilter.Focus();
        }

        private void txtParticipant_KeyDown(object sender, KeyEventArgs e)
        {
           

        }

        private void txtParticipant_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtParticipant_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                User user = null;
                if (txtId.Text != "0" && textBox1.Text != "0")
                {
                    try
                    {
                        using (UserServiceClient svc = new UserServiceClient())
                        {
                            //Esto esta mal. Hay que incluir la posibilidad de mandar el numero y no solo la ID
                            //Cambiar el nombre Request por Usuario
                            //user = svc.GetUser(new GetUserRequest() { IdUser = Convert.ToInt32(textBox1.Text) }).Usuario;
                            user = svc.GetUserByNumber(new GetUserByNumberRequest() { Number = Convert.ToInt32(textBox1.Text) }).Usuario;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }


                    if (user != null)
                    {

                        try
                        {
                            using (ActivityServiceClient svc = new ActivityServiceClient())
                            {
                                //Esto esta mal. Hay que incluir la posibilidad de mandar el numero y no solo la ID
                                //Cambiar el nombre Request por Usuario
                                activity = svc.GetActivity(new GetActivityRequest() { IdActivity = Convert.ToInt32(txtId.Text) }).Actividad;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }

                        if (activity != null)
                        {

                            try
                            {
                                using (ActivityServiceClient svc = new ActivityServiceClient())
                                {
                                    var theParticipantPrevius = svc.GetActivityUser(new GetActivityUserRequest() { IdActivity = (int)activity.Id, IdUser = (int)user.Id });

                                    ActivityUser participant = new ActivityUser();
                                    if (theParticipantPrevius.EsCorrecto) { 
                                        participant = theParticipantPrevius.ActividadUsuario;
                                    }
                                    else {

                                        participant.Id = null;
                                        participant.InscriptionDate = DateTime.Now;
                                        participant.Name = "";
                                        participant.User_Id = (int)user.Id;
                                        participant.Activity_Id = (int)activity.Id;
                                        participant.Comments = string.Empty;
                                        participant.Reserve = false;
                                    }

                                    var test = svc.SaveActivityUser(new SaveActivityUserRequest() { ActividadUsuario = participant });

                                    var theParticipant = svc.GetActivityUser(new GetActivityUserRequest() { IdActivity = (int)activity.Id, IdUser = (int)user.Id });

                                    FrmActivityUser formulario = new FrmActivityUser();
                                    formulario.activityUser = theParticipant.ActividadUsuario;
                                    G.AbrirPantalla(formulario, true, false);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            } 

                            L_refresh_ActivityUsers();
                            //Activity.updatemodifydate(Convert.ToInt32(txtId.Text));
                            L_Load_TotalParticipants();

                            var tes2t = ((List< ActivityUser > )GridActivityUsers.DataSource).FirstOrDefault(x=>x.User_Id == user.Id);
                            if(tes2t!= null && !tes2t.Inscrito)
                            {
                                MessageBox.Show("Advertencia. El inscrito se queda en cola de espera. Avisalo!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Actividad inexistente.", "Alerta", MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El inscrito no existe.", "Alerta", MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("La actividad o el inscrito no son válidos.", "Alerta", MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
                }

            }
        }

        private void GridActivityUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                FrmActivityUser formulario = new FrmActivityUser();
                formulario.activityUser = (ActivityUser)GridActivityUsers.Rows[e.RowIndex].DataBoundItem;
                formulario.reserve = formulario.activityUser.Reserve;
                G.AbrirPantalla(formulario, true, false);
            }
            L_refresh_ActivityUsers();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            L_Save();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtId.Text != "0" && textBox1.Text != "0")
            {
                Activity activity = null;
                try
                {
                    using (ActivityServiceClient svc = new ActivityServiceClient())
                    {
                        //Esto esta mal. Hay que incluir la posibilidad de mandar el numero y no solo la ID
                        //Cambiar el nombre Request por Usuario
                        activity = svc.GetActivity(new GetActivityRequest() { IdActivity = Convert.ToInt32(txtId.Text) }).Actividad;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                if (activity != null)
                {

                    List<ActivityUser> listaActividadUsuarios = new List<ActivityUser>();
                    try
                    {
                        using (ActivityServiceClient svc = new ActivityServiceClient())
                        {
                            listaActividadUsuarios = svc.GetActivityUsers(new GetActivityUsersRequest() { IdActivity = (int)activity.Id }).ListaActividadUsuarios.ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }

                    string path = "activity_" + G.UnixDate(DateTime.Now) + ".txt";
                    if (File.Exists(path))
                    {
                        // Note that no lock is put on the
                        // file and the possibility exists
                        // that another process could do
                        // something with it between
                        // the calls to Exists and Delete.
                        File.Delete(path);
                    }

                    using (var tw = new StreamWriter(path, true))
                    {
                        tw.WriteLine($"Actividad: {this.txtName.Text} Organizador:{this.txtOrganizer.Text}");
                        tw.WriteLine("");
                        tw.WriteLine("Inscritos (en orden de inscripción) :");
                        int i = 1;
                        foreach (ActivityUser user in listaActividadUsuarios)
                        {
                            tw.WriteLine($"{i.ToString("000")} {(!user.Inscrito ? "R" : string.Empty)}> {user.Number} - {user.Name} {(string.IsNullOrEmpty(user.Comments) ? string.Empty : $"({ user.Comments})")}");                            
                            i = i + 1;
                        }
                    
                        tw.Close();
                    }
                    Process proc = new Process();
                    proc.StartInfo.FileName = path;
                    proc.StartInfo.UseShellExecute = true;
                    proc.Start();


                }
                else
                {
                    MessageBox.Show("Seleccione una actividad válida.");
                }
            }
        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
            L_refresh_Activities();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            L_refresh_Activities();
        }
        private void L_Load_TotalParticipants() {
            //this.txtTotal.Text = "" + Activity.CheckParticipants(Convert.ToInt32(txtId.Text));

            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    this.txtTotal.Text = (svc.CheckParticipants(activity).ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private void GridActivityUsers_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow row in GridActivityUsers.Rows)
                if (Convert.ToInt32(row.Cells["colInscrito"].Value) ==0)
                {
                    row.DefaultCellStyle.BackColor = Color.OrangeRed;
                }
        }

        private void GridActivities_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GridActivityUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtOthers_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.F4)
                G.ShowTextDialog(txtOthers.Text);
        }

        private void txtNeeds_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
                G.ShowTextDialog(txtNeeds.Text);
        }

        private void txtComments_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
                G.ShowTextDialog(txtComments.Text);
        }
    }
}
