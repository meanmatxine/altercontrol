﻿namespace AlterParadox.GesUmbras
{
    partial class FrmMejoras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridActivities = new System.Windows.Forms.DataGridView();
            this.Mejora = new System.Windows.Forms.TextBox();
            this.lblOthers = new System.Windows.Forms.Label();
            this.cmdSave = new System.Windows.Forms.Button();
            this.Usuario = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_dni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridActivities)).BeginInit();
            this.SuspendLayout();
            // 
            // GridActivities
            // 
            this.GridActivities.AllowUserToAddRows = false;
            this.GridActivities.AllowUserToDeleteRows = false;
            this.GridActivities.AllowUserToResizeColumns = false;
            this.GridActivities.AllowUserToResizeRows = false;
            this.GridActivities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridActivities.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridActivities.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GridActivities.ColumnHeadersHeight = 30;
            this.GridActivities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridActivities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_dni,
            this.col_name});
            this.GridActivities.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridActivities.Location = new System.Drawing.Point(13, 134);
            this.GridActivities.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.GridActivities.MultiSelect = false;
            this.GridActivities.Name = "GridActivities";
            this.GridActivities.ReadOnly = true;
            this.GridActivities.RowHeadersVisible = false;
            this.GridActivities.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridActivities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridActivities.Size = new System.Drawing.Size(698, 448);
            this.GridActivities.TabIndex = 2;
            // 
            // Mejora
            // 
            this.Mejora.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Mejora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Mejora.Location = new System.Drawing.Point(13, 57);
            this.Mejora.Multiline = true;
            this.Mejora.Name = "Mejora";
            this.Mejora.Size = new System.Drawing.Size(445, 70);
            this.Mejora.TabIndex = 16;
            // 
            // lblOthers
            // 
            this.lblOthers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOthers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblOthers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOthers.Location = new System.Drawing.Point(13, 35);
            this.lblOthers.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOthers.Name = "lblOthers";
            this.lblOthers.Size = new System.Drawing.Size(445, 23);
            this.lblOthers.TabIndex = 19;
            this.lblOthers.Text = "Descripción";
            this.lblOthers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdSave
            // 
            this.cmdSave.Image = global::AlterParadox.GesUmbras.Properties.Resources.disk;
            this.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSave.Location = new System.Drawing.Point(589, 38);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(122, 89);
            this.cmdSave.TabIndex = 18;
            this.cmdSave.Text = "Guardar";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // Usuario
            // 
            this.Usuario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Usuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Usuario.Location = new System.Drawing.Point(132, 9);
            this.Usuario.MaxLength = 100;
            this.Usuario.Name = "Usuario";
            this.Usuario.Size = new System.Drawing.Size(579, 23);
            this.Usuario.TabIndex = 15;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Location = new System.Drawing.Point(13, 9);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(121, 23);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Nombre";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Image = global::AlterParadox.GesUmbras.Properties.Resources.page;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(463, 38);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 89);
            this.button1.TabIndex = 20;
            this.button1.Text = "Nuevo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.FillWeight = 20F;
            this.col_id.HeaderText = "Id";
            this.col_id.MinimumWidth = 10;
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            // 
            // col_dni
            // 
            this.col_dni.DataPropertyName = "usuario";
            this.col_dni.FillWeight = 40F;
            this.col_dni.HeaderText = "Usuario";
            this.col_dni.MinimumWidth = 40;
            this.col_dni.Name = "col_dni";
            this.col_dni.ReadOnly = true;
            // 
            // col_name
            // 
            this.col_name.DataPropertyName = "MejoraText";
            this.col_name.FillWeight = 80F;
            this.col_name.HeaderText = "Mejora";
            this.col_name.MinimumWidth = 40;
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            // 
            // FrmMejoras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 597);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Mejora);
            this.Controls.Add(this.lblOthers);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.Usuario);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.GridActivities);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMejoras";
            this.Text = "Mejoras";
            this.Load += new System.EventHandler(this.FrmMejoras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridActivities)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridActivities;
        private System.Windows.Forms.TextBox Mejora;
        private System.Windows.Forms.Label lblOthers;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.TextBox Usuario;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_dni;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_name;
    }
}