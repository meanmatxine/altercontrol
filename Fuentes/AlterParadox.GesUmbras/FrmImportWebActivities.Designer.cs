﻿namespace AlterParadox.GesUmbras
{
    partial class FrmImportWebActivities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progress = new System.Windows.Forms.ProgressBar();
            this.cmdImport = new System.Windows.Forms.Button();
            this.txtPathFile = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(12, 41);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(569, 23);
            this.progress.TabIndex = 9;
            // 
            // cmdImport
            // 
            this.cmdImport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdImport.Location = new System.Drawing.Point(495, 11);
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Size = new System.Drawing.Size(86, 23);
            this.cmdImport.TabIndex = 7;
            this.cmdImport.Text = "Importar";
            this.cmdImport.UseVisualStyleBackColor = true;
            this.cmdImport.Click += new System.EventHandler(this.cmdImport_Click);
            // 
            // txtPathFile
            // 
            this.txtPathFile.Location = new System.Drawing.Point(12, 14);
            this.txtPathFile.Name = "txtPathFile";
            this.txtPathFile.Size = new System.Drawing.Size(477, 20);
            this.txtPathFile.TabIndex = 10;
            this.txtPathFile.Text = "https://docs.google.com/spreadsheets/d/e/2PACX-1vRKY7L8WzsI67Lfb4wIcS5P93K8eGP0Yv" +
    "22xwm4JwVmz2wLhrxynLwNNL-i2nJZE_UrR0IU6a03RRfV/pub?gid=0&single=true&output=csv";
            // 
            // FrmImportWebActivities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 75);
            this.Controls.Add(this.txtPathFile);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.cmdImport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmImportWebActivities";
            this.Text = "Importar actividades";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Button cmdImport;
        private System.Windows.Forms.TextBox txtPathFile;
    }
}