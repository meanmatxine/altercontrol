﻿namespace AlterParadox.GesUmbras
{
    partial class FrmGames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridJuegos = new System.Windows.Forms.DataGridView();
            this.viewAllButton = new System.Windows.Forms.Button();
            this.viewPrestButton = new System.Windows.Forms.Button();
            this.viewNoPrestButton = new System.Windows.Forms.Button();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.nameSearchButton = new System.Windows.Forms.Button();
            this.inscritoSearchButton = new System.Windows.Forms.Button();
            this.numInscrito = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.gameNameInsertTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gameAssocInsertTB = new System.Windows.Forms.TextBox();
            this.insertButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.gameObsInsertTB = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.filtrarButton = new System.Windows.Forms.Button();
            this.lblDocument = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtAsociacion = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmdExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJuegos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInscrito)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridJuegos
            // 
            this.dataGridJuegos.AllowUserToAddRows = false;
            this.dataGridJuegos.AllowUserToDeleteRows = false;
            this.dataGridJuegos.AllowUserToResizeRows = false;
            this.dataGridJuegos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridJuegos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridJuegos.Location = new System.Drawing.Point(13, 13);
            this.dataGridJuegos.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridJuegos.Name = "dataGridJuegos";
            this.dataGridJuegos.ReadOnly = true;
            this.dataGridJuegos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridJuegos.Size = new System.Drawing.Size(840, 376);
            this.dataGridJuegos.TabIndex = 3;
            this.dataGridJuegos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridJuegos_CellDoubleClick);
            // 
            // viewAllButton
            // 
            this.viewAllButton.Location = new System.Drawing.Point(252, 31);
            this.viewAllButton.Margin = new System.Windows.Forms.Padding(4);
            this.viewAllButton.Name = "viewAllButton";
            this.viewAllButton.Size = new System.Drawing.Size(242, 34);
            this.viewAllButton.TabIndex = 3;
            this.viewAllButton.Text = "Ver Todos";
            this.viewAllButton.UseVisualStyleBackColor = true;
            this.viewAllButton.Click += new System.EventHandler(this.viewAllButton_Click);
            // 
            // viewPrestButton
            // 
            this.viewPrestButton.Location = new System.Drawing.Point(252, 70);
            this.viewPrestButton.Margin = new System.Windows.Forms.Padding(4);
            this.viewPrestButton.Name = "viewPrestButton";
            this.viewPrestButton.Size = new System.Drawing.Size(242, 34);
            this.viewPrestButton.TabIndex = 4;
            this.viewPrestButton.Text = "Ver Prestados";
            this.viewPrestButton.UseVisualStyleBackColor = true;
            this.viewPrestButton.Click += new System.EventHandler(this.viewPrestButton_Click);
            // 
            // viewNoPrestButton
            // 
            this.viewNoPrestButton.Location = new System.Drawing.Point(252, 112);
            this.viewNoPrestButton.Margin = new System.Windows.Forms.Padding(4);
            this.viewNoPrestButton.Name = "viewNoPrestButton";
            this.viewNoPrestButton.Size = new System.Drawing.Size(242, 34);
            this.viewNoPrestButton.TabIndex = 5;
            this.viewNoPrestButton.Text = "Ver NO Prestados";
            this.viewNoPrestButton.UseVisualStyleBackColor = true;
            this.viewNoPrestButton.Click += new System.EventHandler(this.viewNoPrestButton_Click);
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTextBox.Location = new System.Drawing.Point(4, 35);
            this.nameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(244, 26);
            this.nameTextBox.TabIndex = 0;
            this.nameTextBox.Click += new System.EventHandler(this.nameTextBox_Click);
            this.nameTextBox.TextChanged += new System.EventHandler(this.nameTextBox_TextChanged);
            this.nameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nameTextBox_KeyDown);
            // 
            // nameSearchButton
            // 
            this.nameSearchButton.Location = new System.Drawing.Point(3, 68);
            this.nameSearchButton.Margin = new System.Windows.Forms.Padding(4);
            this.nameSearchButton.Name = "nameSearchButton";
            this.nameSearchButton.Size = new System.Drawing.Size(245, 34);
            this.nameSearchButton.TabIndex = 1;
            this.nameSearchButton.Text = "Prestar por Nombre Juego";
            this.nameSearchButton.UseVisualStyleBackColor = true;
            this.nameSearchButton.Click += new System.EventHandler(this.nameSearchButton_Click);
            // 
            // inscritoSearchButton
            // 
            this.inscritoSearchButton.Location = new System.Drawing.Point(7, 70);
            this.inscritoSearchButton.Margin = new System.Windows.Forms.Padding(4);
            this.inscritoSearchButton.Name = "inscritoSearchButton";
            this.inscritoSearchButton.Size = new System.Drawing.Size(242, 64);
            this.inscritoSearchButton.TabIndex = 1;
            this.inscritoSearchButton.Text = "Devolver por Inscrito";
            this.inscritoSearchButton.UseVisualStyleBackColor = true;
            this.inscritoSearchButton.Click += new System.EventHandler(this.inscritoSearchButton_Click);
            // 
            // numInscrito
            // 
            this.numInscrito.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numInscrito.Location = new System.Drawing.Point(7, 36);
            this.numInscrito.Margin = new System.Windows.Forms.Padding(4);
            this.numInscrito.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numInscrito.Name = "numInscrito";
            this.numInscrito.Size = new System.Drawing.Size(242, 26);
            this.numInscrito.TabIndex = 0;
            this.numInscrito.Click += new System.EventHandler(this.numInscrito_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 19);
            this.label1.TabIndex = 9;
            this.label1.Text = "Nombre Juego";
            // 
            // gameNameInsertTB
            // 
            this.gameNameInsertTB.Location = new System.Drawing.Point(7, 54);
            this.gameNameInsertTB.Margin = new System.Windows.Forms.Padding(4);
            this.gameNameInsertTB.Name = "gameNameInsertTB";
            this.gameNameInsertTB.Size = new System.Drawing.Size(267, 27);
            this.gameNameInsertTB.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 85);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "Asociacion";
            // 
            // gameAssocInsertTB
            // 
            this.gameAssocInsertTB.Location = new System.Drawing.Point(7, 108);
            this.gameAssocInsertTB.Margin = new System.Windows.Forms.Padding(4);
            this.gameAssocInsertTB.Name = "gameAssocInsertTB";
            this.gameAssocInsertTB.Size = new System.Drawing.Size(267, 27);
            this.gameAssocInsertTB.TabIndex = 1;
            // 
            // insertButton
            // 
            this.insertButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertButton.Location = new System.Drawing.Point(7, 252);
            this.insertButton.Margin = new System.Windows.Forms.Padding(4);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(242, 64);
            this.insertButton.TabIndex = 3;
            this.insertButton.Text = "Añadir Juego";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 140);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "Observaciones";
            // 
            // gameObsInsertTB
            // 
            this.gameObsInsertTB.Location = new System.Drawing.Point(7, 163);
            this.gameObsInsertTB.Margin = new System.Windows.Forms.Padding(4);
            this.gameObsInsertTB.Multiline = true;
            this.gameObsInsertTB.Name = "gameObsInsertTB";
            this.gameObsInsertTB.Size = new System.Drawing.Size(267, 81);
            this.gameObsInsertTB.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.insertButton);
            this.panel1.Controls.Add(this.gameObsInsertTB);
            this.panel1.Controls.Add(this.gameAssocInsertTB);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.gameNameInsertTB);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(860, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(280, 328);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(3, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(271, 27);
            this.label5.TabIndex = 15;
            this.label5.Text = "Nuevo Juego";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // filtrarButton
            // 
            this.filtrarButton.Location = new System.Drawing.Point(252, 154);
            this.filtrarButton.Margin = new System.Windows.Forms.Padding(4);
            this.filtrarButton.Name = "filtrarButton";
            this.filtrarButton.Size = new System.Drawing.Size(242, 34);
            this.filtrarButton.TabIndex = 2;
            this.filtrarButton.Text = "Filtrar por Nombre";
            this.filtrarButton.UseVisualStyleBackColor = true;
            this.filtrarButton.Click += new System.EventHandler(this.filtrarButton_Click);
            // 
            // lblDocument
            // 
            this.lblDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblDocument.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocument.Location = new System.Drawing.Point(3, 0);
            this.lblDocument.Name = "lblDocument";
            this.lblDocument.Size = new System.Drawing.Size(245, 27);
            this.lblDocument.TabIndex = 5;
            this.lblDocument.Text = "Prestamo de juegos";
            this.lblDocument.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblDocument);
            this.panel3.Controls.Add(this.nameSearchButton);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.nameTextBox);
            this.panel3.Controls.Add(this.viewAllButton);
            this.panel3.Controls.Add(this.filtrarButton);
            this.panel3.Controls.Add(this.viewPrestButton);
            this.panel3.Controls.Add(this.viewNoPrestButton);
            this.panel3.Location = new System.Drawing.Point(12, 396);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(498, 211);
            this.panel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(252, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 27);
            this.label4.TabIndex = 6;
            this.label4.Text = "Filtros";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.numInscrito);
            this.panel2.Controls.Add(this.inscritoSearchButton);
            this.panel2.Location = new System.Drawing.Point(604, 396);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(249, 190);
            this.panel2.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(246, 27);
            this.label6.TabIndex = 7;
            this.label6.Text = "Devolución";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtAsociacion);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.cmdExport);
            this.panel4.Location = new System.Drawing.Point(859, 347);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(281, 114);
            this.panel4.TabIndex = 4;
            // 
            // txtAsociacion
            // 
            this.txtAsociacion.FormattingEnabled = true;
            this.txtAsociacion.Location = new System.Drawing.Point(3, 30);
            this.txtAsociacion.Name = "txtAsociacion";
            this.txtAsociacion.Size = new System.Drawing.Size(273, 27);
            this.txtAsociacion.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(281, 27);
            this.label7.TabIndex = 7;
            this.label7.Text = "Exportar lista de datos";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdExport
            // 
            this.cmdExport.Image = global::AlterParadox.GesUmbras.Properties.Resources.computer_go;
            this.cmdExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExport.Location = new System.Drawing.Point(139, 64);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(4);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(137, 43);
            this.cmdExport.TabIndex = 1;
            this.cmdExport.Text = "Exportar";
            this.cmdExport.UseVisualStyleBackColor = true;
            this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
            // 
            // FrmGames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 605);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridJuegos);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmGames";
            this.Text = "Juegos";
            this.Load += new System.EventHandler(this.FrmGames_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridJuegos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInscrito)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridJuegos;
        private System.Windows.Forms.Button viewAllButton;
        private System.Windows.Forms.Button viewPrestButton;
        private System.Windows.Forms.Button viewNoPrestButton;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button nameSearchButton;
        private System.Windows.Forms.Button inscritoSearchButton;
        private System.Windows.Forms.NumericUpDown numInscrito;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox gameNameInsertTB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox gameAssocInsertTB;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox gameObsInsertTB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button filtrarButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblDocument;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button cmdExport;
        private System.Windows.Forms.ComboBox txtAsociacion;
    }
}