﻿namespace AlterParadox.GesUmbras
{
    partial class MDI
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDI));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.administraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuImportWebUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuImportActivities = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuImportGames = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuDBConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExportJson = new System.Windows.Forms.ToolStripMenuItem();
            this.openFOlderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.MnuParticipantes = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuActivities = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAddGames = new System.Windows.Forms.ToolStripMenuItem();
            this.prestamoDeJuegosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.juegosGlobalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.juegosLocalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asociacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MnuActivitiesList = new System.Windows.Forms.ToolStripMenuItem();
            this.PBThread = new System.Windows.Forms.ToolStripProgressBar();
            this.btnJson = new System.Windows.Forms.ToolStripButton();
            this.MnuTpvList = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAdministrator = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Worker = new System.ComponentModel.BackgroundWorker();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.administraciónToolStripMenuItem,
            this.configuraciónToolStripMenuItem,
            this.mnuExportJson,
            this.openFOlderToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip.Size = new System.Drawing.Size(168, 559);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.Visible = false;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 4);
            this.toolStripMenuItem1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // administraciónToolStripMenuItem
            // 
            this.administraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuImportWebUsers,
            this.mnuImportActivities,
            this.mnuImportGames});
            this.administraciónToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.administraciónToolStripMenuItem.Image = global::AlterParadox.GesUmbras.Properties.Resources.admin;
            this.administraciónToolStripMenuItem.Name = "administraciónToolStripMenuItem";
            this.administraciónToolStripMenuItem.Size = new System.Drawing.Size(149, 36);
            this.administraciónToolStripMenuItem.Text = "Administración";
            this.administraciónToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MnuImportWebUsers
            // 
            this.MnuImportWebUsers.Name = "MnuImportWebUsers";
            this.MnuImportWebUsers.Size = new System.Drawing.Size(243, 24);
            this.MnuImportWebUsers.Text = "Importar Usuarios Web";
            this.MnuImportWebUsers.Click += new System.EventHandler(this.MnuImportWebUsers_Click);
            // 
            // mnuImportActivities
            // 
            this.mnuImportActivities.Name = "mnuImportActivities";
            this.mnuImportActivities.Size = new System.Drawing.Size(243, 24);
            this.mnuImportActivities.Text = "Importar Actividades";
            this.mnuImportActivities.Click += new System.EventHandler(this.MnuImportActivities_Click);
            // 
            // mnuImportGames
            // 
            this.mnuImportGames.Name = "mnuImportGames";
            this.mnuImportGames.Size = new System.Drawing.Size(243, 24);
            this.mnuImportGames.Text = "Importar Juegos";
            this.mnuImportGames.Click += new System.EventHandler(this.mnuImportGames_Click);
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuDBConfig});
            this.configuraciónToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configuraciónToolStripMenuItem.Image = global::AlterParadox.GesUmbras.Properties.Resources.config;
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(149, 36);
            this.configuraciónToolStripMenuItem.Text = "Configuración";
            this.configuraciónToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MnuDBConfig
            // 
            this.MnuDBConfig.Image = global::AlterParadox.GesUmbras.Properties.Resources.database_add;
            this.MnuDBConfig.Name = "MnuDBConfig";
            this.MnuDBConfig.Size = new System.Drawing.Size(169, 24);
            this.MnuDBConfig.Text = "Conexión DB";
            this.MnuDBConfig.Click += new System.EventHandler(this.MnuDBConfig_Click);
            // 
            // mnuExportJson
            // 
            this.mnuExportJson.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuExportJson.Image = global::AlterParadox.GesUmbras.Properties.Resources.execute;
            this.mnuExportJson.Name = "mnuExportJson";
            this.mnuExportJson.Size = new System.Drawing.Size(149, 36);
            this.mnuExportJson.Text = "Exportar JSON";
            this.mnuExportJson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mnuExportJson.Click += new System.EventHandler(this.mnuExportJson_Click);
            // 
            // openFOlderToolStripMenuItem
            // 
            this.openFOlderToolStripMenuItem.Name = "openFOlderToolStripMenuItem";
            this.openFOlderToolStripMenuItem.Size = new System.Drawing.Size(149, 23);
            this.openFOlderToolStripMenuItem.Text = "OpenFOlder";
            this.openFOlderToolStripMenuItem.Click += new System.EventHandler(this.OpenFOlderToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 537);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(2, 0, 21, 0);
            this.statusStrip.Size = new System.Drawing.Size(1189, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel.Text = "Estado";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MnuParticipantes,
            this.mnuActivities,
            this.mnuAddGames,
            this.MnuActivitiesList,
            this.PBThread,
            this.btnJson,
            this.MnuTpvList,
            this.toolStripSeparator1,
            this.btnAdministrator,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1189, 39);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // MnuParticipantes
            // 
            this.MnuParticipantes.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MnuParticipantes.Image = global::AlterParadox.GesUmbras.Properties.Resources.participants;
            this.MnuParticipantes.Name = "MnuParticipantes";
            this.MnuParticipantes.Size = new System.Drawing.Size(142, 39);
            this.MnuParticipantes.Text = "Participantes";
            this.MnuParticipantes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MnuParticipantes.Click += new System.EventHandler(this.MnuParticipantes_Click);
            // 
            // mnuActivities
            // 
            this.mnuActivities.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuActivities.Image = global::AlterParadox.GesUmbras.Properties.Resources.activities;
            this.mnuActivities.Name = "mnuActivities";
            this.mnuActivities.Size = new System.Drawing.Size(133, 39);
            this.mnuActivities.Text = "Actividades";
            this.mnuActivities.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mnuActivities.Click += new System.EventHandler(this.mnuActivities_Click);
            // 
            // mnuAddGames
            // 
            this.mnuAddGames.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prestamoDeJuegosToolStripMenuItem,
            this.juegosGlobalesToolStripMenuItem,
            this.juegosLocalesToolStripMenuItem,
            this.asociacionesToolStripMenuItem});
            this.mnuAddGames.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mnuAddGames.Image = global::AlterParadox.GesUmbras.Properties.Resources.games;
            this.mnuAddGames.Name = "mnuAddGames";
            this.mnuAddGames.Size = new System.Drawing.Size(102, 39);
            this.mnuAddGames.Text = "Juegos";
            this.mnuAddGames.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mnuAddGames.Click += new System.EventHandler(this.mnuAddGames_Click);
            // 
            // prestamoDeJuegosToolStripMenuItem
            // 
            this.prestamoDeJuegosToolStripMenuItem.Image = global::AlterParadox.GesUmbras.Properties.Resources.games;
            this.prestamoDeJuegosToolStripMenuItem.Name = "prestamoDeJuegosToolStripMenuItem";
            this.prestamoDeJuegosToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.prestamoDeJuegosToolStripMenuItem.Text = "Prestamo de juegos";
            this.prestamoDeJuegosToolStripMenuItem.Click += new System.EventHandler(this.PrestamoDeJuegosToolStripMenuItem_Click);
            // 
            // juegosGlobalesToolStripMenuItem
            // 
            this.juegosGlobalesToolStripMenuItem.Name = "juegosGlobalesToolStripMenuItem";
            this.juegosGlobalesToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.juegosGlobalesToolStripMenuItem.Text = "Juegos Globales";
            this.juegosGlobalesToolStripMenuItem.Click += new System.EventHandler(this.JuegosGlobalesToolStripMenuItem_Click);
            // 
            // juegosLocalesToolStripMenuItem
            // 
            this.juegosLocalesToolStripMenuItem.Name = "juegosLocalesToolStripMenuItem";
            this.juegosLocalesToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.juegosLocalesToolStripMenuItem.Text = "Juegos Locales";
            this.juegosLocalesToolStripMenuItem.Click += new System.EventHandler(this.JuegosLocalesToolStripMenuItem_Click);
            // 
            // asociacionesToolStripMenuItem
            // 
            this.asociacionesToolStripMenuItem.Name = "asociacionesToolStripMenuItem";
            this.asociacionesToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.asociacionesToolStripMenuItem.Text = "Asociaciones";
            this.asociacionesToolStripMenuItem.Click += new System.EventHandler(this.AsociacionesToolStripMenuItem_Click);
            // 
            // MnuActivitiesList
            // 
            this.MnuActivitiesList.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MnuActivitiesList.Image = global::AlterParadox.GesUmbras.Properties.Resources.monitor;
            this.MnuActivitiesList.Name = "MnuActivitiesList";
            this.MnuActivitiesList.Size = new System.Drawing.Size(189, 39);
            this.MnuActivitiesList.Text = "Lista de actividades";
            this.MnuActivitiesList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MnuActivitiesList.Click += new System.EventHandler(this.MnuActivitiesList_Click);
            // 
            // PBThread
            // 
            this.PBThread.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.PBThread.Name = "PBThread";
            this.PBThread.Size = new System.Drawing.Size(100, 36);
            this.PBThread.Visible = false;
            this.PBThread.Click += new System.EventHandler(this.PBThread_Click);
            this.PBThread.DoubleClick += new System.EventHandler(this.PBThread_DoubleClick);
            // 
            // btnJson
            // 
            this.btnJson.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnJson.Image = global::AlterParadox.GesUmbras.Properties.Resources.execute;
            this.btnJson.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnJson.Name = "btnJson";
            this.btnJson.Size = new System.Drawing.Size(77, 36);
            this.btnJson.Text = "Json";
            this.btnJson.Visible = false;
            // 
            // MnuTpvList
            // 
            this.MnuTpvList.AutoToolTip = false;
            this.MnuTpvList.Image = global::AlterParadox.GesUmbras.Properties.Resources.moneda;
            this.MnuTpvList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MnuTpvList.Name = "MnuTpvList";
            this.MnuTpvList.Size = new System.Drawing.Size(74, 36);
            this.MnuTpvList.Text = "TPV";
            this.MnuTpvList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MnuTpvList.Click += new System.EventHandler(this.MnuTpvList_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // btnAdministrator
            // 
            this.btnAdministrator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdministrator.Image = global::AlterParadox.GesUmbras.Properties.Resources.key;
            this.btnAdministrator.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdministrator.Name = "btnAdministrator";
            this.btnAdministrator.Size = new System.Drawing.Size(36, 36);
            this.btnAdministrator.Text = "toolStripButton2";
            this.btnAdministrator.Click += new System.EventHandler(this.btnAdministrator_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::AlterParadox.GesUmbras.Properties.Resources.page_add;
            this.toolStripButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(100, 36);
            this.toolStripButton1.Text = "Mejoras";
            this.toolStripButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Worker
            // 
            this.Worker.WorkerReportsProgress = true;
            this.Worker.WorkerSupportsCancellation = true;
            this.Worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Worker_DoWork);
            this.Worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Worker_ProgressChanged);
            this.Worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Worker_RunWorkerCompleted);
            // 
            // MDI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 559);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MDI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MDI";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MDI_FormClosing);
            this.Load += new System.EventHandler(this.MDI_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportJson;
        private System.Windows.Forms.ToolStripMenuItem administraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MnuImportWebUsers;
        private System.Windows.Forms.ToolStripMenuItem mnuImportActivities;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripMenuItem MnuParticipantes;
        private System.Windows.Forms.ToolStripMenuItem mnuActivities;
        private System.Windows.Forms.ToolStripMenuItem MnuActivitiesList;
        private System.Windows.Forms.ToolStripMenuItem MnuDBConfig;
        private System.Windows.Forms.ToolStripMenuItem mnuAddGames;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripProgressBar PBThread;
        private System.Windows.Forms.ToolStripButton btnJson;
        private System.Windows.Forms.ToolStripMenuItem mnuImportGames;
        private System.Windows.Forms.ToolStripButton btnAdministrator;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.ComponentModel.BackgroundWorker Worker;
        private System.Windows.Forms.ToolStripButton MnuTpvList;
        private System.Windows.Forms.ToolStripMenuItem openFOlderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem juegosGlobalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem juegosLocalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asociacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prestamoDeJuegosToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}



