﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    enum RefreshCutreType
    {
        AvailableGames,
        LendedGames,
        All
    }
    public partial class FrmGames_Loan : Form
    {
        public FrmGames_Loan()
        {
            InitializeComponent();
        }

        private void L_Refresh(RefreshCutreType RefreshType = RefreshCutreType.All)
        {
            cmdAddFilters.BackColor = (G.filters.FilterActivate) ? Color.LightGreen :Color.LightGray;
            if(RefreshType==RefreshCutreType.All || RefreshType == RefreshCutreType.AvailableGames)
            {
                G.filters.Filter = txtAvailableFilter.Text.Replace("'","");
                G.filters.Status = 0;

                GridGamesAvailable.AutoGenerateColumns = false;
                GridGamesAvailable.DataSource = LocalGame.getAll(G.filters);
            }

            if (RefreshType == RefreshCutreType.All || RefreshType == RefreshCutreType.LendedGames)
            {
                G.LendingFilter.Filter = txtLendFilter.Text.Replace("'", ""); ;
                G.LendingFilter.Status = 1;

                GridGamesLoan.AutoGenerateColumns = false;
                GridGamesLoan.DataSource = GameLending.getAll(G.LendingFilter);
            }
        }

        private void L_NewGame()
        {
            FrmGames_NewLocalGame formulario = new FrmGames_NewLocalGame();
            G.AbrirPantalla(formulario, true, false);
            L_Refresh();
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            L_NewGame();
        }

        private void FrmGames_Loan_Enter(object sender, EventArgs e)
        {
            L_Refresh();
        }

        private void txtAvailableFilter_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtLendFilter_TextChanged(object sender, EventArgs e)
        {
            //if (txtLendFilter.TextLength >= 3)
            //L_Refresh(RefreshCutreType.LendedGames);
        }

        private void FrmGames_Loan_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                txtAvailableFilter.Focus();
                txtAvailableFilter.Select(0, 1000);
            }
            if (e.KeyCode == Keys.F4)
            {
                txtLendFilter.Focus();
                txtLendFilter.Select(0,1000);
            }
        }

        private void GridGamesAvailable_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                LocalGame _localgame = (LocalGame)GridGamesAvailable.Rows[e.RowIndex].DataBoundItem;
                FrmGamePrestar form = new FrmGamePrestar();
                form._localgame = _localgame;
                G.AbrirPantalla(form, true, false);
                L_Refresh();

            }
        }

        private void GridGamesLoan_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                GameLending _gamelended = (GameLending)GridGamesLoan.Rows[e.RowIndex].DataBoundItem;
                FrmGamePrestar form = new FrmGamePrestar();
                form._gamelended = _gamelended;
                G.AbrirPantalla(form, true, false);
                L_Refresh();

            }
        }

        private void GridGamesAvailable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmdAddFilters_Click(object sender, EventArgs e)
        {
            FrmGameFilters formulario = new FrmGameFilters();
            G.AbrirPantalla(formulario, true, false);

            L_Refresh();
        }

        private void cmdCleanFilter_Click(object sender, EventArgs e)
        {
            G.filters.FilterActivate = false;
            G.LendingFilter.FilterActivate = false;
            L_Refresh();
        }

        private void FrmGames_Loan_Load(object sender, EventArgs e)
        {
            G.filters = new LocalGame_Filter();
            G.LendingFilter = new GameLending_Filter();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            L_Refresh();
        }

        //refrescar
        private Timer tiempoRefresco;
        private void InitTimer()
        {
            tiempoRefresco = new Timer();
            tiempoRefresco.Tick += new EventHandler(tiempoRefresco_tick);
            tiempoRefresco.Interval = 10000; //miliseconds
            tiempoRefresco.Start();
        }
        private void tiempoRefresco_tick(object sender, EventArgs e)
        {
            L_Refresh();
        }

        private void txtAvailableFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //if(txtAvailableFilter.TextLength>=3)
                L_Refresh(RefreshCutreType.AvailableGames);
            }
        }

        private void txtLendFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                //if(txtAvailableFilter.TextLength>=3)
                L_Refresh(RefreshCutreType.LendedGames);
            }
        }
    }
}
