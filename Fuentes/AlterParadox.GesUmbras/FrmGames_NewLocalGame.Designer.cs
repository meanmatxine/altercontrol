﻿namespace AlterParadox.GesUmbras
{
    partial class FrmGames_NewLocalGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGames_NewLocalGame));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.GridHistory = new System.Windows.Forms.DataGridView();
            this.btnSearchAssociation = new System.Windows.Forms.Button();
            this.btnAddAssociation = new System.Windows.Forms.Button();
            this.txtAssociationId = new System.Windows.Forms.TextBox();
            this.txtGameComments = new System.Windows.Forms.TextBox();
            this.lblComments = new System.Windows.Forms.Label();
            this.btnSearchGame = new System.Windows.Forms.Button();
            this.btnAddGame = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.mnuNew = new System.Windows.Forms.ToolStripButton();
            this.mnuSave = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.txtGameId = new System.Windows.Forms.TextBox();
            this.lblAssociation = new System.Windows.Forms.Label();
            this.txtAssociationName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.txtGameName = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnFilterRefresh = new System.Windows.Forms.Button();
            this.btnFilterClean = new System.Windows.Forms.Button();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.GridLocalGames = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAssociation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.colUserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridHistory)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridLocalGames)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.GridHistory);
            this.panel1.Controls.Add(this.btnSearchAssociation);
            this.panel1.Controls.Add(this.btnAddAssociation);
            this.panel1.Controls.Add(this.txtAssociationId);
            this.panel1.Controls.Add(this.txtGameComments);
            this.panel1.Controls.Add(this.lblComments);
            this.panel1.Controls.Add(this.btnSearchGame);
            this.panel1.Controls.Add(this.btnAddGame);
            this.panel1.Controls.Add(this.txtId);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Controls.Add(this.txtGameId);
            this.panel1.Controls.Add(this.lblAssociation);
            this.panel1.Controls.Add(this.txtAssociationName);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.lblName);
            this.panel1.Controls.Add(this.txtGameName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(459, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 503);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(4, 260);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(528, 27);
            this.label1.TabIndex = 47;
            this.label1.Text = "Historial de prestamos";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GridHistory
            // 
            this.GridHistory.AllowUserToAddRows = false;
            this.GridHistory.AllowUserToDeleteRows = false;
            this.GridHistory.AllowUserToResizeColumns = false;
            this.GridHistory.AllowUserToResizeRows = false;
            this.GridHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridHistory.ColumnHeadersHeight = 30;
            this.GridHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUserId,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.GridHistory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridHistory.Location = new System.Drawing.Point(4, 286);
            this.GridHistory.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridHistory.MultiSelect = false;
            this.GridHistory.Name = "GridHistory";
            this.GridHistory.ReadOnly = true;
            this.GridHistory.RowHeadersVisible = false;
            this.GridHistory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridHistory.Size = new System.Drawing.Size(528, 204);
            this.GridHistory.TabIndex = 46;
            // 
            // btnSearchAssociation
            // 
            this.btnSearchAssociation.BackgroundImage = global::AlterParadox.GesUmbras.Properties.Resources.find;
            this.btnSearchAssociation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSearchAssociation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchAssociation.Location = new System.Drawing.Point(192, 69);
            this.btnSearchAssociation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSearchAssociation.Name = "btnSearchAssociation";
            this.btnSearchAssociation.Size = new System.Drawing.Size(30, 27);
            this.btnSearchAssociation.TabIndex = 33;
            this.btnSearchAssociation.UseVisualStyleBackColor = true;
            this.btnSearchAssociation.Click += new System.EventHandler(this.btnSearchAssociation_Click);
            // 
            // btnAddAssociation
            // 
            this.btnAddAssociation.BackgroundImage = global::AlterParadox.GesUmbras.Properties.Resources.page_add;
            this.btnAddAssociation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddAssociation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAssociation.Location = new System.Drawing.Point(123, 69);
            this.btnAddAssociation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAddAssociation.Name = "btnAddAssociation";
            this.btnAddAssociation.Size = new System.Drawing.Size(30, 27);
            this.btnAddAssociation.TabIndex = 32;
            this.btnAddAssociation.UseVisualStyleBackColor = true;
            this.btnAddAssociation.Click += new System.EventHandler(this.btnAddAssociation_Click);
            // 
            // txtAssociationId
            // 
            this.txtAssociationId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssociationId.Enabled = false;
            this.txtAssociationId.Location = new System.Drawing.Point(152, 69);
            this.txtAssociationId.MaxLength = 100;
            this.txtAssociationId.Name = "txtAssociationId";
            this.txtAssociationId.Size = new System.Drawing.Size(41, 27);
            this.txtAssociationId.TabIndex = 2;
            // 
            // txtGameComments
            // 
            this.txtGameComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGameComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGameComments.Location = new System.Drawing.Point(4, 125);
            this.txtGameComments.Multiline = true;
            this.txtGameComments.Name = "txtGameComments";
            this.txtGameComments.Size = new System.Drawing.Size(528, 92);
            this.txtGameComments.TabIndex = 4;
            // 
            // lblComments
            // 
            this.lblComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblComments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComments.Location = new System.Drawing.Point(4, 99);
            this.lblComments.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(528, 27);
            this.lblComments.TabIndex = 30;
            this.lblComments.Text = "Observaciones";
            this.lblComments.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSearchGame
            // 
            this.btnSearchGame.BackgroundImage = global::AlterParadox.GesUmbras.Properties.Resources.find;
            this.btnSearchGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSearchGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchGame.Location = new System.Drawing.Point(192, 39);
            this.btnSearchGame.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSearchGame.Name = "btnSearchGame";
            this.btnSearchGame.Size = new System.Drawing.Size(30, 27);
            this.btnSearchGame.TabIndex = 29;
            this.btnSearchGame.UseVisualStyleBackColor = true;
            this.btnSearchGame.Click += new System.EventHandler(this.btnSearchGame_Click);
            // 
            // btnAddGame
            // 
            this.btnAddGame.BackgroundImage = global::AlterParadox.GesUmbras.Properties.Resources.page_add;
            this.btnAddGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddGame.Location = new System.Drawing.Point(123, 39);
            this.btnAddGame.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAddGame.Name = "btnAddGame";
            this.btnAddGame.Size = new System.Drawing.Size(30, 27);
            this.btnAddGame.TabIndex = 28;
            this.btnAddGame.UseVisualStyleBackColor = true;
            this.btnAddGame.Click += new System.EventHandler(this.btnAddGame_Click);
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(511, 3);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(21, 27);
            this.txtId.TabIndex = 27;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNew,
            this.mnuSave,
            this.toolStripSeparator1,
            this.mnuDelete,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(536, 39);
            this.toolStrip1.TabIndex = 26;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // mnuNew
            // 
            this.mnuNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuNew.Image = ((System.Drawing.Image)(resources.GetObject("mnuNew.Image")));
            this.mnuNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Size = new System.Drawing.Size(36, 36);
            this.mnuNew.Text = "Nuevo";
            this.mnuNew.ToolTipText = "Nuevo (Ctrl+N)";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuSave.Image = ((System.Drawing.Image)(resources.GetObject("mnuSave.Image")));
            this.mnuSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Size = new System.Drawing.Size(36, 36);
            this.mnuSave.Text = "toolStripButton1";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // mnuDelete
            // 
            this.mnuDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuDelete.Image")));
            this.mnuDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(36, 36);
            this.mnuDelete.Text = "toolStripButton3";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // txtGameId
            // 
            this.txtGameId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGameId.Enabled = false;
            this.txtGameId.Location = new System.Drawing.Point(152, 39);
            this.txtGameId.MaxLength = 100;
            this.txtGameId.Name = "txtGameId";
            this.txtGameId.Size = new System.Drawing.Size(41, 27);
            this.txtGameId.TabIndex = 0;
            // 
            // lblAssociation
            // 
            this.lblAssociation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblAssociation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAssociation.Location = new System.Drawing.Point(4, 69);
            this.lblAssociation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAssociation.Name = "lblAssociation";
            this.lblAssociation.Size = new System.Drawing.Size(120, 27);
            this.lblAssociation.TabIndex = 23;
            this.lblAssociation.Text = "Asociación (F4)";
            this.lblAssociation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAssociationName
            // 
            this.txtAssociationName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAssociationName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssociationName.Enabled = false;
            this.txtAssociationName.Location = new System.Drawing.Point(221, 69);
            this.txtAssociationName.MaxLength = 100;
            this.txtAssociationName.Name = "txtAssociationName";
            this.txtAssociationName.Size = new System.Drawing.Size(311, 27);
            this.txtAssociationName.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Image = global::AlterParadox.GesUmbras.Properties.Resources.disk;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(4, 220);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(146, 37);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnLocalGameSave_Click);
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Location = new System.Drawing.Point(4, 39);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(120, 27);
            this.lblName.TabIndex = 18;
            this.lblName.Text = "Juego (F3)";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGameName
            // 
            this.txtGameName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGameName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGameName.Enabled = false;
            this.txtGameName.Location = new System.Drawing.Point(221, 39);
            this.txtGameName.MaxLength = 100;
            this.txtGameName.Name = "txtGameName";
            this.txtGameName.Size = new System.Drawing.Size(311, 27);
            this.txtGameName.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnFilterRefresh);
            this.panel6.Controls.Add(this.btnFilterClean);
            this.panel6.Controls.Add(this.txtFilter);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(454, 33);
            this.panel6.TabIndex = 28;
            // 
            // btnFilterRefresh
            // 
            this.btnFilterRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilterRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilterRefresh.Location = new System.Drawing.Point(425, 3);
            this.btnFilterRefresh.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFilterRefresh.Name = "btnFilterRefresh";
            this.btnFilterRefresh.Size = new System.Drawing.Size(27, 27);
            this.btnFilterRefresh.TabIndex = 23;
            this.btnFilterRefresh.Text = "R";
            this.btnFilterRefresh.UseVisualStyleBackColor = true;
            this.btnFilterRefresh.Click += new System.EventHandler(this.btnFilterRefresh_Click);
            // 
            // btnFilterClean
            // 
            this.btnFilterClean.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilterClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFilterClean.Location = new System.Drawing.Point(399, 3);
            this.btnFilterClean.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnFilterClean.Name = "btnFilterClean";
            this.btnFilterClean.Size = new System.Drawing.Size(27, 27);
            this.btnFilterClean.TabIndex = 22;
            this.btnFilterClean.Text = "X";
            this.btnFilterClean.UseVisualStyleBackColor = true;
            this.btnFilterClean.Click += new System.EventHandler(this.btnFilterClean_Click);
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(3, 3);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(397, 27);
            this.txtFilter.TabIndex = 0;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // GridLocalGames
            // 
            this.GridLocalGames.AllowUserToAddRows = false;
            this.GridLocalGames.AllowUserToDeleteRows = false;
            this.GridLocalGames.AllowUserToResizeColumns = false;
            this.GridLocalGames.AllowUserToResizeRows = false;
            this.GridLocalGames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridLocalGames.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridLocalGames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.GridLocalGames.ColumnHeadersHeight = 30;
            this.GridLocalGames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridLocalGames.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.colAssociation});
            this.GridLocalGames.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridLocalGames.Location = new System.Drawing.Point(2, 36);
            this.GridLocalGames.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridLocalGames.MultiSelect = false;
            this.GridLocalGames.Name = "GridLocalGames";
            this.GridLocalGames.ReadOnly = true;
            this.GridLocalGames.RowHeadersVisible = false;
            this.GridLocalGames.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridLocalGames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridLocalGames.Size = new System.Drawing.Size(450, 466);
            this.GridLocalGames.TabIndex = 0;
            this.GridLocalGames.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridLocalGames_CellContentClick);
            this.GridLocalGames.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridLocalGames_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn3.FillWeight = 11.40375F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Id";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "TheGame";
            this.dataGridViewTextBoxColumn4.FillWeight = 91.22999F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // colAssociation
            // 
            this.colAssociation.DataPropertyName = "association";
            this.colAssociation.HeaderText = "Asociación";
            this.colAssociation.Name = "colAssociation";
            this.colAssociation.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.GridLocalGames);
            this.panel2.Location = new System.Drawing.Point(-1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(454, 504);
            this.panel2.TabIndex = 30;
            // 
            // colUserId
            // 
            this.colUserId.DataPropertyName = "LastModify";
            this.colUserId.FillWeight = 20F;
            this.colUserId.HeaderText = "Fecha";
            this.colUserId.Name = "colUserId";
            this.colUserId.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TheUser";
            this.dataGridViewTextBoxColumn1.FillWeight = 50F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Comments";
            this.dataGridViewTextBoxColumn2.FillWeight = 50F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Comments";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // FrmGames_NewLocalGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 503);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmGames_NewLocalGame";
            this.Text = "Pantalla de juegos";
            this.Load += new System.EventHandler(this.FrmGames_NewGame_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmGames_NewLocalGame_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridHistory)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridLocalGames)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddGame;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton mnuNew;
        private System.Windows.Forms.ToolStripButton mnuSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton mnuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TextBox txtGameId;
        private System.Windows.Forms.Label lblAssociation;
        private System.Windows.Forms.TextBox txtAssociationName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtGameName;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnFilterRefresh;
        private System.Windows.Forms.Button btnFilterClean;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.DataGridView GridLocalGames;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAssociation;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSearchGame;
        private System.Windows.Forms.TextBox txtGameComments;
        private System.Windows.Forms.Label lblComments;
        private System.Windows.Forms.TextBox txtAssociationId;
        private System.Windows.Forms.Button btnSearchAssociation;
        private System.Windows.Forms.Button btnAddAssociation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView GridHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}