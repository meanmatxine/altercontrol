﻿namespace AlterParadox.GesUmbras
{
    using AlterParadox.Core;
    using AlterParadox.GesUmbras.SQLService;
    using AlterParadox.GesUmbras.UserService;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class GameLending_Filter
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public int LocalGame_Id { get; set; }
        public bool All { get; set; }
        public int Status { get; set; }

        public bool FilterActivate { get; set; }
        public int? Family { get; set; }
        public int? Type { get; set; }
        public int? MinPlayers { get; set; }
        public int? MaxPlayers { get; set; }
        public int? Difficulty { get; set; }
        public int? Association { get; set; }

        public GameLending_Filter()
        {
            this.Filter = "";
            this.Id = 0;
            this.LocalGame_Id = 0;
            this.All = true;
            this.Status = 0;

            this.FilterActivate = false;
            this.Family = null;
            this.Type = null;
            this.MinPlayers = null;
            this.MaxPlayers = null;
            this.Difficulty = null;
            this.Association = null;
        }
    }



    public class GameLending_JSON
    {

        //actividadID = "" + Convert.ToInt32(row["id"]),
        //actividadNombre = (string) row["nombrejuego"],

        ////games.editorial, games.family, games.type, games.years, games.player_min, games.player_max, games.difficulty, games.expansion, games.duration, games.comments as gComments

        //actividadPlazasMax = "" + Convert.ToInt32(row["status"])

        public string gameId { get; set; }
        public string gameNombre { get; set; }

        public string gameEditorial { get; set; }
        public string gameFamilia { get; set; }
        public string gameTipo { get; set; }
        public string gameYear { get; set; }
        public string gamePlayerMin { get; set; }
        public string gamePlayerMax { get; set; }
        public string gameDificultad { get; set; }
        public string gameDuracion { get; set; }

        public string gameStatus { get; set; }

        public static GameLending_JSON[] getAll()
        {
            List<GameLending_JSON> thegames = new List<GameLending_JSON>();

            string query = "";
            query = "SELECT  localgames.id, localgames.game_id, games.name as gamename, localgames.association_id, associations.name as associationname, "
                + " games.editorial, games.family, games.type, games.years, games.player_min, games.player_max, games.difficulty, games.expansion, games.duration, games.comments,"
                + " COALESCE((Select TOP 1 status from games_lending where localgame_id = localgames.id Order by modifydate DESC),0) as status"
                + " FROM localgames"
                + " LEFT JOIN games ON localgames.game_id = games.id"
                + " LEFT JOIN associations ON localgames.association_id = associations.id  ";


            query = query + " ";

            //query = query + " ORDER BY activityDate ASC, name";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                //1502397244
                //1502394386

                GameLending_JSON activity = new GameLending_JSON
                {
                    gameId = "" + Convert.ToInt32(row["game_id"]),
                    gameNombre = (string)row["gamename"],

                    gameEditorial = (string)row["editorial"],
                    gameFamilia = G.EnumToDescription((Familia)Convert.ToInt32(row["family"])),
                    gameTipo = G.EnumToDescription((Tipo)Convert.ToInt32(row["type"])),
                    gameYear = Convert.ToInt32(row["years"]).ToString(),
                    gamePlayerMin = Convert.ToInt32(row["player_min"]).ToString(),
                    gamePlayerMax = Convert.ToInt32(row["player_max"]).ToString(),
                    gameDificultad = G.EnumToDescription((Dificultad)Convert.ToInt32(row["difficulty"])),
                    gameDuracion =  Convert.ToInt32(row["duration"]).ToString(),
                    //games.editorial, games.family, games.type, games.years, games.player_min, games.player_max, games.difficulty, games.expansion, games.duration, games.comments as gComments

                    gameStatus =  Convert.ToInt32(row["status"]).ToString()
                };
                //1502535600
                thegames.Add(activity);
            }

            return thegames.ToArray();
        }

    }
   

    public class GameLending
    {
        public int Id { get; set; }
        public LocalGame TheGame { get; set; }
        public User TheUser { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }
        public DateTime LastModify { get; set; }

        public static GameLending[] getAll(GameLending_Filter filters)
        {
            List<GameLending> thegames = new List<GameLending>();

            LocalGame[] thelocalgames = LocalGame.getAll(null);

            User[] theusers = null;

            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    theusers = svc.GetUsers().ListaUsuarios.ToArray();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            string query = "";
            query = "SELECT games_lending.id, games_lending.localgame_id, games.name, users.id as user_id, users.name, localgames.association_id, associations.name as association, games_lending.comments, games_lending.status "
                + " FROM games_lending  "
                + " LEFT JOIN localgames "
                + " ON localgames.id = games_lending.localgame_id "
                + " LEFT JOIN users "
                + " ON users.id = games_lending.user_id "
                + " LEFT JOIN associations "
                + " ON associations.id = localgames.association_id "
                + " LEFT JOIN games "
                + " ON games.id = localgames.game_id ";


            query = query + " WHERE games_lending.id >=0";
            if (filters != null)
            {
                if (filters.Filter != "")
                {
                    query = query + " AND CONCAT(games.name  , '-'  , users.name , '-' , associations.name) COLLATE Latin1_General_CI_AI like  '%" + filters.Filter + "%' COLLATE Latin1_General_CI_AI ";
                }
                if (filters.Status != 0)
                {
                    query = query + " AND games_lending.status = " + filters.Status + " ";
                }

                if (filters.FilterActivate)
                {
                    if (filters.Family != null)
                        query = query + " AND games.family = " + filters.Family + " ";
                    if (filters.Type != null)
                        query = query + " AND games.type = " + filters.Type + " ";

                    if (filters.MinPlayers != null)
                        query = query + " AND games.player_min >= " + filters.MinPlayers + " ";
                    if (filters.MaxPlayers != null)
                        query = query + " AND games.player_max <= " + filters.MaxPlayers + " ";

                    if (filters.Difficulty != null)
                        query = query + " AND games.difficulty = " + filters.Difficulty + " ";

                    if (filters.Association != null)
                        query = query + " AND localgames.association_id = " + filters.Association + " ";
                }
            }
            query = query + " ORDER BY games.name DESC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                GameLending Game = new GameLending
                {
                    Id = Convert.ToInt32(row["id"]),
                    TheGame = thelocalgames.SingleOrDefault(item => item.Id == Convert.ToInt32(row["localgame_id"])),
                    TheUser = theusers.SingleOrDefault(item => item.Id == Convert.ToInt32(row["user_id"])),
                    Comments = (string)row["comments"],
                    Status = Convert.ToInt32(row["status"])

                };
                thegames.Add(Game);
            }

            return thegames.ToArray();
        }

        public static GameLending[] getHistory(GameLending_Filter filters)
        {
            List<GameLending> thegames = new List<GameLending>();

            User[] theusers = null;

            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    theusers = svc.GetUsers().ListaUsuarios.ToArray();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            string query = "";
            /* DE HETTAR NO FUNCIONA
             * query = " SELECT a.id, a.user_id, datetime(a.modifydate, 'localtime') as modifydate, a.comments, a.status, b.name, b.lastname , b.number "
                + " FROM games_lending AS a  "
                + " LEFT JOIN users AS b ON b.id = a.user_id ";*/
            query = " SELECT a.id, a.user_id, a.modifydate, a.comments, a.status, b.name, b.lastname , b.number "
                + " FROM games_lending AS a  "
                + " LEFT JOIN users AS b ON b.id = a.user_id ";

            query = query + " WHERE a.id >=0";
            Console.WriteLine(query);

            if (filters != null)
            {
                if (filters.LocalGame_Id != 0)
                {
                    query = query + " AND a.localgame_id = " + filters.LocalGame_Id + " ";
                }
            }
            query = query + " Order by a.modifydate DESC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                GameLending Game = new GameLending
                {
                    Id = Convert.ToInt32(row["id"]),
                    TheGame = null,
                    TheUser = theusers.SingleOrDefault(item => item.Id == Convert.ToInt32(row["user_id"])),
                    Comments = (string)row["comments"],
                    Status = Convert.ToInt32(row["status"]),
                    LastModify = Convert.ToDateTime(row["modifydate"])

                };
                thegames.Add(Game);
            }

            return thegames.ToArray();
        }

        public static GameLending getGame(GameLending_Filter filters)
        {
           
            GameLending thegame = new GameLending();
            LocalGame[] thelocalgames = LocalGame.getAll(null);
            User[] theusers = null;

            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    theusers = svc.GetUsers().ListaUsuarios.ToArray();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            string query = "";
            query = "SELECT games_lending.id, games_lending.localgame_id, games.name, users.name, localgames.association, games_lending.comments, games_lending.status "
                + " FROM games_lending  "
                + " LEFT JOIN localgames "
                + " ON localgames.id = games_lending.localgame_id "
                + " LEFT JOIN users "
                + " ON users.id = games_lending.user_id "
                + " LEFT JOIN games "
                + " ON games.id = localgames.game_id ";

            query = query + " WHERE GameLendings.id >=0";
            if (filters != null)
            {
                if (filters.Filter != "")
                {
                    query = query + " AND CONCAT(games.name  , '-' , users.name , '-' , localgames.association) COLLATE Latin1_General_CI_AI like '%" + filters.Filter + "%' COLLATE Latin1_General_CI_AI ";
                }
                if (filters.Status != 0)
                {
                    query = query + " AND games_lending.status = " + filters.Status + " ";
                }

            }
            query = query + " ORDER BY games.name DESC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                thegame = new GameLending
                {
                    Id = Convert.ToInt32(row["id"]),
                    TheGame = thelocalgames.SingleOrDefault(item => item.Id == Convert.ToInt32(row["localgame_id"])),
                    TheUser = theusers.SingleOrDefault(item => item.Id == Convert.ToInt32(row["user_id"])),
                    Comments = (string)row["comments"],
                    Status = Convert.ToInt32(row["status"])

                };
                //thegames.Add(Game);
            }

            return thegame;
        }

        public static bool Exists(int idGameLending)
        {
            bool theresult = false;
            string query = "";
            query = "SELECT id "
                + " FROM games_lending  ";

            query = query + " WHERE id =" + idGameLending;

            query = query + " ORDER BY id ASC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                theresult = true;
            }

            return theresult;
        }

        public void Save()
        {
            string query = "";
            if (this.Id == 0)
            {
                //CREATE
                query = "INSERT INTO games_lending(localgame_id, user_id, comments, status) VALUES " +
                    " ( '" + this.TheGame.Id + "','" + this.TheUser.Id + "','" + this.Comments + "'," + this.Status + ")";
            }
            else
            {
                //UPDATE
                query = "UPDATE games_lending SET " +
                    " status= " + this.Status + ", " +
                    " comments= '" + this.Comments + "' " +
                    " WHERE id = " + this.Id + ";";
            }

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Delete()
        {

            if (MessageBox.Show("¿Desea realmente eliminar el juego " + this.TheGame.TheGame.Name + " prestado a " + this.TheUser.Name + " " + this.TheUser.LastName + " ? Este proceso eliminará el juego y todo su log.", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {

                //Elimino el usuario
                var query = "DELETE FROM GameLendings WHERE id = " + this.Id + ";";
                //Elimino el usuario de la tabla de actividades
                var query2 = "DELETE FROM games_lending WHERE GameLending_id = " + this.Id + ";";

                try
                {
                    using (SQLServiceClient svc = new SQLServiceClient())
                    {
                        svc.ExecuteSQL(query);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }

        }

    }
}
