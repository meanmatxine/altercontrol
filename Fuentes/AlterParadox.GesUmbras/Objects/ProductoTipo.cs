﻿using AlterParadox.GesUmbras.SQLService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.GesUmbras.Objects
{
    class ProductoTipo
    {
        public int tipoID { get; set; }
        public string tipoName { get; set; }

        private static string tabla = "TPV_ProductoTipo";

        public ProductoTipo(DataRow r)
        {
            tipoID = (int)r.Field<int>("tipoID");
            tipoName = r.Field<String>("tipoName");
        }

        //Seleccionar todos
        public static DataView SELECT_ALL()
        {
            string query = "SELECT * FROM " + tabla;

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

            DataView productoTipos= DS.Tables[0].DefaultView;

            return productoTipos;
        }
    }
}
