﻿using AlterParadox.GesUmbras.SQLService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.GesUmbras.Objects
{
    public class Pedido
    {
        public int pedidoID { get; set; }
        public string pedidoFecha { get; set; }
        public decimal pedidoTotal { get; set; }
        public decimal pedidoEntregado { get; set; }
        public decimal pedidoDevuelto { get; set; }

        private static string tabla = "TPV_Pedidos";

        public Pedido(DataRow r)
        {
            pedidoID = (int)r.Field<int>("pedidoID");
            pedidoFecha = r.Field<DateTime>("pedidoFecha").ToShortDateString();
            pedidoTotal = (decimal)r.Field<double>("pedidoTotal");
            pedidoEntregado = (decimal)r.Field<double>("pedidoEntregado");
            pedidoDevuelto = (decimal)r.Field<double>("pedidoDevuelto");
        }

        public Pedido()
        {
            pedidoTotal = 0;
            pedidoEntregado = 0;
            pedidoDevuelto = 0;
        }

        //coger uno
        public static Pedido SELECT_ONE(int pID)
        {
            Pedido p = null;

            string query = "SELECT * FROM " + tabla + " WHERE pedidoID=" + pID;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count != 1)
            {
                p = null;
            }
            else
            {
                p = new Pedido(DS.Tables["Table"].Rows[0]);
            }

            return p;
        }


        //coger uno a Pedido
        public static Pedido SELECT_ONE_PEDIDO(int id)
        {
            Pedido p = null;

            string query = "SELECT * FROM "+tabla+" WHERE pedidoID=" + id;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count != 1)
            {
                p = null;
            }
            else
            {
                p = new Pedido(DS.Tables["Table"].Rows[0]);
            }

            return p;
        }

        public static Pedido GetLast()
        {
            Pedido p = null;

            string query = "SELECT TOP 1 * FROM " + tabla + " ORDER BY pedidoID DESC ";
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count != 1)
            {
                p = null;
            }
            else
            {
                p = new Pedido(DS.Tables["Table"].Rows[0]);
            }

            return p;
        }

        //todos a grid (orden)
        public static DataTable SELECT_ALL_GRID(bool reverse)
        {

            string query = "SELECT * FROM " + tabla+ " ORDER BY pedidoID";
            if (reverse)
                query += " DESC";
            else
                query += " ASC";
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            DataTable dt = DS.Tables[0];

            return dt;
        }

        //todos a list (orden)
        public static List<Pedido> SELECT_ALL_LIST(bool reverse)
        {
            List<Pedido> pedidos = new List<Pedido>();

            string query = "SELECT * FROM " + tabla + " ORDER BY pedidoID";
            if (reverse)
                query += " DESC";
            else
                query += " ASC";
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                pedidos.Add(new Pedido(row));
            }

            return pedidos;
        }

        //insertar pedido
        public static void INSERT(Pedido p)
        {
            string pt, pe, pd;
            pt = p.pedidoTotal.ToString().Replace(",", ".");
            pe = p.pedidoEntregado.ToString().Replace(",", ".");
            pd = p.pedidoDevuelto.ToString().Replace(",", ".");
            //Console.WriteLine("insertar");

            string query = "INSERT INTO " + tabla + " ( pedidoFecha, pedidoTotal, pedidoEntregado, pedidoDevuelto) VALUES ("
             + " '" + p.pedidoFecha + "', '" + pt + "', '" + pe + "', '" + pd + "');";
            Console.WriteLine(query);
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        //Pedidos Entre Fechas
        public static DataTable SELECT_DATES(string fini, string ffin)
        {

            string query = "SELECT * FROM " + tabla + " WHERE `pedidoFecha`>='"+fini+"' AND `pedidoFecha`<='"+ffin+"' ORDER BY pedidoID ASC";
            Console.WriteLine(query);
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            DataTable dt = DS.Tables[0];

            return dt;
        }

    }
}
