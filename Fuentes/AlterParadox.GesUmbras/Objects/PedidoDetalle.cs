﻿using AlterParadox.GesUmbras.SQLService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.GesUmbras.Objects
{
    class PedidoDetalle
    {
        public int pedidoID { get; set; }
        public int productoID { get; set; }
        public int pdCantidad { get; set; }

        private static string tabla = "TPV_PedidosDetalle";

        public PedidoDetalle(DataRow r)
        {
            pedidoID = (int)r.Field<int>("pedidoID");
            productoID = (int)r.Field<int>("productoID");
            pdCantidad = (int)r.Field<int>("pdCantidad");
        }

        public PedidoDetalle()
        {

        }

        //coger detalle de un pedido
        public static List<PedidoDetalle> SELECT_PEDIDO(int pID)
        {
            List<PedidoDetalle> pDetalles = new List<PedidoDetalle>();

            string query = "SELECT * FROM "+tabla+" WHERE pedidoID=" + pID;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                pDetalles.Add(new PedidoDetalle(row));
            }

            return pDetalles;
        }

        public static List<PedidoDetalle> SELECT_ONE_GRID(int id)
        {
            List<PedidoDetalle> pDetalles = new List<PedidoDetalle>();

            string query = "SELECT * FROM " + tabla + " WHERE pedidoID=" + id;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                pDetalles.Add(new PedidoDetalle(row));
            }

            return pDetalles;
        }

        public static void Insert(PedidoDetalle pd)
        {

            string query = "INSERT INTO " + tabla + " (pedidoID, productoID, pdCantidad) VALUES ("
            + pd.pedidoID + ", " + pd.productoID + ", " + pd.pdCantidad + ");";
            Console.WriteLine(query);
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
