﻿using AlterParadox.GesUmbras.SQLService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.GesUmbras.Objects
{
    class ControlCaja
    {
        public int cajaMovID { get; set; }
        public string cajaMovFecha { get; set; }//"DD/MM/YYYY HH:MM:SS"
        public string cajaPersona { get; set; }
        public decimal cajaIngreso { get; set; }
        public decimal cajaExtracto { get; set; }
        public string cajaConcepto { get; set; }
        public string cajaObservaciones { get; set; }
        public decimal cajaDineroActual { get; set; }
        public decimal cajaDescuadre { get; set; }
    

        private static string tabla = "TPV_ControlCaja";

        //Constructor desde DBC
        public ControlCaja (DataRow r)
        {
            cajaMovID= (int)r.Field<int>("cajaMovID");
            cajaMovFecha = r.Field<DateTime>("cajaMovFecha").ToShortDateString();
            cajaPersona = r.Field<string>("cajaPersona");
            cajaIngreso = (decimal)(r.Field<double>("cajaIngreso"));
            cajaExtracto = (decimal)(r.Field<double>("cajaExtracto"));
            cajaConcepto = r.Field<string>("cajaConcepto");
            cajaObservaciones = r.Field<string>("cajaObservaciones");
            cajaDineroActual = (decimal)(r.Field<double>("cajaDineroActual"));
            cajaDescuadre = (decimal)(r.Field<double>("cajaDescuadre"));
        }

        //constructor vacio
        public ControlCaja()
        {

        }

        //Seleccionar movimientos

        //Ultimo mov
        public static ControlCaja ultimoMovimiento()
        {

            string query = "SELECT TOP 1 * FROM " + tabla + " ORDER BY cajaMovID DESC ";
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count != 1)
            {
                return null;
            }
            else
            {
               return  new ControlCaja(DS.Tables["Table"].Rows[0]);
            }
        }
        
        //tengoDinero
        public static bool tengoDinero()
        {

            string query = "SELECT * FROM " + tabla;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Insertar
        public static void INSERT(ControlCaja c)
        {
            //conversion
            string cing, cext, cact, cdes;
            cing = c.cajaIngreso.ToString().Replace(",", ".");
            cext = c.cajaExtracto.ToString().Replace(",", ".");
            cact = c.cajaDineroActual.ToString().Replace(",", ".");
            cdes = c.cajaDescuadre.ToString().Replace(",", ".");
            //INSERT

            string query = "INSERT INTO " + tabla + " ( cajaMovFecha, cajaPersona, cajaIngreso, cajaExtracto, cajaConcepto, cajaObservaciones, cajaDineroActual, cajaDescuadre) VALUES "
            + "('" + c.cajaMovFecha + "','"+c.cajaPersona+"','" + cing + "','" + cext + "','" + c.cajaConcepto + "','" + c.cajaObservaciones + "','"+cact+"','" + cdes + "');";
            Console.WriteLine(query);
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //todos a grid
        public static DataTable SELECT_MOVS_GRID()
        {

            string query = "SELECT * FROM "+tabla+" WHERE cajaConcepto LIKE 'Meter Dinero' OR cajaConcepto LIKE 'Sacar Dinero' OR cajaConcepto LIKE 'Cuadre Caja' ORDER BY cajaMovFecha ASC";
            Console.WriteLine(query);
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            DataTable dt = DS.Tables[0];
            return dt;
        }
    }

}
