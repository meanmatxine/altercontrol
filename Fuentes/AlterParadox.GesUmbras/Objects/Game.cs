﻿namespace AlterParadox.GesUmbras
{
    using AlterParadox.GesUmbras.SQLService;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public enum Familia
    {
         Cartas,
         Fichas,
         Tablero,
         Miniaturas,
         Dibujo,
         Dados,
         [Description("Preguntas y respuestas")]
         Preguntas_respuestas,
         Otros
    }
    public enum Tipo
    {
        Familiar,
        Infantil,
        Party,
        Estrategia,
        Abstracto,
        Temático,
        Recursos,
        Wargame,
        Otros
    }
    public enum Dificultad
    {
        Desconocida,
        Baja,
        Medio,
        Alta
    }

    public class Game_Filter
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        public Game_Filter()
        {
            this.Filter = "";
            this.Id = 0;
            this.Name = "";
        }
    }

    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Editorial { get; set; }

        public int Family { get; set; }
        public int Type { get; set; }
        public int Years { get; set; }

        public int PlayerMin { get; set; }
        public int PlayerMax { get; set; }
        public int Difficulty { get; set; }
        public int Duration { get; set; }
        public int IsExpansion { get; set; }

        public string Comments { get; set; }

        public override string ToString()
        {
            return Name;
        }
        public static Game[] getAll(Game_Filter filter)
        {
            List<Game> thegames = new List<Game>();

            string query = "";
            query = "SELECT id, name, editorial, family, type, years, player_min, player_max, difficulty, expansion, duration, comments "
                + " FROM games  ";

            query = query + " WHERE id >=0";
            if(filter != null) { 
                if(filter.Filter != "")
                {
                    query = query + " AND CONCAT(name , '-' , comments) COLLATE Latin1_General_CI_AI like '%" + filter.Filter+ "%' COLLATE Latin1_General_CI_AI ";
                }
            }
            query = query + " ORDER BY name DESC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                Game Game = new Game
                {
                    Id = Convert.ToInt32(row["id"]),
                    Name = (string)row["name"],
                    Editorial = (string)row["editorial"],
                    Family = Convert.ToInt32(row["family"]),
                    Type = Convert.ToInt32(row["type"]),
                    Years = Convert.ToInt32(row["years"]),
                    PlayerMin = Convert.ToInt32(row["player_min"]),
                    PlayerMax = Convert.ToInt32(row["player_max"]),
                    Difficulty = Convert.ToInt32(row["difficulty"]),
                    Duration = Convert.ToInt32(row["duration"]),
                    IsExpansion = Convert.ToInt32(row["expansion"]),
                    Comments = (string)row["comments"]

                };
                thegames.Add(Game);
            }

            return thegames.ToArray();
        }


        public static bool Exists(int idGame)
        {

            bool theresult = false;
            string query = "";
            query = "SELECT id, name, editorial, family, type, years, player_min, player_max, difficulty, expansion, duration, comments "
                + " FROM games  ";

            query = query + " WHERE id =" + idGame;

            query = query + " ORDER BY id ASC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                theresult = true;
            }

            return theresult;
        }

        public void Save()
        {

            string query = "";
            if (this.Id == 0)
            {
                //CREATE
                query = "INSERT INTO games(name, editorial, family, type, years, player_min, player_max, difficulty, expansion, duration, comments) VALUES " +
                    " (  '" + this.Name + "', '" + this.Editorial + "', " + this.Family + ", " + this.Type + ", " + this.Years + ", " +
                    " " + this.PlayerMin + ", " + this.PlayerMax + ", " + this.Difficulty + ",  " +
                    " " + this.IsExpansion + ", " + this.Duration + ", '" + this.Comments + "')";
            }
            else
            {
                //UPDATE
                query = "UPDATE games SET " +
                    " name= '" + this.Name + "', " +
                    " editorial= '" + this.Editorial + "', " +
                    " family= " + this.Family + ", " +
                    " type= " + this.Type + ", " +
                    " years= " + this.Years + ", " +
                    " player_min= " + this.PlayerMin + ", " +
                    " player_max= " + this.PlayerMax + ", " +
                    " difficulty= " + this.Difficulty + ", " +
                    " expansion= " + this.IsExpansion + ", " +
                    " duration= " + this.Duration + ", " +
                    " comments= '" + this.Comments + "' " +
                    " WHERE id = " + this.Id + ";";
            }

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Delete()
        {

            if (MessageBox.Show("¿Desea realmente eliminar el juego " + this.Name + "? Este proceso eliminará el juego y todo su log.", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                
                //Elimino el usuario
                var query1 = "DELETE FROM games WHERE id = " + this.Id + ";";
                //Elimino el usuario
                var query2 = "DELETE FROM localgames WHERE game_id = " + this.Id + ";";
                //Elimino el usuario de la tabla de actividades
                var query3 = "DELETE FROM games_lending WHERE localgame_id not in (Select id from localgames);";

                try
                {
                    using (SQLServiceClient svc = new SQLServiceClient())
                    {
                        svc.ExecuteSQL(query1);
                        svc.ExecuteSQL(query2);
                        svc.ExecuteSQL(query3);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }


        }


    }


}
