﻿using AlterParadox.GesUmbras.SQLService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.GesUmbras.Objects
{
    class Sesion
    {
        public int sesionID { get; set; }
        public string sesionStart { get; set; }//"DD/MM/YYYY HH:MM:SS"
        public string sesionEnd { get; set; }//"DD/MM/YYYY HH:MM:SS"
        public decimal sesionDineroInicial { get; set; }
        public decimal sesionDineroFinal { get; set; }
        public int sesionActiva { get; set; }//0-1
        public decimal sesionDescuadre { get; set; }

        private static string tabla = "tpv_sesiones";

        public Sesion(DataRow r)
        {
            //Console.WriteLine("new Sesion");
            sesionID = (int)r.Field<int>("sesionID");
            sesionStart = r.Field<string>("sesionStart");
            sesionEnd = r.Field<string>("sesionEnd");
            sesionDineroInicial = (decimal)r.Field<double>("sesionDineroInicial");
            sesionDineroFinal = (decimal)r.Field<double>("sesionDineroFinal");
            sesionActiva = (int)r.Field<int>("sesionActiva");
            sesionDescuadre = (decimal)r.Field<double>("sesionDescuadre");
            //Console.WriteLine(productoID+","+productoName+","+productoPrecio+","+productoIva+","+productoCantidad);
        }

        public Sesion()
        {

        }

        //Seleccionar todos
        public static List<Sesion> SELECT_ALL()
        {
            List<Sesion> sesiones = new List<Sesion>();
            string query = "SELECT * FROM " + tabla;

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                sesiones.Add(new Sesion(row));
            }

            return sesiones;
        }

        //buscar sesion activa
        public static Sesion SELECT_ACTIVE()
        {
            Sesion s = null;

            string query = "SELECT * FROM "+tabla+" WHERE sesionActiva=1";
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

            if (DS.Tables["Table"].Rows.Count != 1)
            {
                s = null;
            }
            else
            {
                s = new Sesion(DS.Tables["Table"].Rows[0]);
            }

            return s;
        }

        //insertar sesion
        public static void INSERT(Sesion s)
        {
            string sdi, sdf, sdes;
            sdi = s.sesionDineroInicial.ToString().Replace(",", ".");
            sdf = s.sesionDineroFinal.ToString().Replace(",", ".");
            sdes = s.sesionDescuadre.ToString().Replace(",", ".");

            string query = "INSERT INTO "+tabla+" (sesionStart,sesionEnd,sesionDineroInicial,sesionDineroFinal,sesionActiva,sesionDescuadre) VALUES "
            + "('" + s.sesionStart + "','" + s.sesionEnd + "','" + sdi + "','" + sdf + "'," + s.sesionActiva + ",'" + sdes + "');";
            Console.WriteLine(query);

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

        }

        //updatear el total
        public static void UPDATETOTAL(decimal f, int id)
        {

            string query = "UPDATE " + tabla + " SET sesionDineroFinal='" + f + "' WHERE sesionID= " + id + ";";
            
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }
        }

        //Actualizar Sesion
        public static void UPDATE(Sesion s)
        {
            string sdf, sdes;
            sdf = s.sesionDineroFinal.ToString().Replace(",", ".");
            sdes = s.sesionDescuadre.ToString().Replace(",", ".");
 
            string query = "UPDATE " + tabla + " SET sesionEnd='" + s.sesionEnd + "', sesionDineroFinal='" + sdf + "', sesionActiva='" + 
                s.sesionActiva + "', sesionDescuadre='" + sdes + "' WHERE sesionID=" + s.sesionID + ";";

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }

        }
    }
}
