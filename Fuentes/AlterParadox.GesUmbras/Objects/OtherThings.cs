﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization;
using AlterParadox.GesUmbras.SQLService;

namespace AlterParadox.GesUmbras
{
    class OtherThings
    {
        
        public static List<string> associationList()
        {
            List<string> theactivities = new List<string>();

            //date: AAAA-MM-DD HH:MM:SS
            string query = "";
            query = "SELECT DISTINCT gameAssociation FROM games WHERE gameAssociation!=''  ORDER BY gameAssociation ASC;";
            Console.WriteLine(query);
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                string activity = (string)row["gameAssociation"];
                theactivities.Add(activity);
            }

            return theactivities;
        }

        private static string convertirFecha(DateTime d)
        {
            string AA, MM, DD, HH, MI, SS;
            //año
            AA = d.Year.ToString();
            //mes
            if (d.Month < 10)
            {
                MM = "0" + d.Month;
            }
            else
            {
                MM = d.Month.ToString();
            }
            //dia
            if (d.Day < 10)
            {
                DD = "0" + d.Day;
            }
            else
            {
                DD = d.Day.ToString();
            }
            //Hora
            if (d.Hour < 10)
            {
                HH = "0" + d.Hour;
            }
            else
            {
                HH = d.Hour.ToString();
            }
            //minuto
            if (d.Minute < 10)
            {
                MI = "0" + d.Minute;
            }
            else
            {
                MI = d.Minute.ToString();
            }
            //segundo
            if (d.Second < 10)
            {
                SS = "0" + d.Second;
            }
            else
            {
                SS = d.Second.ToString();
            }
            return DD + "/" + MM + "/" + AA + " " + HH + ":" + MI + ":" + SS;
            //return AA + "-" + MM + "-" + DD + " " + HH + ":" + MI + ":" + SS;

        }
    }
}