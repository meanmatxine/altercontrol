﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AlterParadox.GesUmbras.SQLService;

namespace AlterParadox.GesUmbras.Objects
{
    public class Producto
    {
        public int productoID { get; set; }
        public string productoCB { get; set; }
        public string productoName { get; set; }
        public int productoTipo { get; set; }
        public decimal productoPrecioSinIva { get; set; }
        public int productoIva { get; set; }
        public decimal productoPrecio { get; set; }
        public int productoCantidad { get; set; }
        public string productoImage { get; set; }

        private static string tabla = "TPV_Productos";

        //Constructor desde DBC
        public Producto(DataRow r)
        {
            productoID = (int)r.Field<int>("productoID");
            productoCB = r.Field<string>("productoCB");
            productoName = r.Field<string>("productoName");
            productoTipo = (int)r.Field<int>("productoTipo");
            productoPrecioSinIva = (decimal)(r.Field<double>("productoPrecioSinIva"));
            productoIva = (int)r.Field<int>("productoIva");
            productoPrecio = (decimal)(r.Field<double>("productoPrecio"));
            productoCantidad = (int)r.Field<int>("productoCantidad");
            productoImage = r.Field<string>("productoImage");
            //Console.WriteLine(productoID+","+productoName+","+productoPrecio+","+productoIva+","+productoCantidad);
        }

        //Constructor Vacio
        public Producto()
        {

        }

        //Seleccionar todos //done
        public static DataView SELECT_ALL()
        {

            string query = "SELECT * FROM "+tabla;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
            DataView productos = DS.Tables[0].DefaultView;

            return productos;
        }

        //todos a datagrid //done
        public static DataTable SELECT_ALL_GRID()
        {
            string query = "SELECT * FROM " + tabla;

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            DataTable dt = DS.Tables[0];

            return dt;
        }

        //todos a lista // done
        public static List<Producto> SELECT_ALL_LIST()
        {
            List<Producto> productos = new List<Producto>();

            string query = "SELECT * FROM " + tabla;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                productos.Add(new Producto(row));
            }
            return productos;
        }

        //Seleccion por tipo //done
        public static DataView SELECT_TYPE(int tipo)
        {

            string query = "SELECT * FROM "+tabla+" WHERE productoTipo=" + tipo;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            DataView productos = DS.Tables[0].DefaultView;

            return productos;
        }

        //Select ONE // done
        public static Producto SELECT_ONE(int id)
        {
            Producto p = null;

            string query = "SELECT * FROM "+tabla+" WHERE productoID=" + id;
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count != 1)
            {
                p = null;
            }
            else
            {
                p = new Producto(DS.Tables["Table"].Rows[0]);
            }

            return p;
        }

        //SELECT BY NAME // done
        public static Producto SELECT_BY_NAME(string p)
        {
            Producto prod = null;

            string query = "SELECT * FROM " + tabla + " WHERE productoName= '"+p+"'";
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            if (DS.Tables["Table"].Rows.Count != 1)
            {
                prod = null;
            }
            else
            {
                prod = new Producto(DS.Tables["Table"].Rows[0]);
            }

            return prod;
        }

        //SELECT BY CB // done
        public static Producto SELECT_BY_CB(string cb)
        {
            Producto p = null;

            string query = "SELECT * FROM "+tabla+" WHERE productoCB LIKE '" + cb + "';";
            Console.WriteLine(query);
            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            if (DS.Tables["Table"].Rows.Count != 1)
            {
                p = null;
            }
            else
            {
                p = new Producto(DS.Tables["Table"].Rows[0]);
            }

            return p;
        }

        //ELIMINAR PRODUCTO
        public static void DELETE(string id)
        {
            //borrar
            string query = "DELETE FROM " + tabla + " WHERE productoID=" + id;

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        
        }

        //AÑADIR PRODUCTO // done
        public static void INSERT(Producto p)
        {
            //cambiar comas por puntos
            string psi = p.productoPrecioSinIva.ToString();
            psi = psi.Replace(",", ".");
            string pf = p.productoPrecio.ToString();
            pf = pf.Replace(",", ".");

            string query = "INSERT INTO " + tabla + " VALUES ('" + p.productoCB + "','" + p.productoName + "'," + p.productoTipo + "," + psi + "," + p.productoIva + "," + pf + "," + p.productoCantidad + ",'"+p.productoImage+"');";
            Console.WriteLine(query);
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //Console.WriteLine(dbc.query);
        }

        //modificar producto // done
        public static void MODIFY(Producto p)
        {
            //cambiar comas por puntos
            string psi = p.productoPrecioSinIva.ToString();
            psi = psi.Replace(",", ".");
            string pf = p.productoPrecio.ToString();
            pf = pf.Replace(",", ".");

            string query = "UPDATE " + tabla + " SET productoCB='" + p.productoCB + "', productoName='" + p.productoName + "', productoTipo=" + p.productoTipo + ", productoPrecioSinIva='" + psi +
                "', productoIva=" + p.productoIva + ", productoPrecio='" + pf + "', productoCantidad=" + p.productoCantidad + ", productoImage='"+p.productoImage+"' WHERE productoID=" + p.productoID;
            Console.WriteLine(query);
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //Console.WriteLine(dbc.query);
        }

    }
}
*/