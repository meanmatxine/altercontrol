﻿namespace AlterParadox.GesUmbras
{
    using AlterParadox.Core;
    using AlterParadox.GesUmbras.GameService;
    using AlterParadox.GesUmbras.SQLService;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class LocalGame_Filter
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public bool All { get; set; }
        public int Status { get; set; }

        public bool FilterActivate { get; set; }
        public int? Family { get; set; }
        public int? Type { get; set; }
        public int? MinPlayers { get; set; }
        public int? MaxPlayers { get; set; }
        public int? Difficulty { get; set; }
        public int? Association { get; set; }

        public LocalGame_Filter()
        {
            this.Filter = "";
            this.Id = 0;
            this.All = true;
            this.Status = 99;

            this.FilterActivate = false;
            this.Family = null;
            this.Type = null;
            this.MinPlayers = null;
            this.MaxPlayers = null;
            this.Difficulty = null;
            this.Association = null;

        }
    }

    public class LocalGame
    {
        public int Id { get; set; }
        public Game TheGame { get; set; }
        public Association Association { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }

        public override string ToString()
        {
            if(Association == null)
            return TheGame.Name;
            else
            return TheGame.Name + " (" + Association.Name + ")";
        }

        public static LocalGame[] getAll(LocalGame_Filter filters)
        {
            List<LocalGame> thelocalgames = new List<LocalGame>();

            Game[] thegames = Game.getAll(null);
            
            List<Association> listaAsociaciones = new List<Association>();
            try
            {
                using (GameServiceClient svc = new GameServiceClient())
                {
                    listaAsociaciones = svc.GetAssociations().Asociaciones.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Association[] theassociations = listaAsociaciones.ToArray();
       
            string query = "";
            query = "SELECT  localgames.id, localgames.game_id, games.name as gamename, localgames.association_id, associations.name as associationname, localgames.comments, COALESCE((Select TOP 1 status from games_lending where localgame_id = localgames.id Order by modifydate DESC ),0) as status "
                + " FROM localgames "
                + " LEFT JOIN games ON localgames.game_id = games.id "
                + " LEFT JOIN associations ON localgames.association_id = associations.id ";

            query = query + " WHERE localgames.id >=0";

            if(filters != null) {
                if (filters.Filter != "")
                {
                    query = query + " AND CONCAT(games.name  , '-' , associations.name) COLLATE Latin1_General_CI_AI like  '%" + filters.Filter + "%' COLLATE Latin1_General_CI_AI  ";
                }
                if (filters.Status != 99)
                {
                    query = query + " AND (COALESCE((Select TOP 1 status from games_lending where localgame_id = localgames.id Order by modifydate DESC ),0)) = " + filters.Status  + " ";
                }


                if (filters.FilterActivate)
                {
                    if (filters.Family != null)
                        query = query + " AND games.family = " + filters.Family + " ";
                    if (filters.Type != null)
                        query = query + " AND games.type = " + filters.Type + " ";

                    if (filters.MinPlayers != null)
                        query = query + " AND games.player_min >= " + filters.MinPlayers + " ";
                    if (filters.MaxPlayers != null)
                        query = query + " AND games.player_max <= " + filters.MaxPlayers + " ";

                    if (filters.Difficulty != null)
                        query = query + " AND games.difficulty = " + filters.Difficulty + " ";

                    if (filters.Association != null)
                        query = query + " AND associations.id = " + filters.Association + " ";
                }

            }
            query = query + " ORDER BY games.name DESC, associations.name ASC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                LocalGame thegame = new LocalGame
                {
                    Id = Convert.ToInt32(row["id"]),
                    TheGame = thegames.SingleOrDefault(item => item.Id == Convert.ToInt32(row["game_id"])),
                    Association = theassociations.SingleOrDefault(item => item.Id == Convert.ToInt32(row["association_id"])),
                    Comments = (string)row["comments"],
                    Status = Convert.ToInt32(row["status"])
                };
                thelocalgames.Add(thegame);
            }


            return thelocalgames.ToArray();
        }

        public static LocalGame getGame(LocalGame_Filter filters)
        {
            List<LocalGame> thelocalgames = new List<LocalGame>();

            Game[] thegames = Game.getAll(null);
            List<Association> listaAsociaciones = new List<Association>();
            try
            {
                using (GameServiceClient svc = new GameServiceClient())
                {
                    listaAsociaciones = svc.GetAssociations().Asociaciones.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Association[] theassociations = listaAsociaciones.ToArray();
            LocalGame thegame = null;


            string query = "";
            query = "SELECT localgames.id, localgames.game_id, games.name as gamename, localgames.association_id, associations.name as associationname, localgames.comments, COALESCE((Select TOP 1 status from games_lending where localgame_id = localgames.id Order by modifydate DESC ),0) as status "
                + " FROM localgames "
                + " LEFT JOIN games ON localgames.game_id = games.id "
                + " LEFT JOIN associations ON localgames.association_id = associations.id ";

            query = query + " WHERE localgames.id >=0";
            if (filters != null)
            {
                if (filters.Id != 0)
                {
                    query = query + " AND localgames.id = " + filters.Id + "";
                }

                if (filters.Filter != "")
                {
                    query = query + " AND CONCAT(games.name  , '-' , associations.name) COLLATE Latin1_General_CI_AI  like '%" + filters.Filter + "%' COLLATE Latin1_General_CI_AI  ";
                }
            }
            query = query + " ORDER BY games.name DESC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                thegame = new LocalGame
                {
                    Id = Convert.ToInt32(row["id"]),
                    TheGame = thegames.SingleOrDefault(item => item.Id == Convert.ToInt32(row["game_id"])),
                    Association = theassociations.SingleOrDefault(item => item.Id == Convert.ToInt32(row["association_id"])),
                    Comments = (string)row["comments"],
                    Status = Convert.ToInt32(row["status"])
                };
                thelocalgames.Add(thegame);
            }

            return thegame;
        }

        public static bool Exists(int idGame)
        {

            bool theresult = false;
            string query = "";
            query = "SELECT id, comments "
                + " FROM localgames  ";

            query = query + " WHERE id =" + idGame;

            query = query + " ORDER BY id ASC";

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                theresult = true;
            }


            return theresult;
        }

        public void Save()
        {
            string query = "";

            if (this.Id == 0)
            {
                //CREATE
                query = "INSERT INTO localgames(game_id, association_id, comments) VALUES " +
                    " ( '" + this.TheGame.Id + "', '" + this.Association.Id + "', '" + this.Comments + "')";

            }
            else
            {
                //UPDATE
                query = "UPDATE localgames SET " +
                    " game_id= '" + this.TheGame.Id + "', " +
                    " association_id= '" + this.Association.Id + "', " +
                    " comments= '" + this.Comments + "' " +
                    " WHERE id = " + this.Id + ";";
            }

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        public void Delete()
        {

            if (MessageBox.Show("¿Desea realmente eliminar el juego " + this.TheGame.Name + " de la asociación " + this.Association + "? Este proceso eliminará el juego y todo su log.", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {

                //Elimino el usuario
                string query = "DELETE FROM localgames WHERE id = " + this.Id + ";";
                //Elimino el usuario de la tabla de actividades
                string query1 = "DELETE FROM games_lending WHERE localgame_id = " + this.Id + ";";

                try
                {
                    using (SQLServiceClient svc = new SQLServiceClient())
                    {
                        svc.ExecuteSQL(query);
                        svc.ExecuteSQL(query1);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }


        }

    }
}
