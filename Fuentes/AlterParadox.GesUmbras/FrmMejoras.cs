﻿using AlterParadox.Core.Models;
using AlterParadox.GesUmbras.SQLService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmMejoras : Form
    {
        public FrmMejoras()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Nuevo();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {

            guardar();
        }
        public void Nuevo()
        {
            Usuario.Text = string.Empty;
            Mejora.Text = string.Empty;
            loadMejoras();
            Usuario.Focus();
        }
        public void guardar()
        {
            string query = "";
            query = string.Format("INSERT INTO mejoras(mejora, usuario) VALUES ('{1}','{0}');", Usuario.Text.Replace("'", ""), Mejora.Text.Replace("'", ""));

            List<Mejora> listaMejoras = new List<Mejora>();

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            Nuevo();
        }
        public void loadMejoras()
        {
            GridActivities.AutoGenerateColumns = false;
            string query = "";
            query = "SELECT  * FROM mejoras";
            query = query + " ORDER BY id DESC";

            List<Mejora> listaMejoras = new List<Mejora>();

            DataSet DS = null;
            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    DS = svc.Query(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (DataRow row in DS.Tables["Table"].Rows)
            {
                Mejora theUpgrade = new Mejora
                {
                    Id = Convert.ToInt32(row["id"]),
                    Usuario = Convert.ToString(row["usuario"]),
                    MejoraText = Convert.ToString(row["mejora"]),

                };
                listaMejoras.Add(theUpgrade);
            }

            GridActivities.DataSource = listaMejoras;

        }

        private void FrmMejoras_Load(object sender, EventArgs e)
        {
            Nuevo();
        }
    }
}
