﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
namespace AlterParadox.GesUmbras
{
    using AlterParadox.Core;
    using Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FrmGames_NewLocalGame : Form
    {
        public FrmGames_NewLocalGame()
        {
            InitializeComponent();
        }

        //LOCAL FUNCTIONS AND VARS

        LocalGame _localGame;
       
        private void L_Refresh_LocalGames()
        {
            LocalGame_Filter filters = new LocalGame_Filter();
            filters.Filter = txtFilter.Text;

            GridLocalGames.AutoGenerateColumns = false;
            GridLocalGames.DataSource = LocalGame.getAll(filters);
        }
        void L_Refresh_History()
        {
            if (_localGame.Id != 0)
            {
                GameLending_Filter filters = new GameLending_Filter();
                filters.LocalGame_Id = _localGame.Id;

                GridHistory.AutoGenerateColumns = false;
                GridHistory.DataSource = GameLending.getHistory(filters);
            }
            else
            {
                GridHistory.DataSource = null;
            }
        }
        private void L_New_LocalGame()
        {
            _localGame = new LocalGame();
            txtId.Text = "0";
            txtGameId.Text = "";
            txtGameName.Text = "";
            txtAssociationId.Text = "";
            txtAssociationName.Text = "";
            txtGameComments.Text = "";
            txtAssociationName.Focus();
        }
        private void L_Load_LocalGame()
        {
            txtId.Text = "" + _localGame.Id;
            txtGameId.Text = "" + _localGame.TheGame.Id;
            txtGameName.Text = "" + _localGame.TheGame.Name;
            txtAssociationId.Text = "" + _localGame.Association.Id;
            txtAssociationName.Text = "" + _localGame.Association.Name;
            txtGameComments.Text = "" + _localGame.Comments;

            L_Refresh_History();

            txtAssociationName.Focus();
        }

        private void L_Save_LocalGame()
        {
            //Save
            if ("" + txtGameId.Text == "")
            {
                MessageBox.Show("¡Debe seleccionar un juego!");
                L_Search_Game();
                return;
          }
            if("" + txtAssociationId.Text =="")
            {
                MessageBox.Show("¡Debe seleccionar una asociación!");
                L_Search_Association();
                return;
            }

            _localGame = new LocalGame();
            _localGame.Id = Convert.ToInt32("" + txtId.Text);
            _localGame.TheGame = new Game();
            _localGame.TheGame.Id = Convert.ToInt32("" + txtGameId.Text);
            _localGame.TheGame.Name = "" + txtGameName.Text;
            _localGame.Association = new Association();
            _localGame.Association.Id = Convert.ToInt32("" + txtAssociationId.Text);
            _localGame.Association.Name = "" + txtAssociationName.Text;
            _localGame.Comments = "" + txtGameComments.Text;

            _localGame.Save();
            L_Refresh_LocalGames();
            L_New_LocalGame();
        }
        private void L_Delete_LocalGame()
        {
            if (_localGame.Id != 0)
            {
                _localGame.Delete();
            }
            L_Refresh_LocalGames();
            L_New_LocalGame();
        }
        
        private void L_Search_Game()
        {
            G._searchGame = null;
            FrmGames_SearchGame formulario = new FrmGames_SearchGame();
            G.AbrirPantalla(formulario, true, false);
            if (G._searchGame != null)
            {
                this.txtGameId.Text = "" + G._searchGame.Id;
                this.txtGameName.Text = "" + G._searchGame.Name;
                this.txtGameComments.Focus();
            }
        }
        private void L_Search_Association()
        {
            G._searchAssociation = null;
            FrmAssociation_Search formulario = new FrmAssociation_Search();
            G.AbrirPantalla(formulario, true, false);
            if (G._searchAssociation != null)
            {
                this.txtAssociationId.Text = "" + G._searchAssociation.Id;
                this.txtAssociationName.Text = "" + G._searchAssociation.Name;
                this.txtGameComments.Focus();
            }
        }

        //FORMULARIO
        private void FrmGames_NewGame_Load(object sender, EventArgs e)
        {
            this.mnuDelete.Visible = G.admin;
            this.txtId.Visible = G.admin;
            this.Show();
            L_Refresh_LocalGames();

            L_New_LocalGame();
        }
        
        private void mnuNew_Click(object sender, EventArgs e)
        {
            L_New_LocalGame();
        }

        private void mnuSave_Click(object sender, EventArgs e)
        {
            L_Save_LocalGame();
        }

        private void mnuDelete_Click(object sender, EventArgs e)
        {
            L_Delete_LocalGame();
        }

        private void btnLocalGameSave_Click(object sender, EventArgs e)
        {
            L_Save_LocalGame();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAddGame_Click(object sender, EventArgs e)
        {
            FrmGames_NewGame formulario = new FrmGames_NewGame();
            G.AbrirPantalla(formulario, true, false);
        }

        private void btnSearchGame_Click(object sender, EventArgs e)
        {
            L_Search_Game();
        }

        private void btnAddAssociation_Click(object sender, EventArgs e)
        {
            FrmAssociation formulario = new FrmAssociation();
            G.AbrirPantalla(formulario, true, false);
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_Refresh_LocalGames();
        }

        private void btnFilterClean_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
        }

        private void btnFilterRefresh_Click(object sender, EventArgs e)
        {
            L_Refresh_LocalGames();
        }

        private void FrmGames_NewLocalGame_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                L_Search_Game();
            }
            if (e.KeyCode == Keys.F4)
            {
                L_Search_Association();
            }
        }

        private void btnSearchAssociation_Click(object sender, EventArgs e)
        {
            L_Search_Association();
        }

        private void GridLocalGames_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                _localGame = (LocalGame)GridLocalGames.Rows[e.RowIndex].DataBoundItem;
                L_Load_LocalGame();
            }
        }

        private void GridLocalGames_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
