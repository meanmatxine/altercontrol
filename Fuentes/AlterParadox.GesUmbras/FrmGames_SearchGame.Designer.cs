﻿namespace AlterParadox.GesUmbras
{
    partial class FrmGames_SearchGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRefreshGrid = new System.Windows.Forms.Button();
            this.btnCleanFilter = new System.Windows.Forms.Button();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.GridGames = new System.Windows.Forms.DataGridView();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGames)).BeginInit();
            this.SuspendLayout();
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AccessibleName = "Selección de nuevo elemento";
            this.miniToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ButtonDropDown;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.miniToolStrip.Location = new System.Drawing.Point(165, 10);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.miniToolStrip.Size = new System.Drawing.Size(374, 39);
            this.miniToolStrip.TabIndex = 23;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnRefreshGrid);
            this.panel4.Controls.Add(this.btnCleanFilter);
            this.panel4.Controls.Add(this.txtFilter);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(868, 33);
            this.panel4.TabIndex = 0;
            // 
            // btnRefreshGrid
            // 
            this.btnRefreshGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshGrid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshGrid.Location = new System.Drawing.Point(839, 3);
            this.btnRefreshGrid.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnRefreshGrid.Name = "btnRefreshGrid";
            this.btnRefreshGrid.Size = new System.Drawing.Size(27, 27);
            this.btnRefreshGrid.TabIndex = 2;
            this.btnRefreshGrid.TabStop = false;
            this.btnRefreshGrid.Text = "R";
            this.btnRefreshGrid.UseVisualStyleBackColor = true;
            this.btnRefreshGrid.Click += new System.EventHandler(this.btnRefreshGrid_Click);
            // 
            // btnCleanFilter
            // 
            this.btnCleanFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCleanFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCleanFilter.Location = new System.Drawing.Point(813, 3);
            this.btnCleanFilter.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnCleanFilter.Name = "btnCleanFilter";
            this.btnCleanFilter.Size = new System.Drawing.Size(27, 27);
            this.btnCleanFilter.TabIndex = 1;
            this.btnCleanFilter.TabStop = false;
            this.btnCleanFilter.Text = "X";
            this.btnCleanFilter.UseVisualStyleBackColor = true;
            this.btnCleanFilter.Click += new System.EventHandler(this.btnCleanFilter_Click);
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(3, 3);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(811, 27);
            this.txtFilter.TabIndex = 0;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            this.txtFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFilter_KeyDown);
            this.txtFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFilter_KeyPress);
            // 
            // GridGames
            // 
            this.GridGames.AllowUserToAddRows = false;
            this.GridGames.AllowUserToDeleteRows = false;
            this.GridGames.AllowUserToResizeColumns = false;
            this.GridGames.AllowUserToResizeRows = false;
            this.GridGames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridGames.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridGames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridGames.ColumnHeadersHeight = 30;
            this.GridGames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridGames.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_name});
            this.GridGames.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridGames.Location = new System.Drawing.Point(0, 36);
            this.GridGames.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridGames.MultiSelect = false;
            this.GridGames.Name = "GridGames";
            this.GridGames.ReadOnly = true;
            this.GridGames.RowHeadersVisible = false;
            this.GridGames.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridGames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridGames.Size = new System.Drawing.Size(868, 533);
            this.GridGames.TabIndex = 1;
            this.GridGames.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridGames_CellDoubleClick);
            this.GridGames.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridGames_KeyDown);
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "Id";
            this.col_id.FillWeight = 11.40375F;
            this.col_id.HeaderText = "Id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            // 
            // col_name
            // 
            this.col_name.DataPropertyName = "Name";
            this.col_name.FillWeight = 91.22999F;
            this.col_name.HeaderText = "Nombre";
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            // 
            // FrmGames_SearchGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 570);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.GridGames);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(884, 609);
            this.Name = "FrmGames_SearchGame";
            this.Text = "Pantalla de juegos";
            this.Load += new System.EventHandler(this.FrmGames_NewGame_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGames)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnRefreshGrid;
        private System.Windows.Forms.Button btnCleanFilter;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.DataGridView GridGames;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_name;
    }
}