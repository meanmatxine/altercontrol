﻿using AlterParadox.Core;
using AlterParadox.Core.Models;
using AlterParadox.GesUmbras.UserService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{

    public partial class FrmUsers : Form
    {

        //LOCAL FUNCTIONS AND VARS
        User user;
        User webuser;

        List<User> usuarios;
        List<User> webUsuarios;

        BindingList<PersonaAlCargo> personasAlCargo;

        private void L_Refresh_PersonasAlCargo()
        {
            PersonasAlCargo.AutoGenerateColumns = false;
            PersonasAlCargo.DataSource = personasAlCargo;
        }

        private void L_Refresh_Users()
        {

            var filtro = txtFilter.Text;
            GridUsers.AutoGenerateColumns = false;
            var listaUsuarios = new List<User>();
            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    listaUsuarios = svc.GetUsers().ListaUsuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            GridUsers.DataSource = listaUsuarios
                .Where(x =>
                    G.Contains(string.Format("{0}-{1}-{2}", x.Name, x.LastName, x.Number), filtro)
                ).OrderByDescending(x=> x.Number).ToList();
        }

        private void L_New()
        {
            user = new User();
            txtId.Text = string.Empty;
            txtNumber.Text = string.Empty;
            txtDni.Text = string.Empty;
            txtName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtAsociacion.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtBirthday.CleanControl();
            checkBirthday();
            txtComments.Text = string.Empty;
            txtAviso.Text = string.Empty;
            txtCP.Text = "31001";

            if(webuser != null) 
            {
                chkSleep.Checked = webuser.Sleep;
                chkSleepList.Checked = webuser.DormirListaEspera;
            }
            else
            {
                chkSleep.Checked = false;
                chkSleepList.Checked = false;
            }
            updateSleepStatus();

            ChkLunch.Checked = false;
            ChkFathersPermission.Checked = false;

            txtSecondLunch.Text = string.Empty;

            chkWantShirt.Checked = false;
            chkBringActivity.Checked = false;
            txtRealShirt.Text = "NO";

            personasAlCargo = new BindingList<PersonaAlCargo>();
            L_Refresh_PersonasAlCargo();

            txtDni.Focus();
        }
        private void L_Load()
        {
            txtId.Text = user.Id.ToString();
            txtDni.Text = user.Dni;
            txtName.Text = user.Name;
            txtLastName.Text = user.LastName;
            txtNumber.Text = user.Number.ToString();
            txtEmail.Text = user.Email;
            txtAsociacion.Text = user.Association;
            txtPhone.Text = user.Phone;
            txtBirthday.SetFecha(user.Birthday);
            checkBirthday();
            txtComments.Text = user.Comments;
            txtAviso.Text = user.Aviso;
            txtCP.Text = user.CP;

            chkSleep.Checked = user.Sleep;
            chkSleepList.Checked = user.DormirListaEspera;

            ChkLunch.Checked = user.Lunch;
            ChkFathersPermission.Checked = user.TutorPermission;

            txtSecondLunch.Text = user.SegundoLunch;

            chkWantShirt.Checked = user.Camiseta;
            chkBringActivity.Checked = user.TraeActividades;

            txtRealShirt.Text = user.Shirt;

            personasAlCargo = new BindingList<PersonaAlCargo>();
            var listaBackend = JsonConvert.DeserializeObject<List<PersonaAlCargo>>(user.PersonasAlCargo);
            listaBackend.ToList().ForEach(x => personasAlCargo.Add(x));
            L_Refresh_PersonasAlCargo();

            txtDni.Focus();
        }
        private void updateSleepStatus()
        {
            chkSleepLabel.Text = chkSleep.Checked ? (chkSleepList.Checked ? "Apuntado**" : "Apuntado") : (chkSleepList.Checked ? "En lista" : "No");
            chkSleepLabel.BackColor = chkSleep.Checked ? Color.Transparent : Color.PaleVioletRed;
        }
        private void L_Load_Webuser()
        {
            //txtId.Text = user.Id;
            txtDni.Text = webuser.Dni;
            txtName.Text = webuser.Name;
            txtLastName.Text = webuser.LastName;
            txtNumber.Text = webuser.Number.ToString();
            txtEmail.Text = webuser.Email;
            txtAsociacion.Text = webuser.Association;
            txtPhone.Text = webuser.Phone;            
            txtBirthday.SetFecha(webuser.Birthday);
            checkBirthday();
            txtComments.Text = webuser.Comments;
            txtAviso.Text = string.Empty;
            txtCP.Text = webuser.CP;

            chkSleep.Checked = webuser.Sleep;
            chkSleepList.Checked = webuser.DormirListaEspera;

            ChkLunch.Checked = webuser.Lunch;
            ChkFathersPermission.Checked = webuser.TutorPermission;
            txtRealShirt.Text = webuser.Shirt;

            txtSecondLunch.Text = webuser.SegundoLunch;

            chkWantShirt.Checked = webuser.Camiseta;
            chkBringActivity.Checked = webuser.TraeActividades;

            cmdSave.Focus();
        }
        private void L_Save()
        {
            if (!txtBirthday.GetFecha().HasValue)
            {
                MessageBox.Show("La fecha de nacimiento es obligatoria!", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtBirthday.Focus();
                return;
            }
            int years = G.GetAge(txtBirthday.GetFecha());
            if (years < 18 && ChkFathersPermission.Checked == false)
                MessageBox.Show("Recuerda que los menores de edad deben traer el permiso paterno.","Advertencia",MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            //Save
            user = new User();

            if (!string.IsNullOrEmpty(txtId.Text))
                user.Id = Convert.ToInt32(txtId.Text);

            bool nuevo = user.Id == null;
            if(!string.IsNullOrEmpty(txtNumber.Text))
                user.Number = Convert.ToInt32(txtNumber.Text);

            user.Dni =              txtDni.Text;
            user.Name =             txtName.Text;
            user.LastName =         txtLastName.Text;
            user.Email =            txtEmail.Text;
            user.Association =      txtAsociacion.Text;
            user.Phone =            txtPhone.Text;
            user.Birthday =         txtBirthday.GetFechaForzada();
            user.Comments =         txtComments.Text;
            user.Aviso =            txtAviso.Text;
            user.CP =               txtCP.Text;
 
            user.Sleep =            chkSleep.Checked;
            user.DormirListaEspera = chkSleepList.Checked;
            user.Lunch =            ChkLunch.Checked;
            user.TutorPermission =  ChkFathersPermission.Checked ;
            user.Shirt =            txtRealShirt.Text;
            user.SegundoLunch = txtSecondLunch.Text;
            
            //Proceso de guardar. Voy a almacenar el usuario nuevo y luego todos los posibles al cargo
            var listaResumen = new List<UserResumen>();
            var respuesta = GuardarUsuario(user);
            if(respuesta != null)
            {
                user.Id = respuesta.Id;

                listaResumen.Add(respuesta);
            }

            foreach (var humano in personasAlCargo)
            {
                var userHumano = new User();
                if (humano.Id.HasValue)
                    userHumano.Id = humano.Id.Value;

                if (humano.Number.HasValue)
                    userHumano.Number = humano.Number.Value;

                userHumano.Dni = user.Dni;
                userHumano.Name = humano.Name;
                userHumano.LastName = humano.LastName;
                userHumano.Email = user.Email;
                userHumano.Phone = user.Phone;
                userHumano.Birthday = humano.Birthday;
                userHumano.Comments = string.Empty;
                userHumano.Aviso = user.Aviso;//El mismo aviso que tenga el padre lo tendrá el hijo
                userHumano.CP = user.CP;
                userHumano.Association = string.Empty;
                userHumano.Sleep = user.Sleep;
                userHumano.DormirListaEspera = user.DormirListaEspera;
                userHumano.Lunch = false;
                userHumano.TutorPermission = true;
                userHumano.Shirt = string.Empty;
                userHumano.SegundoLunch = string.Empty;
                userHumano.UserOnCharge = user.Id;

                var respuestaHumano = GuardarUsuario(userHumano);
                if (respuestaHumano != null)
                {
                    respuestaHumano.HumanoDependiente = true;
                    listaResumen.Add(respuestaHumano);
                }
            }

            FrmFichaInscrito form = new FrmFichaInscrito();
            form.Inscritos = listaResumen;//listaResumen
            form.Nuevo = nuevo;
            G.AbrirPantalla(form, true, false);

            L_Refresh_Users();
            L_New();
        }

        private UserResumen GuardarUsuario(User user)
        {
            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    Console.WriteLine("Entra");
                    //SAVEUSER DEBERIA DEVOLVER O EL ID DEL USUARIO NUEVO O LOS DATOS DEL USUARIO
                    var usuarioRespuesta = svc.SaveUser(new SaveUserRequest() { Usuario = user });
                    Console.WriteLine("Repuesta: " + usuarioRespuesta);
                    if (usuarioRespuesta.EsCorrecto)
                    {
                        var theuser = usuarioRespuesta.Usuario;
                        return theuser;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        private void L_Delete()
        {
            if(user.Id != null && user.Id != 0)
            {
                try
                {
                    using (UserServiceClient svc = new UserServiceClient())
                    {
                        svc.DeleteUser(new DeleteUserRequest() { IdUser = (int)user.Id });

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }
            L_Refresh_Users();
            L_New();
        }

        private void L_Search()
        {

        }

        #region Métodos formulario
        
        public FrmUsers()
        {
            InitializeComponent();
            
            PersonasAlCargo.AutoGenerateColumns = false;
            PersonasAlCargo.DataSource = personasAlCargo;

            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    usuarios = svc.GetUsers().ListaUsuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                using (UserServiceClient svc = new UserServiceClient())
                {
                    var respuesta = svc.GetWebUsers();
                    if(respuesta.EsCorrecto)
                        webUsuarios = respuesta.ListaUsuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void FrmUsers_Load(object sender, EventArgs e)
        {
            this.mnuDelete.Visible = G.admin;
            this.txtId.Visible = G.admin;
            this.Show();
            L_Refresh_Users();
            L_New();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            L_Save();
        }

        private void mnuNew_Click(object sender, EventArgs e)
        {
            L_New();
        }

        private void mnuSave_Click(object sender, EventArgs e)
        {
            L_Save();
        }

        private void mnuSearch_Click(object sender, EventArgs e)
        {
            L_Search();

        }

        private void mnuDelete_Click(object sender, EventArgs e)
        {
            L_Delete();
        }

        private void GridUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                try
                {
                    var usuario = ((User)GridUsers.Rows[e.RowIndex].DataBoundItem);
                    if(usuario != null)
                    {
                        var identificador = usuario.UserOnCharge.HasValue ? usuario.UserOnCharge.Value : usuario.Id.Value;
                        using (UserServiceClient svc = new UserServiceClient())
                        {
                            var response = svc.GetUser(new GetUserRequest()
                            {
                                IdUser = identificador
                            });

                            if (response.EsCorrecto)
                                user = response.Usuario;
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    user = null;
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    if (user != null)
                        L_Load();
                }
            }
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_Refresh_Users();
        }

        private void txtDni_Validated(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtId.Text))
            {
                txtDni.Text = G.getDcNIF(txtDni.Text);
                if (webUsuarios == null || !webUsuarios.Any())
                    return;
                if(webUsuarios.Where(x=>x.Dni == txtDni.Text).Any())
                {
                    webuser = webUsuarios.FirstOrDefault(x => x.Dni == txtDni.Text);
                    L_Load_Webuser();
                }
            }
        }

        private void PanelUserData_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmdCleanFilter_Click(object sender, EventArgs e)
        {
            txtFilter.Text = string.Empty;
            txtFilter.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            L_Refresh_Users();
        }
        #endregion

        private void TxtDni_TextChanged(object sender, EventArgs e)
        {

        }

        private void chkSleep_CheckedChanged(object sender, EventArgs e)
        {
            updateSleepStatus();
        }

        private void chkSleepLabel_Click(object sender, EventArgs e)
        {

        }

        private void chkSleepList_CheckedChanged(object sender, EventArgs e)
        {
            updateSleepStatus();
        }

        private void GridUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtSecondLunch_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void addSerHumano_Click(object sender, EventArgs e)
        {
            gestionarPersonaAlCargo(null);
        }

        private void txtYears_TextChanged(object sender, EventArgs e)
        {

        }

        private void PersonasAlCargo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                try
                {
                    var persona = (PersonaAlCargo)PersonasAlCargo.Rows[e.RowIndex].DataBoundItem;
                    gestionarPersonaAlCargo(persona);
                }
                catch (Exception)
                {

                    throw;
                }
               
            }
        }
        private void gestionarPersonaAlCargo(PersonaAlCargo persona)
        {
            var formulario = new FrmPersona();
            formulario.persona = persona;
            G.AbrirPantalla(formulario, true, false,
                s =>
                {
                    if (s == DialogResult.OK)
                    {
                        if (formulario.eliminar != null)
                        {
                            if (formulario.eliminar.Id != null && formulario.eliminar.Id != 0)
                            {
                                try
                                {
                                    using (UserServiceClient svc = new UserServiceClient())
                                    {
                                        svc.DeleteUser(new DeleteUserRequest() { IdUser = (int)formulario.eliminar.Id });

                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.ToString());
                                }
                            }
                            personasAlCargo.Remove(formulario.eliminar);
                            L_Refresh_PersonasAlCargo();
                        }else if(formulario.persona != null)
                        {
                            if (personasAlCargo == null)
                                personasAlCargo = new BindingList<PersonaAlCargo>();
                            if (persona != null)
                                personasAlCargo.Remove(persona);
                            personasAlCargo.Add(formulario.persona);
                            L_Refresh_PersonasAlCargo();
                        }
                    }
                });
        }

        private void txtBirthday_Validated(object sender, EventArgs e)
        {
            checkBirthday();
        }
        private void checkBirthday()
        {
            if (txtBirthday.GetFecha().HasValue)
            {
                int years = G.GetAge(txtBirthday.GetFechaForzada());
                txtYears.Text = $"{years} años.{((years < 18) ? " menor." : "")}";
            }
            else
                txtYears.Text = String.Empty;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Esto no hace nada aún!");
        }

        private void txtComments_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
                G.ShowTextDialog(txtComments.Text);
        }
    }
}
