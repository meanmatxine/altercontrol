﻿namespace AlterParadox.GesUmbras
{
    using Properties;
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Windows.Forms;
 
    public partial class FrmGames_NewGame : Form
    {
        public FrmGames_NewGame()
        {
            InitializeComponent();
        }

        //LOCAL FUNCTIONS AND VARS
        Game _game;
        
        private void L_Refresh_Games()
        {
            Game_Filter filters = new Game_Filter();
            filters.Filter = txtFilter.Text;

            GridGames.AutoGenerateColumns = false;
            GridGames.DataSource = Game.getAll(filters);
        }

        private void L_New_Game()
        {
            _game = new Game();
            txtId.Text = "0";
            txtName.Text = "";
            txtEditorial.Text = "";

            txtFamily.SelectedIndex = 0;
            txtType.SelectedIndex = 0;

            txtYears.Value = 0;

            txtPlayerMin.Value = 0;
            txtPlayerMax.Value = 0;
            txtDuration.Value = 0;
            txtDifficulty.SelectedIndex = 0;
            ChkIsExpansion.Checked = false;
            txtGameComments.Text = "";

            txtName.Focus();
        }
        private void L_Load_Game()
        {
            txtId.Text = "" + _game.Id;
            txtName.Text = "" + _game.Name;
            txtEditorial.Text = "" + _game.Editorial;

            txtFamily.SelectedIndex = _game.Family;
            txtType.SelectedIndex = _game.Type;

            txtYears.Value = _game.Years;
            
            txtPlayerMin.Value = +_game.PlayerMin;
            txtPlayerMax.Value = +_game.PlayerMax;
            txtDuration.Value = +_game.Duration;
            txtDifficulty.SelectedIndex = +_game.Difficulty;
            ChkIsExpansion.Checked = _game.IsExpansion == 0 ? false : true;

            txtGameComments.Text = "" + _game.Comments;

            txtName.Focus();
        }

        private void L_Save_Game()
        {
            //Save
            _game = new Game();
            _game.Id = Convert.ToInt32("" + txtId.Text);
            _game.Name = "" + txtName.Text;
            _game.Editorial = "" + txtEditorial.Text;

            _game.Family =  txtFamily.SelectedIndex;
            _game.Type = txtType.SelectedIndex;

            _game.Years = Convert.ToInt32(txtYears.Value);

            _game.PlayerMin = Convert.ToInt32(txtPlayerMin.Value);
            _game.PlayerMax = Convert.ToInt32(txtPlayerMax.Value);
            _game.Duration = Convert.ToInt32(txtDuration.Value);
            _game.Difficulty = Convert.ToInt32(txtDifficulty.SelectedIndex);
            _game.IsExpansion = ChkIsExpansion.Checked == true ? 1 : 0;
            
            _game.Comments = "" + txtGameComments.Text;

            _game.Save();

            
            L_Refresh_Games();
            L_New_Game();
            //Load Game And Focus on Association 
        }
        private void L_Delete_Game()
        {
            if (_game.Id != 0)
            {
                _game.Delete();
            }
            L_Refresh_Games();
            L_New_Game();
        }


        //FORM EVENTS
        private void FrmGames_NewGame_Load(object sender, EventArgs e)
        {
            this.toolStripButton8.Visible = G.admin;
            this.txtId.Visible = G.admin;
            this.Show();
            L_Refresh_Games();
            L_New_Game();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            L_New_Game();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            L_Save_Game();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            L_Delete_Game();
        }

        private void btnGameSave_Click(object sender, EventArgs e)
        {
            L_Save_Game();
        }

        private void GridGames_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                _game = (Game)GridGames.Rows[e.RowIndex].DataBoundItem;
                L_Load_Game();
            }
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_Refresh_Games();
        }

        private void btnCleanFilter_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
        }

        private void btnRefreshGrid_Click(object sender, EventArgs e)
        {
            L_Refresh_Games();
        }

        private void GridGames_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtFamily_Enter(object sender, EventArgs e)
        {
            txtFamily.DroppedDown = true;
        }

        private void txtType_Enter(object sender, EventArgs e)
        {
            txtType.DroppedDown = true;
        }

        private void txtDifficulty_Enter(object sender, EventArgs e)
        {
            txtDifficulty.DroppedDown = true;
        }
    }
}
