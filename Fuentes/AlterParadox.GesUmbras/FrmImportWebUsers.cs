﻿
namespace AlterParadox.GesUmbras
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.IO;
    using AlterParadox.GesUmbras.UserService;
    using AlterParadox.Core;
    using System.Text.RegularExpressions;
    using AlterParadox.GesUmbras.SQLService;

    public partial class FrmImportWebUsers : Form
    {
        public FrmImportWebUsers()
        {
            InitializeComponent();
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "CSV Document|*.csv";

            DialogResult result = dlg.ShowDialog();


            if (result == DialogResult.OK) // Test result.
            {
                txtPathFile.Text = "" + dlg.FileName;
            }
            Console.WriteLine(result); // <-- For debugging use.
        }

        private const int campoId = 00;
        private const int campoFechaEnvio = 01;
        private const int campoNombre = 02;
        private const int campoApellidos = 03;
        private const int campoAsociacion          = 04;
        private const int campoEmail = 05;
        private const int campoTelefono            = 06;
        private const int campoPais                = 07;
        private const int campoCP = 08;
        private const int campoDNI = 09;
        private const int campoFechaNacimiento = 10;
        private const int campoDormir = 11;
        private const int campoActividades = 12;
        private const int campoDormirListaDeEspera = 13;
        private const int campoCamiseta = 14;
        private const int campoTallaCamiseta = 15;
        private const int campoComida = 16;
        private const int campoSegundoComida = 17;
        private const int campoObservaciones = 18;

        private void cmdImport_Click(object sender, EventArgs e)
        {

            var query = " DELETE FROM web_users";

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                    svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            string filename = txtPathFile.Text;

            var CSV = File.ReadAllText(filename).Split('\n'); ;

            List<User> users = new List<User>();
            var firstLinea = true;
            foreach (var row in CSV)
            {
                int iCampo = 0;
                DateTime fechaCumple = DateTime.Now;
                User user = new User();
                int idGroup = 2;
                Regex regexObj = new Regex(@"(""([^""]*)""|[^,]*)(,|$)");
                Match matchResults = regexObj.Match(row);

                while (matchResults.Success)
                {
                    if (!firstLinea)
                    {

                        if (iCampo == campoNombre)
                            user.Name = matchResults.Groups[idGroup].Value;
                        if (iCampo == campoApellidos)
                            user.LastName = matchResults.Groups[idGroup].Value;
                        if (iCampo == campoAsociacion)
                            user.Association = matchResults.Groups[idGroup].Value;
                        if (iCampo == campoEmail)
                            user.Email = matchResults.Groups[idGroup].Value;
                        if (iCampo == campoTelefono)
                            user.Phone = matchResults.Groups[idGroup].Value;
                        //if (iCampo == campoPa�s)
                        //    user.campoCP = matchResults.Groups[idGroup].Value;
                        if (iCampo == campoCP)
                            user.CP = matchResults.Groups[idGroup].Value;
                        if (iCampo == campoDNI)
                            user.Dni = matchResults.Groups[idGroup].Value;

                        if (iCampo == campoFechaNacimiento)
                        {
                            //18/05/1996
                            DateTime fechaOut = DateTime.Now;
                            var fecha = DateTime.TryParse(matchResults.Groups[idGroup].Value, out fechaOut);
                            user.Birthday = fechaOut;
                        }
                        if (iCampo == campoDormir)
                            user.Sleep = matchResults.Groups[idGroup].Value.ToString().Equals("Marcado") ? true : false;

                        if (iCampo == campoActividades)
                            user.TraeActividades = matchResults.Groups[idGroup].Value.ToString().Equals("Marcado") ? true : false;

                        if (iCampo == campoDormirListaDeEspera)
                            user.DormirListaEspera = matchResults.Groups[idGroup].Value.ToString().Equals("Marcado") ? true : false;

                        if (iCampo == campoCamiseta)
                            user.Camiseta = matchResults.Groups[idGroup].Value.ToString().Equals("Marcado") ? true : false;

                        if (iCampo == campoTallaCamiseta)
                            user.Shirt = matchResults.Groups[idGroup].Value;

                        if (iCampo == campoComida)
                            user.Lunch = matchResults.Groups[idGroup].Value.ToString().Equals("Marcado") ? true : false;

                        if (iCampo == campoSegundoComida)
                            user.SegundoLunch = matchResults.Groups[idGroup].Value;

                        if (iCampo == campoObservaciones)
                            user.Comments = matchResults.Groups[idGroup].Value;

                    }
                    iCampo = iCampo + 1;
                    matchResults = matchResults.NextMatch();
                }
                //actividad.save();
                users.Add(user);
                firstLinea = false;
            }

            //var contents = File.ReadAllText(filename).Split('\n');
            //var csv = from line in contents
            //          select line.Split(';').ToArray();

            //var count = csv.Count();

            //progress.Value = 0;
            //progress.Maximum = (int)count*2;

            //List<User> users = new List<User>();

            //foreach (string[] row in csv)
            //{

            //    //2017
            //    //"#","Fecha Presentado","Nombre","Apellidos","Asociación","Email","Teléfono","País","CP","DNI","Fecha de nacimiento","Observaciones y/o necesidades especiales","¡¡Me quedo a dormir!!","tshirt","Apúntame a la comida!!!"
            //    //"1";"07/03/2017";"Fermin";"Larraya Huarte";"";"fermin.larraya@gmail.com";"626689343";"España ";"31012";"44627054Q";"16/11/1982";"Quiero a Turri vestido de Mujer";"unchecked";"XL";"checked"
            //    //END2017

            //    //2018
            //    // 0        1               2       3           4           5       6           7   8       9   10                      11                      12                      13                                              14                          15                                      16              
            //    //"#","Fecha de envío","Nombre","Apellidos","Asociación","Email","Teléfono","Paí­s","CP","DNI","Fecha de nacimiento","¡¡Me quedo a dormir!!","Quiero Camiseta","Talla de la Camiseta (Mujer/Hombre - S/M/L/XL/XXL)","Apúntame a la comida!!!","Observaciones y/o necesidades especiales","Acepto las condiciones de uso"
            //    //"1","05/31/2018","Fermin","Larraya","Alter Paradox","fermin.larraya@gmail.com","Pamplona","España ","310112","44627054Q","16/11/1982","Desmarcado","Desmarcado","","Marcado","Prueba","Desmarcado"
            //    //"260","08/04/2018","Víctor","Ochoa López","","Victor8al@hotmail.com","","España ","31110","78749508S","20/08/1984","Desmarcado","Desmarcado","","Desmarcado","","Marcado"
            //    //END2018
            //    if (row[0].Replace("\"", "") != "#" && row[0].Replace("\"", "") != "")
            //    {
            //        User user = new User
            //        {
            //            Id = Convert.ToInt32((string)row[0].Replace("\"", "")),
            //            Name = (string)row[2].Replace("\"", ""),
            //            LastName = (string)row[3].Replace("\"", ""),
            //            Email = (string)row[5].Replace("\"", ""),
            //            Phone = (string)row[6].Replace("\"", ""),
            //            CP = (string)row[8].Replace("\"", ""),
            //            Dni = G.getDcNIF((string)row[9].Replace("\"", "")).ToUpper() ,
            //            Birthday = row[10].Replace("\"", "") != "" ? Convert.ToDateTime(row[10].Replace("\"", "")) : Convert.ToDateTime("01/01/1980"),
            //            Comments = (string)row[15].Replace("\"", ""),
            //            Association = (string)row[4].Replace("\"", ""),
            //            Shirt = (string)row[12].Replace("\"", "") == "Marcado" ? "SI - " + (string)row[13].Replace("\"", "") : "NO - " + (string)row[13].Replace("\"", ""),
            //            Sleep = (string)row[11].Replace("\"", "") == "Marcado" ? true : false,
            //            Lunch = (string)row[14].Replace("\"", "") == "Marcado" ? true : false,
            //        };
            //        users.Add(user);
            //    }

            //    if (progress.Value< progress.Maximum) {
            //        progress.Value = progress.Value + 1;
            //        this.Refresh();
            //        this.progress.Refresh();
            //        Application.DoEvents();
            //    }
            //}
            foreach (User user in users)
            {

                try
                {
                    using (UserServiceClient svc = new UserServiceClient())
                    {
                        svc.SaveWebUser( new SaveWebUserRequest() { Usuario = user });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                if (progress.Value < progress.Maximum)
                {
                    progress.Value = progress.Value + 1;
                    this.Refresh();
                    this.progress.Refresh();
                    Application.DoEvents();
                }
            }
            MessageBox.Show("Importación finalizada. " + users.Count + " usuarios importados.");
        }
    }
}
