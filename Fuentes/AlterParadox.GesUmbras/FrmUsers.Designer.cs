﻿namespace AlterParadox.GesUmbras
{
    partial class FrmUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsers));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.cmdCleanFilter = new System.Windows.Forms.Button();
            this.GridUsers = new System.Windows.Forms.DataGridView();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_dni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_lastnames = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.PanelUserData = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAsociacion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAviso = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtYears = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkSleepList = new System.Windows.Forms.CheckBox();
            this.chkWantShirt = new System.Windows.Forms.CheckBox();
            this.chkSleep = new System.Windows.Forms.CheckBox();
            this.addSerHumano = new System.Windows.Forms.Button();
            this.PersonasAlCargo = new System.Windows.Forms.DataGridView();
            this.numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chkSleepLabel = new System.Windows.Forms.Label();
            this.chkBringActivity = new System.Windows.Forms.CheckBox();
            this.txtSecondLunch = new System.Windows.Forms.TextBox();
            this.ChkFathersPermission = new System.Windows.Forms.CheckBox();
            this.ChkLunch = new System.Windows.Forms.CheckBox();
            this.txtRealShirt = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.lblCP = new System.Windows.Forms.Label();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.lblComments = new System.Windows.Forms.Label();
            this.lblBirthday = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.lblNumber = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cmdSave = new System.Windows.Forms.Button();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.lblDocument = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.mnuNew = new System.Windows.Forms.ToolStripButton();
            this.mnuSave = new System.Windows.Forms.ToolStripButton();
            this.mnuSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBirthday = new AlterParadox.Core.Controls.Fecha();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridUsers)).BeginInit();
            this.PanelUserData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonasAlCargo)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.cmdCleanFilter);
            this.panel1.Controls.Add(this.GridUsers);
            this.panel1.Controls.Add(this.txtFilter);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(445, 658);
            this.panel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(424, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(21, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "R";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmdCleanFilter
            // 
            this.cmdCleanFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCleanFilter.Location = new System.Drawing.Point(404, 0);
            this.cmdCleanFilter.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdCleanFilter.Name = "cmdCleanFilter";
            this.cmdCleanFilter.Size = new System.Drawing.Size(21, 23);
            this.cmdCleanFilter.TabIndex = 9;
            this.cmdCleanFilter.Text = "X";
            this.cmdCleanFilter.UseVisualStyleBackColor = true;
            this.cmdCleanFilter.Click += new System.EventHandler(this.cmdCleanFilter_Click);
            // 
            // GridUsers
            // 
            this.GridUsers.AllowUserToAddRows = false;
            this.GridUsers.AllowUserToDeleteRows = false;
            this.GridUsers.AllowUserToResizeColumns = false;
            this.GridUsers.AllowUserToResizeRows = false;
            this.GridUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridUsers.ColumnHeadersHeight = 30;
            this.GridUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_number,
            this.col_dni,
            this.col_name,
            this.col_lastnames});
            this.GridUsers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridUsers.Location = new System.Drawing.Point(0, 22);
            this.GridUsers.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridUsers.MultiSelect = false;
            this.GridUsers.Name = "GridUsers";
            this.GridUsers.ReadOnly = true;
            this.GridUsers.RowHeadersVisible = false;
            this.GridUsers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridUsers.RowTemplate.Height = 30;
            this.GridUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridUsers.Size = new System.Drawing.Size(445, 633);
            this.GridUsers.TabIndex = 1;
            this.GridUsers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridUsers_CellContentClick);
            this.GridUsers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridUsers_CellDoubleClick);
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.FillWeight = 5F;
            this.col_id.HeaderText = "Id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            this.col_id.Visible = false;
            // 
            // col_number
            // 
            this.col_number.DataPropertyName = "number";
            this.col_number.FillWeight = 10F;
            this.col_number.HeaderText = "Nº";
            this.col_number.Name = "col_number";
            this.col_number.ReadOnly = true;
            // 
            // col_dni
            // 
            this.col_dni.DataPropertyName = "dni";
            this.col_dni.FillWeight = 20F;
            this.col_dni.HeaderText = "Dni";
            this.col_dni.Name = "col_dni";
            this.col_dni.ReadOnly = true;
            // 
            // col_name
            // 
            this.col_name.DataPropertyName = "name";
            this.col_name.FillWeight = 40F;
            this.col_name.HeaderText = "Nombre";
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            // 
            // col_lastnames
            // 
            this.col_lastnames.DataPropertyName = "lastname";
            this.col_lastnames.FillWeight = 40F;
            this.col_lastnames.HeaderText = "Apellidos";
            this.col_lastnames.Name = "col_lastnames";
            this.col_lastnames.ReadOnly = true;
            // 
            // txtFilter
            // 
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(0, 0);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(405, 23);
            this.txtFilter.TabIndex = 0;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // PanelUserData
            // 
            this.PanelUserData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelUserData.Controls.Add(this.chkBringActivity);
            this.PanelUserData.Controls.Add(this.ChkLunch);
            this.PanelUserData.Controls.Add(this.label8);
            this.PanelUserData.Controls.Add(this.label7);
            this.PanelUserData.Controls.Add(this.txtAsociacion);
            this.PanelUserData.Controls.Add(this.txtBirthday);
            this.PanelUserData.Controls.Add(this.label6);
            this.PanelUserData.Controls.Add(this.txtAviso);
            this.PanelUserData.Controls.Add(this.button2);
            this.PanelUserData.Controls.Add(this.txtYears);
            this.PanelUserData.Controls.Add(this.label1);
            this.PanelUserData.Controls.Add(this.chkSleepList);
            this.PanelUserData.Controls.Add(this.chkWantShirt);
            this.PanelUserData.Controls.Add(this.chkSleep);
            this.PanelUserData.Controls.Add(this.addSerHumano);
            this.PanelUserData.Controls.Add(this.PersonasAlCargo);
            this.PanelUserData.Controls.Add(this.label4);
            this.PanelUserData.Controls.Add(this.label3);
            this.PanelUserData.Controls.Add(this.label2);
            this.PanelUserData.Controls.Add(this.chkSleepLabel);
            this.PanelUserData.Controls.Add(this.txtSecondLunch);
            this.PanelUserData.Controls.Add(this.ChkFathersPermission);
            this.PanelUserData.Controls.Add(this.txtRealShirt);
            this.PanelUserData.Controls.Add(this.txtPhone);
            this.PanelUserData.Controls.Add(this.lblPhone);
            this.PanelUserData.Controls.Add(this.txtEmail);
            this.PanelUserData.Controls.Add(this.txtCP);
            this.PanelUserData.Controls.Add(this.lblCP);
            this.PanelUserData.Controls.Add(this.txtComments);
            this.PanelUserData.Controls.Add(this.lblComments);
            this.PanelUserData.Controls.Add(this.lblBirthday);
            this.PanelUserData.Controls.Add(this.txtNumber);
            this.PanelUserData.Controls.Add(this.lblNumber);
            this.PanelUserData.Controls.Add(this.txtId);
            this.PanelUserData.Controls.Add(this.cmdSave);
            this.PanelUserData.Controls.Add(this.txtLastName);
            this.PanelUserData.Controls.Add(this.lblLastName);
            this.PanelUserData.Controls.Add(this.txtName);
            this.PanelUserData.Controls.Add(this.lblName);
            this.PanelUserData.Controls.Add(this.txtDni);
            this.PanelUserData.Controls.Add(this.lblDocument);
            this.PanelUserData.Controls.Add(this.toolStrip1);
            this.PanelUserData.Controls.Add(this.label5);
            this.PanelUserData.Location = new System.Drawing.Point(450, 0);
            this.PanelUserData.Name = "PanelUserData";
            this.PanelUserData.Size = new System.Drawing.Size(572, 658);
            this.PanelUserData.TabIndex = 0;
            this.PanelUserData.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelUserData_Paint);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(3, 252);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 23);
            this.label8.TabIndex = 46;
            this.label8.Text = "Asociación";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(3, 226);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 23);
            this.label7.TabIndex = 45;
            this.label7.Text = "Email";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAsociacion
            // 
            this.txtAsociacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAsociacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAsociacion.Location = new System.Drawing.Point(125, 252);
            this.txtAsociacion.MaxLength = 100;
            this.txtAsociacion.Name = "txtAsociacion";
            this.txtAsociacion.Size = new System.Drawing.Size(443, 23);
            this.txtAsociacion.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Yellow;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(3, 577);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 23);
            this.label6.TabIndex = 42;
            this.label6.Text = "Aviso!!!";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAviso
            // 
            this.txtAviso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAviso.Location = new System.Drawing.Point(125, 577);
            this.txtAviso.MaxLength = 100;
            this.txtAviso.Name = "txtAviso";
            this.txtAviso.Size = new System.Drawing.Size(445, 23);
            this.txtAviso.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Image = global::AlterParadox.GesUmbras.Properties.Resources.printer;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(439, 446);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 40);
            this.button2.TabIndex = 40;
            this.button2.Text = "Permiso tutor";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // txtYears
            // 
            this.txtYears.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYears.Location = new System.Drawing.Point(239, 147);
            this.txtYears.Name = "txtYears";
            this.txtYears.ReadOnly = true;
            this.txtYears.Size = new System.Drawing.Size(178, 23);
            this.txtYears.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(3, 279);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 23);
            this.label1.TabIndex = 37;
            this.label1.Text = "Actividades";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkSleepList
            // 
            this.chkSleepList.Enabled = false;
            this.chkSleepList.Location = new System.Drawing.Point(177, 358);
            this.chkSleepList.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkSleepList.Name = "chkSleepList";
            this.chkSleepList.Size = new System.Drawing.Size(44, 19);
            this.chkSleepList.TabIndex = 13;
            this.chkSleepList.Text = "En lista de espera";
            this.chkSleepList.UseVisualStyleBackColor = true;
            this.chkSleepList.Visible = false;
            this.chkSleepList.CheckedChanged += new System.EventHandler(this.chkSleepList_CheckedChanged);
            // 
            // chkWantShirt
            // 
            this.chkWantShirt.Enabled = false;
            this.chkWantShirt.Location = new System.Drawing.Point(129, 309);
            this.chkWantShirt.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkWantShirt.Name = "chkWantShirt";
            this.chkWantShirt.Size = new System.Drawing.Size(92, 19);
            this.chkWantShirt.TabIndex = 10;
            this.chkWantShirt.Text = "Reservada";
            this.chkWantShirt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkWantShirt.UseVisualStyleBackColor = true;
            this.chkWantShirt.Visible = false;
            // 
            // chkSleep
            // 
            this.chkSleep.Enabled = false;
            this.chkSleep.Location = new System.Drawing.Point(129, 358);
            this.chkSleep.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkSleep.Name = "chkSleep";
            this.chkSleep.Size = new System.Drawing.Size(44, 19);
            this.chkSleep.TabIndex = 12;
            this.chkSleep.Text = "Me quedo a dormir";
            this.chkSleep.UseVisualStyleBackColor = true;
            this.chkSleep.Visible = false;
            this.chkSleep.CheckedChanged += new System.EventHandler(this.chkSleep_CheckedChanged);
            // 
            // addSerHumano
            // 
            this.addSerHumano.Image = global::AlterParadox.GesUmbras.Properties.Resources.add;
            this.addSerHumano.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.addSerHumano.Location = new System.Drawing.Point(228, 446);
            this.addSerHumano.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.addSerHumano.Name = "addSerHumano";
            this.addSerHumano.Size = new System.Drawing.Size(125, 40);
            this.addSerHumano.TabIndex = 12;
            this.addSerHumano.Text = "Ser humano";
            this.addSerHumano.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addSerHumano.UseVisualStyleBackColor = true;
            this.addSerHumano.Click += new System.EventHandler(this.addSerHumano_Click);
            // 
            // PersonasAlCargo
            // 
            this.PersonasAlCargo.AllowUserToAddRows = false;
            this.PersonasAlCargo.AllowUserToDeleteRows = false;
            this.PersonasAlCargo.AllowUserToResizeColumns = false;
            this.PersonasAlCargo.AllowUserToResizeRows = false;
            this.PersonasAlCargo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonasAlCargo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PersonasAlCargo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.PersonasAlCargo.ColumnHeadersHeight = 30;
            this.PersonasAlCargo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.PersonasAlCargo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numero,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.Edad});
            this.PersonasAlCargo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.PersonasAlCargo.Location = new System.Drawing.Point(228, 301);
            this.PersonasAlCargo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PersonasAlCargo.MultiSelect = false;
            this.PersonasAlCargo.Name = "PersonasAlCargo";
            this.PersonasAlCargo.ReadOnly = true;
            this.PersonasAlCargo.RowHeadersVisible = false;
            this.PersonasAlCargo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.PersonasAlCargo.RowTemplate.Height = 30;
            this.PersonasAlCargo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PersonasAlCargo.Size = new System.Drawing.Size(340, 141);
            this.PersonasAlCargo.TabIndex = 34;
            this.PersonasAlCargo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PersonasAlCargo_CellDoubleClick);
            // 
            // numero
            // 
            this.numero.DataPropertyName = "number";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numero.DefaultCellStyle = dataGridViewCellStyle3;
            this.numero.FillWeight = 30F;
            this.numero.HeaderText = "Nº";
            this.numero.Name = "numero";
            this.numero.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn4.FillWeight = 60F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "lastname";
            this.dataGridViewTextBoxColumn5.FillWeight = 60F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Apellidos";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // Edad
            // 
            this.Edad.DataPropertyName = "Edad";
            this.Edad.FillWeight = 40F;
            this.Edad.HeaderText = "Edad";
            this.Edad.Name = "Edad";
            this.Edad.ReadOnly = true;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(3, 405);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 23);
            this.label4.TabIndex = 33;
            this.label4.Text = "Comida";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(3, 356);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(222, 23);
            this.label3.TabIndex = 32;
            this.label3.Text = "Dormir WEB";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(3, 307);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 23);
            this.label2.TabIndex = 31;
            this.label2.Text = "Camiseta WEB";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkSleepLabel
            // 
            this.chkSleepLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.chkSleepLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkSleepLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSleepLabel.Location = new System.Drawing.Point(3, 378);
            this.chkSleepLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.chkSleepLabel.Name = "chkSleepLabel";
            this.chkSleepLabel.Size = new System.Drawing.Size(222, 23);
            this.chkSleepLabel.TabIndex = 29;
            this.chkSleepLabel.Text = "Apuntado";
            this.chkSleepLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkSleepLabel.Click += new System.EventHandler(this.chkSleepLabel_Click);
            // 
            // chkBringActivity
            // 
            this.chkBringActivity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.chkBringActivity.Enabled = false;
            this.chkBringActivity.Location = new System.Drawing.Point(9, 281);
            this.chkBringActivity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.chkBringActivity.Name = "chkBringActivity";
            this.chkBringActivity.Size = new System.Drawing.Size(20, 19);
            this.chkBringActivity.TabIndex = 28;
            this.chkBringActivity.Text = "Trae actividades";
            this.chkBringActivity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkBringActivity.UseVisualStyleBackColor = false;
            // 
            // txtSecondLunch
            // 
            this.txtSecondLunch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSecondLunch.Location = new System.Drawing.Point(3, 427);
            this.txtSecondLunch.MaxLength = 100;
            this.txtSecondLunch.Name = "txtSecondLunch";
            this.txtSecondLunch.Size = new System.Drawing.Size(221, 23);
            this.txtSecondLunch.TabIndex = 10;
            this.txtSecondLunch.TextChanged += new System.EventHandler(this.txtSecondLunch_TextChanged);
            // 
            // ChkFathersPermission
            // 
            this.ChkFathersPermission.AutoSize = true;
            this.ChkFathersPermission.Location = new System.Drawing.Point(422, 149);
            this.ChkFathersPermission.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ChkFathersPermission.Name = "ChkFathersPermission";
            this.ChkFathersPermission.Size = new System.Drawing.Size(120, 20);
            this.ChkFathersPermission.TabIndex = 26;
            this.ChkFathersPermission.Text = "Permiso Paterno";
            this.ChkFathersPermission.UseVisualStyleBackColor = true;
            // 
            // ChkLunch
            // 
            this.ChkLunch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ChkLunch.Location = new System.Drawing.Point(9, 408);
            this.ChkLunch.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ChkLunch.Name = "ChkLunch";
            this.ChkLunch.Size = new System.Drawing.Size(21, 19);
            this.ChkLunch.TabIndex = 9;
            this.ChkLunch.Text = "Apuntado";
            this.ChkLunch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ChkLunch.UseVisualStyleBackColor = false;
            // 
            // txtRealShirt
            // 
            this.txtRealShirt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRealShirt.Location = new System.Drawing.Point(3, 329);
            this.txtRealShirt.MaxLength = 100;
            this.txtRealShirt.Name = "txtRealShirt";
            this.txtRealShirt.ReadOnly = true;
            this.txtRealShirt.Size = new System.Drawing.Size(222, 23);
            this.txtRealShirt.TabIndex = 9;
            // 
            // txtPhone
            // 
            this.txtPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone.Location = new System.Drawing.Point(125, 199);
            this.txtPhone.MaxLength = 100;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(443, 23);
            this.txtPhone.TabIndex = 6;
            // 
            // lblPhone
            // 
            this.lblPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPhone.Location = new System.Drawing.Point(3, 199);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(123, 23);
            this.lblPhone.TabIndex = 20;
            this.lblPhone.Text = "Teléfono";
            this.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Location = new System.Drawing.Point(125, 226);
            this.txtEmail.MaxLength = 100;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(443, 23);
            this.txtEmail.TabIndex = 7;
            // 
            // txtCP
            // 
            this.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCP.Location = new System.Drawing.Point(125, 173);
            this.txtCP.MaxLength = 10;
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(443, 23);
            this.txtCP.TabIndex = 4;
            // 
            // lblCP
            // 
            this.lblCP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblCP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCP.Location = new System.Drawing.Point(3, 173);
            this.lblCP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(123, 23);
            this.lblCP.TabIndex = 16;
            this.lblCP.Text = "CP";
            this.lblCP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComments.Location = new System.Drawing.Point(3, 511);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(567, 63);
            this.txtComments.TabIndex = 11;
            this.txtComments.Text = "\r\n";
            this.txtComments.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtComments_KeyDown);
            // 
            // lblComments
            // 
            this.lblComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblComments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComments.Location = new System.Drawing.Point(3, 489);
            this.lblComments.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(567, 23);
            this.lblComments.TabIndex = 14;
            this.lblComments.Text = "Observaciones";
            this.lblComments.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBirthday
            // 
            this.lblBirthday.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblBirthday.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBirthday.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirthday.Location = new System.Drawing.Point(3, 147);
            this.lblBirthday.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(123, 23);
            this.lblBirthday.TabIndex = 13;
            this.lblBirthday.Text = "F. Nacimiento";
            this.lblBirthday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumber
            // 
            this.txtNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNumber.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumber.Location = new System.Drawing.Point(125, 37);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.ReadOnly = true;
            this.txtNumber.Size = new System.Drawing.Size(143, 27);
            this.txtNumber.TabIndex = 0;
            // 
            // lblNumber
            // 
            this.lblNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumber.Location = new System.Drawing.Point(3, 37);
            this.lblNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(123, 27);
            this.lblNumber.TabIndex = 11;
            this.lblNumber.Text = "Nº Inscrito";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(404, 0);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(36, 23);
            this.txtId.TabIndex = 10;
            // 
            // cmdSave
            // 
            this.cmdSave.Image = global::AlterParadox.GesUmbras.Properties.Resources.disk;
            this.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSave.Location = new System.Drawing.Point(2, 606);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(97, 40);
            this.cmdSave.TabIndex = 5;
            this.cmdSave.Text = "Guardar";
            this.cmdSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // txtLastName
            // 
            this.txtLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLastName.Location = new System.Drawing.Point(125, 120);
            this.txtLastName.MaxLength = 150;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(443, 23);
            this.txtLastName.TabIndex = 2;
            // 
            // lblLastName
            // 
            this.lblLastName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLastName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastName.Location = new System.Drawing.Point(3, 120);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(123, 23);
            this.lblLastName.TabIndex = 7;
            this.lblLastName.Text = "Apellidos";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(125, 94);
            this.txtName.MaxLength = 100;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(443, 23);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(3, 94);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(123, 23);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Nombre";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDni
            // 
            this.txtDni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDni.Location = new System.Drawing.Point(125, 68);
            this.txtDni.MaxLength = 20;
            this.txtDni.Name = "txtDni";
            this.txtDni.Size = new System.Drawing.Size(143, 23);
            this.txtDni.TabIndex = 0;
            this.txtDni.TextChanged += new System.EventHandler(this.TxtDni_TextChanged);
            this.txtDni.Validated += new System.EventHandler(this.txtDni_Validated);
            // 
            // lblDocument
            // 
            this.lblDocument.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblDocument.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDocument.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocument.Location = new System.Drawing.Point(3, 68);
            this.lblDocument.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDocument.Name = "lblDocument";
            this.lblDocument.Size = new System.Drawing.Size(123, 23);
            this.lblDocument.TabIndex = 2;
            this.lblDocument.Text = "DNI";
            this.lblDocument.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNew,
            this.mnuSave,
            this.mnuSearch,
            this.toolStripSeparator1,
            this.mnuDelete,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(572, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // mnuNew
            // 
            this.mnuNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuNew.Image = ((System.Drawing.Image)(resources.GetObject("mnuNew.Image")));
            this.mnuNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Size = new System.Drawing.Size(36, 36);
            this.mnuNew.Text = "Nuevo";
            this.mnuNew.ToolTipText = "Nuevo (Ctrl+N)";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuSave.Image = ((System.Drawing.Image)(resources.GetObject("mnuSave.Image")));
            this.mnuSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Size = new System.Drawing.Size(36, 36);
            this.mnuSave.Text = "toolStripButton1";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSearch
            // 
            this.mnuSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuSearch.Image = ((System.Drawing.Image)(resources.GetObject("mnuSearch.Image")));
            this.mnuSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Size = new System.Drawing.Size(36, 36);
            this.mnuSearch.Text = "toolStripButton2";
            this.mnuSearch.Visible = false;
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // mnuDelete
            // 
            this.mnuDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuDelete.Image")));
            this.mnuDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(36, 36);
            this.mnuDelete.Text = "toolStripButton3";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(228, 279);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(340, 23);
            this.label5.TabIndex = 36;
            this.label5.Text = "Personas al cargo";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBirthday
            // 
            this.txtBirthday.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBirthday.Location = new System.Drawing.Point(125, 147);
            this.txtBirthday.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.txtBirthday.Name = "txtBirthday";
            this.txtBirthday.Size = new System.Drawing.Size(115, 23);
            this.txtBirthday.TabIndex = 3;
            this.txtBirthday.Validated += new System.EventHandler(this.txtBirthday_Validated);
            // 
            // FrmUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 658);
            this.Controls.Add(this.PanelUserData);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimumSize = new System.Drawing.Size(714, 415);
            this.Name = "FrmUsers";
            this.Text = "Participantes";
            this.Load += new System.EventHandler(this.FrmUsers_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridUsers)).EndInit();
            this.PanelUserData.ResumeLayout(false);
            this.PanelUserData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonasAlCargo)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PanelUserData;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton mnuNew;
        private System.Windows.Forms.ToolStripButton mnuSave;
        private System.Windows.Forms.ToolStripButton mnuSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton mnuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.Label lblDocument;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.DataGridView GridUsers;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblBirthday;
        private System.Windows.Forms.TextBox txtComments;
        private System.Windows.Forms.Label lblComments;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button cmdCleanFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_number;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_dni;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lastnames;
        private System.Windows.Forms.TextBox txtRealShirt;
        private System.Windows.Forms.CheckBox ChkLunch;
        private System.Windows.Forms.CheckBox ChkFathersPermission;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtSecondLunch;
        private System.Windows.Forms.CheckBox chkWantShirt;
        private System.Windows.Forms.CheckBox chkBringActivity;
        private System.Windows.Forms.Label chkSleepLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkSleepList;
        private System.Windows.Forms.CheckBox chkSleep;
        private System.Windows.Forms.Button addSerHumano;
        private System.Windows.Forms.DataGridView PersonasAlCargo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtYears;
        private System.Windows.Forms.DataGridViewTextBoxColumn numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Edad;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAviso;
        private Core.Controls.Fecha txtBirthday;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAsociacion;
    }
}