﻿namespace AlterParadox.GesUmbras
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.IO;
   // using Newtonsoft.Json;
    using Properties;
    using System.Threading;
    using System.Net;
    using AlterParadox.GesUmbras.Objects;
    using System.Diagnostics;
    using Newtonsoft.Json;
    using AlterParadox.GesUmbras.UserService;
    using AlterParadox.GesUmbras.ActivityService;

    public partial class MDI : Form
    {
        int minutes = 0;
        int seconds = 0;

        public MDI()
        {
            InitializeComponent();
            G.MdiForm = this;
        }

        private void MDI_Load(object sender, EventArgs e)
        {
            if (G.CheckForInternetConnection())
            {
                PBThread.Visible = true;
                this.Refresh();
                timer1.Enabled = true;
            }

            this.Show();

            G.admin = false;

            //if (Settings.Default.SQLITE_PATH == "")
            //{
            //    FrmConnector formulario = new FrmConnector();
            //    G.AbrirPantalla(formulario, true, false);
            //}
            //if (Settings.Default.SQLITE_PATH == "")
            //{
            //    MessageBox.Show("No se pudo conectar a la DB.");
            //    this.Close();
            //    Application.Exit();
            //    return;
            //}
            menuStrip.Visible = G.admin ;

            
        }

        private void MnuParticipantes_Click(object sender, EventArgs e)
        {
            FrmUsers formulario = new FrmUsers();
            G.AbrirPantalla(formulario, false, false);
        }

        private void mnuActivities_Click(object sender, EventArgs e)
        {
            FrmActivities formulario = new FrmActivities();
            G.AbrirPantalla(formulario, false, false);
        }

        private void JuegosButton_Click(object sender, EventArgs e)
        {
            
        }

        private void mnuExportJson_Click(object sender, EventArgs e)
        {
            l_uploadgames();
            l_uploadactivities();
        }
        private void MnuImportActivities_Click(object sender, EventArgs e)
        {
            FrmImportWebActivities formulario = new FrmImportWebActivities();
            G.AbrirPantalla(formulario, false, false);
        }

        private void MnuActivitiesList_Click(object sender, EventArgs e)
        {
            FrmActWindow formulario = new FrmActWindow();
            G.AbrirPantalla(formulario, false, true);
        }

        private void MnuImportWebUsers_Click(object sender, EventArgs e)
        {
            FrmImportWebUsers formulario = new FrmImportWebUsers();
            G.AbrirPantalla(formulario, false, false);
        }

        private void MnuDBConfig_Click(object sender, EventArgs e)
        {
           
        }

        private void mnuAddGames_Click(object sender, EventArgs e)
        {
            
        }

        private void MnuTpvList_Click(object sender, EventArgs e)
        {
            Main formTpvMain = new Main();
            //sesion
            if (!ControlCaja.tengoDinero())
            {
                Console.WriteLine("sin dinero");
                //No hay sesion
                IniciarSesion ise = new IniciarSesion();
                ise.ShowDialog();
                if (ControlCaja.tengoDinero())
                    G.AbrirPantalla(formTpvMain, false, false);
            }
            else
            {
                Console.WriteLine("con dinero");
                G.AbrirPantalla(formTpvMain, false, false);
            }
            
        }

        private void mnuImportGames_Click(object sender, EventArgs e)
        {
            FrmImportGames formulario = new FrmImportGames();
            G.AbrirPantalla(formulario, false, false);
        }

        private void btnAdministrator_Click(object sender, EventArgs e)
        {
            string password = "";

            if (G.ShowInputDialog(ref password, true) == DialogResult.OK)
            {
                switch (password)
                {
                    case "1234":
                        G.admin = true;
                        break;
                    default:
                        MessageBox.Show("Password incorrecto");
                        return;
                }
            }
            else
            {
                return;
            }
            menuStrip.Visible = G.admin;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            seconds = seconds + 10;
            if (seconds > 59)
            {
                minutes = minutes + 1;
                seconds = 0;
            }
            if (minutes >= 10)
            {
                minutes = 0;

                try {

                    if (Worker.IsBusy != true)
                    {
                        
                        Worker.RunWorkerAsync();
                    }
                }
                catch(Exception E)
                {
                    Console.Write(E.Message);
                }
            }
        }

        private void MDI_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (Worker.WorkerSupportsCancellation == true)
            {
                // Cancel the asynchronous operation.
                Worker.CancelAsync();
            }
        }
        bool onWork = true;
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            if (onWork)
            {
                l_uploadgames();

                l_uploadactivities();
            }
            
        }

        private void l_uploadgames()
        {
            try
            {
                string ftpurl = "ftp://singermorning.com/";
                string filename = "games.json";

                //JSON
                //var obj = Activity_JSON.getAll();
                //var obj = Activity_JSON.getAll();
                //var obj = GameLending_JSON.getAll();
                UserServiceClient svc = new UserServiceClient();
                var listaJuegos = svc.GetLocalGames();

                string json = JsonConvert.SerializeObject(listaJuegos, Formatting.Indented);
                File.WriteAllText(filename, json, Encoding.UTF8);
                //FTP
                FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(ftpurl + "" + filename);
                ftpClient.Credentials = new System.Net.NetworkCredential("umbras", "Umbras%Paradox2019");
                ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                ftpClient.UseBinary = true;
                ftpClient.KeepAlive = true;
                System.IO.FileInfo fi = new System.IO.FileInfo(filename);
                ftpClient.ContentLength = fi.Length;
                byte[] buffer = new byte[4097];
                int bytes = 0;
                int total_bytes = (int)fi.Length;
                int total_bytes_original = (int)fi.Length;
                System.IO.FileStream fs = fi.OpenRead();
                System.IO.Stream rs = ftpClient.GetRequestStream();
                while (total_bytes > 0)
                {
                    bytes = fs.Read(buffer, 0, buffer.Length);
                    rs.Write(buffer, 0, bytes);
                    total_bytes = total_bytes - bytes;

                    //if (worker.CancellationPending)
                    //{
                    //    // Set the e.Cancel flag so that the WorkerCompleted event
                    //    // knows that the process was cancelled.
                    //    e.Cancel = true;
                    //    worker.ReportProgress(0);
                    //    return;
                    //}

                    //worker.ReportProgress(((total_bytes_original - total_bytes) * 100) / total_bytes_original);
                    Thread.Sleep(100);
                }
                //fs.Flush();
                fs.Close();
                rs.Close();
                FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
                string value = uploadResponse.StatusDescription;
                uploadResponse.Close();
            }
            catch (Exception)
            {
                onWork = false;
                //throw;
            }
            
        }

        private void l_uploadactivities()
        {
            try
            {
                string ftpurl = "ftp://singermorning.com/";
                string filename = "activities.json";

                //JSON
                //var obj = Activity_JSON.getAll();
                //var obj = Activity_JSON.getAll();
                ActivityServiceClient svc = new ActivityServiceClient();
                var listaActividades = svc.GetActivities();

                string json = JsonConvert.SerializeObject(listaActividades, Formatting.Indented);
                File.WriteAllText(filename, json, Encoding.UTF8);

                //FTP
                FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(ftpurl + "" + filename);
                ftpClient.Credentials = new System.Net.NetworkCredential("umbras", "Umbras%Paradox2019");
                ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                ftpClient.UseBinary = true;
                ftpClient.KeepAlive = true;
                System.IO.FileInfo fi = new System.IO.FileInfo(filename);
                ftpClient.ContentLength = fi.Length;
                byte[] buffer = new byte[4097];
                int bytes = 0;
                int total_bytes = (int)fi.Length;
                int total_bytes_original = (int)fi.Length;
                System.IO.FileStream fs = fi.OpenRead();
                System.IO.Stream rs = ftpClient.GetRequestStream();
                while (total_bytes > 0)
                {
                    bytes = fs.Read(buffer, 0, buffer.Length);
                    rs.Write(buffer, 0, bytes);
                    total_bytes = total_bytes - bytes;
                    Thread.Sleep(100);
                }
                //fs.Flush();
                fs.Close();
                rs.Close();
                FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
                string value = uploadResponse.StatusDescription;
                uploadResponse.Close();
            }
            catch (Exception)
            {
                onWork = false;
                //throw;
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PBThread.Value = e.ProgressPercentage;
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            PBThread.Value = 0;
            //PBThread.Visible = false;
        }

        private void PBThread_DoubleClick(object sender, EventArgs e)
        {
            minutes = 10;
        }

        private void OpenFOlderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(System.IO.Directory.GetCurrentDirectory().ToString()); 
        }

        private void PBThread_Click(object sender, EventArgs e)
        {
            minutes = minutes + 1;
        }

        private void JuegosGlobalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGames_NewGame formulario = new FrmGames_NewGame();
            G.AbrirPantalla(formulario, false, false);
        }

        private void JuegosLocalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGames_NewLocalGame formulario = new FrmGames_NewLocalGame();
            G.AbrirPantalla(formulario, false, false);
        }

        private void AsociacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            FrmAssociation formulario = new FrmAssociation();
            G.AbrirPantalla(formulario, false, false);
        }

        private void PrestamoDeJuegosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGames_Loan formulario = new FrmGames_Loan();
            G.AbrirPantalla(formulario, false, false);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FrmMejoras formulario = new FrmMejoras();
            G.AbrirPantalla(formulario, false, false);
        }
    }
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }
}
