﻿namespace AlterParadox.GesUmbras
{
    partial class FrmGameFilters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDifficulty = new System.Windows.Forms.ComboBox();
            this.txtType = new System.Windows.Forms.ComboBox();
            this.txtFamily = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearchAssociation = new System.Windows.Forms.Button();
            this.txtAssociationId = new System.Windows.Forms.TextBox();
            this.lblAssociation = new System.Windows.Forms.Label();
            this.txtAssociationName = new System.Windows.Forms.TextBox();
            this.cmdApplyFilters = new System.Windows.Forms.Button();
            this.txtPlayerMax = new AlterParadox.GesUmbras.NumericUpDownPro();
            this.txtPlayerMin = new AlterParadox.GesUmbras.NumericUpDownPro();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMin)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDifficulty
            // 
            this.txtDifficulty.AutoCompleteCustomSource.AddRange(new string[] {
            "Desconocida",
            "Baja",
            "Media",
            "Alta"});
            this.txtDifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtDifficulty.FormattingEnabled = true;
            this.txtDifficulty.Items.AddRange(new object[] {
            "Desconocida",
            "Baja",
            "Media",
            "Alta"});
            this.txtDifficulty.Location = new System.Drawing.Point(123, 136);
            this.txtDifficulty.Name = "txtDifficulty";
            this.txtDifficulty.Size = new System.Drawing.Size(278, 27);
            this.txtDifficulty.TabIndex = 4;
            // 
            // txtType
            // 
            this.txtType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtType.FormattingEnabled = true;
            this.txtType.Items.AddRange(new object[] {
            "Familiar",
            "Infantil",
            "Party",
            "Estrategia",
            "Abstracto",
            "Temático",
            "Recursos",
            "Wargame",
            "Otros"});
            this.txtType.Location = new System.Drawing.Point(123, 43);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(278, 27);
            this.txtType.TabIndex = 1;
            // 
            // txtFamily
            // 
            this.txtFamily.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFamily.FormattingEnabled = true;
            this.txtFamily.Items.AddRange(new object[] {
            "Cartas",
            "Fichas",
            "Tablero",
            "Miniaturas",
            "Dibujo",
            "Dados",
            "Preguntas y respuestas",
            "Otros"});
            this.txtFamily.Location = new System.Drawing.Point(123, 12);
            this.txtFamily.Name = "txtFamily";
            this.txtFamily.Size = new System.Drawing.Size(278, 27);
            this.txtFamily.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(4, 12);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 27);
            this.label9.TabIndex = 67;
            this.label9.Text = "Familia";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(4, 43);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 27);
            this.label8.TabIndex = 66;
            this.label8.Text = "Tipo";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(4, 136);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 27);
            this.label5.TabIndex = 63;
            this.label5.Text = "Dificultad";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(4, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 27);
            this.label4.TabIndex = 61;
            this.label4.Text = "Máx Players";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(4, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 27);
            this.label2.TabIndex = 60;
            this.label2.Text = "Min Players";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSearchAssociation
            // 
            this.btnSearchAssociation.BackgroundImage = global::AlterParadox.GesUmbras.Properties.Resources.find;
            this.btnSearchAssociation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSearchAssociation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchAssociation.Location = new System.Drawing.Point(163, 167);
            this.btnSearchAssociation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnSearchAssociation.Name = "btnSearchAssociation";
            this.btnSearchAssociation.Size = new System.Drawing.Size(30, 27);
            this.btnSearchAssociation.TabIndex = 6;
            this.btnSearchAssociation.UseVisualStyleBackColor = true;
            this.btnSearchAssociation.Click += new System.EventHandler(this.btnSearchAssociation_Click);
            // 
            // txtAssociationId
            // 
            this.txtAssociationId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssociationId.Enabled = false;
            this.txtAssociationId.Location = new System.Drawing.Point(123, 167);
            this.txtAssociationId.MaxLength = 100;
            this.txtAssociationId.Name = "txtAssociationId";
            this.txtAssociationId.Size = new System.Drawing.Size(41, 27);
            this.txtAssociationId.TabIndex = 5;
            // 
            // lblAssociation
            // 
            this.lblAssociation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblAssociation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAssociation.Location = new System.Drawing.Point(4, 167);
            this.lblAssociation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAssociation.Name = "lblAssociation";
            this.lblAssociation.Size = new System.Drawing.Size(120, 27);
            this.lblAssociation.TabIndex = 72;
            this.lblAssociation.Text = "Asociación";
            this.lblAssociation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAssociationName
            // 
            this.txtAssociationName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssociationName.Enabled = false;
            this.txtAssociationName.Location = new System.Drawing.Point(192, 167);
            this.txtAssociationName.MaxLength = 100;
            this.txtAssociationName.Name = "txtAssociationName";
            this.txtAssociationName.Size = new System.Drawing.Size(209, 27);
            this.txtAssociationName.TabIndex = 7;
            // 
            // cmdApplyFilters
            // 
            this.cmdApplyFilters.Location = new System.Drawing.Point(225, 198);
            this.cmdApplyFilters.Name = "cmdApplyFilters";
            this.cmdApplyFilters.Size = new System.Drawing.Size(176, 48);
            this.cmdApplyFilters.TabIndex = 8;
            this.cmdApplyFilters.Text = "Aplicar filtros";
            this.cmdApplyFilters.UseVisualStyleBackColor = true;
            this.cmdApplyFilters.Click += new System.EventHandler(this.cmdApplyFilters_Click);
            // 
            // txtPlayerMax
            // 
            this.txtPlayerMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlayerMax.Location = new System.Drawing.Point(123, 105);
            this.txtPlayerMax.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtPlayerMax.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtPlayerMax.Name = "txtPlayerMax";
            this.txtPlayerMax.Size = new System.Drawing.Size(120, 27);
            this.txtPlayerMax.TabIndex = 3;
            this.txtPlayerMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPlayerMin
            // 
            this.txtPlayerMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlayerMin.Location = new System.Drawing.Point(123, 74);
            this.txtPlayerMin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtPlayerMin.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtPlayerMin.Name = "txtPlayerMin";
            this.txtPlayerMin.Size = new System.Drawing.Size(120, 27);
            this.txtPlayerMin.TabIndex = 2;
            this.txtPlayerMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // FrmGameFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 251);
            this.Controls.Add(this.cmdApplyFilters);
            this.Controls.Add(this.btnSearchAssociation);
            this.Controls.Add(this.txtAssociationId);
            this.Controls.Add(this.lblAssociation);
            this.Controls.Add(this.txtAssociationName);
            this.Controls.Add(this.txtDifficulty);
            this.Controls.Add(this.txtType);
            this.Controls.Add(this.txtFamily);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPlayerMax);
            this.Controls.Add(this.txtPlayerMin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmGameFilters";
            this.Text = "Filtros de juegos";
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox txtDifficulty;
        private System.Windows.Forms.ComboBox txtType;
        private System.Windows.Forms.ComboBox txtFamily;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private NumericUpDownPro txtPlayerMax;
        private NumericUpDownPro txtPlayerMin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearchAssociation;
        private System.Windows.Forms.TextBox txtAssociationId;
        private System.Windows.Forms.Label lblAssociation;
        private System.Windows.Forms.TextBox txtAssociationName;
        private System.Windows.Forms.Button cmdApplyFilters;
    }
}