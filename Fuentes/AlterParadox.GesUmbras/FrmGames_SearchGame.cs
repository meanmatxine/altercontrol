﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
namespace AlterParadox.GesUmbras
{
    using Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FrmGames_SearchGame : Form
    {
        public FrmGames_SearchGame()
        {
            InitializeComponent();
        }

        //LOCAL FUNCTIONS AND VARS
        Game _game;
        LocalGame _localGame;
        private void L_Refresh_Games()
        {
            Game_Filter filters = new Game_Filter();
            filters.Filter = txtFilter.Text;

            GridGames.AutoGenerateColumns = false;
            GridGames.DataSource = Game.getAll(filters);
        }


        //FORMULARIO
        private void FrmGames_NewGame_Load(object sender, EventArgs e)
        {
            L_Refresh_Games();
        }

        private void GridGames_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                G._searchGame = (Game)GridGames.SelectedRows[0].DataBoundItem;
                this.Close();
            }
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_Refresh_Games();
        }

        private void btnCleanFilter_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
        }

        private void btnRefreshGrid_Click(object sender, EventArgs e)
        {
            L_Refresh_Games();
        }

        private void txtFilter_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                GridGames.Focus();
            }
        }

        private void GridGames_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (GridGames.SelectedRows.Count > 0)
                {
                    G._searchGame = (Game)GridGames.SelectedRows[0].DataBoundItem;
                    this.Close();
                }
            }
        }
    }
}
