﻿using AlterParadox.Core.Models;
using AlterParadox.GesUmbras.UserService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmPersona : Form
    {
        public PersonaAlCargo persona;
        public PersonaAlCargo eliminar = null;

        public FrmPersona()
        {
            InitializeComponent();
           
        }
        private void CargarPersona()
        {
            txtName.Text = persona.Name;
            txtApellidos.Text = persona.LastName;
            txtBirthday.SetFecha(persona.Birthday);
            validarEdad();
            //txtYears.Text = persona.Edad.ToString();
        }

        private bool Validar()
        {
            if (string.IsNullOrEmpty(txtName.Text))
            {
                MessageBox.Show("Debe especificar un nombre!");
                txtName.Focus();
                return false;
            }

            if (string.IsNullOrEmpty(txtApellidos.Text))
            {
                MessageBox.Show("Debe especificar un apellido!");
                txtApellidos.Focus();
                return false;
            }
            if (!txtBirthday.GetFecha().HasValue)
            {
                MessageBox.Show("Debe especificar una fecha!");
                txtBirthday.Focus();
                return false;
            }
            
            return true;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!Validar())
                return;

            if(persona == null)
            {
                persona = new PersonaAlCargo(txtName.Text, txtApellidos.Text, txtBirthday.GetFechaForzada());

            }
            else
            {
                persona.Birthday = txtBirthday.GetFechaForzada();
                persona.Name = txtName.Text;
                persona.LastName = txtApellidos.Text;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Esta acción eliminará completamente el vínculo entre el ser humano y su tutor/a. ¿Desea continuar?","Advertencia",MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                eliminar = persona;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void FrmPersona_Load(object sender, EventArgs e)
        {
            eliminar = null;
            
            if (persona != null)
            {
                CargarPersona();
            }
            btnEliminar.Enabled = (persona != null);
        }

        private void txtBirthday_Validated(object sender, EventArgs e)
        {
            validarEdad();
        }
        private void validarEdad()
        {
            if (txtBirthday.GetFecha().HasValue)
            {
                int years = G.GetAge(txtBirthday.GetFechaForzada());
                txtYears.Text = $"{years} años.{((years < 18) ? " menor" : "")}";
            }
            else
                txtYears.Text = String.Empty;
        }
    }
}
