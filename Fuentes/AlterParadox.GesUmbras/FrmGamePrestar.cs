﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.UserService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmGamePrestar : Form
    {
        public LocalGame _localgame;
        public GameLending _gamelended;

        public FrmGamePrestar()
        {
            InitializeComponent();
        }

        void L_Refresh_LocalGame()
        {
            txtId.Text = "0";
            txtGame.Text = "" + _localgame.TheGame.Name;
            txtAssociation.Text = "" + _localgame.Association.Name;
            txtComments.Text = "" + _localgame.Comments;

        }

        void L_Refresh_GameLending()
        {
            txtId.Text = _gamelended.Id.ToString();
            txtGame.Text = "" + _gamelended.TheGame.TheGame.Name;
            txtAssociation.Text = "" + _gamelended.TheGame.Association.Name;
            txtComments.Text = "" + _gamelended.TheGame.TheGame.Comments;
            txtNumber.Value = (int) _gamelended.TheUser.Number;

            txtUserName.Text = "" + _gamelended.TheUser.Name + " " + _gamelended.TheUser.LastName;
            observTexBox.Text = "" + _gamelended.Comments;
        }

        void L_Save(bool lend = true)
        {

            //Save
            if ("" + txtNumber.Value == "0")
            {
                MessageBox.Show("¡Debe seleccionar un usuario!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtNumber.Focus();
                return;
            }

            User user = new User();
            if (txtNumber.Text != "0")
            {

                try
                {
                    using (UserServiceClient svc = new UserServiceClient())
                    {
                        //Esto esta mal. Hay que incluir la posibilidad de mandar el numero y no solo la ID
                        //Cambiar el nombre Request por Usuario
                        user = svc.GetUserByNumber(new GetUserByNumberRequest() { Number = Convert.ToInt32(txtNumber.Text) }).Usuario;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                
                if (user==null)
                {
                    MessageBox.Show("El inscrito no existe.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtNumber.Focus();
                    return;
                }

            }

            _gamelended = new GameLending();
            _gamelended.Id = Convert.ToInt32("" + txtId.Text);

            _gamelended.TheGame = _localgame;

            _gamelended.TheUser = user;

            _gamelended.Comments = "" + observTexBox.Text;

            _gamelended.Status = lend ? 1 : 0;

            _gamelended.Save();

            this.Close();

        }
        void l_load_user()
        {
            if (txtNumber.Text != "0")
            {
                User user = new User();

                try
                {
                    using (UserServiceClient svc = new UserServiceClient())
                    {
                        //Esto esta mal. Hay que incluir la posibilidad de mandar el numero y no solo la ID
                        //Cambiar el nombre Request por Usuario
                        user = svc.GetUserByNumber(new GetUserByNumberRequest() { Number = Convert.ToInt32(txtNumber.Text) }).Usuario;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                if (user != null)
                {
                    txtUserName.Text = "" + user.Name + " " + user.LastName;
                    txtAviso.Text = user.Aviso;
                }
                else
                {
                    MessageBox.Show("El inscrito no existe.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("El inscrito no son válidos.", "Alerta", MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);
            }

        }
        private void FrmGamePrestar_Load(object sender, EventArgs e)
        {

            if (_localgame != null)
            {
                //MessageBox.Show("Local game!");
                L_Refresh_LocalGame();
                txtNumber.Enabled = true;
            }
            if (_gamelended != null)
            {
                //MessageBox.Show("Game lended!");
                L_Refresh_GameLending();
                txtNumber.Enabled = false;

            }

        }

        private void txtNUmber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                l_load_user();


            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            L_Save();
        }

        private void btnGameSave_Click(object sender, EventArgs e)
        {
            L_Save(false);
        }

        private void txtNumber_Validated(object sender, EventArgs e)
        {
            l_load_user();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void txtNumber_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
