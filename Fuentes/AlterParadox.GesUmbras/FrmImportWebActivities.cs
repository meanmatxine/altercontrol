﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using AlterParadox.GesUmbras.SQLService;
using AlterParadox.Core;
using AlterParadox.GesUmbras.ActivityService;

namespace AlterParadox.GesUmbras
{
    public partial class FrmImportWebActivities : Form
    {
        public FrmImportWebActivities()
        {
            InitializeComponent();
        }

        private int campoOrganizador = 0;
        private int campoAsociacion = 1;
        private int campoNombre = 2;
        private int campoFecha = 3;
        private int campoHora = 4;
        private int campoTipo = 5;
        private int campoPlazas = 6;
        private int campoMaxPlazas = 7;
        private int campoDescCorta = 8;
        private int campoDescLarga = 9;
        private int campoNecesidades = 10;
        
        private int campoDuracion = 11;
        private int campoLocalizacion = 12;

        private void cmdImport_Click(object sender, EventArgs e)
        {
            var query = " DELETE FROM activities";

            try
            {
                using (SQLServiceClient svc = new SQLServiceClient())
                {
                     svc.ExecuteSQL(query);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            string filename = txtPathFile.Text;

            string CSV = G.GetCSV(filename);

            var result = Regex.Split(CSV, "FinLinea");
            List<Activity> activities = new List<Activity>();
            var firstLinea = true;
            foreach(var line in result)
            {
                var lineareal = line.Replace("\"\"", "'");
                int iCampo = 0;
                DateTime fechaActividad = DateTime.Now;
                Activity actividad = new Activity();
                actividad.Email = string.Empty;
                actividad.Phone = string.Empty;

                Regex regexObj = new Regex(@"(""([^""]*)""|[^,]*)(,|$)");
                Match matchResults = regexObj.Match(lineareal);
                
                while (matchResults.Success)
                {
                    if (firstLinea)
                    {
                        switch (matchResults.Groups[1].Value)
                        {
                            case "Organizador":
                                campoOrganizador = iCampo;
                                break;
                            case "Asociación":
                                campoAsociacion = iCampo;
                                break;
                            case "Nombre":
                                campoNombre = iCampo;
                                break;
                            case "Fecha (NameOfDay Day)":
                                campoFecha = iCampo;
                                break;
                            case "Hora (HH:mm)":
                                campoHora = iCampo;
                                break;
                            case "Tipo (Normalizar)":
                                campoTipo = iCampo;
                                break;
                            case "PlazasTexto":
                                campoPlazas = iCampo;
                                break;
                            case "Máx. Plazas (número)":
                                campoMaxPlazas = iCampo;
                                break;
                            case "Desc. Corta + duración rev":
                                campoDescCorta = iCampo;
                                break;
                            case "Desc. Larga":
                                campoDescLarga = iCampo;
                                break;
                            case "Necesidades":
                                campoNecesidades = iCampo;
                                break;
                            case "Duracion (minutos)":
                                campoDuracion = iCampo;
                                break;
                            case "Localizacion":
                            case "Localización":
                                campoLocalizacion = iCampo;
                                break;
                            default:
                                break;
                        }
                        //Console.WriteLine(matchResults.Groups[1].Value);

                    }
                    else {

                        if (iCampo == campoFecha) {
                            switch (matchResults.Groups[1].Value)
                            {
                                case "Jueves 15":
                                case "jueves 15":
                                    fechaActividad = new DateTime(2019, 8, 15, fechaActividad.Hour, fechaActividad.Minute, 0);
                                    break;
                                case "Viernes 16":
                                case "viernes 16":
                                    fechaActividad = new DateTime(2019, 8, 16, fechaActividad.Hour, fechaActividad.Minute, 0);
                                    break;
                                case "Sabado 17":
                                case "Sábado 17":
                                case "sabado 17":
                                case "sábado 17":
                                    fechaActividad = new DateTime(2019, 8, 17, fechaActividad.Hour, fechaActividad.Minute, 0);
                                    break;
                                case "Domingo 18":
                                case "domingo 18":
                                    fechaActividad = new DateTime(2019, 8, 18, fechaActividad.Hour, fechaActividad.Minute, 0);
                                    break;
                                default:
                                    fechaActividad = new DateTime(2019, 8, 15, fechaActividad.Hour, fechaActividad.Minute, 0);
                                    break;
                            }
                            
                        }

                        if (iCampo == campoHora)
                        {
                            if (!string.IsNullOrEmpty(matchResults.Groups[1].Value.ToString())) { 
                                var spliteado = matchResults.Groups[1].Value.ToString().Split(':');
                                if(spliteado.Any() && spliteado.Count()==2)
                                    fechaActividad = new DateTime(fechaActividad.Year, fechaActividad.Month, fechaActividad.Day, int.Parse(spliteado[0]), int.Parse(spliteado[1]), 0);
                            }
                        }
                        
                        if (iCampo == campoOrganizador)
                            actividad.Organizer = matchResults.Groups[1].Value.ToString().Replace("\"\"","'").Replace("\"","");
                        if (iCampo == campoAsociacion)
                            actividad.Association = matchResults.Groups[1].Value.ToString().Replace("\"\"", "'").Replace("\"", "");
                        if (iCampo == campoNombre)
                            actividad.Name = matchResults.Groups[1].Value.ToString().Replace("\"\"", "'").Replace("\"", "");
                        if (iCampo == campoFecha || iCampo == campoHora)
                            actividad.ActivityDate = fechaActividad;
                        if (iCampo == campoTipo)
                            actividad.Type = matchResults.Groups[1].Value;
                        if (iCampo == campoPlazas)
                            actividad.Participants = matchResults.Groups[1].Value;
                        if (iCampo == campoMaxPlazas) {
                            int outputint = 0;
                            int.TryParse(matchResults.Groups[1].Value.ToString(), out outputint);
                            actividad.MaxParticipants = outputint;
                        }

                        actividad.MaxParticipants = 0;

                        if (iCampo == campoDescCorta)
                            actividad.Summary = matchResults.Groups[1].Value.ToString().Replace("\"\"", "'").Replace("\"", "");
                        if (iCampo == campoDescLarga)
                            actividad.Comments = matchResults.Groups[1].Value.ToString().Replace("\"\"", "'").Replace("\"", "");
                        if (iCampo == campoNecesidades)
                            actividad.Needs = matchResults.Groups[1].Value.ToString().Replace("\"\"", "'").Replace("\"", "");
                        if (iCampo == campoLocalizacion)
                            actividad.Place = matchResults.Groups[1].Value.ToString().Replace("\"\"", "'").Replace("\"", "");

                        if (iCampo == campoDuracion)
                        {
                            int outputint = 0;
                            int.TryParse(matchResults.Groups[1].Value.ToString(), out outputint);
                            actividad.Duration = outputint;
                        }
                            

                        Console.WriteLine(matchResults.Groups[1].Value);
                    }
                    iCampo = iCampo + 1;
                    matchResults = matchResults.NextMatch();
                }
                //actividad.save();
                activities.Add(actividad);
                firstLinea = false;
            }

            progress.Value = 0;
            progress.Maximum = activities.Count();
            foreach (Activity activity in activities)
            {

                try
                {
                    using (ActivityServiceClient svc = new ActivityServiceClient())
                    {
                        //SAVEUSER DEBERIA DEVOLVER O EL ID DEL USUARIO NUEVO O LOS DATOS DEL USUARIO
                        var actividadRespuesta = svc.SaveActivity(new SaveActivityRequest() { Actividad = activity });

                        if (!actividadRespuesta.EsCorrecto)
                            MessageBox.Show(string.Format("Actividad incorrecta: {0}", activity.Name));

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                if (progress.Value < progress.Maximum)
                {
                    progress.Value = progress.Value + 1;
                    this.Refresh();
                    this.progress.Refresh();
                    Application.DoEvents();
                }
            }
            MessageBox.Show("Importación finalizada.");

            
        }
    }
}
