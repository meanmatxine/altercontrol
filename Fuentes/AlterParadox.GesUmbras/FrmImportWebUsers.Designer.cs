﻿namespace AlterParadox.GesUmbras
{
    partial class FrmImportWebUsers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSearch = new System.Windows.Forms.Button();
            this.cmdImport = new System.Windows.Forms.Button();
            this.txtPathFile = new System.Windows.Forms.TextBox();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // cmdSearch
            // 
            this.cmdSearch.Location = new System.Drawing.Point(341, 12);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(86, 23);
            this.cmdSearch.TabIndex = 0;
            this.cmdSearch.Text = "Buscar Fichero";
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdImport
            // 
            this.cmdImport.Location = new System.Drawing.Point(15, 40);
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Size = new System.Drawing.Size(86, 35);
            this.cmdImport.TabIndex = 3;
            this.cmdImport.Text = "Importar";
            this.cmdImport.UseVisualStyleBackColor = true;
            this.cmdImport.Click += new System.EventHandler(this.cmdImport_Click);
            // 
            // txtPathFile
            // 
            this.txtPathFile.Location = new System.Drawing.Point(12, 14);
            this.txtPathFile.Name = "txtPathFile";
            this.txtPathFile.Size = new System.Drawing.Size(323, 20);
            this.txtPathFile.TabIndex = 4;
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(107, 52);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(320, 23);
            this.progress.TabIndex = 5;
            // 
            // FrmImportWebUsers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 82);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.txtPathFile);
            this.Controls.Add(this.cmdImport);
            this.Controls.Add(this.cmdSearch);
            this.Name = "FrmImportWebUsers";
            this.Text = "FrmImportWebUsers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdSearch;
        private System.Windows.Forms.Button cmdImport;
        private System.Windows.Forms.TextBox txtPathFile;
        private System.Windows.Forms.ProgressBar progress;
    }
}