﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.ActivityService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmActWindow : Form
    {
        DataTable gridActividades;
        List<Activity> actividades;
        public FrmActWindow()
        {
            InitializeComponent();
                //var timer = new System.Threading.Timer((e)=> {
                //    refrescarVentana();
                //},null,TimeSpan.Zero,TimeSpan.FromMinutes(4));
        }

        private void refrescarVentana() {
            gridActividades = new DataTable();
            DateTime ahora = DateTime.Now;
            Console.WriteLine(ahora.ToString());
            string fecha = convertirFecha(ahora);


            List<Activity> listaActividades = new List<Activity>();
            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    listaActividades = svc.GetActivities().ListaActividades.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            var fechaTOTAL = DateTime.Now.AddHours(-1);
            fechaTOTAL = new DateTime(fechaTOTAL.Year, fechaTOTAL.Month, fechaTOTAL.Day, fechaTOTAL.Hour, 0, 0);
            actividades = listaActividades
                .Where(x =>
                     x.ActivityDate.CompareTo(fechaTOTAL) > 0 
                ).ToList(); ;


            actGridView.AutoGenerateColumns = false;
            this.actGridView.DataSource = actividades;
            this.actGridView.ClearSelection();

            if (gridActividades.Rows.Count > 0)
            {
                

                //actGridView.Columns["AbleToJoin"].Visible = false;

                //foreach (DataGridViewRow row in actGridView.Rows)
                //    if (row.Cells["AbleToJoin"].Value=="False")
                //        row.DefaultCellStyle.Font = new Font(this.Font, FontStyle.Strikeout);

            }
        }

        private string convertirFecha(DateTime d) {
            string AA, MM, DD, HH, MI, SS;
            //año
            AA = d.Year.ToString();
            //mes
            if (d.Month < 10)
            {
                MM = "0" + d.Month;
            }
            else {
                MM = d.Month.ToString();
            }
            //dia
            if (d.Day < 10)
            {
                DD = "0" + d.Day;
            }
            else
            {
                DD = d.Day.ToString();
            }
            //Hora
            if (d.Hour < 10)
            {
                HH = "0" + d.Hour;
            }
            else
            {
                HH = d.Hour.ToString();
            }
            //minuto
            if (d.Minute < 10)
            {
                MI = "0" + d.Minute;
            }
            else
            {
                MI = d.Minute.ToString();
            }
            //segundo
            if (d.Second < 10)
            {
                SS = "0" + d.Second;
            }
            else
            {
                SS = d.Second.ToString();
            }
            return DD + "/" + MM + "/" + AA + " " + HH + ":" + MI + ":" + SS;
            //return AA + "-" + MM + "-" + DD + " " + HH + ":" + MI + ":" + SS;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            refrescarVentana();
        }

        private void FrmActWindow_Load(object sender, EventArgs e)
        {
            this.Show();
            refrescarVentana();
        }

        private void actGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            foreach (DataGridViewRow row in actGridView.Rows)
                if (!(bool)row.Cells["colAbleToJoin"].Value)
                    row.DefaultCellStyle.BackColor = Color.IndianRed;
                    //row.DefaultCellStyle.Font = new Font();
        }
    }
}
