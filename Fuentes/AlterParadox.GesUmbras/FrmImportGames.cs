﻿namespace AlterParadox.GesUmbras
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.IO;

    public partial class FrmImportGames : Form
    {
        public FrmImportGames()
        {
            InitializeComponent();
        }

        private void cmdSearch_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "CSV Document|*.csv";


            DialogResult result = dlg.ShowDialog();


            if (result == DialogResult.OK) // Test result.
            {
                txtPathFile.Text = "" + dlg.FileName;
            }
            Console.WriteLine(result); // <-- For debugging use.
        }

        private void cmdImport_Click(object sender, EventArgs e)
        {
            string filename = txtPathFile.Text;

            var contents = File.ReadAllText(filename).Split('\n');
            var csv = from line in contents
                      select line.Split(';').ToArray();

            var count = csv.Count();

            progress.Value = 0;
            progress.Maximum = (int)count * 2;

            List<Game> games = new List<Game>();

            foreach (string[] row in csv)
            {
                if (row.Length > 1)
                {
                    //0     1       2       3       4           5           6       7       8
                    //Name;Family;Type;Dificultad;player_min;player_max;comments;duration;years;;;;;
                    //¡No Gracias!;0;0;1;3;7;4-5;20-30;8;¡No Gracias!, es un juego sencillísimo y muy divertido. Si quieres ganar, al igual que cualquier buen jugador de póquer, deberás sopesar detenidamente los riesgos y ser capaz de marcarte algún que otro farol. Distrae a tus amigos de tus verdaderas intenciones con un rotundo: ¡No Gracias!;;;;
                    if ("" + row[0].Replace("\"", "") != "Name")
                    {

 
                        string name = "";
                        int family = 8;
                        int type = 7;
                        int dificulty = 0;
                        int player_min = 0;
                        int player_max = 999;
                        string comments = "";
                        int duration = 60;
                        int years = 5;

                        name = "" + (string)row[0].Replace("\"", "");
                        comments = "" + (string)row[6].Replace("\"", "");

                        if ("" + (string)row[1].Replace("\"", "") != "")
                        {
                            family = Convert.ToInt32("" + (string)row[1].Replace("\"", ""));
                        }
                        if ("" + (string)row[2].Replace("\"", "") != "")
                        {
                            type = Convert.ToInt32("" + (string)row[2].Replace("\"", ""));
                        }
                        if ("" + (string)row[3].Replace("\"", "") != "")
                        {
                            dificulty = Convert.ToInt32("" + (string)row[3].Replace("\"", ""));
                        }
                        if ("" + (string)row[4].Replace("\"", "") != "")
                        {
                            player_min = Convert.ToInt32("" + (string)row[4].Replace("\"", ""));
                        }
                        if ("" + (string)row[5].Replace("\"", "") != "")
                        {
                            player_max = Convert.ToInt32("" + (string)row[5].Replace("\"", ""));
                        }
                        if ("" + (string)row[7].Replace("\"", "") != "")
                        {
                            duration = Convert.ToInt32("" + (string)row[7].Replace("\"", ""));
                        }
                        if ("" + (string)row[8].Replace("\"", "").Replace("\r", "") != "")
                        {
                            years = Convert.ToInt32("" + (string)row[8].Replace("\"", "").Replace("\r", ""));
                        }

                        Game game = new Game
                        {
                            Name = name,
                            Family = family,
                            Type = type,
                            Difficulty = dificulty,
                            PlayerMin = player_min,
                            PlayerMax = player_max,
                            Comments = comments,
                            Duration = duration,
                            Years = years
                        };
                        games.Add(game);
                    }
                }
                if (progress.Value < progress.Maximum)
                {
                    progress.Value = progress.Value + 1;
                    this.Refresh();
                    this.progress.Refresh();
                    Application.DoEvents();
                }
            }

            foreach (Game game in games)
            {
                game.Save();

                if (progress.Value < progress.Maximum)
                {
                    progress.Value = progress.Value + 1;
                    this.Refresh();
                    this.progress.Refresh();
                    Application.DoEvents();
                }
            }
        }
    }
}
