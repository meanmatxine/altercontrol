﻿using AlterParadox.GesUmbras.Objects;

using System;
using System.Globalization;
using System.Windows.Forms;


namespace AlterParadox.GesUmbras
{
    public partial class IniciarSesion : Form
    {

        public IniciarSesion()
        {
            InitializeComponent();
        }

        private void IniciarSesion_Load(object sender, EventArgs e)
        {
            if (ControlCaja.tengoDinero())
            { 
                MessageBox.Show("Oups, esto no deberia haber ocurrido, reinicie la aplicacin y si sigue viendo el mensaje localize a fermin, si no esta siga trabajando y desee que luego cuadren las cuentas, porque si no el dios Zeus bajara y te metera un rayo por el culo. Y a Fermín dos.", "Error inexperado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonIniciar_Click(object sender, EventArgs e)
        {
            if (dineroInicial.Value <= 0)
            {
                MessageBox.Show("Valor no Válido", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else {
                //iniciar la caja //AQUI
                DateTime localDate = DateTime.Now;
                string culture = "dd/MM/yyyy HH:mm:ss";
                ControlCaja c = new ControlCaja();
                c.cajaMovFecha= localDate.ToString(culture);
                c.cajaIngreso = dineroInicial.Value;
                c.cajaExtracto = 0;
                c.cajaConcepto = "Dinero Inicial";
                c.cajaObservaciones = "";
                c.cajaDineroActual = c.cajaIngreso;
                c.cajaDescuadre = 0;
                ControlCaja.INSERT(c);
                this.Close();

            }
        }

        private void dineroInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
