﻿using AlterParadox.GesUmbras.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras.Formularios_TPV
{
    public partial class MoverDinero : Form
    {
        ControlCaja cajaActual;

        public MoverDinero()
        {
            InitializeComponent();
            pruebasFecha();
            //dinero actual
            cajaActual = ControlCaja.ultimoMovimiento();
            dineroActual.Text = cajaActual.cajaDineroActual.ToString()+" €.";
        }

        private void sacarDinero_Click(object sender, EventArgs e)
        {
            ControlCaja movimientoSacar = new ControlCaja
                ();
            movimientoSacar.cajaExtracto= decimal.Parse(dineroMovido.Text);
            if (movimientoSacar.cajaExtracto <= 0)
            {
                MessageBox.Show("Debe ser Mayor que 0", "Dinero Insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else if(string.IsNullOrWhiteSpace(textPersona.Text))
            {
                MessageBox.Show("Debe haber una persona responsable", "Persona Responsable", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DateTime localDate = DateTime.Now;
                var culture = new CultureInfo("es-ES");
                movimientoSacar.cajaMovFecha = localDate.ToString(culture);
                movimientoSacar.cajaPersona = textPersona.Text;
                movimientoSacar.cajaIngreso = 0;
                movimientoSacar.cajaConcepto = "Sacar Dinero";
                movimientoSacar.cajaObservaciones = comentarios.Text;
                movimientoSacar.cajaDineroActual = cajaActual.cajaDineroActual - movimientoSacar.cajaExtracto;
                movimientoSacar.cajaDescuadre = cajaActual.cajaDescuadre;
                //Insertar el movimiento
                ControlCaja.INSERT(movimientoSacar);
                //Mensaje
                MessageBox.Show("Se han sacado" + movimientoSacar.cajaExtracto + "€ en concepto de " + movimientoSacar.cajaObservaciones, "Dinero sacado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            
        }

        private void meterDinero_Click(object sender, EventArgs e)
        {
            ControlCaja movimientoMeter = new ControlCaja();
            movimientoMeter.cajaIngreso = decimal.Parse(dineroMovido.Text);
            if (movimientoMeter.cajaIngreso <= 0)
            {
                MessageBox.Show("Debe ser Mayor que 0", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (string.IsNullOrWhiteSpace(textPersona.Text))
            {
                MessageBox.Show("Debe haber una persona responsable", "Persona Responsable", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DateTime localDate = DateTime.Now;
                string culture = "dd/MM/yyyy HH:mm:ss";
                movimientoMeter.cajaMovFecha = localDate.ToString(culture);
                movimientoMeter.cajaPersona = textPersona.Text;
                movimientoMeter.cajaExtracto = 0;
                movimientoMeter.cajaConcepto = "Meter Dinero";
                movimientoMeter.cajaObservaciones = comentarios.Text;
                movimientoMeter.cajaDineroActual = cajaActual.cajaDineroActual + movimientoMeter.cajaIngreso;
                movimientoMeter.cajaDescuadre = cajaActual.cajaDescuadre;
                //Insertar el movimiento
                ControlCaja.INSERT(movimientoMeter);
                //Mensaje
                MessageBox.Show("Se han metido" + movimientoMeter.cajaIngreso + "€ en concepto de " + movimientoMeter.cajaObservaciones, "Dinero metido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void cerrarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //pruebas fecha
        private void pruebasFecha()
        {
            DateTime localDate1 = new DateTime(2018, 9, 9, 9, 59, 00);
            DateTime localDate2 =new DateTime(2018,10,10,10,00,00);
            Console.WriteLine(localDate1.ToString("dd/MM/yyyy HH:mm:ss"));
        }

    }
}
