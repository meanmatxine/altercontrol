﻿namespace AlterParadox.GesUmbras
{
    partial class CerrarSesion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.monedas001 = new System.Windows.Forms.NumericUpDown();
            this.monedas002 = new System.Windows.Forms.NumericUpDown();
            this.monedas005 = new System.Windows.Forms.NumericUpDown();
            this.monedas01 = new System.Windows.Forms.NumericUpDown();
            this.monedas02 = new System.Windows.Forms.NumericUpDown();
            this.monedas05 = new System.Windows.Forms.NumericUpDown();
            this.monedas1 = new System.Windows.Forms.NumericUpDown();
            this.monedas2 = new System.Windows.Forms.NumericUpDown();
            this.total001 = new System.Windows.Forms.Label();
            this.total002 = new System.Windows.Forms.Label();
            this.total005 = new System.Windows.Forms.Label();
            this.total01 = new System.Windows.Forms.Label();
            this.total02 = new System.Windows.Forms.Label();
            this.total05 = new System.Windows.Forms.Label();
            this.total1 = new System.Windows.Forms.Label();
            this.total2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.billete5 = new System.Windows.Forms.NumericUpDown();
            this.billete10 = new System.Windows.Forms.NumericUpDown();
            this.billete20 = new System.Windows.Forms.NumericUpDown();
            this.billete50 = new System.Windows.Forms.NumericUpDown();
            this.total5 = new System.Windows.Forms.Label();
            this.total10 = new System.Windows.Forms.Label();
            this.total20 = new System.Windows.Forms.Label();
            this.total50 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textActual = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textConteo = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textDescuadrePre = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numSacar = new System.Windows.Forms.NumericUpDown();
            this.numMeter = new System.Windows.Forms.NumericUpDown();
            this.textFinal = new System.Windows.Forms.Label();
            this.ValidarButton = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.textDescuadrePost = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.monedas001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSacar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMeter)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Moneda 0,01€";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Moneda 0,02€";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Moneda 0,05€";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Moneda 0,10€";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Moneda 0,20€";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Moneda 0,50€";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Moneda 1€";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Moneda 2€";
            // 
            // monedas001
            // 
            this.monedas001.Location = new System.Drawing.Point(109, 21);
            this.monedas001.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas001.Name = "monedas001";
            this.monedas001.Size = new System.Drawing.Size(88, 20);
            this.monedas001.TabIndex = 1;
            this.monedas001.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas001.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas001.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas002
            // 
            this.monedas002.Location = new System.Drawing.Point(109, 50);
            this.monedas002.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas002.Name = "monedas002";
            this.monedas002.Size = new System.Drawing.Size(88, 20);
            this.monedas002.TabIndex = 2;
            this.monedas002.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas002.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas002.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas005
            // 
            this.monedas005.Location = new System.Drawing.Point(109, 79);
            this.monedas005.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas005.Name = "monedas005";
            this.monedas005.Size = new System.Drawing.Size(88, 20);
            this.monedas005.TabIndex = 3;
            this.monedas005.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas005.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas005.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas01
            // 
            this.monedas01.Location = new System.Drawing.Point(109, 108);
            this.monedas01.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas01.Name = "monedas01";
            this.monedas01.Size = new System.Drawing.Size(88, 20);
            this.monedas01.TabIndex = 4;
            this.monedas01.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas01.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas01.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas02
            // 
            this.monedas02.Location = new System.Drawing.Point(109, 137);
            this.monedas02.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas02.Name = "monedas02";
            this.monedas02.Size = new System.Drawing.Size(88, 20);
            this.monedas02.TabIndex = 5;
            this.monedas02.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas02.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas02.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas05
            // 
            this.monedas05.Location = new System.Drawing.Point(109, 166);
            this.monedas05.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas05.Name = "monedas05";
            this.monedas05.Size = new System.Drawing.Size(88, 20);
            this.monedas05.TabIndex = 6;
            this.monedas05.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas05.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas05.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas1
            // 
            this.monedas1.Location = new System.Drawing.Point(109, 195);
            this.monedas1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas1.Name = "monedas1";
            this.monedas1.Size = new System.Drawing.Size(88, 20);
            this.monedas1.TabIndex = 7;
            this.monedas1.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas1.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas1.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // monedas2
            // 
            this.monedas2.Location = new System.Drawing.Point(109, 224);
            this.monedas2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.monedas2.Name = "monedas2";
            this.monedas2.Size = new System.Drawing.Size(88, 20);
            this.monedas2.TabIndex = 8;
            this.monedas2.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.monedas2.Click += new System.EventHandler(this.monedas001_Click);
            this.monedas2.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // total001
            // 
            this.total001.AutoSize = true;
            this.total001.Location = new System.Drawing.Point(216, 23);
            this.total001.Name = "total001";
            this.total001.Size = new System.Drawing.Size(19, 13);
            this.total001.TabIndex = 16;
            this.total001.Text = "0€";
            // 
            // total002
            // 
            this.total002.AutoSize = true;
            this.total002.Location = new System.Drawing.Point(216, 52);
            this.total002.Name = "total002";
            this.total002.Size = new System.Drawing.Size(19, 13);
            this.total002.TabIndex = 17;
            this.total002.Text = "0€";
            // 
            // total005
            // 
            this.total005.AutoSize = true;
            this.total005.Location = new System.Drawing.Point(216, 81);
            this.total005.Name = "total005";
            this.total005.Size = new System.Drawing.Size(19, 13);
            this.total005.TabIndex = 18;
            this.total005.Text = "0€";
            // 
            // total01
            // 
            this.total01.AutoSize = true;
            this.total01.Location = new System.Drawing.Point(216, 110);
            this.total01.Name = "total01";
            this.total01.Size = new System.Drawing.Size(19, 13);
            this.total01.TabIndex = 19;
            this.total01.Text = "0€";
            // 
            // total02
            // 
            this.total02.AutoSize = true;
            this.total02.Location = new System.Drawing.Point(216, 139);
            this.total02.Name = "total02";
            this.total02.Size = new System.Drawing.Size(19, 13);
            this.total02.TabIndex = 20;
            this.total02.Text = "0€";
            // 
            // total05
            // 
            this.total05.AutoSize = true;
            this.total05.Location = new System.Drawing.Point(216, 168);
            this.total05.Name = "total05";
            this.total05.Size = new System.Drawing.Size(19, 13);
            this.total05.TabIndex = 21;
            this.total05.Text = "0€";
            // 
            // total1
            // 
            this.total1.AutoSize = true;
            this.total1.Location = new System.Drawing.Point(216, 197);
            this.total1.Name = "total1";
            this.total1.Size = new System.Drawing.Size(19, 13);
            this.total1.TabIndex = 22;
            this.total1.Text = "0€";
            // 
            // total2
            // 
            this.total2.AutoSize = true;
            this.total2.Location = new System.Drawing.Point(216, 226);
            this.total2.Name = "total2";
            this.total2.Size = new System.Drawing.Size(19, 13);
            this.total2.TabIndex = 23;
            this.total2.Text = "0€";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(320, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Billete 5€";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(320, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Billete 10€";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(320, 81);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Billete 20€";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(320, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Billete 50€";
            // 
            // billete5
            // 
            this.billete5.Location = new System.Drawing.Point(395, 21);
            this.billete5.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.billete5.Name = "billete5";
            this.billete5.Size = new System.Drawing.Size(88, 20);
            this.billete5.TabIndex = 9;
            this.billete5.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.billete5.Click += new System.EventHandler(this.monedas001_Click);
            this.billete5.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // billete10
            // 
            this.billete10.Location = new System.Drawing.Point(395, 50);
            this.billete10.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.billete10.Name = "billete10";
            this.billete10.Size = new System.Drawing.Size(88, 20);
            this.billete10.TabIndex = 10;
            this.billete10.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.billete10.Click += new System.EventHandler(this.monedas001_Click);
            this.billete10.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // billete20
            // 
            this.billete20.Location = new System.Drawing.Point(395, 79);
            this.billete20.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.billete20.Name = "billete20";
            this.billete20.Size = new System.Drawing.Size(88, 20);
            this.billete20.TabIndex = 11;
            this.billete20.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.billete20.Click += new System.EventHandler(this.monedas001_Click);
            this.billete20.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // billete50
            // 
            this.billete50.Location = new System.Drawing.Point(395, 108);
            this.billete50.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.billete50.Name = "billete50";
            this.billete50.Size = new System.Drawing.Size(88, 20);
            this.billete50.TabIndex = 12;
            this.billete50.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.billete50.Click += new System.EventHandler(this.monedas001_Click);
            this.billete50.Enter += new System.EventHandler(this.monedas001_Enter);
            // 
            // total5
            // 
            this.total5.AutoSize = true;
            this.total5.Location = new System.Drawing.Point(526, 23);
            this.total5.Name = "total5";
            this.total5.Size = new System.Drawing.Size(19, 13);
            this.total5.TabIndex = 32;
            this.total5.Text = "0€";
            // 
            // total10
            // 
            this.total10.AutoSize = true;
            this.total10.Location = new System.Drawing.Point(526, 52);
            this.total10.Name = "total10";
            this.total10.Size = new System.Drawing.Size(19, 13);
            this.total10.TabIndex = 33;
            this.total10.Text = "0€";
            // 
            // total20
            // 
            this.total20.AutoSize = true;
            this.total20.Location = new System.Drawing.Point(526, 81);
            this.total20.Name = "total20";
            this.total20.Size = new System.Drawing.Size(19, 13);
            this.total20.TabIndex = 34;
            this.total20.Text = "0€";
            // 
            // total50
            // 
            this.total50.AutoSize = true;
            this.total50.Location = new System.Drawing.Point(526, 110);
            this.total50.Name = "total50";
            this.total50.Size = new System.Drawing.Size(19, 13);
            this.total50.TabIndex = 35;
            this.total50.Text = "0€";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(320, 182);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 16);
            this.label13.TabIndex = 36;
            this.label13.Text = "Caja Actual: ";
            // 
            // textActual
            // 
            this.textActual.AutoSize = true;
            this.textActual.Location = new System.Drawing.Point(457, 184);
            this.textActual.Name = "textActual";
            this.textActual.Size = new System.Drawing.Size(19, 13);
            this.textActual.TabIndex = 37;
            this.textActual.Text = "0€";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(320, 211);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 16);
            this.label14.TabIndex = 38;
            this.label14.Text = "Segun Conteo: ";
            // 
            // textConteo
            // 
            this.textConteo.AutoSize = true;
            this.textConteo.Location = new System.Drawing.Point(457, 213);
            this.textConteo.Name = "textConteo";
            this.textConteo.Size = new System.Drawing.Size(19, 13);
            this.textConteo.TabIndex = 39;
            this.textConteo.Text = "0€";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(320, 240);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 16);
            this.label15.TabIndex = 40;
            this.label15.Text = "Descaudre Actual: ";
            // 
            // textDescuadrePre
            // 
            this.textDescuadrePre.AutoSize = true;
            this.textDescuadrePre.Location = new System.Drawing.Point(457, 242);
            this.textDescuadrePre.Name = "textDescuadrePre";
            this.textDescuadrePre.Size = new System.Drawing.Size(19, 13);
            this.textDescuadrePre.TabIndex = 41;
            this.textDescuadrePre.Text = "0€";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 318);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Sacamos";
            this.label16.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(147, 318);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(50, 13);
            this.label17.TabIndex = 44;
            this.label17.Text = "Metemos";
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(231, 318);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "Caja Tras Conteo";
            // 
            // numSacar
            // 
            this.numSacar.DecimalPlaces = 2;
            this.numSacar.Location = new System.Drawing.Point(13, 351);
            this.numSacar.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numSacar.Name = "numSacar";
            this.numSacar.Size = new System.Drawing.Size(88, 20);
            this.numSacar.TabIndex = 13;
            this.numSacar.Visible = false;
            this.numSacar.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.numSacar.Click += new System.EventHandler(this.monedas001_Click);
            this.numSacar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSacar_KeyPress);
            // 
            // numMeter
            // 
            this.numMeter.DecimalPlaces = 2;
            this.numMeter.Location = new System.Drawing.Point(126, 351);
            this.numMeter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMeter.Name = "numMeter";
            this.numMeter.Size = new System.Drawing.Size(88, 20);
            this.numMeter.TabIndex = 14;
            this.numMeter.Visible = false;
            this.numMeter.ValueChanged += new System.EventHandler(this.monedas001_ValueChanged);
            this.numMeter.Click += new System.EventHandler(this.monedas001_Click);
            this.numMeter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numMeter_KeyPress);
            // 
            // textFinal
            // 
            this.textFinal.AutoSize = true;
            this.textFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textFinal.Location = new System.Drawing.Point(262, 355);
            this.textFinal.Name = "textFinal";
            this.textFinal.Size = new System.Drawing.Size(22, 16);
            this.textFinal.TabIndex = 48;
            this.textFinal.Text = "0€";
            // 
            // ValidarButton
            // 
            this.ValidarButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValidarButton.Location = new System.Drawing.Point(180, 405);
            this.ValidarButton.Name = "ValidarButton";
            this.ValidarButton.Size = new System.Drawing.Size(244, 44);
            this.ValidarButton.TabIndex = 15;
            this.ValidarButton.Text = "VALIDAR";
            this.ValidarButton.UseVisualStyleBackColor = true;
            this.ValidarButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(356, 318);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 13);
            this.label19.TabIndex = 50;
            this.label19.Text = "Descuadre Tras Conteo";
            // 
            // textDescuadrePost
            // 
            this.textDescuadrePost.AutoSize = true;
            this.textDescuadrePost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDescuadrePost.Location = new System.Drawing.Point(402, 355);
            this.textDescuadrePost.Name = "textDescuadrePost";
            this.textDescuadrePost.Size = new System.Drawing.Size(22, 16);
            this.textDescuadrePost.TabIndex = 51;
            this.textDescuadrePost.Text = "0€";
            // 
            // CerrarSesion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 482);
            this.Controls.Add(this.textDescuadrePost);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.ValidarButton);
            this.Controls.Add(this.textFinal);
            this.Controls.Add(this.numMeter);
            this.Controls.Add(this.numSacar);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textDescuadrePre);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textConteo);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textActual);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.total50);
            this.Controls.Add(this.total20);
            this.Controls.Add(this.total10);
            this.Controls.Add(this.total5);
            this.Controls.Add(this.billete50);
            this.Controls.Add(this.billete20);
            this.Controls.Add(this.billete10);
            this.Controls.Add(this.billete5);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.total2);
            this.Controls.Add(this.total1);
            this.Controls.Add(this.total05);
            this.Controls.Add(this.total02);
            this.Controls.Add(this.total01);
            this.Controls.Add(this.total005);
            this.Controls.Add(this.total002);
            this.Controls.Add(this.total001);
            this.Controls.Add(this.monedas2);
            this.Controls.Add(this.monedas1);
            this.Controls.Add(this.monedas05);
            this.Controls.Add(this.monedas02);
            this.Controls.Add(this.monedas01);
            this.Controls.Add(this.monedas005);
            this.Controls.Add(this.monedas002);
            this.Controls.Add(this.monedas001);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "CerrarSesion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Contar Caja";
            ((System.ComponentModel.ISupportInitialize)(this.monedas001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.monedas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billete50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSacar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMeter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown monedas001;
        private System.Windows.Forms.NumericUpDown monedas002;
        private System.Windows.Forms.NumericUpDown monedas005;
        private System.Windows.Forms.NumericUpDown monedas01;
        private System.Windows.Forms.NumericUpDown monedas02;
        private System.Windows.Forms.NumericUpDown monedas05;
        private System.Windows.Forms.NumericUpDown monedas1;
        private System.Windows.Forms.NumericUpDown monedas2;
        private System.Windows.Forms.Label total001;
        private System.Windows.Forms.Label total002;
        private System.Windows.Forms.Label total005;
        private System.Windows.Forms.Label total01;
        private System.Windows.Forms.Label total02;
        private System.Windows.Forms.Label total05;
        private System.Windows.Forms.Label total1;
        private System.Windows.Forms.Label total2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown billete5;
        private System.Windows.Forms.NumericUpDown billete10;
        private System.Windows.Forms.NumericUpDown billete20;
        private System.Windows.Forms.NumericUpDown billete50;
        private System.Windows.Forms.Label total5;
        private System.Windows.Forms.Label total10;
        private System.Windows.Forms.Label total20;
        private System.Windows.Forms.Label total50;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label textActual;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label textConteo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label textDescuadrePre;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numSacar;
        private System.Windows.Forms.NumericUpDown numMeter;
        private System.Windows.Forms.Label textFinal;
        private System.Windows.Forms.Button ValidarButton;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label textDescuadrePost;
    }
}