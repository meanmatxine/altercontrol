﻿namespace AlterParadox.GesUmbras
{
    partial class pedidoFinal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.total = new System.Windows.Forms.Label();
            this.entregado = new System.Windows.Forms.Label();
            this.devolver = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.printbutton = new System.Windows.Forms.Button();
            this.ValidarButton = new System.Windows.Forms.Button();
            this.listPedido = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.Location = new System.Drawing.Point(12, 342);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(51, 20);
            this.total.TabIndex = 1;
            this.total.Text = "label1";
            // 
            // entregado
            // 
            this.entregado.AutoSize = true;
            this.entregado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.entregado.Location = new System.Drawing.Point(12, 380);
            this.entregado.Name = "entregado";
            this.entregado.Size = new System.Drawing.Size(51, 20);
            this.entregado.TabIndex = 2;
            this.entregado.Text = "label1";
            // 
            // devolver
            // 
            this.devolver.AutoSize = true;
            this.devolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devolver.Location = new System.Drawing.Point(12, 419);
            this.devolver.Name = "devolver";
            this.devolver.Size = new System.Drawing.Size(51, 20);
            this.devolver.TabIndex = 3;
            this.devolver.Text = "label1";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(14, 461);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(89, 36);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancelar Pedido";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // printbutton
            // 
            this.printbutton.Location = new System.Drawing.Point(171, 461);
            this.printbutton.Name = "printbutton";
            this.printbutton.Size = new System.Drawing.Size(76, 36);
            this.printbutton.TabIndex = 5;
            this.printbutton.Text = "Imprimir Ticket";
            this.printbutton.UseVisualStyleBackColor = true;
            this.printbutton.Click += new System.EventHandler(this.printbutton_Click);
            // 
            // ValidarButton
            // 
            this.ValidarButton.Location = new System.Drawing.Point(308, 461);
            this.ValidarButton.Name = "ValidarButton";
            this.ValidarButton.Size = new System.Drawing.Size(66, 35);
            this.ValidarButton.TabIndex = 4;
            this.ValidarButton.Text = "Validar";
            this.ValidarButton.UseVisualStyleBackColor = true;
            this.ValidarButton.Click += new System.EventHandler(this.ValidarButton_Click);
            // 
            // listPedido
            // 
            this.listPedido.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.listPedido.ColumnCount = 4;
            this.listPedido.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.listPedido.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.listPedido.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.listPedido.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.listPedido.Location = new System.Drawing.Point(14, 37);
            this.listPedido.Name = "listPedido";
            this.listPedido.RowCount = 2;
            this.listPedido.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.listPedido.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.listPedido.Size = new System.Drawing.Size(360, 278);
            this.listPedido.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Pedido Detallado";
            // 
            // pedidoFinal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 509);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listPedido);
            this.Controls.Add(this.ValidarButton);
            this.Controls.Add(this.printbutton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.devolver);
            this.Controls.Add(this.entregado);
            this.Controls.Add(this.total);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "pedidoFinal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "pedidoFinal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label entregado;
        private System.Windows.Forms.Label devolver;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button printbutton;
        private System.Windows.Forms.Button ValidarButton;
        private System.Windows.Forms.TableLayoutPanel listPedido;
        private System.Windows.Forms.Label label1;
    }
}