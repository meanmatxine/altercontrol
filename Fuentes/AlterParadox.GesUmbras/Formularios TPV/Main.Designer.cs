﻿namespace AlterParadox.GesUmbras
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.precioTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCobrar = new System.Windows.Forms.Button();
            this.productosPanel = new System.Windows.Forms.Panel();
            this.panelTipos = new System.Windows.Forms.Panel();
            this.App = new System.Windows.Forms.Label();
            this.newPedido = new System.Windows.Forms.Button();
            this.contarCajaB = new System.Windows.Forms.Button();
            this.btn_cobrarDirecto = new System.Windows.Forms.Button();
            this.logoLabel = new System.Windows.Forms.Label();
            this.datosPedidoGrid = new System.Windows.Forms.DataGridView();
            this.Quitar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripPedidos = new System.Windows.Forms.ToolStripDropDownButton();
            this.nuevoPedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.verPedidosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.modificarProductosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.sacarMeterDineroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimientosDeCajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.pedidosPorFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidosPorProductoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturaciónFinalUmbrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_cobrarExacto = new System.Windows.Forms.Button();
            this.intTodo = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.calcBack = new System.Windows.Forms.Button();
            this.calcComa = new System.Windows.Forms.Button();
            this.calc3 = new System.Windows.Forms.Button();
            this.calc6 = new System.Windows.Forms.Button();
            this.calc9 = new System.Windows.Forms.Button();
            this.calc0 = new System.Windows.Forms.Button();
            this.calc2 = new System.Windows.Forms.Button();
            this.calc5 = new System.Windows.Forms.Button();
            this.calc8 = new System.Windows.Forms.Button();
            this.calcCLR = new System.Windows.Forms.Button();
            this.calc1 = new System.Windows.Forms.Button();
            this.calc4 = new System.Windows.Forms.Button();
            this.calc7 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.datosPedidoGrid)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // precioTotal
            // 
            this.precioTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.precioTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.precioTotal.Enabled = false;
            this.precioTotal.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.precioTotal.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.precioTotal.Location = new System.Drawing.Point(271, 370);
            this.precioTotal.Name = "precioTotal";
            this.precioTotal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.precioTotal.Size = new System.Drawing.Size(59, 28);
            this.precioTotal.TabIndex = 13;
            this.precioTotal.Text = "15 €";
            this.precioTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.precioTotal.UseMnemonic = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(213, 369);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(52, 28);
            this.label1.TabIndex = 4;
            this.label1.Text = "Total:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.UseMnemonic = false;
            // 
            // btnCobrar
            // 
            this.btnCobrar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnCobrar.Location = new System.Drawing.Point(494, 4);
            this.btnCobrar.Name = "btnCobrar";
            this.btnCobrar.Size = new System.Drawing.Size(136, 43);
            this.btnCobrar.TabIndex = 3;
            this.btnCobrar.Text = "COBRAR";
            this.btnCobrar.UseVisualStyleBackColor = true;
            this.btnCobrar.Click += new System.EventHandler(this.pedidoFinalizar_Click);
            // 
            // productosPanel
            // 
            this.productosPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.productosPanel.AutoScroll = true;
            this.productosPanel.Location = new System.Drawing.Point(422, 67);
            this.productosPanel.Name = "productosPanel";
            this.productosPanel.Size = new System.Drawing.Size(400, 463);
            this.productosPanel.TabIndex = 40;
            // 
            // panelTipos
            // 
            this.panelTipos.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelTipos.Location = new System.Drawing.Point(422, 11);
            this.panelTipos.Name = "panelTipos";
            this.panelTipos.Size = new System.Drawing.Size(400, 50);
            this.panelTipos.TabIndex = 7;
            // 
            // App
            // 
            this.App.AutoSize = true;
            this.App.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.App.Location = new System.Drawing.Point(155, 658);
            this.App.Name = "App";
            this.App.Size = new System.Drawing.Size(313, 31);
            this.App.TabIndex = 10;
            this.App.Text = "TPV Umbras de Paradox";
            this.App.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newPedido
            // 
            this.newPedido.Location = new System.Drawing.Point(12, 27);
            this.newPedido.Name = "newPedido";
            this.newPedido.Size = new System.Drawing.Size(97, 27);
            this.newPedido.TabIndex = 12;
            this.newPedido.Text = "Pedido Nuevo";
            this.newPedido.UseVisualStyleBackColor = true;
            this.newPedido.Click += new System.EventHandler(this.newPedido_Click);
            // 
            // contarCajaB
            // 
            this.contarCajaB.Location = new System.Drawing.Point(115, 27);
            this.contarCajaB.Name = "contarCajaB";
            this.contarCajaB.Size = new System.Drawing.Size(107, 27);
            this.contarCajaB.TabIndex = 14;
            this.contarCajaB.Text = "Contar Caja";
            this.contarCajaB.UseVisualStyleBackColor = true;
            this.contarCajaB.Click += new System.EventHandler(this.finSesionB_Click);
            // 
            // btn_cobrarDirecto
            // 
            this.btn_cobrarDirecto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cobrarDirecto.Location = new System.Drawing.Point(233, 5);
            this.btn_cobrarDirecto.Name = "btn_cobrarDirecto";
            this.btn_cobrarDirecto.Size = new System.Drawing.Size(157, 43);
            this.btn_cobrarDirecto.TabIndex = 4;
            this.btn_cobrarDirecto.Text = "COBRAR Y NUEVO PEDIDO";
            this.btn_cobrarDirecto.UseVisualStyleBackColor = true;
            this.btn_cobrarDirecto.Click += new System.EventHandler(this.cobrarDirecto_Click);
            // 
            // logoLabel
            // 
            this.logoLabel.Location = new System.Drawing.Point(12, 628);
            this.logoLabel.Name = "logoLabel";
            this.logoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.logoLabel.Size = new System.Drawing.Size(137, 92);
            this.logoLabel.TabIndex = 11;
            // 
            // datosPedidoGrid
            // 
            this.datosPedidoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosPedidoGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quitar});
            this.datosPedidoGrid.Location = new System.Drawing.Point(12, 60);
            this.datosPedidoGrid.MaximumSize = new System.Drawing.Size(320, 300);
            this.datosPedidoGrid.MinimumSize = new System.Drawing.Size(320, 300);
            this.datosPedidoGrid.Name = "datosPedidoGrid";
            this.datosPedidoGrid.Size = new System.Drawing.Size(320, 300);
            this.datosPedidoGrid.TabIndex = 17;
            this.datosPedidoGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.datosPedidoGrid_CellContentClick);
            // 
            // Quitar
            // 
            this.Quitar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Quitar.HeaderText = "Quitar";
            this.Quitar.Name = "Quitar";
            this.Quitar.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Quitar.Text = "X";
            this.Quitar.ToolTipText = "Quitar";
            this.Quitar.UseColumnTextForButtonValue = true;
            this.Quitar.Width = 41;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripPedidos,
            this.toolStripLabel2,
            this.toolStripLabel4,
            this.toolStripLabel3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(840, 25);
            this.toolStrip1.TabIndex = 41;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripPedidos
            // 
            this.toolStripPedidos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoPedidoToolStripMenuItem1,
            this.verPedidosToolStripMenuItem1});
            this.toolStripPedidos.Name = "toolStripPedidos";
            this.toolStripPedidos.Size = new System.Drawing.Size(62, 22);
            this.toolStripPedidos.Text = "Pedidos";
            // 
            // nuevoPedidoToolStripMenuItem1
            // 
            this.nuevoPedidoToolStripMenuItem1.Name = "nuevoPedidoToolStripMenuItem1";
            this.nuevoPedidoToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.nuevoPedidoToolStripMenuItem1.Text = "Nuevo Pedido";
            this.nuevoPedidoToolStripMenuItem1.Click += new System.EventHandler(this.nuevoPedidoToolStripMenuItem1_Click);
            // 
            // verPedidosToolStripMenuItem1
            // 
            this.verPedidosToolStripMenuItem1.Name = "verPedidosToolStripMenuItem1";
            this.verPedidosToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.verPedidosToolStripMenuItem1.Text = "Ver Pedidos";
            this.verPedidosToolStripMenuItem1.Click += new System.EventHandler(this.verPedidosToolStripMenuItem1_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modificarProductosToolStripMenuItem1});
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(74, 22);
            this.toolStripLabel2.Text = "Productos";
            // 
            // modificarProductosToolStripMenuItem1
            // 
            this.modificarProductosToolStripMenuItem1.Name = "modificarProductosToolStripMenuItem1";
            this.modificarProductosToolStripMenuItem1.Size = new System.Drawing.Size(182, 22);
            this.modificarProductosToolStripMenuItem1.Text = "Modificar Productos";
            this.modificarProductosToolStripMenuItem1.Click += new System.EventHandler(this.modificarProductosToolStripMenuItem1_Click);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sacarMeterDineroToolStripMenuItem,
            this.movimientosDeCajaToolStripMenuItem});
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(55, 22);
            this.toolStripLabel4.Text = "Dinero";
            // 
            // sacarMeterDineroToolStripMenuItem
            // 
            this.sacarMeterDineroToolStripMenuItem.Name = "sacarMeterDineroToolStripMenuItem";
            this.sacarMeterDineroToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.sacarMeterDineroToolStripMenuItem.Text = "Sacar/Meter  Dinero";
            this.sacarMeterDineroToolStripMenuItem.Click += new System.EventHandler(this.sacarMeterDineroToolStripMenuItem_Click);
            // 
            // movimientosDeCajaToolStripMenuItem
            // 
            this.movimientosDeCajaToolStripMenuItem.Name = "movimientosDeCajaToolStripMenuItem";
            this.movimientosDeCajaToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.movimientosDeCajaToolStripMenuItem.Text = "Movimientos de Caja";
            this.movimientosDeCajaToolStripMenuItem.Click += new System.EventHandler(this.movimientosDeCajaToolStripMenuItem_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidosPorFechaToolStripMenuItem,
            this.pedidosPorProductoToolStripMenuItem,
            this.facturaciónFinalUmbrasToolStripMenuItem});
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(67, 22);
            this.toolStripLabel3.Text = "Informes";
            // 
            // pedidosPorFechaToolStripMenuItem
            // 
            this.pedidosPorFechaToolStripMenuItem.Name = "pedidosPorFechaToolStripMenuItem";
            this.pedidosPorFechaToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.pedidosPorFechaToolStripMenuItem.Text = "Pedidos Por fecha";
            this.pedidosPorFechaToolStripMenuItem.Click += new System.EventHandler(this.pedidosPorFechaToolStripMenuItem_Click);
            // 
            // pedidosPorProductoToolStripMenuItem
            // 
            this.pedidosPorProductoToolStripMenuItem.Name = "pedidosPorProductoToolStripMenuItem";
            this.pedidosPorProductoToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.pedidosPorProductoToolStripMenuItem.Text = "Pedidos Por Producto";
            this.pedidosPorProductoToolStripMenuItem.Click += new System.EventHandler(this.pedidosPorProductoToolStripMenuItem_Click);
            // 
            // facturaciónFinalUmbrasToolStripMenuItem
            // 
            this.facturaciónFinalUmbrasToolStripMenuItem.Name = "facturaciónFinalUmbrasToolStripMenuItem";
            this.facturaciónFinalUmbrasToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.facturaciónFinalUmbrasToolStripMenuItem.Text = "Facturación final Umbras";
            this.facturaciónFinalUmbrasToolStripMenuItem.Click += new System.EventHandler(this.FacturaciónFinalUmbrasToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btn_cobrarExacto);
            this.panel1.Controls.Add(this.btn_cobrarDirecto);
            this.panel1.Controls.Add(this.btnCobrar);
            this.panel1.Location = new System.Drawing.Point(189, 536);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(633, 50);
            this.panel1.TabIndex = 42;
            // 
            // btn_cobrarExacto
            // 
            this.btn_cobrarExacto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cobrarExacto.Location = new System.Drawing.Point(3, 4);
            this.btn_cobrarExacto.Name = "btn_cobrarExacto";
            this.btn_cobrarExacto.Size = new System.Drawing.Size(157, 43);
            this.btn_cobrarExacto.TabIndex = 5;
            this.btn_cobrarExacto.Text = "IMPORTE EXACTO";
            this.btn_cobrarExacto.UseVisualStyleBackColor = true;
            this.btn_cobrarExacto.Click += new System.EventHandler(this.btn_cobrarExacto_Click);
            // 
            // intTodo
            // 
            this.intTodo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intTodo.Location = new System.Drawing.Point(12, 374);
            this.intTodo.Margin = new System.Windows.Forms.Padding(2);
            this.intTodo.Name = "intTodo";
            this.intTodo.Size = new System.Drawing.Size(188, 23);
            this.intTodo.TabIndex = 43;
            this.intTodo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.intTodo_KeyPress);
            this.intTodo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.intTodo_MouseDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.calcBack);
            this.panel2.Controls.Add(this.calcComa);
            this.panel2.Controls.Add(this.calc3);
            this.panel2.Controls.Add(this.calc6);
            this.panel2.Controls.Add(this.calc9);
            this.panel2.Controls.Add(this.calc0);
            this.panel2.Controls.Add(this.calc2);
            this.panel2.Controls.Add(this.calc5);
            this.panel2.Controls.Add(this.calc8);
            this.panel2.Controls.Add(this.calcCLR);
            this.panel2.Controls.Add(this.calc1);
            this.panel2.Controls.Add(this.calc4);
            this.panel2.Controls.Add(this.calc7);
            this.panel2.Location = new System.Drawing.Point(12, 402);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(172, 184);
            this.panel2.TabIndex = 44;
            // 
            // calcBack
            // 
            this.calcBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcBack.Location = new System.Drawing.Point(128, 2);
            this.calcBack.Margin = new System.Windows.Forms.Padding(2);
            this.calcBack.Name = "calcBack";
            this.calcBack.Size = new System.Drawing.Size(38, 41);
            this.calcBack.TabIndex = 12;
            this.calcBack.Text = "<-";
            this.calcBack.UseVisualStyleBackColor = true;
            this.calcBack.Click += new System.EventHandler(this.calcBack_Click);
            // 
            // calcComa
            // 
            this.calcComa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcComa.Location = new System.Drawing.Point(86, 139);
            this.calcComa.Margin = new System.Windows.Forms.Padding(2);
            this.calcComa.Name = "calcComa";
            this.calcComa.Size = new System.Drawing.Size(38, 41);
            this.calcComa.TabIndex = 11;
            this.calcComa.Text = ",";
            this.calcComa.UseVisualStyleBackColor = true;
            this.calcComa.Click += new System.EventHandler(this.calcComa_Click);
            // 
            // calc3
            // 
            this.calc3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc3.Location = new System.Drawing.Point(86, 93);
            this.calc3.Margin = new System.Windows.Forms.Padding(2);
            this.calc3.Name = "calc3";
            this.calc3.Size = new System.Drawing.Size(38, 41);
            this.calc3.TabIndex = 10;
            this.calc3.Text = "3";
            this.calc3.UseVisualStyleBackColor = true;
            this.calc3.Click += new System.EventHandler(this.calc3_Click);
            // 
            // calc6
            // 
            this.calc6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc6.Location = new System.Drawing.Point(86, 48);
            this.calc6.Margin = new System.Windows.Forms.Padding(2);
            this.calc6.Name = "calc6";
            this.calc6.Size = new System.Drawing.Size(38, 41);
            this.calc6.TabIndex = 9;
            this.calc6.Text = "6";
            this.calc6.UseVisualStyleBackColor = true;
            this.calc6.Click += new System.EventHandler(this.calc6_Click);
            // 
            // calc9
            // 
            this.calc9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc9.Location = new System.Drawing.Point(86, 2);
            this.calc9.Margin = new System.Windows.Forms.Padding(2);
            this.calc9.Name = "calc9";
            this.calc9.Size = new System.Drawing.Size(38, 41);
            this.calc9.TabIndex = 8;
            this.calc9.Text = "9";
            this.calc9.UseVisualStyleBackColor = true;
            this.calc9.Click += new System.EventHandler(this.calc9_Click);
            // 
            // calc0
            // 
            this.calc0.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc0.Location = new System.Drawing.Point(44, 139);
            this.calc0.Margin = new System.Windows.Forms.Padding(2);
            this.calc0.Name = "calc0";
            this.calc0.Size = new System.Drawing.Size(38, 41);
            this.calc0.TabIndex = 7;
            this.calc0.Text = "0";
            this.calc0.UseVisualStyleBackColor = true;
            this.calc0.Click += new System.EventHandler(this.calc0_Click);
            // 
            // calc2
            // 
            this.calc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc2.Location = new System.Drawing.Point(44, 93);
            this.calc2.Margin = new System.Windows.Forms.Padding(2);
            this.calc2.Name = "calc2";
            this.calc2.Size = new System.Drawing.Size(38, 41);
            this.calc2.TabIndex = 6;
            this.calc2.Text = "2";
            this.calc2.UseVisualStyleBackColor = true;
            this.calc2.Click += new System.EventHandler(this.calc2_Click);
            // 
            // calc5
            // 
            this.calc5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc5.Location = new System.Drawing.Point(44, 48);
            this.calc5.Margin = new System.Windows.Forms.Padding(2);
            this.calc5.Name = "calc5";
            this.calc5.Size = new System.Drawing.Size(38, 41);
            this.calc5.TabIndex = 5;
            this.calc5.Text = "5";
            this.calc5.UseVisualStyleBackColor = true;
            this.calc5.Click += new System.EventHandler(this.calc5_Click);
            // 
            // calc8
            // 
            this.calc8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc8.Location = new System.Drawing.Point(44, 2);
            this.calc8.Margin = new System.Windows.Forms.Padding(2);
            this.calc8.Name = "calc8";
            this.calc8.Size = new System.Drawing.Size(38, 41);
            this.calc8.TabIndex = 4;
            this.calc8.Text = "8";
            this.calc8.UseVisualStyleBackColor = true;
            this.calc8.Click += new System.EventHandler(this.calc8_Click);
            // 
            // calcCLR
            // 
            this.calcCLR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcCLR.Location = new System.Drawing.Point(2, 139);
            this.calcCLR.Margin = new System.Windows.Forms.Padding(2);
            this.calcCLR.Name = "calcCLR";
            this.calcCLR.Size = new System.Drawing.Size(38, 41);
            this.calcCLR.TabIndex = 3;
            this.calcCLR.Text = "clr";
            this.calcCLR.UseVisualStyleBackColor = true;
            this.calcCLR.Click += new System.EventHandler(this.calcCLR_Click);
            // 
            // calc1
            // 
            this.calc1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc1.Location = new System.Drawing.Point(2, 93);
            this.calc1.Margin = new System.Windows.Forms.Padding(2);
            this.calc1.Name = "calc1";
            this.calc1.Size = new System.Drawing.Size(38, 41);
            this.calc1.TabIndex = 2;
            this.calc1.Text = "1";
            this.calc1.UseVisualStyleBackColor = true;
            this.calc1.Click += new System.EventHandler(this.calc1_Click);
            // 
            // calc4
            // 
            this.calc4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc4.Location = new System.Drawing.Point(2, 48);
            this.calc4.Margin = new System.Windows.Forms.Padding(2);
            this.calc4.Name = "calc4";
            this.calc4.Size = new System.Drawing.Size(38, 41);
            this.calc4.TabIndex = 1;
            this.calc4.Text = "4";
            this.calc4.UseVisualStyleBackColor = true;
            this.calc4.Click += new System.EventHandler(this.calc4_Click);
            // 
            // calc7
            // 
            this.calc7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calc7.Location = new System.Drawing.Point(2, 2);
            this.calc7.Margin = new System.Windows.Forms.Padding(2);
            this.calc7.Name = "calc7";
            this.calc7.Size = new System.Drawing.Size(38, 41);
            this.calc7.TabIndex = 0;
            this.calc7.Text = "7";
            this.calc7.UseVisualStyleBackColor = true;
            this.calc7.Click += new System.EventHandler(this.calc7_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(194, 402);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 127);
            this.label2.TabIndex = 45;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(840, 591);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.intTodo);
            this.Controls.Add(this.precioTotal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.datosPedidoGrid);
            this.Controls.Add(this.contarCajaB);
            this.Controls.Add(this.newPedido);
            this.Controls.Add(this.logoLabel);
            this.Controls.Add(this.App);
            this.Controls.Add(this.panelTipos);
            this.Controls.Add(this.productosPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "UmbrasTPV";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.datosPedidoGrid)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label precioTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCobrar;
        private System.Windows.Forms.Panel productosPanel;
        private System.Windows.Forms.Panel panelTipos;
        private System.Windows.Forms.Label App;
        private System.Windows.Forms.Label logoLabel;
        private System.Windows.Forms.Button newPedido;
        private System.Windows.Forms.Button contarCajaB;
        private System.Windows.Forms.Button btn_cobrarDirecto;
        private System.Windows.Forms.DataGridView datosPedidoGrid;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripPedidos;
        private System.Windows.Forms.ToolStripMenuItem nuevoPedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem verPedidosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel2;
        private System.Windows.Forms.ToolStripMenuItem modificarProductosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel3;
        private System.Windows.Forms.ToolStripDropDownButton toolStripLabel4;
        private System.Windows.Forms.ToolStripMenuItem sacarMeterDineroToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewButtonColumn Quitar;
        private System.Windows.Forms.ToolStripMenuItem movimientosDeCajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidosPorFechaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidosPorProductoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturaciónFinalUmbrasToolStripMenuItem;
        private System.Windows.Forms.TextBox intTodo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button calcBack;
        private System.Windows.Forms.Button calcComa;
        private System.Windows.Forms.Button calc3;
        private System.Windows.Forms.Button calc6;
        private System.Windows.Forms.Button calc9;
        private System.Windows.Forms.Button calc0;
        private System.Windows.Forms.Button calc2;
        private System.Windows.Forms.Button calc5;
        private System.Windows.Forms.Button calc8;
        private System.Windows.Forms.Button calcCLR;
        private System.Windows.Forms.Button calc1;
        private System.Windows.Forms.Button calc4;
        private System.Windows.Forms.Button calc7;
        private System.Windows.Forms.Button btn_cobrarExacto;
        private System.Windows.Forms.Label label2;
    }
}

