﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.Objects;
using AlterParadox.GesUmbras.ProductService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
 * tipo Pedidos Por fecha->pFecha
 * tipo pedidos por producto -> pProducto
 * tipo facturacion umbras -> facturacion
 * 
 */
namespace AlterParadox.GesUmbras.Formularios_TPV
{
    public partial class Informes : Form
    {
        string printTitle;
        string printSubTitle;

        public Informes(string tipo)
        {
            InitializeComponent();

            switch (tipo)
            {
                case "pFecha":
                    pedidosPorFecha();
                    break;
                case "pProducto":
                    pedidosPorProducto();
                    break;
                case "facturacion":
                    facturacion();
                    break;
            }
        }

        //PEDIDOS POR FECHA
        private void pedidosPorFecha()
        {
            pFechaPanel.Visible = true;
            printTitle = "Pedidos por Fecha";   
        }

        //PEDIDOS POR PRODUCTO
        private void pedidosPorProducto()
        {
            pProductoPanel.Visible = true;
            printTitle = "Pedidos por Producto";
            //coger productos
            List<Product> productos=new List<Product>();
            try
            {
                using (ProductServiceClient svc = new ProductServiceClient())
                {
                    productos = svc.GetProducts().ListaProductos.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //productos =Producto.SELECT_ALL_LIST();
            foreach (Product p in productos)
            {
                productosCombo.Items.Add(p.productoName);
            } 
            printSubTitle = "Pedidos de producto X";
        }

        //FACTURACION
        private void facturacion()
        {
            
        }

        //buscar por fecha
        private void buscarFechaButton_Click(object sender, EventArgs e)
        {
            //coger fecha inicio
            string fInicios = fechaInicio.ToString().Split(' ')[2].Split('/')[0];
            string fFinals = fechaFin.ToString().Split(' ')[2].Split('/')[0];
            printSubTitle = "Pedidos de fecha "+fInicios+"/08/2018 a fecha "+fFinals+"/08/2018";
            int i = int.Parse(fFinals);
            i += 1;
            fFinals =i.ToString();
            //Console.WriteLine("inicio: "+fInicios);
            //Console.WriteLine("fin: "+ fFinals);
            DataTable dt = Pedido.SELECT_DATES(fInicios, fFinals);
            datosgrid.DataSource = dt;
        }

        //buscar por producto
        private void button1_Click(object sender, EventArgs e)
        {
            
            /*if (productosCombo.SelectedItem != null)
            {
                //coger producto
                string pros = productosCombo.SelectedItem.ToString();
                Producto p = Producto.SELECT_BY_NAME(pros);
                Pedido.SELECT_BY_PRODUCT(p.productoID);
            }*/
        }

        //IMPRIMIR
        private void printButton_Click(object sender, EventArgs e)
        {
            DGVPrinter printer = new DGVPrinter();
            printer.Title = printTitle;
            printer.SubTitle = printSubTitle;
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = false;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.Footer = "Alter Paradox";
            printer.FooterSpacing = 15;
            printer.PageSettings.Landscape = true;
            printer.PrintDataGridView(datosgrid);
        }

        
    }
}
