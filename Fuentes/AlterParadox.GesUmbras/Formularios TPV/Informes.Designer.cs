﻿namespace AlterParadox.GesUmbras.Formularios_TPV
{
    partial class Informes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pFechaPanel = new System.Windows.Forms.Panel();
            this.buscarFechaButton = new System.Windows.Forms.Button();
            this.fechaFin = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.fechaInicio = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pProductoPanel = new System.Windows.Forms.Panel();
            this.buscarProdButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.productosCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pFacturaPanel = new System.Windows.Forms.Panel();
            this.datosgrid = new System.Windows.Forms.DataGridView();
            this.printButton = new System.Windows.Forms.Button();
            this.pFechaPanel.SuspendLayout();
            this.pProductoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datosgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // pFechaPanel
            // 
            this.pFechaPanel.Controls.Add(this.buscarFechaButton);
            this.pFechaPanel.Controls.Add(this.fechaFin);
            this.pFechaPanel.Controls.Add(this.label3);
            this.pFechaPanel.Controls.Add(this.fechaInicio);
            this.pFechaPanel.Controls.Add(this.label2);
            this.pFechaPanel.Controls.Add(this.label1);
            this.pFechaPanel.Location = new System.Drawing.Point(14, 13);
            this.pFechaPanel.Name = "pFechaPanel";
            this.pFechaPanel.Size = new System.Drawing.Size(288, 179);
            this.pFechaPanel.TabIndex = 0;
            this.pFechaPanel.Visible = false;
            // 
            // buscarFechaButton
            // 
            this.buscarFechaButton.Location = new System.Drawing.Point(92, 103);
            this.buscarFechaButton.Name = "buscarFechaButton";
            this.buscarFechaButton.Size = new System.Drawing.Size(125, 23);
            this.buscarFechaButton.TabIndex = 5;
            this.buscarFechaButton.Text = "Buscar";
            this.buscarFechaButton.UseVisualStyleBackColor = true;
            this.buscarFechaButton.Click += new System.EventHandler(this.buscarFechaButton_Click);
            // 
            // fechaFin
            // 
            this.fechaFin.Location = new System.Drawing.Point(80, 64);
            this.fechaFin.MaxDate = new System.DateTime(2018, 8, 21, 0, 0, 0, 0);
            this.fechaFin.MinDate = new System.DateTime(2018, 8, 13, 0, 0, 0, 0);
            this.fechaFin.Name = "fechaFin";
            this.fechaFin.Size = new System.Drawing.Size(195, 20);
            this.fechaFin.TabIndex = 4;
            this.fechaFin.Value = new System.DateTime(2018, 8, 13, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Fecha Fin";
            // 
            // fechaInicio
            // 
            this.fechaInicio.Location = new System.Drawing.Point(80, 38);
            this.fechaInicio.MaxDate = new System.DateTime(2018, 8, 21, 0, 0, 0, 0);
            this.fechaInicio.MinDate = new System.DateTime(2018, 8, 13, 0, 0, 0, 0);
            this.fechaInicio.Name = "fechaInicio";
            this.fechaInicio.Size = new System.Drawing.Size(195, 20);
            this.fechaInicio.TabIndex = 2;
            this.fechaInicio.Value = new System.DateTime(2018, 8, 13, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Fecha Inicio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(89, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pedidos Por Fecha";
            // 
            // pProductoPanel
            // 
            this.pProductoPanel.Controls.Add(this.buscarProdButton);
            this.pProductoPanel.Controls.Add(this.label5);
            this.pProductoPanel.Controls.Add(this.productosCombo);
            this.pProductoPanel.Controls.Add(this.label4);
            this.pProductoPanel.Location = new System.Drawing.Point(14, 198);
            this.pProductoPanel.Name = "pProductoPanel";
            this.pProductoPanel.Size = new System.Drawing.Size(288, 179);
            this.pProductoPanel.TabIndex = 1;
            this.pProductoPanel.Visible = false;
            // 
            // buscarProdButton
            // 
            this.buscarProdButton.Location = new System.Drawing.Point(80, 107);
            this.buscarProdButton.Name = "buscarProdButton";
            this.buscarProdButton.Size = new System.Drawing.Size(143, 23);
            this.buscarProdButton.TabIndex = 6;
            this.buscarProdButton.Text = "Buscar";
            this.buscarProdButton.UseVisualStyleBackColor = true;
            this.buscarProdButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Producto";
            // 
            // productosCombo
            // 
            this.productosCombo.FormattingEnabled = true;
            this.productosCombo.Location = new System.Drawing.Point(80, 54);
            this.productosCombo.Name = "productosCombo";
            this.productosCombo.Size = new System.Drawing.Size(195, 21);
            this.productosCombo.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(77, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Pedidos Por Producto";
            // 
            // pFacturaPanel
            // 
            this.pFacturaPanel.Location = new System.Drawing.Point(14, 383);
            this.pFacturaPanel.Name = "pFacturaPanel";
            this.pFacturaPanel.Size = new System.Drawing.Size(288, 179);
            this.pFacturaPanel.TabIndex = 2;
            this.pFacturaPanel.Visible = false;
            // 
            // datosgrid
            // 
            this.datosgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datosgrid.Location = new System.Drawing.Point(318, 13);
            this.datosgrid.Name = "datosgrid";
            this.datosgrid.Size = new System.Drawing.Size(631, 508);
            this.datosgrid.TabIndex = 3;
            // 
            // printButton
            // 
            this.printButton.Location = new System.Drawing.Point(874, 539);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 4;
            this.printButton.Text = "Imprimir";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // Informes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 570);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.datosgrid);
            this.Controls.Add(this.pFacturaPanel);
            this.Controls.Add(this.pProductoPanel);
            this.Controls.Add(this.pFechaPanel);
            this.Name = "Informes";
            this.Text = "Informes";
            this.pFechaPanel.ResumeLayout(false);
            this.pFechaPanel.PerformLayout();
            this.pProductoPanel.ResumeLayout(false);
            this.pProductoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datosgrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pFechaPanel;
        private System.Windows.Forms.Button buscarFechaButton;
        private System.Windows.Forms.DateTimePicker fechaFin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker fechaInicio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pProductoPanel;
        private System.Windows.Forms.Button buscarProdButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox productosCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pFacturaPanel;
        private System.Windows.Forms.DataGridView datosgrid;
        private System.Windows.Forms.Button printButton;
    }
}