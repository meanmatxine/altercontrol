﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Globalization;

using AlterParadox.GesUmbras.Objects;
using AlterParadox.Core;

namespace AlterParadox.GesUmbras
{
    public partial class pedidoFinal : Form
    {
        Pedido pedido;
        List<Product> pDetalle;
        //pedido cancelado
        public bool pedidoCancelado;
        int alturaLinea = 20;

        //constructor
        public pedidoFinal(Pedido p, List<Product> pDet)
        {
            InitializeComponent();
            pedidoCancelado = false;
            pedido = p;
            Console.WriteLine(pedido.pedidoTotal+"--"+p.pedidoTotal);
            pDetalle = new List<Product>();
            pDetalle = pDet;
            listPedido.RowCount = 1;
            listPedido.RowStyles.Clear();
            listPedido.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
            listPedido.Controls.Add(new Label() { Text = "Producto" }, 0, 0);
            listPedido.Controls.Add(new Label() { Text = "Cant" }, 1, 0);
            listPedido.Controls.Add(new Label() { Text = "Precio ud" }, 2, 0);
            listPedido.Controls.Add(new Label() { Text = "Total" }, 3, 0);
            foreach (Product pd in pDet) {
                if (pd.productoCantidad > 0) {
                    int linea = listPedido.RowCount;
                    listPedido.RowStyles.Add(new RowStyle(SizeType.Absolute, 20f));
                    listPedido.Controls.Add(new Label() { Text = pd.productoName }, 0, linea);
                    listPedido.Controls.Add(new Label() { Text = pd.productoCantidad.ToString() }, 1, linea);
                    listPedido.Controls.Add(new Label() { Text = pd.productoPrecio.ToString() }, 2, linea);
                    listPedido.Controls.Add(new Label() { Text = (pd.productoPrecio * pd.productoCantidad).ToString() }, 3, linea);
                    listPedido.RowCount++; 
                }
            }
            //Total
            total.Text = "Total: " + p.pedidoTotal.ToString();
            //entregado
            entregado.Text = "Entregado: " + p.pedidoEntregado.ToString();
            //a devolver
            devolver.Text = "A Devolver: " + p.pedidoDevuelto.ToString();
            ValidarButton.Focus();
        }

        //cancelar pedido
        private void cancelButton_Click(object sender, EventArgs e)
        {
            pedidoCancelado = true;
            this.Close();
        }

        //validar agregar a la base de datos y volver al formulario
        private void ValidarButton_Click(object sender, EventArgs e)
        {
            //insertamos pedido en la dbc
            insertarPedido();
            //cerrarmos y volvemos
            this.Close();
           
        }

        //imprimir ticket
        private void printbutton_Click(object sender, EventArgs e)
        {
            //insertamos pedido en la dbc
            insertarPedido();
            //impresion
            PrintDocument pd = new PrintDocument();
            //alturaPapel = calcularAltura();
            PaperSize ps = new PaperSize("A4", 850, 1100);
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.PrintController = new StandardPrintController();
            pd.DefaultPageSettings.Margins.Left = 0;
            pd.DefaultPageSettings.Margins.Right = 0;
            pd.DefaultPageSettings.Margins.Top = 0;
            pd.DefaultPageSettings.Margins.Bottom = 0;
            pd.DefaultPageSettings.PaperSize = ps;
            //pruebas
            PrintDialog pDialog = new PrintDialog();
            pDialog.Document = pd;
            DialogResult res = pDialog.ShowDialog();
            if (res == DialogResult.OK) {
                pd.Print();
                this.Close();
            }
        }


        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            //cogerpedido
            Pedido pe = Pedido.GetLast();
            Graphics g = e.Graphics;
            //g.DrawRectangle(Pens.Black, 5, 5, 240, alturaPapel-5);
            Console.WriteLine("path: "+Application.StartupPath);
            g.DrawImage(Properties.Resources.Logotipo150, 20, 10);
            Font fBody = new Font("LucidaConsole", 11, FontStyle.Regular);
            Font fTitle = new Font("LucidaConsole", 14, FontStyle.Bold);
            SolidBrush sb = new SolidBrush(Color.Black);
            g.DrawString("Alter Paradox", fTitle, sb, 220, 50);
            g.DrawString("XI Umbras de Paradox - 2018", fBody, sb, 220, 70);
            //fecha y factura
            DateTime localDate = DateTime.Now;
            string culture = "dd/MM/yyyy HH:mm:ss";
            string fecha = localDate.ToString(culture);
            g.DrawString("Fecha: " + fecha, fBody, sb, 10, 200);
            g.DrawString("Factura Pedido " + pe.pedidoID, fBody, sb, 10, 220);
            g.DrawString("------------------------------------------------------------", fBody, sb, 10, 230);
            //datos pedido
            int colProducto, colCant, colPrecio, colIva, colFinal, colTotal;
            colProducto = 10;
            colPrecio = 200;
            colIva = 300;
            colFinal = 450;
            colCant = 560;
            colTotal = 610;
            //tabla head
            g.DrawString("Producto", fTitle, sb,colProducto, 250);
            g.DrawString("Precio", fTitle, sb, colPrecio, 250);
            g.DrawString("IVA", fTitle, sb, colIva, 250);
            g.DrawString("Precio Final", fTitle, sb, colFinal, 250);
            g.DrawString("Cant", fTitle, sb, colCant, 250);
            g.DrawString("Total", fTitle, sb, colTotal, 250);
            //detalles pedido
            int lineaInicial = 280;
            int linea =0;
            foreach  (Product pde in pDetalle)
            {
                if (pde.productoCantidad > 0) {
                    g.DrawString(pde.productoName, fBody, sb, colProducto, lineaInicial + (linea * alturaLinea));
                    g.DrawString(pde.productoCantidad.ToString(), fBody, sb, colCant, lineaInicial + (linea * alturaLinea));
                    //decimal precioSIva = (calcularSinIva(pde));
                    g.DrawString(pde.productoPrecioSinIva + " €", fBody, sb, colPrecio, lineaInicial + (linea * alturaLinea));
                    decimal iva = calcularIva(pde);
                    g.DrawString("IVA (" + pde.productoIVA + "): " + iva + " €", fBody, sb, colIva, lineaInicial + (linea * alturaLinea));
                    g.DrawString(pde.productoPrecio+ " €", fBody, sb, colFinal, lineaInicial + (linea * alturaLinea));
                    g.DrawString(pde.productoCantidad * pde.productoPrecio + " €", fBody, sb, colTotal, lineaInicial + (linea * alturaLinea));
                    linea++;
                }  
            }
            g.DrawString("------------------------------------------------------------", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("TOTAL: " + pedido.pedidoTotal+" €", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("Entregado: " + pedido.pedidoEntregado+ "€", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("Devuelto: " + (pedido.pedidoEntregado - pedido.pedidoTotal)+ "€", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            g.Dispose();
        }

        int calcularAltura() {
            return 250+(pDetalle.Count*alturaLinea);
        }

        //calcular precio sin iva
        decimal calcularSinIva(Product p) {
            Decimal d = (decimal)p.productoPrecio / (1 + (p.productoIVA / 100));
            d = Math.Round(d, 2);
            return d;
        }

        //calcular IVA
        decimal calcularIva(Product p) {
            decimal d=(p.productoPrecioSinIva * p.productoIVA) / 100;
            d = Math.Round(d, 2);
            return d;
        }

        void insertarPedido() {
            //insertamos el pedido
            Pedido.INSERT(pedido);
            //cogemos el ultimo pedido
            Pedido ultimo = Pedido.GetLast();
            //agregamos el id al detalle e insertamos en DBC
            foreach (Product pd in pDetalle)
            {
                if (pd.productoCantidad > 0)
                {
                    PedidoDetalle pdet = new PedidoDetalle();
                    pdet.pedidoID = ultimo.pedidoID;
                    pdet.productoID = pd.productoID;
                    pdet.pdCantidad = pd.productoCantidad;
                    PedidoDetalle.Insert(pdet);
                }
            }
            //añadimos el movimiento de caja
            ControlCaja c = new ControlCaja();
            c.cajaMovFecha = ultimo.pedidoFecha;
            c.cajaPersona = "Caja";
            c.cajaIngreso = ultimo.pedidoTotal;
            c.cajaExtracto = 0;
            c.cajaConcepto = "Pedido";
            c.cajaObservaciones = "";
            ControlCaja ultimoMov = ControlCaja.ultimoMovimiento();
            c.cajaDineroActual = ultimoMov.cajaDineroActual+c.cajaIngreso;
            c.cajaDescuadre = ultimoMov.cajaDescuadre;
            ControlCaja.INSERT(c);
        }
    }
}
