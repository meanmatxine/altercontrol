﻿using AlterParadox.GesUmbras.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class CerrarSesion : Form
    {
        decimal totalReal;
        decimal descuadreFinal;
        decimal cajaFinal;
        ControlCaja cajaActual;


        public CerrarSesion()
        {
            InitializeComponent();
            iniciar();
        }

        private void iniciar()
        {
            totalReal = descuadreFinal = cajaFinal = 0;
            //coger ultimo movimiento de la caja
            cajaActual = ControlCaja.ultimoMovimiento();
            //caja actual
            textActual.Text = cajaActual.cajaDineroActual.ToString() + "€";
            //segun conteo
            textConteo.Text = "0€";
            //descuadre actual
            textDescuadrePre.Text = cajaActual.cajaDescuadre.ToString() + "€";
            //caja tras conteo
            textFinal.Text = cajaFinal + "€";
            //descuadre tras conteo
            descuadreFinal = cajaActual.cajaDescuadre + (totalReal- cajaActual.cajaDineroActual);
            textDescuadrePost.Text = descuadreFinal + "€";
        }

        // puntos x comas en numSacar
        private void numSacar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        // puntos x comas en numMeter
        private void numMeter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.Equals('.') || e.KeyChar.Equals(','))
            {
                e.KeyChar = ((CultureInfo)CultureInfo.CurrentCulture).NumberFormat.NumberDecimalSeparator.ToCharArray()[0];
            }
        }

        void calcularTotal() {
            //calcular labels
            decimal m001, m002, m005, m01, m02, m05, m1, m2, b5, b10, b20, b50=0;
            //001
            m001 = monedas001.Value * (decimal)0.01;
            total001.Text = m001 + "€";
            //002
            m002 = monedas002.Value * (decimal)0.02;
            total002.Text = m002 + "€";
            //005
            m005 = monedas005.Value * (decimal)0.05;
            total005.Text = m005 + "€";
            //01
            m01 = monedas01.Value*(decimal)0.1;
            total01.Text = m01 + "€";
            //02
            m02 = monedas02.Value * (decimal)0.2;
            total02.Text = m02 + "€";
            //05
            m05 = monedas05.Value * (decimal)0.5;
            total05.Text = m05 + "€";
            //1
            m1 = monedas1.Value * (decimal)1;
            total1.Text = m1 + "€";
            //2
            m2 = monedas2.Value * (decimal)2;
            total2.Text = m2 + "€";
            //5
            b5 = billete5.Value * (decimal)5;
            total5.Text = b5 + "€";
            //10
            b10 = billete10.Value * (decimal)10;
            total10.Text = b10 + "€";
            //20
            b20 = billete20.Value * (decimal)20;
            total20.Text = b20 + "€";
            //50
            b50 = billete50.Value * (decimal)50;
            total50.Text = b50 + "€";
            //calcularTotal
            totalReal = m001 + m002 + m005 + m01 + m02 + m05 + m1 + m2 + b5 + b10 + b20 + b50;
            //ajustar total
            //caja actual
            textActual.Text = cajaActual.cajaDineroActual.ToString() + "€";
            //segun conteo
            textConteo.Text = totalReal+"€";
            //descuadre actual
            textDescuadrePre.Text = cajaActual.cajaDescuadre.ToString() + "€";
            //caja tras conteo
            textFinal.Text = cajaFinal + "€";
            //descuadre tras conteo
            descuadreFinal = totalReal - cajaActual.cajaDineroActual;
            textDescuadrePost.Text = descuadreFinal + "€";
        }

        private void monedas001_ValueChanged(object sender, EventArgs e)
        {
            calcularTotal();
        }

        //validar
        private void button1_Click(object sender, EventArgs e)
        {
            calcularTotal();
            DateTime localDate = DateTime.Now;
            string culture = "dd/MM/yyyy HH:mm:ss";
            //hacer movimiento conteo
            ControlCaja movConteo = new ControlCaja();
            movConteo.cajaMovFecha = localDate.ToString(culture);
            movConteo.cajaPersona = "Conteo";
            movConteo.cajaIngreso = 0;
            movConteo.cajaExtracto = 0;
            movConteo.cajaConcepto = "Cuadre Caja";
            movConteo.cajaObservaciones = "";
            movConteo.cajaDineroActual = totalReal;
            movConteo.cajaDescuadre = descuadreFinal;
            ControlCaja.INSERT(movConteo);
            //iniciamos de nuevo
           ///ultimoagain
            ControlCaja ultimo = ControlCaja.ultimoMovimiento();
            //Metemos otro con descuadre a 0 para iniciar otra vez
            ControlCaja nuevo = new ControlCaja();
            nuevo.cajaMovFecha = localDate.ToString(culture);
            nuevo.cajaPersona = "Nueva Sesion";
            nuevo.cajaIngreso = 0;
            nuevo.cajaExtracto = 0;
            nuevo.cajaConcepto = "Nueva Sesión con descuadre a 0";
            nuevo.cajaObservaciones = "";
            nuevo.cajaDineroActual = totalReal;
            nuevo.cajaDescuadre = 0;
            ControlCaja.INSERT(nuevo);
            /*//Si se ha sacado dinero añadir un movimiento de sacar dinero
            if (numSacar.Value!=0)
            {
                ControlCaja movimiento = new ControlCaja();
                movimiento.cajaMovFecha = localDate.ToString(culture);
                movimiento.cajaIngreso = 0;
                movimiento.cajaExtracto = numSacar.Value;
                movimiento.cajaConcepto = "Extracto en Cuadre Caja";
                movimiento.cajaObservaciones = "";
                movimiento.cajaDescuadre = ultimo.cajaDescuadre;
                ControlCaja.INSERT(movimiento);
            }
            //Si se ha metido dinero añadir un movimiento de meter dinero
            if (numMeter.Value != 0)
            {
                ControlCaja movimiento = new ControlCaja();
                movimiento.cajaMovFecha = localDate.ToString(culture);
                movimiento.cajaIngreso = numMeter.Value;
                movimiento.cajaExtracto = 0;
                movimiento.cajaConcepto = "Ingreso en Cuadre Caja";
                movimiento.cajaObservaciones = "";
                movimiento.cajaDescuadre = ultimo.cajaDescuadre;
                ControlCaja.INSERT(movimiento);
            }*/
            //cerramos formulario
            this.Close();
        }

        private void monedas001_Click(object sender, EventArgs e)
        {

            NumericUpDown nd = sender as NumericUpDown;
            //nd.Select(0, 4);
            
        }

        private void monedas001_Enter(object sender, EventArgs e)
        {
            NumericUpDown n= sender as NumericUpDown;
            n.Select(0, n.Text.Length);
        }
    }
}
