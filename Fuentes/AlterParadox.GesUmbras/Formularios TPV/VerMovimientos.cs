﻿using AlterParadox.GesUmbras.Objects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras.Formularios_TPV
{
    public partial class VerMovimientos : Form
    {
        DataTable dtMov;
        List<ControlCaja> movCaja;

        public VerMovimientos()
        {
            InitializeComponent();
            movCaja = new List<ControlCaja>();
            dtMov = ControlCaja.SELECT_MOVS_GRID();
            dataViewMovimientos.DataSource = dtMov;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void printButton_Click(object sender, EventArgs e)
        {
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Movimientos de Caja";
            printer.SubTitle = "Movimientos de Caja en Umbras 2018";
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = false;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.Footer = "Alter Paradox";
            printer.FooterSpacing = 15;
            printer.PageSettings.Landscape = true;
            printer.PrintDataGridView(dataViewMovimientos);
        }

       

        
    }
}
