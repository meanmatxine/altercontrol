﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.Objects;
using AlterParadox.GesUmbras.ProductService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace AlterParadox.GesUmbras
{
    public partial class VerPedidos : Form
    {
        DataTable dtPed;
        DataTable dtDet;
        List<PedidoDetalle> listDetalles;
        List<Product> TodosProductos;
        public VerPedidos()
        {
            InitializeComponent();
            dtPed = Pedido.SELECT_ALL_GRID(true);
            datosPedidos.DataSource = dtPed;
            listDetalles = new List<PedidoDetalle>();
            //productos
            try
            {
                using (ProductServiceClient svc = new ProductServiceClient())
                {
                    TodosProductos = svc.GetProducts().ListaProductos.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void datosPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //VER
            if (e.ColumnIndex == 0)
            {
                //limpiar lista
                listDetalles.Clear();
                //Seleccionar pedido
                int pID = int.Parse(datosPedidos.Rows[e.RowIndex].Cells[1].Value.ToString());
                //cofer detalles de ese pedido
                //crear data table
                dtDet = new DataTable();
                dtDet.Columns.Add(new DataColumn("Producto"));
                dtDet.Columns.Add(new DataColumn("Cantidad"));
                dtDet.Columns.Add(new DataColumn("Precio"));
                dtDet.Columns.Add(new DataColumn("Total"));
                //detalles pedido
                listDetalles = PedidoDetalle.SELECT_ONE_GRID(pID);
                foreach (PedidoDetalle pd in listDetalles) {
                    //nombre
                    //Product p = Producto.SELECT_ONE(pd.productoID);
                    Product p = TodosProductos.Find(x => x.productoID == pd.productoID);
                    decimal total = p.productoPrecio * pd.pdCantidad;
                    dtDet.Rows.Add(new Object[] { p.productoName,pd.pdCantidad,p.productoPrecio,total});
                }
                datosDetalle.DataSource = dtDet;
                
            }
        }

        private void printListButton_Click(object sender, EventArgs e)
        {
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "Listado de Pedidos";
            printer.SubTitle = "Pedidos de Umbras 2018";
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = false;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.Footer = "Alter Paradox";
            printer.FooterSpacing = 15;
            printer.PageSettings.Landscape = false;
            printer.PrintDataGridView(datosPedidos);
        }
    }
}
