﻿

namespace AlterParadox.GesUmbras.Formularios_TPV
{
    using System.Windows.Forms;
    partial class MoverDinero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dineroMovido = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.sacarDinero = new System.Windows.Forms.Button();
            this.meterDinero = new System.Windows.Forms.Button();
            this.comentarios = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cerrarButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dineroActual = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textPersona = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dineroMovido)).BeginInit();
            this.SuspendLayout();
            // 
            // dineroMovido
            // 
            this.dineroMovido.DecimalPlaces = 2;
            this.dineroMovido.Location = new System.Drawing.Point(56, 76);
            this.dineroMovido.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.dineroMovido.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.dineroMovido.Name = "dineroMovido";
            this.dineroMovido.Size = new System.Drawing.Size(120, 20);
            this.dineroMovido.TabIndex = 0;
            this.dineroMovido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Dinero a Mover";
            // 
            // sacarDinero
            // 
            this.sacarDinero.Location = new System.Drawing.Point(24, 279);
            this.sacarDinero.Name = "sacarDinero";
            this.sacarDinero.Size = new System.Drawing.Size(75, 51);
            this.sacarDinero.TabIndex = 3;
            this.sacarDinero.Text = "Sacar Dinero";
            this.sacarDinero.UseVisualStyleBackColor = true;
            this.sacarDinero.Click += new System.EventHandler(this.sacarDinero_Click);
            // 
            // meterDinero
            // 
            this.meterDinero.Location = new System.Drawing.Point(133, 279);
            this.meterDinero.Name = "meterDinero";
            this.meterDinero.Size = new System.Drawing.Size(75, 51);
            this.meterDinero.TabIndex = 4;
            this.meterDinero.Text = "Meter Dinero";
            this.meterDinero.UseVisualStyleBackColor = true;
            this.meterDinero.Click += new System.EventHandler(this.meterDinero_Click);
            // 
            // comentarios
            // 
            this.comentarios.Location = new System.Drawing.Point(24, 209);
            this.comentarios.Multiline = true;
            this.comentarios.Name = "comentarios";
            this.comentarios.Size = new System.Drawing.Size(184, 53);
            this.comentarios.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "Comentarios";
            // 
            // cerrarButton
            // 
            this.cerrarButton.Location = new System.Drawing.Point(24, 336);
            this.cerrarButton.Name = "cerrarButton";
            this.cerrarButton.Size = new System.Drawing.Size(184, 33);
            this.cerrarButton.TabIndex = 5;
            this.cerrarButton.Text = "Cerrar";
            this.cerrarButton.UseVisualStyleBackColor = true;
            this.cerrarButton.Click += new System.EventHandler(this.cerrarButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Dinero Actual en la Caja:";
            // 
            // dineroActual
            // 
            this.dineroActual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dineroActual.AutoSize = true;
            this.dineroActual.Location = new System.Drawing.Point(138, 22);
            this.dineroActual.Name = "dineroActual";
            this.dineroActual.Size = new System.Drawing.Size(19, 13);
            this.dineroActual.TabIndex = 8;
            this.dineroActual.Text = "0€";
            this.dineroActual.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 90;
            this.label4.Text = "Persona Responsable";
            // 
            // textPersona
            // 
            this.textPersona.Location = new System.Drawing.Point(59, 141);
            this.textPersona.Name = "textPersona";
            this.textPersona.Size = new System.Drawing.Size(116, 20);
            this.textPersona.TabIndex = 1;
            // 
            // MoverDinero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 379);
            this.Controls.Add(this.textPersona);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dineroActual);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cerrarButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comentarios);
            this.Controls.Add(this.meterDinero);
            this.Controls.Add(this.sacarDinero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dineroMovido);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MoverDinero";
            this.Text = "MoverDinero";
            ((System.ComponentModel.ISupportInitialize)(this.dineroMovido)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NumericUpDown dineroMovido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button sacarDinero;
        private System.Windows.Forms.Button meterDinero;
        private System.Windows.Forms.TextBox comentarios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cerrarButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label dineroActual;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textPersona;
    }
}