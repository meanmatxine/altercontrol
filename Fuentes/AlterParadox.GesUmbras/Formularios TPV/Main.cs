﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.Formularios_TPV;
using AlterParadox.GesUmbras.Objects;
using AlterParadox.GesUmbras.ProductService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;


namespace AlterParadox.GesUmbras
{

    public partial class Main : Form
    {
        //todos los productos
        List<Product> TodosProductos;
        //productos por tipo
        List<Product> productosSelec;
        //productos a vender
        List<Product> ventaProductos;
        DataView productosTipo;
        //DataView sesionView;
        //botones
        List<Button> botones;//botones producto
        List<Button> botonesTipo;//botones tipo
        //TBPedidosDetalle tbDetalles;
        //listas
        
        //BindingList<Producto> bindingProductos;
        decimal totalPedido;
        //Pedido
        public Pedido pedidoTemp;
        //public List<PedidoDetalle> pedidoDet;
        //sesion activa
        //Sesion activeSesion;
        //Grid pedidos
        DataTable gridPedidos;


        public Main()
        {
            InitializeComponent();

        }

        private void Main_Load(object sender, EventArgs e)
        {
            //new SQLite();
            this.Show();
            start();
        }


        private void start()
        {
            // PRUEBAS

            //FIN PRUEBAS

            Pedido p = Pedido.GetLast();
            //Console.WriteLine(p.pedidoID + "-" + p.pedidoTotal + "-"+p.pedidoEntregado + "-" + p.pedidoDevuelto);
            //posicion de botones
            Point newLoc = new Point(10, 10);
            //botones tipo
            botonesTipo = new List<Button>();
            //cogemos tipos
            productosTipo = ProductoTipo.SELECT_ALL();
            //creamos los botones en el formulario
            newLoc = new Point(10, 10);
            for (int i = 0; i < productosTipo.Table.Rows.Count; i++)
            {
                ProductoTipo pt = new ProductoTipo(productosTipo.Table.Rows[i]);
                Button b = new Button();
                b.Text = pt.tipoName;
                b.Name = pt.tipoID.ToString();
                b.Size = new Size(100, 45);
                b.Location = newLoc;
                b.Click += new EventHandler(changeType);
                b.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
                newLoc.Offset(b.Width + 10, 0);
                panelTipos.Controls.Add(b);
            }
            //BotonesProducto
            botones = new List<Button>();
            //productos de la venta
            TodosProductos = new List<Product>();
            //cogemos productos al inicializar
            //inicializar productos
            inicializarProductos();
            productosSelec = TodosProductos.FindAll(x => x.productoTipo == 1);

            //creamos los botones en el formulario
            cambiarBotones();
            newLoc = new Point(10, 10);
            precioTotal.Text = "0 €";
            totalPedido = 0;
            //iniciamos pedido
            pedidoTemp = new Pedido();
            //pedidoDet = new List<PedidoDetalle>();
            //grid
            generarGrid();
            datosPedidoGrid.DataSource = gridPedidos;
            datosPedidoGrid.Columns[1].Width = 130;
            datosPedidoGrid.Columns[2].Width = 60;
            datosPedidoGrid.Columns[3].Width = 60;
            datosPedidoGrid.Columns[4].Width = 60;
            //dataGridView1.AutoGenerateColumns = true;
            intTodo.Focus();

        }

        //inicializar pedidos
        void inicializarProductos()
        {
            try
            {
                using (ProductServiceClient svc = new ProductServiceClient())
                {
                    TodosProductos = svc.GetProducts().ListaProductos.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //venta productos
            ventaProductos = new List<Product>();
            foreach (Product p in TodosProductos)
            {
                Product pTemp = new Product();
                pTemp = p;
                pTemp.productoCantidad = 0;
                ventaProductos.Add(pTemp);
            }
            //productos = Producto.SELECT_ALL();
            /*for (int i = 0; i < productos.Table.Rows.Count; i++)
            {
                Producto pTemp = new Producto(productos.Table.Rows[i]);
                pTemp.productoCantidad = 0;
                TodosProductos.Add(pTemp);
            }*/
        }

        //boton tipo
        private void changeType(object sender, EventArgs e)
        {
            Button b = sender as Button;
            //productos = Producto.SELECT_TYPE(int.Parse(b.Name));
            productosSelec = TodosProductos.FindAll(x => x.productoTipo == int.Parse(b.Name));
            cambiarBotones();
        }

        //cambiar botones segun tipo
        private void cambiarBotones()
        {
            Point newLoc = new Point(10, 10);
            productosPanel.Controls.Clear();
            int i = 0;
            foreach (Product p in productosSelec)
            {
                p.productoCantidad = 0;
                //BOTON
                Button b = new Button()
                {
                    Name = p.productoID.ToString(),
                    Size = new Size(100, 100),
                    Location = newLoc,
                    Anchor = (AnchorStyles.Top | AnchorStyles.Left),
                    BackgroundImage = GetImage(p.productoImage),
                    BackgroundImageLayout = ImageLayout.Stretch
                };
                b.Click += new EventHandler(addItem);
                botones.Add(b);
                productosPanel.Controls.Add(b);
                //label
                Label l = new Label()
                {
                    Name = "Label" + i,
                    Text = p.productoName,
                    AutoSize = false,
                    Location = new Point(newLoc.X, newLoc.Y + b.Height + 1),
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font=new Font("Arial",9,FontStyle.Regular),
                    Size = new Size(100, 50),
                    Dock = DockStyle.None
                };
                productosPanel.Controls.Add(l);
                //new LOC
                newLoc.Offset(b.Width + 10, 0);
                if (newLoc.X > 550)
                {
                    newLoc.X = 10;
                    newLoc.Y += b.Height + 60;
                }
                i++;
            }
            /*for (int i = 0; i < productos.Table.Rows.Count; i++)
            {
                Producto p = new Producto(productos.Table.Rows[i]);
                p.productoCantidad = 0;
                
            }*/
            intTodo.Focus();
        }

        //get image from resources
        private Bitmap GetImage(string s)
        {
            if (s != "" && s != null)
            {
                Assembly asm = Assembly.GetExecutingAssembly();
                string resource = asm.GetName().Name += ".Properties.Resources";
                var rm = new System.Resources.ResourceManager(resource, asm);
                if (rm.GetObject(s) == null)
                {
                    return Properties.Resources._default;
                }
                else
                {
                    return (Bitmap)rm.GetObject(s);
                }
                
            }
            else
            {
                return Properties.Resources._default;
            }

        }
        //evento click de los botones, añade items
        private void addItem(object sender, EventArgs e)
        {
            Button b = sender as Button;
            //Console.WriteLine("entra por boton");
            foreach (Product pr in ventaProductos)
            {
                Console.WriteLine("boton name: "+ b.Name+ " producto ID: "+ pr.productoID);
                if (pr.productoID.ToString() == b.Name)
                {
                    int resultado = 1;
                    if (intTodo.Text != null && intTodo.Text != "")
                    {
                        resultado = cogerTexto(intTodo.Text);
                    }
                    AddItemToList(pr, resultado);
                    break;
                }
            }
        }

        //coger texto
        private int cogerTexto(string t)
        {
            int r = -1;
            string[] rs = intTodo.Text.Split(',');
            if (rs.Length > 1)
            {
                MessageBox.Show("Para Añadir productos en decimal habla con Antman a ver como lo ven en el universo Cuántico", "Decimales", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (int.Parse(t) > 100)
                {
                    MessageBox.Show("No se pueden vender mas de 100 productos de algo, que luego no hay sitio en la mochila para las antorchas", "Limite de Carga", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    r = int.Parse(t);
                    intTodo.Text = "";
                }
            }
            return r;
        }

        //Entrada de item 
        void AddItemToList(Product p, int c)
        {
            //MessageBox.Show(p.productoName + "-" + p.productoCantidad);
            p.productoCantidad += c;
            //MessageBox.Show(p.productoName + "-"+p.productoCantidad);
            actualizarLista();
        }

        //actualizar lista
        private void actualizarLista()
        {
            //limpiarTabla();
            totalPedido = 0;
            //DATA GRID
            generarGrid();
            //lista pedido

            foreach (Product p in ventaProductos)
            {
                if (p.productoCantidad > 0)
                {
                    //list view
                    //int linea = tableVentas.RowCount;
                    //Console.WriteLine(tableVentas.RowCount);
                    //tableVentas.Controls.Add(new Label() { Text = p.productoName }, 0, linea);
                    //tableVentas.Controls.Add(new Label() { Text = p.productoCantidad.ToString() }, 1, linea);
                    //tableVentas.Controls.Add(new Label() { Text = p.productoPrecio.ToString() }, 2, linea);
                    //tableVentas.Controls.Add(new Label() { Text = (p.productoPrecio * p.productoCantidad).ToString() }, 3, linea);
                    Button b = new Button();
                    b.Text = "X";
                    b.Name = p.productoID.ToString();
                    b.Click += new EventHandler(removeItem);
                    //tableVentas.Controls.Add(b, 4, linea);
                    //tableVentas.RowCount++;
                    totalPedido += (decimal)(p.productoPrecio * p.productoCantidad);
                    //pedido det
                    PedidoDetalle pd = new PedidoDetalle();
                    pd.productoID = p.productoID;
                    pd.pdCantidad = p.productoCantidad;
                    //pedidoDet.Add(pd);
                    //DATAGRID
                    decimal total = p.productoPrecio * p.productoCantidad;
                    gridPedidos.Rows.Add(new Object[] { p.productoName, p.productoCantidad, p.productoPrecio, total });
                    //bindingProductos.Add(p);
                }
                //cambiar total
                precioTotal.Text = totalPedido.ToString() + " €";
                //DATAGRID
                datosPedidoGrid.DataSource = gridPedidos;
                //revisar
                intTodo.Focus();
            }
        }

        //generarGrid
        void generarGrid()
        {
            gridPedidos = new DataTable();
            /*DataColumn colProducto;
            colProducto=new DataColumn("Producto")
            colProducto.*/
            gridPedidos.Columns.Add(new DataColumn("Producto"));
            gridPedidos.Columns.Add(new DataColumn("Cantidad"));
            gridPedidos.Columns.Add(new DataColumn("Precio"));
            gridPedidos.Columns.Add(new DataColumn("Total"));
        }

        //click de la lista (quita items)
        private void removeItem(object sender, EventArgs e)
        {
            Button b = sender as Button;
            foreach (Product pr in ventaProductos)
            {
                Console.WriteLine("aqui");
                if (pr.productoID.ToString() == b.Name)
                {
                    pr.productoCantidad--;
                    if (pr.productoCantidad < 0) { pr.productoCantidad = 0; }
                    actualizarLista();
                    break;
                }
            }
        }

        //quitar objeto en Grid
        private void datosPedidoGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                //quitarlo del grid
                foreach (Product pr in ventaProductos)
                {
                    if (pr.productoName == datosPedidoGrid.Rows[e.RowIndex].Cells[1].Value.ToString())
                    {
                        pr.productoCantidad--;
                        if (pr.productoCantidad < 0) { pr.productoCantidad = 0; }
                        actualizarLista();
                        break;
                    }
                }
                //quitarlo del detalle

            }
        }

        //COBRAR

        //exacto
        private void btn_cobrarExacto_Click(object sender, EventArgs e)
        {
            finalizarPedido("exacto");
        }

        //Cobrar directo 
        private void cobrarDirecto_Click(object sender, EventArgs e)
        {
            finalizarPedido("directo");
        }

        //con ventana
        private void pedidoFinalizar_Click(object sender, EventArgs e)
        {
            finalizarPedido("ventana");
        }

        //funcion cobrar
        private void finalizarPedido(string mode)
        {
            bool cobrar = true;
            if (totalPedido <= 0)
            {
                MessageBox.Show("No hay productos en el pedido, cobramos el aire?", "No hay pedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cobrar = false;
            }
            switch (mode)
            {
                case "exacto":
                    intTodo.Text = totalPedido.ToString();
                    break;
                case "directo":
                case "ventana":
                    if (intTodo.Text == null || intTodo.Text == "")
                    {
                        MessageBox.Show("Poner cuanto ha pagado como lo ves?", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cobrar = false;
                    }
                    else
                    {
                        if (decimal.Parse(intTodo.Text) < totalPedido)
                        {
                            MessageBox.Show("Ha pagado menos de lo que cuesta el pedido, pensaba que sabias sumar", "Dinero insuficiente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cobrar = false;
                        }
                    }
                    break;
            }

            if (cobrar)
            {
                //Datos pedido
                DateTime localDate = DateTime.Now;
                string culture = "dd/MM/yyyy HH:mm:ss";
                pedidoTemp.pedidoFecha = localDate.ToString(culture);
                pedidoTemp.pedidoTotal = (decimal)totalPedido;
                pedidoTemp.pedidoEntregado = decimal.Parse(intTodo.Text);
                //a devolver 
                Decimal dev = (decimal)(decimal.Parse(intTodo.Text) - totalPedido);
                dev = Math.Round(dev, 2);
                pedidoTemp.pedidoDevuelto = dev;
                if (mode == "directo" || mode == "exacto")
                {
                    //insertamos el pedido
                    Pedido.INSERT(pedidoTemp);
                    //cogemos el ultimo pedido
                    Pedido ultimo = Pedido.GetLast();
                    //agregamos el id al detalle e insertamos en DBC
                    foreach (Product pd in ventaProductos)
                    {
                        if (pd.productoCantidad > 0)
                        {
                            PedidoDetalle pdet = new PedidoDetalle();
                            pdet.pedidoID = ultimo.pedidoID;
                            pdet.productoID = pd.productoID;
                            pdet.pdCantidad = pd.productoCantidad;
                            PedidoDetalle.Insert(pdet);
                        }
                    }
                    //Añadimos el movimiento de caja
                    ControlCaja c = new ControlCaja();
                    c.cajaMovFecha = ultimo.pedidoFecha;
                    c.cajaPersona = "Caja";
                    c.cajaIngreso = ultimo.pedidoTotal;
                    c.cajaExtracto = 0;
                    c.cajaConcepto = "Pedido";
                    c.cajaObservaciones = "";
                    ControlCaja ultimoMov = ControlCaja.ultimoMovimiento();
                    c.cajaDineroActual = ultimoMov.cajaDineroActual + c.cajaIngreso;
                    c.cajaDescuadre = ultimoMov.cajaDescuadre;
                    ControlCaja.INSERT(c);
                    nuevoPedido();
                }
                else
                {
                    //Mostrar formulario venta Final
                    pedidoFinal pf = new pedidoFinal(pedidoTemp, ventaProductos);
                    pf.ShowDialog();
                    //tras cerrar limpia el pedido y genera uno nuevo si el pedido no ha sido cancelado
                    if (!pf.pedidoCancelado)
                    {
                        nuevoPedido();
                    }
                }
            }
        }

        private void nuevoPedido()
        {
            pedidoTemp = null;
            //pedidoDet = null;
            ventaProductos = null;
            ventaProductos = new List<Product>();
            pedidoTemp = new Pedido();
            //pedidoDet = new List<PedidoDetalle>();
            intTodo.Text = "0";
            totalPedido = 0;
            precioTotal.Text = totalPedido.ToString() + " €";
            this.Close();
            Main formTpvMain = new Main();
            G.AbrirPantalla(formTpvMain, false, false);
        }

        //codigo barras
        private void codigoBarras(string cb)
        {
            //buscar por codigo de barras
            Console.WriteLine(intTodo.Text);
            //Product pTemp = Producto.SELECT_BY_CB(intTodo.Text);
            Product pTemp = TodosProductos.Find(x => x.productoCB.Contains(intTodo.Text));
            //agregarlo a productos
            //Console.WriteLine(pTemp.productoID);
            if (pTemp != null)
            {
                Console.WriteLine(pTemp + " lo añade");
                //buscar el producto en la lista y lo añade
                foreach (Product p in TodosProductos)
                {
                    if (p.productoID == pTemp.productoID)
                    {
                        AddItemToList(p, 1);
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Has puesto un Codigo de barras que no existe rompiendo el universo o quieres cobrar mas de 100€ y te crees casta!", "Ya la has jodido!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //limpiar texto
            intTodo.Text = "";
        }

        //nuevo pedido
        private void newPedido_Click(object sender, EventArgs e)
        {
            nuevoPedido();
        }

        //cerrar la sesion
        void cerrarSesion()
        {

        }

        //boton contarCaja
        private void finSesionB_Click(object sender, EventArgs e)
        {
            //sacar mensaje
            DialogResult result = MessageBox.Show("¿Desea Contar la Caja?, Podra Cuadrar la caja y reajustar el dinero disponible", "Contar Caja", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                CerrarSesion cs = new CerrarSesion();
                cs.ShowDialog();
            }
            else
            {
                //nada
            }
        }

        //formulario ver pedidos 
        private void verPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        //MENU
        //pedidos
        private void nuevoPedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            nuevoPedido();
        }

        private void verPedidosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            VerPedidos vp = new VerPedidos();
            vp.ShowDialog();
        }
        //productos 
        private void modificarProductosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Productos p = new Productos();
            p.ShowDialog();
        }
        //informes
        private void informeDeSesiónToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //informeSesion();
        }

        private void sacarMeterDineroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoverDinero md = new MoverDinero();
            md.ShowDialog();
        }

        //movimientos de Caja
        private void movimientosDeCajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VerMovimientos vm = new VerMovimientos();
            vm.ShowDialog();
        }

        private void pedidosPorFechaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Informes inf = new Informes("pFecha");
            inf.ShowDialog();
        }

        private void pedidosPorProductoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Informes inf = new Informes("pProducto");
            inf.ShowDialog();
        }

        private void FacturaciónFinalUmbrasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Desea imprimir la facturación de umbras, le costara un poco", "Imprimir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
                facturacionFinal();
        }

        //Facturacion Final
        int pedidoCount = 0;
        List<Pedido> pedidosTotal = new List<Pedido>();

        public void facturacionFinal()
        {
            pedidosTotal = Pedido.SELECT_ALL_LIST(false);

            PrintDocument pd = new PrintDocument();
            pd.PrintController = new StandardPrintController();
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            PrintDialog pDialog = new PrintDialog();
            pDialog.Document = pd;
            DialogResult res = pDialog.ShowDialog();
            if (res == DialogResult.OK)
                pd.Print();
        }


        //imprimir ticket
        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics g = e.Graphics;
            PaperSize ps = new PaperSize("A4", 850, 1100);
            e.PageSettings.PaperSize = ps;
            Font fBody = new Font("LucidaConsole", 11, FontStyle.Regular);
            Font fTitle = new Font("LucidaConsole", 14, FontStyle.Bold);
            SolidBrush sb = new SolidBrush(Color.Black);
            if (pedidoCount == 0)
            {
                //hoja inicial
            }
            //coger pedidos
            Pedido pedido = pedidosTotal[pedidoCount];
            g.DrawImage(Properties.Resources.Logotipo150, 20, 10);
            g.DrawString("Alter Paradox", fTitle, sb, 220, 50);
            g.DrawString("XI Umbras de Paradox - 2018", fBody, sb, 220, 70);
            //fecha y factura
            DateTime localDate = DateTime.Now;
            string culture = "dd/MM/yyyy HH:mm:ss";
            string fecha = localDate.ToString(culture);
            g.DrawString("Fecha: " + fecha, fBody, sb, 10, 200);
            g.DrawString("Factura Pedido " + pedido.pedidoID, fBody, sb, 10, 220);
            g.DrawString("------------------------------------------------------------", fBody, sb, 10, 230);
            //datos pedido
            int alturaLinea = 20;
            int colProducto, colCant, colPrecio, colIva, colFinal, colTotal;
            colProducto = 10;
            colPrecio = 200;
            colIva = 300;
            colFinal = 450;
            colCant = 560;
            colTotal = 610;
            //tabla head
            g.DrawString("Producto", fTitle, sb, colProducto, 250);
            g.DrawString("Precio", fTitle, sb, colPrecio, 250);
            g.DrawString("IVA", fTitle, sb, colIva, 250);
            g.DrawString("Precio Final", fTitle, sb, colFinal, 250);
            g.DrawString("Cant", fTitle, sb, colCant, 250);
            g.DrawString("Total", fTitle, sb, colTotal, 250);
            //detalles pedido
            int lineaInicial = 280;
            int linea = 0;
            //coger el detalle
            List<PedidoDetalle> pDetalle = new List<PedidoDetalle>();
            pDetalle = PedidoDetalle.SELECT_PEDIDO(pedido.pedidoID);
            //productos
            List<Product> productosDetalle = new List<Product>();
            foreach (PedidoDetalle pd in pDetalle)
            {
                //Product pr = Product.SELECT_ONE(pd.productoID);
                Product pr = TodosProductos.Find(x => x.productoID == pd.productoID);
                //ajustar cantidad
                pr.productoCantidad = pd.pdCantidad;
                productosDetalle.Add(pr);
            }
            //linea por cada producto
            foreach (Product pr in productosDetalle)
            {
                g.DrawString(pr.productoName, fBody, sb, colProducto, lineaInicial + (linea * alturaLinea));
                g.DrawString(pr.productoCantidad.ToString(), fBody, sb, colCant, lineaInicial + (linea * alturaLinea));
                //decimal precioSIva = (calcularSinIva(pde));
                g.DrawString(pr.productoPrecioSinIva + " €", fBody, sb, colPrecio, lineaInicial + (linea * alturaLinea));
                decimal iva = calcularIva(pr);
                g.DrawString("IVA (" + pr.productoIVA + "): " + iva + " €", fBody, sb, colIva, lineaInicial + (linea * alturaLinea));
                g.DrawString(pr.productoPrecio + " €", fBody, sb, colFinal, lineaInicial + (linea * alturaLinea));
                g.DrawString(pr.productoCantidad * pr.productoPrecio + " €", fBody, sb, colTotal, lineaInicial + (linea * alturaLinea));
                linea++;
            }
            g.DrawString("------------------------------------------------------------", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("TOTAL: " + pedido.pedidoTotal + " €", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("Entregado: " + pedido.pedidoEntregado + "€", fBody, sb, 10, lineaInicial + (linea * alturaLinea));
            linea++;
            g.DrawString("Devuelto: " + (pedido.pedidoEntregado - pedido.pedidoTotal) + "€", fBody, sb, 10, lineaInicial + (linea * alturaLinea));

            if (pedidoCount < pedidosTotal.Count - 1)
            {
                e.HasMorePages = true;
                pedidoCount++;
            }
            else
            {
                e.HasMorePages = false;
                g.Dispose();
            }
        }

        //calcular IVA
        decimal calcularIva(Product p)
        {
            decimal d = (p.productoPrecioSinIva * p.productoIVA) / 100;
            d = Math.Round(d, 2);
            return d;
        }

        //Teclado Numerico
        //clr
        private void calcCLR_Click(object sender, EventArgs e)
        {
            intTodo.Text = "";
            intTodo.Focus();
        }
        //0
        private void calc0_Click(object sender, EventArgs e)
        {
            if (intTodo.Text.Length > 0)
                checkText(intTodo.Text, "0");
        }
        //1
        private void calc1_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "1");
        }
        //2
        private void calc2_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "2");
        }
        //3
        private void calc3_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "3");
        }
        //4
        private void calc4_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "4");
        }
        //5
        private void calc5_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "5");
        }
        //6
        private void calc6_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "6");
        }
        //7
        private void calc7_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "7");
        }
        //8
        private void calc8_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "8");
        }
        //9
        private void calc9_Click(object sender, EventArgs e)
        {
            checkText(intTodo.Text, "9");
        }
        //back
        private void calcBack_Click(object sender, EventArgs e)
        {
            if (intTodo.Text.Length > 0)
            {
                intTodo.Text = intTodo.Text.Remove(intTodo.Text.Length - 1);
                intTodo.Focus();
            }  
        }
        //coma
        private void calcComa_Click(object sender, EventArgs e)
        {
            if (intTodo.Text.Length == 0)
            {
                intTodo.Text = "0" + ",";
            }
            else
            {
                intTodo.Text = intTodo.Text + ",";
            }
            intTodo.Focus();
        }
        //check Text
        private void checkText(string t, string add)
        {
            string temp = t;
            temp = intTodo.Text + add;
            if (decimal.Parse(temp) > 100)
            {
                MessageBox.Show("No se aceptan pagos de más de 100€; Casta!", "Limite Excedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                string[] separado = temp.Split(',');
                if (separado.Length > 1)
                {
                    if (separado[1].Length > 2)
                    {
                        MessageBox.Show("No mas de dos decimales cara anchoa!", "Limite Excedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        intTodo.Text = intTodo.Text + add;
                    }
                }
                else
                {
                    intTodo.Text = intTodo.Text + add;
                }
            }
            intTodo.Focus();
        }

        private void intTodo_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool b = true;
            bool intro = false;
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                b = false;
            }
            //intro
            if (e.KeyChar == (char)13)
            {
                intro = true;
                b = false;
                //codigoBarras(intTodo.Text);
            }
            //change . for , 
            if (e.KeyChar == '.' || (e.KeyChar == ','))
            {
                // only allow one decimal point
                if ((sender as TextBox).Text.IndexOf(',') > -1)
                {
                    b = false;
                }
                else
                {
                    if (intTodo.Text.Length == 0)
                    {
                        intTodo.Text = "0" + ",";
                    }
                    else
                    {
                        intTodo.Text = intTodo.Text + ",";
                    }
                    intTodo.SelectionStart = intTodo.Text.Length;
                    intTodo.SelectionLength = 0;
                }

            }
            //intro Cobrar / Codigo Barras
            if (intro)
            {
                string temp = intTodo.Text;
                decimal d = 0;
                decimal.TryParse(temp, out d);
                if (d == 0)
                {
                    MessageBox.Show("Con 0 o sin texto, poca ostia vamos a hacer, tuercebotas!", "No Hay Texto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (d > 100)
                {
                    codigoBarras(intTodo.Text);
                }
                else
                {
                    finalizarPedido("ventana");
                }
            }
            /*if (b)
            {
                string temp = intTodo.Text + e.KeyChar.ToString();
                decimal d = 0;
                decimal.TryParse(temp, out d);
                if (d > 100)
                {
                    MessageBox.Show("No se aceptan pagos de más de 100€; Casta!", "Limite Excedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    b = false;
                }
            }*/
            //only 2 decimals
            if (b)
            {
                string temp = intTodo.Text + e.KeyChar.ToString();
                string[] s = temp.Split(',');
                if (s.Length > 1)
                {
                    if (s[1].Length > 2)
                    {
                        MessageBox.Show("No mas de dos decimales cara anchoa!", "Limite Excedido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        b = false;
                    }
                }
            }
            //handle
            if (!b)
                e.Handled = true;
        }

        private void intTodo_MouseDown(object sender, MouseEventArgs e)
        {
            intTodo.SelectionStart = intTodo.Text.Length;
            intTodo.SelectionLength = 0;
        }


    }
}
