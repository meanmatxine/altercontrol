﻿namespace AlterParadox.GesUmbras
{
    partial class FrmImportGames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progress = new System.Windows.Forms.ProgressBar();
            this.txtPathFile = new System.Windows.Forms.TextBox();
            this.cmdImport = new System.Windows.Forms.Button();
            this.cmdSearch = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(107, 50);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(320, 23);
            this.progress.TabIndex = 13;
            // 
            // txtPathFile
            // 
            this.txtPathFile.Location = new System.Drawing.Point(12, 12);
            this.txtPathFile.Name = "txtPathFile";
            this.txtPathFile.Size = new System.Drawing.Size(323, 20);
            this.txtPathFile.TabIndex = 12;
            // 
            // cmdImport
            // 
            this.cmdImport.Location = new System.Drawing.Point(15, 38);
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Size = new System.Drawing.Size(86, 35);
            this.cmdImport.TabIndex = 11;
            this.cmdImport.Text = "Importar";
            this.cmdImport.UseVisualStyleBackColor = true;
            this.cmdImport.Click += new System.EventHandler(this.cmdImport_Click);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Location = new System.Drawing.Point(341, 10);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(86, 23);
            this.cmdSearch.TabIndex = 10;
            this.cmdSearch.Text = "Buscar Fichero";
            this.cmdSearch.UseVisualStyleBackColor = true;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // FrmImportGames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 88);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.txtPathFile);
            this.Controls.Add(this.cmdImport);
            this.Controls.Add(this.cmdSearch);
            this.Name = "FrmImportGames";
            this.Text = "FrmImportGames";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.TextBox txtPathFile;
        private System.Windows.Forms.Button cmdImport;
        private System.Windows.Forms.Button cmdSearch;
    }
}