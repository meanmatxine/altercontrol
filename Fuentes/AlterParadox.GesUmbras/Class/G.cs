﻿using System.Drawing;
using System.Windows.Forms;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net;
using System.Reflection;
using System.ComponentModel;
using System.IO;
using AlterParadox.Core;
using System.Globalization;

namespace AlterParadox.GesUmbras
{

    public static class G
    {
        public static LocalGame_Filter filters;
        public static GameLending_Filter LendingFilter ;

        public static MDI MdiForm;
        public static bool admin = false;
        public static string _searchResult = "";
        public static Game _searchGame = null;
        public static Association _searchAssociation = null;

        public static bool Contains(string source, string toCheck)
        {
            if (string.IsNullOrEmpty(toCheck) || string.IsNullOrEmpty(source))
                return true;

            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            if (compareInfo.IndexOf(source.ToUpper().ToString(), toCheck.ToUpper().ToString(), CompareOptions.IgnoreNonSpace) >= 0)
                return true;

            return false;
        }


        public static void AbrirPantalla(Form Formulario, Boolean EsModal, Boolean Monitor, Action<DialogResult> respuestaDialogo = null)
        {
            bool alreadyopen = false;
            Form theopenform = null;
            Screen[] screens = Screen.AllScreens;

            Formulario.MaximizeBox = false;
            Formulario.MinimizeBox = false;
            if (EsModal)
            {
                Formulario.StartPosition = FormStartPosition.CenterScreen;
                Iniciar(Formulario);
                if (respuestaDialogo != null)
                    respuestaDialogo.Invoke(Formulario.ShowDialog());
                else
                    Formulario.ShowDialog();

            }
            else if (Monitor)
            {
               

                Iniciar(Formulario);
                if(screens.Length > 1)
                {
                    Rectangle bounds = screens[1].Bounds;
                    Formulario.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                }
                Formulario.StartPosition = FormStartPosition.Manual;
                Formulario.Show();

            }
            else
            {
                foreach (Form openForm in Application.OpenForms)
                {
                    if (openForm.Name == Formulario.Name)
                    {
                        alreadyopen = true;
                        theopenform = openForm;
                    }
                }

                if (alreadyopen) {
                    theopenform.Activate();
                }
                else { 
                    Formulario.MdiParent = MdiForm;
                    Formulario.Show();
                    Iniciar(Formulario);
                    Formulario.Location = new Point(0, 0);
                }
            }
        }

        public static void Iniciar(Form formulario)
        {
            ScreenModel.G_Preparar_Pantalla(formulario);
        }

        public static void Calculadora(NumericUpDown Control)
        {
            FrmCalc Formulario = new FrmCalc();
            G.AbrirPantalla(Formulario, true, false);
            if (G._searchResult != "")
            {
                Control.Value = Decimal.Parse("" + G._searchResult);
            }
        }
        
        public static long UnixDate(DateTime date)
        {
            var timeSpan = (date - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }
        public static DialogResult ShowInputDialog(ref string input, bool encrypt = false)
        {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();
            inputBox.StartPosition = FormStartPosition.CenterScreen;
            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ControlBox = false;
            inputBox.ClientSize = size;
            inputBox.Text = "Password";

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            if (encrypt){
                textBox.PasswordChar = Convert.ToChar("*");
            }            
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        public static DialogResult ShowTextDialog(string input)
        {
            System.Drawing.Size size = new System.Drawing.Size(800, 800);
            Form inputBox = new Form();
            inputBox.StartPosition = FormStartPosition.CenterScreen;
            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ControlBox = true;
            inputBox.MaximizeBox = false;
            inputBox.MinimizeBox = false;
            inputBox.ClientSize = size;
            inputBox.Font = new Font("Tahoma", 12);
            inputBox.Text = "Información adicional";

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Multiline = true;
            textBox.Size = new System.Drawing.Size(size.Width - 10, size.Height-10);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        public static Boolean IsNumeric(System.Object Expression)
        {

            //if (Char.IsDigit(e.KeyChar))

            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { } // just dismiss errors but return false
            return false;
        }

        public static string getDcNIF(string DNI)
        {
            DNI = DNI.Trim().ToUpper();
            try
            {
                //Regex re = new Regex(@"(^\d{1,8}$)|(^\d{1,8}[A-Z]$)|(^[K-MX-Z]\d{1,7}$)|(^[K-MX-Z]\d{1,7}[A-Z]$)", RegexOptions.IgnoreCase);
                Regex re = new Regex(@"(^\d{8}$)", RegexOptions.IgnoreCase);
                if (!(re.IsMatch(DNI)))
                {
                    return DNI;
                }

                re = new Regex(@"(\d{1,8})");

                string numeros = re.Match(DNI).Value.PadLeft(7, '0');

                char firstChar = DNI.ToCharArray()[0];

                if(firstChar == 'X')
                {
                    numeros = "0" + numeros;
                }
                else if (firstChar == 'Y')
                {
                    numeros = "1" + numeros;
                }else if (firstChar == 'Z')
                {
                    numeros = "2" + numeros;
                }
                //  0T  1R  2W  3A  4G  5M  6Y  7F  8P  9D
                // 10X 11B 12N 13J 14Z 15S 16Q 17V 18H 19L
                // 20C 21K 22E 23T
                int dn = Convert.ToInt32(numeros);
                int r = dn % 23;
                char dc = Convert.ToChar("TRWAGMYFPDXBNJZSQVHLCKE".Substring(r, 1));
                return DNI + dc;

            }
            catch ( Exception e)
            {
                return DNI;
            }
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://google.es"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static Int32 GetAge(DateTime? dateOfBirth)
        {
            if (!dateOfBirth.HasValue)
                return -1;
            else
            {
                var today = DateTime.Today;
                var dateOfB = dateOfBirth.Value;
                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (dateOfB.Year * 100 + dateOfB.Month) * 100 + dateOfB.Day;

                return (a - b) / 10000;
            }
        }

        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }

        public static string EnumToDescription<TEnum>(this TEnum EnumValue) where TEnum : struct
        {
            return GetEnumDescription((Enum)(object)((TEnum)EnumValue));
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string GetCSV(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string results = sr.ReadToEnd();
            sr.Close();

            return results;
        }

    }
}
