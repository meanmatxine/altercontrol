﻿namespace AlterParadox.GesUmbras
{
    using System.Windows.Forms;
    class ScreenModel
    {
        public static void KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(""+e.KeyCode);
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Form ElFormulario = (Form)sender;
                    ElFormulario.Close();
                    break;
            }
        }

        public static void KeyPressed(object o, KeyPressEventArgs e)
        {
            //switch (e.KeyChar.ToString())
            //{
            //    //case "\r":
            //    //    //e.Handled = true;
            //    //    //SendKeys.Send("{TAB}");
            //    //    break;
            //    case "'":
            //        //e.KeyChar:
            //        e.Handled = true;
            //        break;
            //}
        }

        public static void G_Preparar_Pantalla(Form formulario)
        {
            formulario.KeyPress += new KeyPressEventHandler(KeyPressed);
            formulario.KeyDown += new KeyEventHandler(KeyDown);

            formulario.KeyPreview = true;


            if (formulario.Modal)
            {
                formulario.Left = Screen.PrimaryScreen.WorkingArea.Width / 2 - (formulario.Width / 2);
                formulario.Top = Screen.PrimaryScreen.WorkingArea.Height / 2 - (formulario.Height / 2);
            }
            else
            {
                formulario.Left = 0;
                formulario.Top = 0;
            }
        }

    }
}
