﻿namespace AlterParadox.GesUmbras
{
    partial class FrmFichaInscrito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFichaInscrito));
            this.Number = new System.Windows.Forms.Label();
            this.DNI = new System.Windows.Forms.Label();
            this.NameUser = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.GridUsers = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_lastnames = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // Number
            // 
            this.Number.BackColor = System.Drawing.Color.White;
            this.Number.Location = new System.Drawing.Point(15, 36);
            this.Number.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Number.Name = "Number";
            this.Number.Size = new System.Drawing.Size(118, 45);
            this.Number.TabIndex = 0;
            this.Number.Text = "0";
            this.Number.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DNI
            // 
            this.DNI.BackColor = System.Drawing.Color.White;
            this.DNI.Location = new System.Drawing.Point(254, 9);
            this.DNI.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.DNI.Name = "DNI";
            this.DNI.Size = new System.Drawing.Size(486, 25);
            this.DNI.TabIndex = 1;
            this.DNI.Text = "00000000X";
            // 
            // NameUser
            // 
            this.NameUser.BackColor = System.Drawing.Color.White;
            this.NameUser.Location = new System.Drawing.Point(254, 56);
            this.NameUser.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.NameUser.Name = "NameUser";
            this.NameUser.Size = new System.Drawing.Size(486, 25);
            this.NameUser.TabIndex = 2;
            this.NameUser.Text = "Señor Umbras";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(15, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Número";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(148, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 25);
            this.label5.TabIndex = 4;
            this.label5.Text = "DNI:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(148, 56);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Nombre:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // GridUsers
            // 
            this.GridUsers.AllowUserToAddRows = false;
            this.GridUsers.AllowUserToDeleteRows = false;
            this.GridUsers.AllowUserToResizeColumns = false;
            this.GridUsers.AllowUserToResizeRows = false;
            this.GridUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridUsers.ColumnHeadersHeight = 35;
            this.GridUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_number,
            this.col_name,
            this.col_lastnames});
            this.GridUsers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridUsers.Location = new System.Drawing.Point(2, 141);
            this.GridUsers.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GridUsers.MultiSelect = false;
            this.GridUsers.Name = "GridUsers";
            this.GridUsers.ReadOnly = true;
            this.GridUsers.RowHeadersVisible = false;
            this.GridUsers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridUsers.RowTemplate.Height = 35;
            this.GridUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridUsers.Size = new System.Drawing.Size(750, 251);
            this.GridUsers.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 112);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "Personas al cargo:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.FillWeight = 5F;
            this.col_id.HeaderText = "Id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            this.col_id.Visible = false;
            // 
            // col_number
            // 
            this.col_number.DataPropertyName = "number";
            this.col_number.FillWeight = 10F;
            this.col_number.HeaderText = "Nº";
            this.col_number.Name = "col_number";
            this.col_number.ReadOnly = true;
            // 
            // col_name
            // 
            this.col_name.DataPropertyName = "name";
            this.col_name.FillWeight = 40F;
            this.col_name.HeaderText = "Nombre";
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            // 
            // col_lastnames
            // 
            this.col_lastnames.DataPropertyName = "lastname";
            this.col_lastnames.FillWeight = 40F;
            this.col_lastnames.HeaderText = "Apellidos";
            this.col_lastnames.Name = "col_lastnames";
            this.col_lastnames.ReadOnly = true;
            // 
            // FrmFichaInscrito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(755, 395);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GridUsers);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NameUser);
            this.Controls.Add(this.DNI);
            this.Controls.Add(this.Number);
            this.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmFichaInscrito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ficha de inscrito";
            this.Load += new System.EventHandler(this.FrmFichaInscrito_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmFichaInscrito_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.GridUsers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Number;
        private System.Windows.Forms.Label DNI;
        private System.Windows.Forms.Label NameUser;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView GridUsers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_number;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_lastnames;
    }
}