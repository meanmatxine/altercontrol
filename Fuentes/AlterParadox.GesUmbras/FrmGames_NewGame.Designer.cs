﻿namespace AlterParadox.GesUmbras
{
    partial class FrmGames_NewGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGames_NewGame));
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRefreshGrid = new System.Windows.Forms.Button();
            this.btnCleanFilter = new System.Windows.Forms.Button();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.GridGames = new System.Windows.Forms.DataGridView();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtDifficulty = new System.Windows.Forms.ComboBox();
            this.txtType = new System.Windows.Forms.ComboBox();
            this.txtFamily = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtYears = new AlterParadox.GesUmbras.NumericUpDownPro();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDuration = new AlterParadox.GesUmbras.NumericUpDownPro();
            this.txtPlayerMax = new AlterParadox.GesUmbras.NumericUpDownPro();
            this.txtPlayerMin = new AlterParadox.GesUmbras.NumericUpDownPro();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkIsExpansion = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEditorial = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnGameSave = new System.Windows.Forms.Button();
            this.lblGameName = new System.Windows.Forms.Label();
            this.txtGameComments = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblGameComments = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGames)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYears)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMin)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AccessibleName = "Selección de nuevo elemento";
            this.miniToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ButtonDropDown;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.miniToolStrip.Location = new System.Drawing.Point(165, 10);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.miniToolStrip.Size = new System.Drawing.Size(374, 39);
            this.miniToolStrip.TabIndex = 23;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnRefreshGrid);
            this.panel4.Controls.Add(this.btnCleanFilter);
            this.panel4.Controls.Add(this.txtFilter);
            this.panel4.Location = new System.Drawing.Point(1, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(594, 34);
            this.panel4.TabIndex = 2;
            // 
            // btnRefreshGrid
            // 
            this.btnRefreshGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshGrid.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshGrid.Location = new System.Drawing.Point(565, 3);
            this.btnRefreshGrid.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnRefreshGrid.Name = "btnRefreshGrid";
            this.btnRefreshGrid.Size = new System.Drawing.Size(27, 27);
            this.btnRefreshGrid.TabIndex = 23;
            this.btnRefreshGrid.Text = "R";
            this.btnRefreshGrid.UseVisualStyleBackColor = true;
            this.btnRefreshGrid.Click += new System.EventHandler(this.btnRefreshGrid_Click);
            // 
            // btnCleanFilter
            // 
            this.btnCleanFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCleanFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCleanFilter.Location = new System.Drawing.Point(539, 3);
            this.btnCleanFilter.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnCleanFilter.Name = "btnCleanFilter";
            this.btnCleanFilter.Size = new System.Drawing.Size(27, 27);
            this.btnCleanFilter.TabIndex = 22;
            this.btnCleanFilter.Text = "X";
            this.btnCleanFilter.UseVisualStyleBackColor = true;
            this.btnCleanFilter.Click += new System.EventHandler(this.btnCleanFilter_Click);
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(3, 3);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(537, 27);
            this.txtFilter.TabIndex = 1;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // GridGames
            // 
            this.GridGames.AllowUserToAddRows = false;
            this.GridGames.AllowUserToDeleteRows = false;
            this.GridGames.AllowUserToResizeColumns = false;
            this.GridGames.AllowUserToResizeRows = false;
            this.GridGames.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridGames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridGames.ColumnHeadersHeight = 30;
            this.GridGames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridGames.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_name});
            this.GridGames.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridGames.Location = new System.Drawing.Point(-1, 38);
            this.GridGames.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridGames.MultiSelect = false;
            this.GridGames.Name = "GridGames";
            this.GridGames.ReadOnly = true;
            this.GridGames.RowHeadersVisible = false;
            this.GridGames.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridGames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridGames.Size = new System.Drawing.Size(596, 520);
            this.GridGames.TabIndex = 1;
            this.GridGames.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridGames_CellContentClick);
            this.GridGames.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridGames_CellDoubleClick);
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "Id";
            this.col_id.FillWeight = 11.40375F;
            this.col_id.HeaderText = "Id";
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            // 
            // col_name
            // 
            this.col_name.DataPropertyName = "Name";
            this.col_name.FillWeight = 91.22999F;
            this.col_name.HeaderText = "Nombre";
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtDifficulty);
            this.panel1.Controls.Add(this.txtType);
            this.panel1.Controls.Add(this.txtFamily);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtYears);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtDuration);
            this.panel1.Controls.Add(this.txtPlayerMax);
            this.panel1.Controls.Add(this.txtPlayerMin);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.ChkIsExpansion);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtEditorial);
            this.panel1.Controls.Add(this.txtId);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Controls.Add(this.btnGameSave);
            this.panel1.Controls.Add(this.lblGameName);
            this.panel1.Controls.Add(this.txtGameComments);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.lblGameComments);
            this.panel1.Location = new System.Drawing.Point(597, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 555);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtDifficulty
            // 
            this.txtDifficulty.AutoCompleteCustomSource.AddRange(new string[] {
            "Desconocida",
            "Baja",
            "Media",
            "Alta"});
            this.txtDifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtDifficulty.FormattingEnabled = true;
            this.txtDifficulty.Items.AddRange(new object[] {
            "Desconocida",
            "Baja",
            "Media",
            "Alta"});
            this.txtDifficulty.Location = new System.Drawing.Point(120, 287);
            this.txtDifficulty.Name = "txtDifficulty";
            this.txtDifficulty.Size = new System.Drawing.Size(278, 27);
            this.txtDifficulty.TabIndex = 8;
            this.txtDifficulty.Enter += new System.EventHandler(this.txtDifficulty_Enter);
            // 
            // txtType
            // 
            this.txtType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtType.FormattingEnabled = true;
            this.txtType.Items.AddRange(new object[] {
            "Familiar",
            "Infantil",
            "Party",
            "Estrategia",
            "Abstracto",
            "Temático",
            "Recursos",
            "Wargame",
            "Otros"});
            this.txtType.Location = new System.Drawing.Point(121, 132);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(278, 27);
            this.txtType.TabIndex = 3;
            this.txtType.Enter += new System.EventHandler(this.txtType_Enter);
            // 
            // txtFamily
            // 
            this.txtFamily.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtFamily.FormattingEnabled = true;
            this.txtFamily.Items.AddRange(new object[] {
            "Cartas",
            "Fichas",
            "Tablero",
            "Miniaturas",
            "Dibujo",
            "Dados",
            "Preguntas y respuestas",
            "Otros"});
            this.txtFamily.Location = new System.Drawing.Point(121, 101);
            this.txtFamily.Name = "txtFamily";
            this.txtFamily.Size = new System.Drawing.Size(278, 27);
            this.txtFamily.TabIndex = 2;
            this.txtFamily.Enter += new System.EventHandler(this.txtFamily_Enter);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(2, 101);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 27);
            this.label9.TabIndex = 52;
            this.label9.Text = "Familia";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(2, 132);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 27);
            this.label8.TabIndex = 51;
            this.label8.Text = "Tipo";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(240, 163);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 27);
            this.label7.TabIndex = 50;
            this.label7.Text = "años";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtYears
            // 
            this.txtYears.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYears.Location = new System.Drawing.Point(121, 163);
            this.txtYears.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtYears.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtYears.Name = "txtYears";
            this.txtYears.Size = new System.Drawing.Size(120, 27);
            this.txtYears.TabIndex = 4;
            this.txtYears.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(2, 163);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 27);
            this.label1.TabIndex = 49;
            this.label1.Text = "A partir de ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDuration
            // 
            this.txtDuration.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDuration.Location = new System.Drawing.Point(121, 256);
            this.txtDuration.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.txtDuration.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtDuration.Name = "txtDuration";
            this.txtDuration.Size = new System.Drawing.Size(120, 27);
            this.txtDuration.TabIndex = 7;
            this.txtDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPlayerMax
            // 
            this.txtPlayerMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlayerMax.Location = new System.Drawing.Point(121, 225);
            this.txtPlayerMax.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtPlayerMax.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtPlayerMax.Name = "txtPlayerMax";
            this.txtPlayerMax.Size = new System.Drawing.Size(120, 27);
            this.txtPlayerMax.TabIndex = 6;
            this.txtPlayerMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPlayerMin
            // 
            this.txtPlayerMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlayerMin.Location = new System.Drawing.Point(121, 194);
            this.txtPlayerMin.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtPlayerMin.Minimum = new decimal(new int[] {
            99999999,
            0,
            0,
            -2147483648});
            this.txtPlayerMin.Name = "txtPlayerMin";
            this.txtPlayerMin.Size = new System.Drawing.Size(120, 27);
            this.txtPlayerMin.TabIndex = 5;
            this.txtPlayerMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(2, 287);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 27);
            this.label5.TabIndex = 47;
            this.label5.Text = "Dificultad";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ChkIsExpansion
            // 
            this.ChkIsExpansion.AutoSize = true;
            this.ChkIsExpansion.Location = new System.Drawing.Point(7, 321);
            this.ChkIsExpansion.Name = "ChkIsExpansion";
            this.ChkIsExpansion.Size = new System.Drawing.Size(221, 23);
            this.ChkIsExpansion.TabIndex = 9;
            this.ChkIsExpansion.Text = "Es expansión de otro juego";
            this.ChkIsExpansion.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(2, 256);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 27);
            this.label6.TabIndex = 35;
            this.label6.Text = "Duración (min)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(2, 225);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 27);
            this.label4.TabIndex = 31;
            this.label4.Text = "Máx Players";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(2, 194);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 27);
            this.label2.TabIndex = 27;
            this.label2.Text = "Min Players";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(2, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 27);
            this.label3.TabIndex = 25;
            this.label3.Text = "Editorial";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtEditorial
            // 
            this.txtEditorial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEditorial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEditorial.Location = new System.Drawing.Point(121, 70);
            this.txtEditorial.MaxLength = 100;
            this.txtEditorial.Name = "txtEditorial";
            this.txtEditorial.Size = new System.Drawing.Size(278, 27);
            this.txtEditorial.TabIndex = 1;
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(366, 3);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(35, 27);
            this.txtId.TabIndex = 24;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton7,
            this.toolStripSeparator1,
            this.toolStripButton8,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(401, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton1.Text = "Nuevo";
            this.toolStripButton1.ToolTipText = "Nuevo (Ctrl+N)";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton2.Text = "toolStripButton1";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton7.Text = "toolStripButton2";
            this.toolStripButton7.Visible = false;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton8.Text = "toolStripButton3";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // btnGameSave
            // 
            this.btnGameSave.Image = global::AlterParadox.GesUmbras.Properties.Resources.disk;
            this.btnGameSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGameSave.Location = new System.Drawing.Point(2, 469);
            this.btnGameSave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnGameSave.Name = "btnGameSave";
            this.btnGameSave.Size = new System.Drawing.Size(146, 38);
            this.btnGameSave.TabIndex = 11;
            this.btnGameSave.Text = "Save";
            this.btnGameSave.UseVisualStyleBackColor = true;
            this.btnGameSave.Click += new System.EventHandler(this.btnGameSave_Click);
            // 
            // lblGameName
            // 
            this.lblGameName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblGameName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGameName.Location = new System.Drawing.Point(2, 39);
            this.lblGameName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGameName.Name = "lblGameName";
            this.lblGameName.Size = new System.Drawing.Size(120, 27);
            this.lblGameName.TabIndex = 18;
            this.lblGameName.Text = "Nombre";
            this.lblGameName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGameComments
            // 
            this.txtGameComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGameComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGameComments.Location = new System.Drawing.Point(2, 373);
            this.txtGameComments.Multiline = true;
            this.txtGameComments.Name = "txtGameComments";
            this.txtGameComments.Size = new System.Drawing.Size(397, 92);
            this.txtGameComments.TabIndex = 10;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(121, 39);
            this.txtName.MaxLength = 100;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(278, 27);
            this.txtName.TabIndex = 0;
            // 
            // lblGameComments
            // 
            this.lblGameComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGameComments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblGameComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGameComments.Location = new System.Drawing.Point(2, 347);
            this.lblGameComments.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGameComments.Name = "lblGameComments";
            this.lblGameComments.Size = new System.Drawing.Size(397, 27);
            this.lblGameComments.TabIndex = 20;
            this.lblGameComments.Text = "Observaciones";
            this.lblGameComments.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmGames_NewGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 570);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.GridGames);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(884, 609);
            this.Name = "FrmGames_NewGame";
            this.Text = "Pantalla de juegos";
            this.Load += new System.EventHandler(this.FrmGames_NewGame_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGames)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtYears)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlayerMin)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnRefreshGrid;
        private System.Windows.Forms.Button btnCleanFilter;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.DataGridView GridGames;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_name;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEditorial;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Button btnGameSave;
        private System.Windows.Forms.Label lblGameName;
        private System.Windows.Forms.TextBox txtGameComments;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblGameComments;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox ChkIsExpansion;
        private System.Windows.Forms.Label label5;
        private NumericUpDownPro txtPlayerMin;
        private NumericUpDownPro txtDuration;
        private NumericUpDownPro txtPlayerMax;
        private System.Windows.Forms.Label label7;
        private NumericUpDownPro txtYears;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox txtType;
        private System.Windows.Forms.ComboBox txtFamily;
        private System.Windows.Forms.ComboBox txtDifficulty;
    }
}