﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
namespace AlterParadox.GesUmbras
{
    using AlterParadox.Core;
    using AlterParadox.GesUmbras.GameService;
    using Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class FrmAssociation_Search : Form
    {
        public FrmAssociation_Search()
        {
            InitializeComponent();
        }

        //LOCAL FUNCTIONS AND VARS
        Association _association;
       
        private void L_ApplyLanguage()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.Language);
            //this.Text = StringResources.UsersWindowTitle;
        }

        private void L_Refresh_Associations()
        {
            var filtro = txtFilter.Text;

            List<Association> listaAsociaciones = new List<Association>();
            try
            {
                using (GameServiceClient svc = new GameServiceClient())
                {
                    listaAsociaciones = svc.GetAssociations().Asociaciones.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Grid.AutoGenerateColumns = false;
            Grid.DataSource = listaAsociaciones
                .Where(x =>
                    (x.Name.Contains(filtro))
                ).ToList();
        }

        //FORMULARIO
        private void FrmGames_NewGame_Load(object sender, EventArgs e)
        {
            L_Refresh_Associations();
        }

        private void Grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            G._searchAssociation = (Association)Grid.SelectedRows[0].DataBoundItem;
            this.Close();
        }

        private void btnCleanFilter_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
        }

        private void btnRefreshGrid_Click(object sender, EventArgs e)
        {
            L_Refresh_Associations();
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_Refresh_Associations();
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Grid.SelectedRows.Count > 0)
                {
                    G._searchAssociation = (Association)Grid.SelectedRows[0].DataBoundItem;
                    this.Close();
                }
            }
        }

        private void txtFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                Grid.Focus();
            }
        }
    }
}
