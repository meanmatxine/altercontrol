﻿namespace AlterParadox.GesUmbras
{
    partial class FrmActWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.actGridView = new System.Windows.Forms.DataGridView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.colFecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colActividades = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOrganizador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAsociacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLugar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Inscritos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAbleToJoin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.actGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // actGridView
            // 
            this.actGridView.AllowUserToAddRows = false;
            this.actGridView.AllowUserToDeleteRows = false;
            this.actGridView.AllowUserToResizeColumns = false;
            this.actGridView.AllowUserToResizeRows = false;
            this.actGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.actGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.actGridView.ColumnHeadersHeight = 30;
            this.actGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.actGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFecha,
            this.colActividades,
            this.colOrganizador,
            this.colAsociacion,
            this.colLugar,
            this.colTipo,
            this.Inscritos,
            this.colAbleToJoin});
            this.actGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.actGridView.Location = new System.Drawing.Point(0, 0);
            this.actGridView.Margin = new System.Windows.Forms.Padding(4);
            this.actGridView.MultiSelect = false;
            this.actGridView.Name = "actGridView";
            this.actGridView.ReadOnly = true;
            this.actGridView.RowHeadersVisible = false;
            this.actGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.actGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.actGridView.Size = new System.Drawing.Size(1822, 845);
            this.actGridView.TabIndex = 0;
            this.actGridView.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.actGridView_CellPainting);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 6000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // colFecha
            // 
            this.colFecha.DataPropertyName = "ActivityDate";
            dataGridViewCellStyle1.Format = "g";
            dataGridViewCellStyle1.NullValue = null;
            this.colFecha.DefaultCellStyle = dataGridViewCellStyle1;
            this.colFecha.FillWeight = 60F;
            this.colFecha.HeaderText = "Fecha";
            this.colFecha.Name = "colFecha";
            this.colFecha.ReadOnly = true;
            // 
            // colActividades
            // 
            this.colActividades.DataPropertyName = "Name";
            this.colActividades.FillWeight = 140F;
            this.colActividades.HeaderText = "Actividad";
            this.colActividades.Name = "colActividades";
            this.colActividades.ReadOnly = true;
            // 
            // colOrganizador
            // 
            this.colOrganizador.DataPropertyName = "Organizer";
            this.colOrganizador.FillWeight = 60F;
            this.colOrganizador.HeaderText = "Organizador";
            this.colOrganizador.Name = "colOrganizador";
            this.colOrganizador.ReadOnly = true;
            this.colOrganizador.Visible = false;
            // 
            // colAsociacion
            // 
            this.colAsociacion.DataPropertyName = "Association";
            this.colAsociacion.FillWeight = 60F;
            this.colAsociacion.HeaderText = "Asociación";
            this.colAsociacion.Name = "colAsociacion";
            this.colAsociacion.ReadOnly = true;
            this.colAsociacion.Visible = false;
            // 
            // colLugar
            // 
            this.colLugar.DataPropertyName = "Place";
            this.colLugar.FillWeight = 60F;
            this.colLugar.HeaderText = "Lugar";
            this.colLugar.Name = "colLugar";
            this.colLugar.ReadOnly = true;
            // 
            // colTipo
            // 
            this.colTipo.DataPropertyName = "Type";
            this.colTipo.FillWeight = 40F;
            this.colTipo.HeaderText = "Tipo";
            this.colTipo.Name = "colTipo";
            this.colTipo.ReadOnly = true;
            // 
            // Inscritos
            // 
            this.Inscritos.DataPropertyName = "Participantes";
            this.Inscritos.FillWeight = 40F;
            this.Inscritos.HeaderText = "Plazas";
            this.Inscritos.Name = "Inscritos";
            this.Inscritos.ReadOnly = true;
            // 
            // colAbleToJoin
            // 
            this.colAbleToJoin.DataPropertyName = "IsAbleToJoin";
            this.colAbleToJoin.HeaderText = "AbleToJoin";
            this.colAbleToJoin.Name = "colAbleToJoin";
            this.colAbleToJoin.ReadOnly = true;
            this.colAbleToJoin.Visible = false;
            // 
            // FrmActWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1822, 845);
            this.Controls.Add(this.actGridView);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmActWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista de actividades";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmActWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.actGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView actGridView;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn colActividades;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOrganizador;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAsociacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLugar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Inscritos;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAbleToJoin;
    }
}