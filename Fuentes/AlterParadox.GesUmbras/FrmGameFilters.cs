﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmGameFilters : Form
    {
        public FrmGameFilters()
        {
            InitializeComponent();
        }

        private void L_Search_Association()
        {
            G._searchAssociation = null;
            FrmAssociation_Search formulario = new FrmAssociation_Search();
            G.AbrirPantalla(formulario, true, false);
            if (G._searchAssociation != null)
            {
                this.txtAssociationId.Text = "" + G._searchAssociation.Id;
                this.txtAssociationName.Text = "" + G._searchAssociation.Name;
                this.cmdApplyFilters.Focus();
            }
        }


        private void btnSearchAssociation_Click(object sender, EventArgs e)
        {
            L_Search_Association();
        }

        private void cmdApplyFilters_Click(object sender, EventArgs e)
        {
            G.filters.FilterActivate = G.LendingFilter.FilterActivate = true;

            if (this.txtFamily.SelectedIndex != -1)
                G.filters.Family = G.LendingFilter.Family = this.txtFamily.SelectedIndex;

            if (this.txtType.SelectedIndex != -1)
                G.filters.Type = G.LendingFilter.Type = this.txtType.SelectedIndex;

            if (this.txtPlayerMin.Value != 0)
                G.filters.MinPlayers = G.LendingFilter.MinPlayers = Convert.ToInt32(this.txtPlayerMin.Value);

            if (this.txtPlayerMax.Value != 0)
                G.filters.MaxPlayers = G.LendingFilter.MaxPlayers = Convert.ToInt32( this.txtPlayerMax.Value);

            if (this.txtDifficulty.SelectedIndex != -1)
                G.filters.Difficulty = G.LendingFilter.Difficulty = this.txtDifficulty.SelectedIndex;

            if (!string.IsNullOrEmpty(this.txtAssociationId.Text))
                G.filters.Association = G.LendingFilter.Association = Convert.ToInt32(this.txtAssociationId.Text);

            this.Close();
        }
    }
}
