﻿namespace AlterParadox.GesUmbras
{
    using System.Windows.Forms;
    partial class FrmActivities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmActivities));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.PanelUserData = new System.Windows.Forms.Panel();
            this.txtMinParticipants = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWinner = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Duracion = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDisable = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtType = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.GridActivityUsers = new System.Windows.Forms.DataGridView();
            this.colUserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReserve = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInscrito = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNumParticipants = new System.Windows.Forms.NumericUpDown();
            this.txtHour = new System.Windows.Forms.DateTimePicker();
            this.txtDate = new System.Windows.Forms.DateTimePicker();
            this.txtNeeds = new System.Windows.Forms.TextBox();
            this.lblNeeds = new System.Windows.Forms.Label();
            this.txtPlace = new System.Windows.Forms.TextBox();
            this.lblPlace = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.txtParticipantsComment = new System.Windows.Forms.TextBox();
            this.lblParticipants = new System.Windows.Forms.Label();
            this.txtOrganizer = new System.Windows.Forms.TextBox();
            this.lblOrganizer = new System.Windows.Forms.Label();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.lblComments = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOthers = new System.Windows.Forms.TextBox();
            this.lblOthers = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.cmdSave = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.mnuNew = new System.Windows.Forms.ToolStripButton();
            this.mnuSave = new System.Windows.Forms.ToolStripButton();
            this.mnuSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.cmdCleanFilter = new System.Windows.Forms.Button();
            this.GridActivities = new System.Windows.Forms.DataGridView();
            this.col_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_dni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMaxParticipants = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.PanelUserData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinParticipants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Duracion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridActivityUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumParticipants)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridActivities)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelUserData
            // 
            this.PanelUserData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelUserData.Controls.Add(this.txtMinParticipants);
            this.PanelUserData.Controls.Add(this.label7);
            this.PanelUserData.Controls.Add(this.label6);
            this.PanelUserData.Controls.Add(this.txtWinner);
            this.PanelUserData.Controls.Add(this.label5);
            this.PanelUserData.Controls.Add(this.Duracion);
            this.PanelUserData.Controls.Add(this.label4);
            this.PanelUserData.Controls.Add(this.lblDisable);
            this.PanelUserData.Controls.Add(this.label3);
            this.PanelUserData.Controls.Add(this.txtTotal);
            this.PanelUserData.Controls.Add(this.button1);
            this.PanelUserData.Controls.Add(this.txtType);
            this.PanelUserData.Controls.Add(this.textBox1);
            this.PanelUserData.Controls.Add(this.GridActivityUsers);
            this.PanelUserData.Controls.Add(this.label2);
            this.PanelUserData.Controls.Add(this.txtNumParticipants);
            this.PanelUserData.Controls.Add(this.txtHour);
            this.PanelUserData.Controls.Add(this.txtDate);
            this.PanelUserData.Controls.Add(this.txtNeeds);
            this.PanelUserData.Controls.Add(this.lblNeeds);
            this.PanelUserData.Controls.Add(this.txtPlace);
            this.PanelUserData.Controls.Add(this.lblPlace);
            this.PanelUserData.Controls.Add(this.lblType);
            this.PanelUserData.Controls.Add(this.txtParticipantsComment);
            this.PanelUserData.Controls.Add(this.lblParticipants);
            this.PanelUserData.Controls.Add(this.txtOrganizer);
            this.PanelUserData.Controls.Add(this.lblOrganizer);
            this.PanelUserData.Controls.Add(this.txtComments);
            this.PanelUserData.Controls.Add(this.lblComments);
            this.PanelUserData.Controls.Add(this.label1);
            this.PanelUserData.Controls.Add(this.txtOthers);
            this.PanelUserData.Controls.Add(this.lblOthers);
            this.PanelUserData.Controls.Add(this.lblDate);
            this.PanelUserData.Controls.Add(this.txtId);
            this.PanelUserData.Controls.Add(this.cmdSave);
            this.PanelUserData.Controls.Add(this.txtName);
            this.PanelUserData.Controls.Add(this.lblName);
            this.PanelUserData.Controls.Add(this.toolStrip1);
            this.PanelUserData.Location = new System.Drawing.Point(450, 0);
            this.PanelUserData.Name = "PanelUserData";
            this.PanelUserData.Size = new System.Drawing.Size(751, 593);
            this.PanelUserData.TabIndex = 1;
            // 
            // txtMinParticipants
            // 
            this.txtMinParticipants.Location = new System.Drawing.Point(322, 96);
            this.txtMinParticipants.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMinParticipants.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtMinParticipants.Name = "txtMinParticipants";
            this.txtMinParticipants.Size = new System.Drawing.Size(70, 23);
            this.txtMinParticipants.TabIndex = 59;
            this.txtMinParticipants.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(395, 96);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 23);
            this.label7.TabIndex = 58;
            this.label7.Text = "Participantes";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Location = new System.Drawing.Point(200, 96);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 23);
            this.label6.TabIndex = 57;
            this.label6.Text = "Min. Participantes";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtWinner
            // 
            this.txtWinner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWinner.Location = new System.Drawing.Point(125, 278);
            this.txtWinner.MaxLength = 100;
            this.txtWinner.Name = "txtWinner";
            this.txtWinner.Size = new System.Drawing.Size(622, 23);
            this.txtWinner.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Location = new System.Drawing.Point(3, 278);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 23);
            this.label5.TabIndex = 56;
            this.label5.Text = "Premio/Ganador";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Duracion
            // 
            this.Duracion.Location = new System.Drawing.Point(424, 69);
            this.Duracion.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Duracion.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Duracion.Name = "Duracion";
            this.Duracion.Size = new System.Drawing.Size(93, 23);
            this.Duracion.TabIndex = 54;
            this.Duracion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(302, 69);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 23);
            this.label4.TabIndex = 53;
            this.label4.Text = "Duración (min)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDisable
            // 
            this.lblDisable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblDisable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDisable.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisable.Location = new System.Drawing.Point(3, 439);
            this.lblDisable.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDisable.Name = "lblDisable";
            this.lblDisable.Size = new System.Drawing.Size(179, 69);
            this.lblDisable.TabIndex = 51;
            this.lblDisable.Text = "Inscripciones de usuario bloqueadas por fecha y hora";
            this.lblDisable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(3, 511);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 23);
            this.label3.TabIndex = 50;
            this.label3.Text = "Total";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(106, 511);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(76, 23);
            this.txtTotal.TabIndex = 49;
            // 
            // button1
            // 
            this.button1.Image = global::AlterParadox.GesUmbras.Properties.Resources.computer_go;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(3, 537);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 34);
            this.button1.TabIndex = 48;
            this.button1.Text = "Exportar lista";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtType
            // 
            this.txtType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtType.FormattingEnabled = true;
            this.txtType.Items.AddRange(new object[] {
            "Actividad",
            "REV",
            "Rol",
            "Demostración",
            "Taller"});
            this.txtType.Location = new System.Drawing.Point(576, 42);
            this.txtType.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(171, 22);
            this.txtType.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(3, 407);
            this.textBox1.MaxLength = 100;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(179, 33);
            this.textBox1.TabIndex = 10;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // GridActivityUsers
            // 
            this.GridActivityUsers.AllowUserToAddRows = false;
            this.GridActivityUsers.AllowUserToDeleteRows = false;
            this.GridActivityUsers.AllowUserToResizeColumns = false;
            this.GridActivityUsers.AllowUserToResizeRows = false;
            this.GridActivityUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridActivityUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridActivityUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.GridActivityUsers.ColumnHeadersHeight = 30;
            this.GridActivityUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridActivityUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUserId,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.colReserve,
            this.colInscrito});
            this.GridActivityUsers.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridActivityUsers.Location = new System.Drawing.Point(187, 367);
            this.GridActivityUsers.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridActivityUsers.MultiSelect = false;
            this.GridActivityUsers.Name = "GridActivityUsers";
            this.GridActivityUsers.ReadOnly = true;
            this.GridActivityUsers.RowHeadersVisible = false;
            this.GridActivityUsers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridActivityUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridActivityUsers.Size = new System.Drawing.Size(560, 218);
            this.GridActivityUsers.TabIndex = 45;
            this.GridActivityUsers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridActivityUsers_CellContentClick);
            this.GridActivityUsers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridActivityUsers_CellDoubleClick);
            this.GridActivityUsers.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.GridActivityUsers_DataBindingComplete);
            // 
            // colUserId
            // 
            this.colUserId.DataPropertyName = "Number";
            this.colUserId.FillWeight = 21.52697F;
            this.colUserId.HeaderText = "Nº";
            this.colUserId.Name = "colUserId";
            this.colUserId.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn3.FillWeight = 53.81742F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Comments";
            this.dataGridViewTextBoxColumn4.FillWeight = 53.81742F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Comments";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // colReserve
            // 
            this.colReserve.DataPropertyName = "Reserve";
            this.colReserve.FillWeight = 14.59455F;
            this.colReserve.HeaderText = "Reserve";
            this.colReserve.Name = "colReserve";
            this.colReserve.ReadOnly = true;
            this.colReserve.Visible = false;
            // 
            // colInscrito
            // 
            this.colInscrito.DataPropertyName = "Inscrito";
            this.colInscrito.FillWeight = 20F;
            this.colInscrito.HeaderText = "Inscrito";
            this.colInscrito.Name = "colInscrito";
            this.colInscrito.ReadOnly = true;
            this.colInscrito.Visible = false;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 367);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 41);
            this.label2.TabIndex = 43;
            this.label2.Text = "Nº Participante";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNumParticipants
            // 
            this.txtNumParticipants.Location = new System.Drawing.Point(124, 96);
            this.txtNumParticipants.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNumParticipants.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.txtNumParticipants.Name = "txtNumParticipants";
            this.txtNumParticipants.Size = new System.Drawing.Size(70, 23);
            this.txtNumParticipants.TabIndex = 2;
            this.txtNumParticipants.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtHour
            // 
            this.txtHour.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.txtHour.Location = new System.Drawing.Point(217, 69);
            this.txtHour.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtHour.Name = "txtHour";
            this.txtHour.ShowUpDown = true;
            this.txtHour.Size = new System.Drawing.Size(81, 23);
            this.txtHour.TabIndex = 41;
            this.txtHour.Value = new System.DateTime(2017, 3, 17, 0, 0, 0, 0);
            // 
            // txtDate
            // 
            this.txtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDate.Location = new System.Drawing.Point(124, 69);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(94, 23);
            this.txtDate.TabIndex = 40;
            this.txtDate.Value = new System.DateTime(2018, 8, 16, 0, 0, 0, 0);
            // 
            // txtNeeds
            // 
            this.txtNeeds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNeeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNeeds.Location = new System.Drawing.Point(251, 172);
            this.txtNeeds.Multiline = true;
            this.txtNeeds.Name = "txtNeeds";
            this.txtNeeds.Size = new System.Drawing.Size(245, 75);
            this.txtNeeds.TabIndex = 6;
            this.txtNeeds.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNeeds_KeyDown);
            // 
            // lblNeeds
            // 
            this.lblNeeds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNeeds.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblNeeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNeeds.Location = new System.Drawing.Point(251, 150);
            this.lblNeeds.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNeeds.Name = "lblNeeds";
            this.lblNeeds.Size = new System.Drawing.Size(245, 23);
            this.lblNeeds.TabIndex = 39;
            this.lblNeeds.Text = "Necesidades (F4)";
            this.lblNeeds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPlace
            // 
            this.txtPlace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPlace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlace.Location = new System.Drawing.Point(124, 123);
            this.txtPlace.MaxLength = 100;
            this.txtPlace.Name = "txtPlace";
            this.txtPlace.Size = new System.Drawing.Size(623, 23);
            this.txtPlace.TabIndex = 4;
            // 
            // lblPlace
            // 
            this.lblPlace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblPlace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPlace.Location = new System.Drawing.Point(2, 123);
            this.lblPlace.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPlace.Name = "lblPlace";
            this.lblPlace.Size = new System.Drawing.Size(123, 23);
            this.lblPlace.TabIndex = 36;
            this.lblPlace.Text = "Lugar";
            this.lblPlace.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblType
            // 
            this.lblType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblType.Location = new System.Drawing.Point(452, 42);
            this.lblType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(123, 23);
            this.lblType.TabIndex = 34;
            this.lblType.Text = "Tipo";
            this.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtParticipantsComment
            // 
            this.txtParticipantsComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtParticipantsComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtParticipantsComment.Location = new System.Drawing.Point(517, 96);
            this.txtParticipantsComment.MaxLength = 100;
            this.txtParticipantsComment.Name = "txtParticipantsComment";
            this.txtParticipantsComment.Size = new System.Drawing.Size(230, 23);
            this.txtParticipantsComment.TabIndex = 3;
            // 
            // lblParticipants
            // 
            this.lblParticipants.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblParticipants.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblParticipants.Location = new System.Drawing.Point(2, 96);
            this.lblParticipants.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblParticipants.Name = "lblParticipants";
            this.lblParticipants.Size = new System.Drawing.Size(123, 23);
            this.lblParticipants.TabIndex = 31;
            this.lblParticipants.Text = "Max. Participantes";
            this.lblParticipants.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOrganizer
            // 
            this.txtOrganizer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOrganizer.Location = new System.Drawing.Point(125, 251);
            this.txtOrganizer.MaxLength = 100;
            this.txtOrganizer.Name = "txtOrganizer";
            this.txtOrganizer.Size = new System.Drawing.Size(622, 23);
            this.txtOrganizer.TabIndex = 8;
            // 
            // lblOrganizer
            // 
            this.lblOrganizer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblOrganizer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrganizer.Location = new System.Drawing.Point(3, 251);
            this.lblOrganizer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOrganizer.Name = "lblOrganizer";
            this.lblOrganizer.Size = new System.Drawing.Size(123, 23);
            this.lblOrganizer.TabIndex = 27;
            this.lblOrganizer.Text = "Organizador";
            this.lblOrganizer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtComments.Location = new System.Drawing.Point(500, 172);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(247, 75);
            this.txtComments.TabIndex = 7;
            this.txtComments.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtComments_KeyDown);
            // 
            // lblComments
            // 
            this.lblComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblComments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblComments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblComments.Location = new System.Drawing.Point(500, 150);
            this.lblComments.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(247, 23);
            this.lblComments.TabIndex = 25;
            this.lblComments.Text = "Comments (F4)";
            this.lblComments.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 342);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(747, 26);
            this.label1.TabIndex = 23;
            this.label1.Text = "Inscritos";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtOthers
            // 
            this.txtOthers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOthers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOthers.Location = new System.Drawing.Point(2, 172);
            this.txtOthers.Multiline = true;
            this.txtOthers.Name = "txtOthers";
            this.txtOthers.Size = new System.Drawing.Size(245, 75);
            this.txtOthers.TabIndex = 5;
            this.txtOthers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOthers_KeyDown);
            // 
            // lblOthers
            // 
            this.lblOthers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOthers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblOthers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOthers.Location = new System.Drawing.Point(2, 150);
            this.lblOthers.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOthers.Name = "lblOthers";
            this.lblOthers.Size = new System.Drawing.Size(245, 23);
            this.lblOthers.TabIndex = 14;
            this.lblOthers.Text = "Descripción (F4)";
            this.lblOthers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDate.Location = new System.Drawing.Point(2, 69);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(123, 23);
            this.lblDate.TabIndex = 13;
            this.lblDate.Text = "Fecha";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtId
            // 
            this.txtId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(712, 0);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(36, 23);
            this.txtId.TabIndex = 10;
            // 
            // cmdSave
            // 
            this.cmdSave.Image = global::AlterParadox.GesUmbras.Properties.Resources.disk;
            this.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSave.Location = new System.Drawing.Point(3, 306);
            this.cmdSave.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(122, 34);
            this.cmdSave.TabIndex = 9;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtName.Location = new System.Drawing.Point(124, 42);
            this.txtName.MaxLength = 100;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(324, 23);
            this.txtName.TabIndex = 0;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Location = new System.Drawing.Point(2, 42);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(123, 23);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Nombre";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNew,
            this.mnuSave,
            this.mnuSearch,
            this.toolStripSeparator1,
            this.mnuDelete,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(751, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // mnuNew
            // 
            this.mnuNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuNew.Image = ((System.Drawing.Image)(resources.GetObject("mnuNew.Image")));
            this.mnuNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Size = new System.Drawing.Size(36, 36);
            this.mnuNew.Text = "Nuevo";
            this.mnuNew.ToolTipText = "Nuevo (Ctrl+N)";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuSave.Image = ((System.Drawing.Image)(resources.GetObject("mnuSave.Image")));
            this.mnuSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Size = new System.Drawing.Size(36, 36);
            this.mnuSave.Text = "toolStripButton1";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSearch
            // 
            this.mnuSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuSearch.Image = ((System.Drawing.Image)(resources.GetObject("mnuSearch.Image")));
            this.mnuSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Size = new System.Drawing.Size(36, 36);
            this.mnuSearch.Text = "toolStripButton2";
            this.mnuSearch.Visible = false;
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // mnuDelete
            // 
            this.mnuDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mnuDelete.Image = ((System.Drawing.Image)(resources.GetObject("mnuDelete.Image")));
            this.mnuDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(36, 36);
            this.mnuDelete.Text = "toolStripButton3";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.chkAll);
            this.panel1.Controls.Add(this.cmdCleanFilter);
            this.panel1.Controls.Add(this.GridActivities);
            this.panel1.Controls.Add(this.txtFilter);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(445, 593);
            this.panel1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(423, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(21, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "R";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Location = new System.Drawing.Point(3, 565);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(109, 20);
            this.chkAll.TabIndex = 10;
            this.chkAll.Text = "Mostrar Todas";
            this.chkAll.UseVisualStyleBackColor = true;
            this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            // 
            // cmdCleanFilter
            // 
            this.cmdCleanFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCleanFilter.Location = new System.Drawing.Point(403, 0);
            this.cmdCleanFilter.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdCleanFilter.Name = "cmdCleanFilter";
            this.cmdCleanFilter.Size = new System.Drawing.Size(21, 23);
            this.cmdCleanFilter.TabIndex = 9;
            this.cmdCleanFilter.Text = "X";
            this.cmdCleanFilter.UseVisualStyleBackColor = true;
            this.cmdCleanFilter.Click += new System.EventHandler(this.cmdCleanFilter_Click);
            // 
            // GridActivities
            // 
            this.GridActivities.AllowUserToAddRows = false;
            this.GridActivities.AllowUserToDeleteRows = false;
            this.GridActivities.AllowUserToResizeColumns = false;
            this.GridActivities.AllowUserToResizeRows = false;
            this.GridActivities.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridActivities.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GridActivities.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.GridActivities.ColumnHeadersHeight = 30;
            this.GridActivities.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridActivities.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_id,
            this.col_dni,
            this.col_name,
            this.colMaxParticipants});
            this.GridActivities.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridActivities.Location = new System.Drawing.Point(0, 22);
            this.GridActivities.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.GridActivities.MultiSelect = false;
            this.GridActivities.Name = "GridActivities";
            this.GridActivities.ReadOnly = true;
            this.GridActivities.RowHeadersVisible = false;
            this.GridActivities.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridActivities.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridActivities.Size = new System.Drawing.Size(445, 537);
            this.GridActivities.TabIndex = 1;
            this.GridActivities.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridActivities_CellContentClick);
            this.GridActivities.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridActivities_CellDoubleClick);
            // 
            // col_id
            // 
            this.col_id.DataPropertyName = "id";
            this.col_id.FillWeight = 20F;
            this.col_id.HeaderText = "Id";
            this.col_id.MinimumWidth = 10;
            this.col_id.Name = "col_id";
            this.col_id.ReadOnly = true;
            this.col_id.Visible = false;
            // 
            // col_dni
            // 
            this.col_dni.DataPropertyName = "activitydate";
            this.col_dni.FillWeight = 40F;
            this.col_dni.HeaderText = "Fecha";
            this.col_dni.MinimumWidth = 40;
            this.col_dni.Name = "col_dni";
            this.col_dni.ReadOnly = true;
            // 
            // col_name
            // 
            this.col_name.DataPropertyName = "name";
            this.col_name.FillWeight = 80F;
            this.col_name.HeaderText = "Nombre";
            this.col_name.MinimumWidth = 40;
            this.col_name.Name = "col_name";
            this.col_name.ReadOnly = true;
            // 
            // colMaxParticipants
            // 
            this.colMaxParticipants.DataPropertyName = "Participantes";
            this.colMaxParticipants.FillWeight = 20F;
            this.colMaxParticipants.HeaderText = "P";
            this.colMaxParticipants.MinimumWidth = 10;
            this.colMaxParticipants.Name = "colMaxParticipants";
            this.colMaxParticipants.ReadOnly = true;
            // 
            // txtFilter
            // 
            this.txtFilter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilter.Location = new System.Drawing.Point(0, 0);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(404, 23);
            this.txtFilter.TabIndex = 0;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // FrmActivities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 597);
            this.Controls.Add(this.PanelUserData);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmActivities";
            this.Text = "Actividades";
            this.Load += new System.EventHandler(this.FrmActivities_Load);
            this.PanelUserData.ResumeLayout(false);
            this.PanelUserData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMinParticipants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Duracion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridActivityUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumParticipants)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridActivities)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelUserData;
        private System.Windows.Forms.TextBox txtOthers;
        private System.Windows.Forms.Label lblOthers;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton mnuNew;
        private System.Windows.Forms.ToolStripButton mnuSave;
        private System.Windows.Forms.ToolStripButton mnuSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton mnuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdCleanFilter;
        private System.Windows.Forms.DataGridView GridActivities;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtComments;
        private System.Windows.Forms.Label lblComments;
        private System.Windows.Forms.TextBox txtOrganizer;
        private System.Windows.Forms.Label lblOrganizer;
        private System.Windows.Forms.TextBox txtPlace;
        private System.Windows.Forms.Label lblPlace;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.TextBox txtParticipantsComment;
        private System.Windows.Forms.Label lblParticipants;
        private System.Windows.Forms.TextBox txtNeeds;
        private System.Windows.Forms.Label lblNeeds;
        private DateTimePicker txtDate;
        private DateTimePicker txtHour;
        private NumericUpDown txtNumParticipants;
        private System.Windows.Forms.DataGridView GridActivityUsers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox txtType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkAll;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTotal;
        private Label lblDisable;
        private Label label4;
        private NumericUpDown Duracion;
        private TextBox txtWinner;
        private Label label5;
        private DataGridViewTextBoxColumn colUserId;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn colReserve;
        private DataGridViewTextBoxColumn colInscrito;
        private NumericUpDown txtMinParticipants;
        private Label label7;
        private Label label6;
        private DataGridViewTextBoxColumn col_id;
        private DataGridViewTextBoxColumn col_dni;
        private DataGridViewTextBoxColumn col_name;
        private DataGridViewTextBoxColumn colMaxParticipants;
    }
}