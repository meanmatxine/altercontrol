﻿namespace AlterParadox.GesUmbras
{
    partial class FrmGames_Loan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.GridGamesLoan = new System.Windows.Forms.DataGridView();
            this.colLoanId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLoanTheGame = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColLoanTheUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtLendFilter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdCleanFilter = new System.Windows.Forms.Button();
            this.cmdAddFilters = new System.Windows.Forms.Button();
            this.btnNewGame = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.GridGamesAvailable = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTheGame = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAssociation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtAvailableFilter = new System.Windows.Forms.TextBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGamesLoan)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGamesAvailable)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.RefreshButton, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.88889F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1109, 569);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.GridGamesLoan, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtLendFilter, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(557, 35);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(549, 471);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // GridGamesLoan
            // 
            this.GridGamesLoan.AllowUserToAddRows = false;
            this.GridGamesLoan.AllowUserToDeleteRows = false;
            this.GridGamesLoan.AllowUserToResizeColumns = false;
            this.GridGamesLoan.AllowUserToResizeRows = false;
            this.GridGamesLoan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridGamesLoan.ColumnHeadersHeight = 30;
            this.GridGamesLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridGamesLoan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLoanId,
            this.colLoanTheGame,
            this.ColLoanTheUser});
            this.GridGamesLoan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridGamesLoan.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridGamesLoan.Location = new System.Drawing.Point(3, 35);
            this.GridGamesLoan.MultiSelect = false;
            this.GridGamesLoan.Name = "GridGamesLoan";
            this.GridGamesLoan.ReadOnly = true;
            this.GridGamesLoan.RowHeadersVisible = false;
            this.GridGamesLoan.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridGamesLoan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridGamesLoan.Size = new System.Drawing.Size(543, 433);
            this.GridGamesLoan.TabIndex = 1;
            this.GridGamesLoan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridGamesLoan_CellDoubleClick);
            // 
            // colLoanId
            // 
            this.colLoanId.DataPropertyName = "Id";
            this.colLoanId.HeaderText = "Id";
            this.colLoanId.Name = "colLoanId";
            this.colLoanId.ReadOnly = true;
            this.colLoanId.Visible = false;
            // 
            // colLoanTheGame
            // 
            this.colLoanTheGame.DataPropertyName = "TheGame";
            this.colLoanTheGame.FillWeight = 50F;
            this.colLoanTheGame.HeaderText = "Nombre del juego";
            this.colLoanTheGame.Name = "colLoanTheGame";
            this.colLoanTheGame.ReadOnly = true;
            // 
            // ColLoanTheUser
            // 
            this.ColLoanTheUser.DataPropertyName = "TheUser";
            this.ColLoanTheUser.FillWeight = 50F;
            this.ColLoanTheUser.HeaderText = "Usuario";
            this.ColLoanTheUser.Name = "ColLoanTheUser";
            this.ColLoanTheUser.ReadOnly = true;
            // 
            // txtLendFilter
            // 
            this.txtLendFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLendFilter.Location = new System.Drawing.Point(3, 3);
            this.txtLendFilter.Name = "txtLendFilter";
            this.txtLendFilter.Size = new System.Drawing.Size(543, 27);
            this.txtLendFilter.TabIndex = 0;
            this.txtLendFilter.TextChanged += new System.EventHandler(this.txtLendFilter_TextChanged);
            this.txtLendFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLendFilter_KeyPress);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(4, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(546, 32);
            this.label5.TabIndex = 18;
            this.label5.Text = "Juegos disponibles (F2)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(558, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(547, 32);
            this.label1.TabIndex = 19;
            this.label1.Text = "Juegos prestados (F4)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.cmdCleanFilter, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmdAddFilters, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnNewGame, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 512);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(548, 54);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // cmdCleanFilter
            // 
            this.cmdCleanFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdCleanFilter.Location = new System.Drawing.Point(367, 3);
            this.cmdCleanFilter.Name = "cmdCleanFilter";
            this.cmdCleanFilter.Size = new System.Drawing.Size(178, 48);
            this.cmdCleanFilter.TabIndex = 2;
            this.cmdCleanFilter.Text = "Limpiar filtro";
            this.cmdCleanFilter.UseVisualStyleBackColor = true;
            this.cmdCleanFilter.Click += new System.EventHandler(this.cmdCleanFilter_Click);
            // 
            // cmdAddFilters
            // 
            this.cmdAddFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAddFilters.Location = new System.Drawing.Point(185, 3);
            this.cmdAddFilters.Name = "cmdAddFilters";
            this.cmdAddFilters.Size = new System.Drawing.Size(176, 48);
            this.cmdAddFilters.TabIndex = 1;
            this.cmdAddFilters.Text = "Filtro de juegos";
            this.cmdAddFilters.UseVisualStyleBackColor = true;
            this.cmdAddFilters.Click += new System.EventHandler(this.cmdAddFilters_Click);
            // 
            // btnNewGame
            // 
            this.btnNewGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewGame.Location = new System.Drawing.Point(3, 3);
            this.btnNewGame.Name = "btnNewGame";
            this.btnNewGame.Size = new System.Drawing.Size(176, 48);
            this.btnNewGame.TabIndex = 0;
            this.btnNewGame.Text = "Juegos locales";
            this.btnNewGame.UseVisualStyleBackColor = true;
            this.btnNewGame.Click += new System.EventHandler(this.btnNewGame_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.GridGamesAvailable, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtAvailableFilter, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 35);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(548, 471);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // GridGamesAvailable
            // 
            this.GridGamesAvailable.AllowUserToAddRows = false;
            this.GridGamesAvailable.AllowUserToDeleteRows = false;
            this.GridGamesAvailable.AllowUserToResizeColumns = false;
            this.GridGamesAvailable.AllowUserToResizeRows = false;
            this.GridGamesAvailable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridGamesAvailable.ColumnHeadersHeight = 30;
            this.GridGamesAvailable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridGamesAvailable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colTheGame,
            this.colAssociation});
            this.GridGamesAvailable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridGamesAvailable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.GridGamesAvailable.Location = new System.Drawing.Point(3, 35);
            this.GridGamesAvailable.MultiSelect = false;
            this.GridGamesAvailable.Name = "GridGamesAvailable";
            this.GridGamesAvailable.ReadOnly = true;
            this.GridGamesAvailable.RowHeadersVisible = false;
            this.GridGamesAvailable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridGamesAvailable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridGamesAvailable.Size = new System.Drawing.Size(542, 433);
            this.GridGamesAvailable.TabIndex = 1;
            this.GridGamesAvailable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridGamesAvailable_CellContentClick);
            this.GridGamesAvailable.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridGamesAvailable_CellDoubleClick);
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Visible = false;
            // 
            // colTheGame
            // 
            this.colTheGame.DataPropertyName = "TheGame";
            this.colTheGame.FillWeight = 60F;
            this.colTheGame.HeaderText = "Nombre del juego";
            this.colTheGame.Name = "colTheGame";
            this.colTheGame.ReadOnly = true;
            // 
            // colAssociation
            // 
            this.colAssociation.DataPropertyName = "Association";
            this.colAssociation.FillWeight = 40F;
            this.colAssociation.HeaderText = "Asociación";
            this.colAssociation.Name = "colAssociation";
            this.colAssociation.ReadOnly = true;
            // 
            // txtAvailableFilter
            // 
            this.txtAvailableFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAvailableFilter.Location = new System.Drawing.Point(3, 3);
            this.txtAvailableFilter.Name = "txtAvailableFilter";
            this.txtAvailableFilter.Size = new System.Drawing.Size(542, 27);
            this.txtAvailableFilter.TabIndex = 0;
            this.txtAvailableFilter.TextChanged += new System.EventHandler(this.txtAvailableFilter_TextChanged);
            this.txtAvailableFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAvailableFilter_KeyPress);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Location = new System.Drawing.Point(557, 512);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(120, 51);
            this.RefreshButton.TabIndex = 20;
            this.RefreshButton.Text = "Refrescar";
            this.RefreshButton.UseVisualStyleBackColor = true;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // FrmGames_Loan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 569);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmGames_Loan";
            this.Text = "Prestamo de juegos";
            this.Load += new System.EventHandler(this.FrmGames_Loan_Load);
            this.Enter += new System.EventHandler(this.FrmGames_Loan_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmGames_Loan_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGamesLoan)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGamesAvailable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnNewGame;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.DataGridView GridGamesAvailable;
        private System.Windows.Forms.TextBox txtAvailableFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.DataGridView GridGamesLoan;
        private System.Windows.Forms.TextBox txtLendFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTheGame;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAssociation;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLoanId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLoanTheGame;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColLoanTheUser;
        private System.Windows.Forms.Button cmdAddFilters;
        private System.Windows.Forms.Button cmdCleanFilter;
        private System.Windows.Forms.Button RefreshButton;
    }
}