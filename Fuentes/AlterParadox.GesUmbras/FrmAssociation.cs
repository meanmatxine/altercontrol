﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.GameService;
using AlterParadox.GesUmbras.Properties;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;

namespace AlterParadox.GesUmbras
{
    public partial class FrmAssociation : Form
    {
        public FrmAssociation()
        {
            InitializeComponent();
        }

        private Association _association;
        private void L_Refresh_Associations()
        {
            var filtro = txtFilter.Text;

            List<Association> listaAsociaciones = new List<Association>();
            try
            {
                using (GameServiceClient svc = new GameServiceClient())
                {
                    listaAsociaciones = svc.GetAssociations().Asociaciones.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Grid.AutoGenerateColumns = false;
            Grid.DataSource = listaAsociaciones
                .Where(x =>
                    (x.Name.Contains(filtro)) 
                ).ToList();
        }

        private void L_New_Association()
        {
            L_Load_Association(new Association());
        }

        private void L_Load_Association(Association asociacion)
        {
            _association = asociacion;
            txtId.DataBindings.Clear();
            txtId.DataBindings.Add("Text", _association, "Id", true, DataSourceUpdateMode.OnPropertyChanged);
            txtName.DataBindings.Clear();
            txtName.DataBindings.Add("Text", _association, "Name", true, DataSourceUpdateMode.OnPropertyChanged);
            txtContact.DataBindings.Clear();
            txtContact.DataBindings.Add("Text", _association, "Contact", true, DataSourceUpdateMode.OnPropertyChanged);
            txtEmail.DataBindings.Clear();
            txtEmail.DataBindings.Add("Text", _association, "Email", true, DataSourceUpdateMode.OnPropertyChanged);
            txtPhone.DataBindings.Clear();
            txtPhone.DataBindings.Add("Text", _association, "Phone", true, DataSourceUpdateMode.OnPropertyChanged);
            txtComments.DataBindings.Clear();
            txtComments.DataBindings.Add("Text", _association, "Comments", true, DataSourceUpdateMode.OnPropertyChanged);
            txtName.Focus();
        }
        private void L_Save_Association()
        {
            try
            {
                using (GameServiceClient svc = new GameServiceClient())
                {
                    var response = svc.SaveAssociation( new SaveAssociationRequest() { Asociacion = _association });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            L_Refresh_Associations();
            L_New_Association();
        }

        //FORM EVENTS
        private void FrmAssociation_Load(object sender, EventArgs e)
        {
            L_Refresh_Associations();
            L_New_Association();
        }

        private void btnGameSave_Click(object sender, EventArgs e)
        {
            L_Save_Association();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            L_New_Association();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            L_Save_Association();
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            L_Refresh_Associations();
        }

        private void btnCleanFilter_Click(object sender, EventArgs e)
        {
            txtFilter.Text = "";
        }

        private void btnRefreshGrid_Click(object sender, EventArgs e)
        {
            L_Refresh_Associations();
        }

        private void Grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                L_Load_Association((Association)Grid.Rows[e.RowIndex].DataBoundItem);
            }
        }
    }
}
