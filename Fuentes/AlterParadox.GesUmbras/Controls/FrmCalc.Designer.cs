﻿namespace AlterParadox.GesUmbras
{
    partial class FrmCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Resultado = new System.Windows.Forms.TextBox();
            this.Formula = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Resultado
            // 
            this.Resultado.Enabled = false;
            this.Resultado.Location = new System.Drawing.Point(545, 71);
            this.Resultado.Margin = new System.Windows.Forms.Padding(6);
            this.Resultado.Name = "Resultado";
            this.Resultado.Size = new System.Drawing.Size(220, 20);
            this.Resultado.TabIndex = 5;
            // 
            // Formula
            // 
            this.Formula.Location = new System.Drawing.Point(15, 15);
            this.Formula.Margin = new System.Windows.Forms.Padding(6);
            this.Formula.Name = "Formula";
            this.Formula.Size = new System.Drawing.Size(751, 20);
            this.Formula.TabIndex = 4;
            this.Formula.TextChanged += new System.EventHandler(this.Formula_TextChanged);
            this.Formula.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Formula_KeyDown);
            this.Formula.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Formula_KeyUp);
            // 
            // FrmCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 115);
            this.Controls.Add(this.Resultado);
            this.Controls.Add(this.Formula);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmCalc";
            this.Text = "FrmCalc";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Resultado;
        private System.Windows.Forms.TextBox Formula;
    }
}