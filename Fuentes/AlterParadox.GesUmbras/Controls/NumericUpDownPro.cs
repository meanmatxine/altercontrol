﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class NumericUpDownPro : System.Windows.Forms.NumericUpDown
    {

        public NumericUpDownPro()
        {
            //this.DecimalPlaces = 2;
            this.TextAlign = HorizontalAlignment.Right;
            this.Minimum = -99999999;
            this.Maximum = 99999999;
        }
        protected override void OnEnter(EventArgs e)
        {
            this.Select(0, 1000);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                G.Calculadora(this);
                this.Select(0, 1000);

            }
        }
        protected override void OnKeyPress(KeyPressEventArgs e)
        {

            //MessageBox.Show(e.KeyChar.ToString());
            switch (e.KeyChar.ToString())
            {
                //case Keys.OemOpenBrackets:
                //  e.SuppressKeyPress = true;
                case ".":
                    //e.KeyChar:
                    e.Handled = true;
                    SendKeys.Send(",");
                    break;
            }


        }
  
}
}
