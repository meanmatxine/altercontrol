﻿using System;
using System.Data;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmCalc : Form
    {
        public FrmCalc()
        {
            InitializeComponent();
        }
        static double Evaluate(string expression)
        {
            var loDataTable = new DataTable();
            try
            {
                var loDataColumn = new DataColumn("Eval", typeof(double), expression);
                loDataTable.Columns.Add(loDataColumn);
                loDataTable.Rows.Add(0);
                return (double)(loDataTable.Rows[0]["Eval"]);
            }
            catch (SyntaxErrorException e)
            {
                MessageBox.Show(e.ToString());
                return 0;
            }
        }

        private void Formula_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                
            }
        }

        private void Formula_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Formula.Text = "";
                    break;
                case Keys.Enter:
                    G._searchResult = Resultado.Text;
                    this.Close();
                    break;
                case Keys.NumPad0:
                case Keys.NumPad1:
                case Keys.NumPad2:
                case Keys.NumPad3:
                case Keys.NumPad4:
                case Keys.NumPad5:
                case Keys.NumPad6:
                case Keys.NumPad7:
                case Keys.NumPad8:
                case Keys.NumPad9:
                    string mystring = Formula.Text;
                    Double result = Evaluate(mystring);
                    Resultado.Text = "" + result;
                    break;
            }
        }

        private void Formula_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
