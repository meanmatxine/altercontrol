﻿using AlterParadox.Core;
using AlterParadox.GesUmbras.ActivityService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.GesUmbras
{
    public partial class FrmActivityUser : Form
    {
        public ActivityUser activityUser;
        public bool reserve = false;
        public FrmActivityUser()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            activityUser.Comments = txtOthers.Text;
            activityUser.Reserve = reserve;
            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    svc.SaveActivityUser(new SaveActivityUserRequest() { ActividadUsuario = activityUser });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            activityUser.Comments = txtOthers.Text;
            activityUser.Reserve = true;

            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    svc.SaveActivityUser(new SaveActivityUserRequest() {  ActividadUsuario = activityUser });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                using (ActivityServiceClient svc = new ActivityServiceClient())
                {
                    svc.DeleteActivityUser(new DeleteActivityUserRequest() {  Id = (int)activityUser.Id });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            this.Close();
        }

        private void FrmActivityUser_Load(object sender, EventArgs e)
        {
            this.Show();
            if(this.reserve)
            {
                button1.Enabled = false;
            }
            txtName.Text = ""  + activityUser.Name;
            txtAviso.Text = ""  + activityUser.Aviso;
            txtOthers.Text = "" + activityUser.Comments;
        }

        
    }
}
