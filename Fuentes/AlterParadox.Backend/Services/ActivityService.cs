﻿using AlterParadox.Backend.DAL;
using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Configuration;

namespace AlterParadox.Backend.Services
{
    [ServiceContract]
    public interface IActivityService
    {
       
        [OperationContract]
        GetActivityResponse GetActivity(GetActivityRequest request);

        [OperationContract]
        GetActivitiesResponse GetActivities();

        [OperationContract]
        SaveActivityResponse SaveActivity(SaveActivityRequest request);

        [OperationContract]
        DeleteActivityResponse DeleteActivity(DeleteActivityRequest request);

        [OperationContract]
        int CheckParticipants(Activity activity);

        #region actividadesUsuario
        
        [OperationContract]
        GetActivityUserResponse GetActivityUser(GetActivityUserRequest request);

        [OperationContract]
        GetActivityUsersResponse GetActivityUsers(GetActivityUsersRequest request);

        [OperationContract]
        SaveActivityUserResponse SaveActivityUser(SaveActivityUserRequest request);

        [OperationContract]
        DeleteActivityUserResponse DeleteActivityUser(DeleteActivityUserRequest request);

        #endregion

    }

    public class ActivityService : IActivityService
    {
        private readonly IActivityServiceRepository _ActivityServiceRepository;
        private readonly ISQLServer _sqlServer;
        public ActivityService(
            IActivityServiceRepository ActivityServiceRepository,
            ISQLServer sqlServer)
        {
            if (ActivityServiceRepository == null) throw new ArgumentNullException("IActivityServiceRepository no puede ser nulo en ActivityService");
            if (sqlServer == null) throw new ArgumentNullException("ISQLServer no puede ser nulo en ActivityService");

            _ActivityServiceRepository = ActivityServiceRepository;
            _sqlServer = sqlServer;

            _sqlServer.Init();
        }
        public GetActivityResponse GetActivity(GetActivityRequest request)
        {
            var responseError = GetActivityResponse.RespuestaFallo(Result<GetActivityRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ActivityServiceRepository.GetActivity(request);

            return resultadoEjecucion;
        }

        public GetActivitiesResponse GetActivities()
        {
            var resultadoEjecucion = _ActivityServiceRepository.GetActivities();
            return resultadoEjecucion;
        }

        public SaveActivityResponse SaveActivity(SaveActivityRequest request)
        {
            var responseError = SaveActivityResponse.RespuestaFallo(Result<SaveActivityRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ActivityServiceRepository.SaveActivity(request);

            return resultadoEjecucion;
        }
        
        public DeleteActivityResponse DeleteActivity(DeleteActivityRequest request)
        {
            var responseError = DeleteActivityResponse.RespuestaFallo(Result<DeleteActivityRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ActivityServiceRepository.DeleteActivity(request);

            return resultadoEjecucion;
        }

        public int CheckParticipants(Activity activity)
        {
            return _ActivityServiceRepository.CheckParticipants(activity);
        }

        public GetActivityUserResponse GetActivityUser(GetActivityUserRequest request)
        {
            var responseError = GetActivityUserResponse.RespuestaFallo(Result<GetActivityUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ActivityServiceRepository.GetActivityUser(request);

            return resultadoEjecucion;
        }

        public GetActivityUsersResponse GetActivityUsers(GetActivityUsersRequest request)
        {
            var resultadoEjecucion = _ActivityServiceRepository.GetActivityUsers(request);
            return resultadoEjecucion;
        }

        public SaveActivityUserResponse SaveActivityUser(SaveActivityUserRequest request)
        {
            var responseError = SaveActivityUserResponse.RespuestaFallo(Result<SaveActivityUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ActivityServiceRepository.SaveActivityUser(request);

            return resultadoEjecucion;
        }

        public DeleteActivityUserResponse DeleteActivityUser(DeleteActivityUserRequest request)
        {
            var responseError = DeleteActivityUserResponse.RespuestaFallo(Result<DeleteActivityUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ActivityServiceRepository.DeleteActivityUser(request);

            return resultadoEjecucion;
        }

    }
}