﻿using AlterParadox.Backend.DAL;
using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Configuration;

namespace AlterParadox.Backend.Services
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        DeleteUserResponse DeleteUser(DeleteUserRequest request);
        [OperationContract]
        GetUserResponse GetUser(GetUserRequest request);
        [OperationContract]
        GetUserByNumberResponse GetUserByNumber(GetUserByNumberRequest request);
        [OperationContract]
        GetUsersResponse GetUsers();
        [OperationContract]
        GetWebUsersResponse GetWebUsers();
        [OperationContract]
        SaveUserResponse SaveUser(SaveUserRequest request);
        [OperationContract]
        SaveWebUserResponse SaveWebUser(SaveWebUserRequest request);

        [OperationContract]
        GetLocalGamesResponse GetLocalGames();
    }

    public class UserService : IUserService
    {
        private readonly IUserServiceRepository _userServiceRepository;
        private readonly ISQLServer _sqlServer;
        public UserService(
            IUserServiceRepository userServiceRepository,
            ISQLServer sqlServer)
        {
            if (userServiceRepository == null)  throw new ArgumentNullException("IUserServiceRepository no puede ser nulo en UserService");
            if (sqlServer == null)              throw new ArgumentNullException("ISQLServer no puede ser nulo en UserService");

            _userServiceRepository  = userServiceRepository;
            _sqlServer              = sqlServer;

            _sqlServer.Init();
        }


        public DeleteUserResponse DeleteUser(DeleteUserRequest request)
        {
            var responseError = DeleteUserResponse.RespuestaFallo(Result<DeleteUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _userServiceRepository.DeleteUser(request);

            return resultadoEjecucion;
        }

        public GetUserResponse GetUser(GetUserRequest request)
        {
            var responseError = GetUserResponse.RespuestaFallo(Result<GetUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _userServiceRepository.GetUser(request);

            return resultadoEjecucion;
        }

        public GetUserByNumberResponse GetUserByNumber(GetUserByNumberRequest request)
        {
            var responseError = GetUserByNumberResponse.RespuestaFallo(Result<GetUserByNumberRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _userServiceRepository.GetUserByNumber(request);

            return resultadoEjecucion;
        }

        public GetUsersResponse GetUsers()
        {
            var resultadoEjecucion = _userServiceRepository.GetUsers();
            return resultadoEjecucion;
        }

        public GetWebUsersResponse GetWebUsers()
        {

            var resultadoEjecucion = _userServiceRepository.GetWebUsers();
            return resultadoEjecucion;
        }

        public SaveUserResponse SaveUser(SaveUserRequest request)
        {
            var responseError = SaveUserResponse.RespuestaFallo(Result<SaveUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _userServiceRepository.SaveUser(request);

            return resultadoEjecucion;
        }

        public SaveWebUserResponse SaveWebUser(SaveWebUserRequest request)
        {
            var responseError = SaveWebUserResponse.RespuestaFallo(Result<SaveWebUserRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _userServiceRepository.SaveWebUser(request);

            return resultadoEjecucion;
        }

        public GetLocalGamesResponse GetLocalGames()
        {
            var resultadoEjecucion = _userServiceRepository.GetLocalGames();
            return resultadoEjecucion;
        }

        //Métodos de ejemplo
        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public string testConnectionData()
        //{

        //    SqlConnection con = new SqlConnection();
        //    try
        //    {
        //        con.ConnectionString = ConfigurationManager.ConnectionStrings["BBDD"].ConnectionString;
        //        con.Open();
        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        return string.Format("Connection failed: {0}", ex.ToString());
        //    }


        //    return "Connection OK";
        //    //return string.Format("You entered",);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}
    }
}