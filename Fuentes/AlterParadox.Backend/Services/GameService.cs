﻿using AlterParadox.Backend.DAL;
using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Configuration;

namespace AlterParadox.Backend.Services
{
    [ServiceContract]
    public interface IGameService
    {
        #region Associations

        [OperationContract]
        GetAssociationResponse GetAssociation(GetAssociationRequest request);

        [OperationContract]
        GetAssociationsResponse GetAssociations();

        [OperationContract]
        SaveAssociationResponse SaveAssociation(SaveAssociationRequest request);

        #endregion

        [OperationContract]
        GetLocalGamesResponse GetLocalGames();

    }

    public class GameService : IGameService
    {
        private readonly IGameServiceRepository _GameServiceRepository;
        private readonly ISQLServer _sqlServer;
        public GameService(
            IGameServiceRepository GameServiceRepository,
            ISQLServer sqlServer)
        {
            if (GameServiceRepository == null) throw new ArgumentNullException("IGameServiceRepository no puede ser nulo en GameService");
            if (sqlServer == null) throw new ArgumentNullException("ISQLServer no puede ser nulo en GameService");

            _GameServiceRepository = GameServiceRepository;
            _sqlServer = sqlServer;

            _sqlServer.Init();
        }

        public GetAssociationResponse GetAssociation(GetAssociationRequest request)
        {
            var responseError = GetAssociationResponse.RespuestaFallo(Result<GetAssociationRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _GameServiceRepository.GetAssociation(request);

            return resultadoEjecucion;
        }

        public GetAssociationsResponse GetAssociations()
        {
            var resultadoEjecucion = _GameServiceRepository.GetAssociations();
            return resultadoEjecucion;
        }

        public SaveAssociationResponse SaveAssociation(SaveAssociationRequest request)
        {
            var responseError = SaveAssociationResponse.RespuestaFallo(Result<SaveAssociationRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _GameServiceRepository.SaveAssociation(request);

            return resultadoEjecucion;
        }

        public GetLocalGamesResponse GetLocalGames()
        {
            var resultadoEjecucion = _GameServiceRepository.GetLocalGames();
            return resultadoEjecucion;
        }
    }
}