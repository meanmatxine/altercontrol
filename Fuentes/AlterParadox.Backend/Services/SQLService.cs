﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AlterParadox.Backend.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IOtherService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface ISQLService
    {
        [OperationContract]
        bool ExecuteSQL(string SQL);
        [OperationContract]
        DataSet Query(string SQL);
    }

    public class SQLService : ISQLService
    {

        private readonly ISQLServer _sqlServer;
        public SQLService(
            ISQLServer sqlServer)
        {
            _sqlServer = sqlServer;
            _sqlServer.Init();
        }


        public bool ExecuteSQL(string SQL)
        {
            return _sqlServer.executeSQL(SQL);
        }

        public DataSet Query(string SQL)
        {
            return _sqlServer.Query(SQL);
        }
    }

}
