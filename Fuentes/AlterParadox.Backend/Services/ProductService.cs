﻿using AlterParadox.Backend.DAL;
using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Configuration;

namespace AlterParadox.Backend.Services
{
    [ServiceContract]
    public interface IProductService
    {
        [OperationContract]
        DeleteProductResponse DeleteProduct(DeleteProductRequest request);
        [OperationContract]
        GetProductResponse GetProduct(GetProductRequest request);
        [OperationContract]
        GetProductsResponse GetProducts();
        [OperationContract]
        SaveProductResponse SaveProduct(SaveProductRequest request);
    }

    public class ProductService : IProductService
    {
        private readonly IProductServiceRepository _ProductServiceRepository;
        private readonly ISQLServer _sqlServer;

        public ProductService(
            IProductServiceRepository ProductServiceRepository,
            ISQLServer sqlServer)
        {
            if (ProductServiceRepository == null)  throw new ArgumentNullException("IProductServiceRepository no puede ser nulo en ProductService");
            if (sqlServer == null)              throw new ArgumentNullException("ISQLServer no puede ser nulo en ProductService");

            _ProductServiceRepository  = ProductServiceRepository;
            _sqlServer              = sqlServer;

            _sqlServer.Init();
        }


        public DeleteProductResponse DeleteProduct(DeleteProductRequest request)
        {
            var responseError = DeleteProductResponse.RespuestaFallo(Result<DeleteProductRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ProductServiceRepository.DeleteProduct(request);

            return resultadoEjecucion;
        }

        public GetProductResponse GetProduct(GetProductRequest request)
        {
            var responseError = GetProductResponse.RespuestaFallo(Result<GetProductRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ProductServiceRepository.GetProduct(request);

            return resultadoEjecucion;
        }

        public GetProductsResponse GetProducts()
        {
            var responseError = GetProductsResponse.RespuestaFallo(Result<GetProductsRequest>.CreateFailed(""));

            //if (request == null)
            //{
            //    responseError.MensajeError = "Request nulo";
            //    return responseError;
            //}

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            //var resultadoValidacion = request.Validar();
            //if (!resultadoValidacion.Succeeded)
            //{
            //    responseError.MensajeError = resultadoValidacion.ErrorMessage;
            //    return responseError;
            //}

            var resultadoEjecucion = _ProductServiceRepository.GetProducts();

            return resultadoEjecucion;
        }

        public SaveProductResponse SaveProduct(SaveProductRequest request)
        {
            var responseError = SaveProductResponse.RespuestaFallo(Result<SaveProductRequest>.CreateFailed(""));

            if (request == null)
            {
                responseError.MensajeError = "Request nulo";
                return responseError;
            }

            //Verifico que los campos obligatorios no estan vacios. No comprobamos que los datos sean correctos, en teoría los damos por buenos.
            var resultadoValidacion = request.Validar();
            if (!resultadoValidacion.Succeeded)
            {
                responseError.MensajeError = resultadoValidacion.ErrorMessage;
                return responseError;
            }

            var resultadoEjecucion = _ProductServiceRepository.SaveProduct(request);

            return resultadoEjecucion;
        }
    }
}