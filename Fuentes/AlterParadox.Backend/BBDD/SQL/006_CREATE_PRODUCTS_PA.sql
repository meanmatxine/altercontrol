﻿/*SELECT ALL*/
CREATE PROCEDURE [dbo].[GetProducts]

AS
	SELECT productoID, productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad, productoImage, productoActivo
    FROM TPV_Productos WHERE productoActivo=1
GO

/*SELECT BY ID*/
CREATE PROCEDURE [dbo].[GetProduct]
	@Id int
AS
	SELECT productoID, productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad, productoImage
    FROM TPV_Productos 
	WHERE productoID = @Id
GO

/*SAVE AND INSERT*/
CREATE  PROCEDURE [dbo].[SaveProduct]
@productoID int,
	@productoCB varchar(100),
	@productoName varchar(100),
	@productoTipo int,
	@productoPrecioSinIva float,
	@productoIVA int,
	@productoPrecio float,
	@productoCantidad int,
	@productoImage varchar(50),
	@productoActivo bit
AS
	DECLARE @Action VARCHAR(10)
	IF EXISTS (SELECT * FROM TPV_Productos WHERE productoID=@productoID)
	BEGIN
	SET @Action = 'UPDATE'
		UPDATE TPV_Productos SET
			productoCB = @productoCB,
			productoName = @productoName,
			productoTipo = @productoTipo,
			productoPrecioSinIva = @productoPrecioSinIva,
			productoIVA = @productoIVA,
			productoPrecio = @productoPrecio,
			productoCantidad = @productoCantidad,
			productoImage = @productoImage,
			productoActivo = @productoActivo
		WHERE productoID = @productoID
	END
	ELSE
	BEGIN
	SET @Action = 'INSERT'
		INSERT INTO TPV_Productos(productoCB, productoName, productoTipo, productoPrecioSinIVA, productoIVA, productoPrecio, productoCantidad, productoImage, productoActivo) VALUES
		(@productoCB, @productoName, @productoTipo, @productoPrecioSinIva, @productoIVA, @productoPrecio, @productoCantidad, @productoImage, @productoActivo)
		SELECT @productoID = @@IDENTITY ;
	END	
	Select @Action as Action, @productoID as productoID, @productoName as productoName
GO

/*DELETE PRODUCT*/
CREATE PROCEDURE [dbo].[DeleteProduct]
	@productoID int = NULL
AS
	IF @productoID IS NOT NULL
	BEGIN
		UPDATE TPV_Productos set productoActivo=0 WHERE productoID=@productoID
	END 
GO