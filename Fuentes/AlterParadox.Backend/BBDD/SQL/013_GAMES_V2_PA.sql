CREATE PROCEDURE [dbo].[GetAssociations]

AS
	SELECT id as Id, name as Name, contact as Contact, email as Email, phone as Phone, comments as Comments FROM associations ORDER BY name ASC

GO

CREATE PROCEDURE [dbo].[GetAssociation]
	@Id int = NULL
AS
	SELECT id as Id, name as Name, contact as Contact, email as Email, phone as Phone, comments as Comments FROM associations WHERE id =@Id ORDER BY name ASC

GO

CREATE PROCEDURE [dbo].[SaveAssociation]
	@Id int = NULL,
	@Name varchar(100),
	@Contact varchar(100),
	@Email varchar(100),
	@Phone varchar(100),
	@Comments text

AS

	DECLARE @Action VARCHAR(10)
	
	IF (@Id IS NULL)
	BEGIN
		SET @Action = 'INSERT'
		INSERT INTO associations(name, contact, email, phone, comments) VALUES
			( @Name, @Contact, @Email, @Phone, @Comments)
		SELECT @Id = @@IDENTITY ; 
	END
	ELSE
	BEGIN
		SET @Action = 'UPDATE'
		UPDATE associations SET
			name = @Name,
			contact = @Contact,
			email = @Email,
			phone = @Phone,
			comments = @Comments
		WHERE id = @Id
		
	END

	Select @Action as Action, @Id as Id, name as Name
		FROM associations Where id = @Id

GO
