﻿
CREATE PROCEDURE [dbo].[GetLocalGames]
AS

	SELECT  localgames.id, localgames.game_id, games.name as gamename, localgames.association_id, associations.name as associationname, 
		games.editorial, games.family, games.type, games.years, games.player_min, games.player_max, games.difficulty, games.expansion, games.duration, games.comments,
		COALESCE((Select TOP 1 status from games_lending where localgame_id = localgames.id Order by modifydate DESC),0) as status
		FROM localgames
		LEFT JOIN games ON localgames.game_id = games.id
		LEFT JOIN associations ON localgames.association_id = associations.id

GO
