﻿CREATE TABLE [dbo].[parameters](
    [SQLVersion] [int] NOT NULL DEFAULT(0),
	[activitiesenabled_revsthirtyminutes] [bit] NOT NULL DEFAULT(0),
) ON [PRIMARY]
   
GO
INSERT INTO [dbo].[parameters](SQLVersion) Values (0)
GO
 
CREATE PROCEDURE [dbo].[GetParameters]

AS
	SELECT * FROM [dbo].[parameters]
GO

CREATE PROCEDURE [dbo].[SaveParameters]
	@SQLVersion int = 0
AS
	UPDATE [dbo].[parameters] set SQLVersion = @SQLVersion;
GO