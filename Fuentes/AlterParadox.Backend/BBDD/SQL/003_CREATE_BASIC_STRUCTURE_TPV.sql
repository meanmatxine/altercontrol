﻿--CREATE TABLE [TPV_ProductoTipo]
CREATE TABLE [dbo].[TPV_ProductoTipo](
	[tipoID] [int] IDENTITY(1,1) NOT NULL,
	[tipoName] [text] NOT NULL,
 CONSTRAINT [PK_TPV_ProductoTipo] PRIMARY KEY CLUSTERED 
(
	[tipoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
--Resetea el indice autonumerico a 0, para coger el primer valor el 1
DBCC CHECKIDENT ('TPV_ProductoTipo', RESEED, 1);  
GO
DELETE FROM [dbo].[TPV_ProductoTipo];
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Bebida');
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Comida');
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Ropa');
INSERT INTO [dbo].[TPV_ProductoTipo] ( tipoName) VALUES('Otros');
GO

--CREATE TABLE TPV_Productos
CREATE TABLE [dbo].[TPV_Productos](
	[productoID] [int] IDENTITY(1,1) NOT NULL,
	[productoCB] [text] NOT NULL,
	[productoName] [text] NOT NULL,
	[productoTipo] [int] NOT NULL ,
	[productoPrecioSinIva] [float] NOT NULL DEFAULT (0),
	[productoIVA] [int] NOT NULL DEFAULT (0),
	[productoPrecio] [float] NOT NULL DEFAULT (0),
	[productoCantidad] [int] NOT NULL DEFAULT (0),
	[productoImage] [text] NOT NULL DEFAULT('default'),
	[productoActivo] [int] NOT NULL DEFAULT (1)
 CONSTRAINT [PK_TPV_Productos] PRIMARY KEY CLUSTERED 
(
	[productoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

--ALTER TABLE [dbo].[TPV_Productos]  WITH CHECK ADD  CONSTRAINT [FK_TPV_Productos_TPV_ProductoTipo] FOREIGN KEY([productoTipo])
--REFERENCES [dbo].[TPV_ProductoTipo] ([tipoID])
--GO

--ALTER TABLE [dbo].[TPV_Productos] CHECK CONSTRAINT [FK_TPV_Productos_TPV_ProductoTipo]
--GO

--Create Tabla TPV_Pedidos
CREATE TABLE [dbo].[TPV_Pedidos](
	[pedidoID] [int] IDENTITY(1,1) NOT NULL,
	[pedidoFecha] [datetime] NOT NULL DEFAULT (getdate()) ,
	[pedidoTotal] [float] NOT NULL DEFAULT (0) ,
	[pedidoEntregado] [float] NOT NULL DEFAULT (0) ,
	[pedidoDevuelto] [float] NOT NULL DEFAULT (0) ,
 CONSTRAINT [PK_TPV_Pedidos] PRIMARY KEY CLUSTERED 
(
	[pedidoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--Create Tabla TPV_PedidoDetalle
CREATE TABLE [dbo].[TPV_PedidosDetalle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pedidoID] [int] NOT NULL,
	[productoID] [int] NOT NULL,
	[pdCantidad] [int] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_TPV_PedidosDetalle] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TPV_PedidosDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Pedidos] FOREIGN KEY([pedidoID])
REFERENCES [dbo].[TPV_Pedidos] ([pedidoID])
GO

ALTER TABLE [dbo].[TPV_PedidosDetalle] CHECK CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Pedidos]
GO

--ALTER TABLE [dbo].[TPV_PedidosDetalle]  WITH CHECK ADD  CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Productos] FOREIGN KEY([productoID])
--REFERENCES [dbo].[TPV_Productos] ([productoID])
--GO

--ALTER TABLE [dbo].[TPV_PedidosDetalle] CHECK CONSTRAINT [FK_TPV_PedidosDetalle_TPV_Productos]
--GO

--CREATE TABLE TPV_ControlCaja
CREATE TABLE [dbo].[TPV_ControlCaja](
	[cajaMovID] [int] IDENTITY(1,1) NOT NULL,
	[cajaMovFecha] [datetime] NOT NULL,
	[cajaPersona] [text] NOT NULL,
	[cajaIngreso] [float] NOT NULL DEFAULT (0),
	[cajaExtracto] [float] NOT NULL DEFAULT (0),
	[cajaConcepto] [text] NOT NULL,
	[cajaObservaciones] [text] NOT NULL,
	[cajaDineroActual] [float] NOT NULL DEFAULT (0),
	[cajaDescuadre] [float] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_TPV_ControlCaja] PRIMARY KEY CLUSTERED 
(
	[cajaMovID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
