﻿Delete from TPV_Productos;
GO
DBCC CHECKIDENT (TPV_Productos, RESEED, 0)
GO
INSERT INTO [dbo].[TPV_Productos] (productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad,productoImage, productoActivo) 
VALUES 
/*bebida*/
('5449000000996','Cocacola',1,0.91,10,1.0,100,'coke',1),
('5449000131805', 'Cocacola Zero', 1, 0.91, 10, 1.0, 100,'coke0',1),
('', 'Cocacola 00', 1, 0.91, 10, 1.0, 100,'coke00',1),
('5449000006004', 'Fanta Limon', 1, 0.91, 10, 1.0, 100,'fantaL',1),
('5449000011527', 'FANTA NARANJA', 1, 0.91, 10, 1.0, 100,'fantaN',1),
('5449000121059', 'Agua Mineral', 1, 0.54, 10, 0.6, 100,'agua',1),
('5449000012913', 'Nestea', 1, 0.91, 10, 1.0, 100,'nestea',1),
('5449000058560', 'Aquarius Límon', 1, 0.91, 10, 1.0, 100,'aquL',1),
('5449000033819', 'Aquarius Naranja', 1, 0.91, 10, 1.0, 100,'aquN',1),
('5060166690380', 'MONSTER', 1, 1.81, 10, 2.0, 100,'monster',1),
('', 'Monster Expresso', 1, 0.91, 10, 1.0, 100,'monsterE',1),
('','Zumo Naranja', 1, 0.72, 10, 0.8, 100,'mmN',1),
('','Zumo Mango', 1, 0.72, 10, 0.8, 1000,'mmMa',1),
('', 'Batido Chocolate', 1, 0.91, 10, 1.0, 100,'batidoC',1),
/*comida*/
('', 'Bony', 2, 0.91, 10, 1.0, 44,'bony',1),
('', 'Tigreton', 2, 0.91, 10, 1.0, 44,'tigreton',1),
('', 'Pantera Rosa', 2, 0.91, 10, 1.0, 44,'panteraR',1),
('8410022108219', 'Donuts Glace 1€', 2, 0.91, 10, 1.0, 44,'donut',1),
('8410022108226', 'Donuts Bombón 1€', 2, 0.91, 10, 1.0, 44,'donutB',1),
('8410022109094', 'Donettes Clasicos 1€', 2, 0.91, 10, 1.0, 20,'donetteC',1),
('8410022109117', 'Donettes Rayados 1€', 2, 0.91, 10, 1.0, 20,'donetteR',1),
('0000000000003', 'Palmera Hojaldre', 2, 0.91, 10, 1.0, 35,'palmeraH',1),
('8410022012035', 'Palmera Bollo Rellena 1€', 2, 0.91, 10, 1.0, 30,'palmeraB',1),
('8410022110717', 'Bollycao Clasicos 1€', 2, 0.91, 10, 1.0, 14,'bollycaoC',1),
('8410022110724', 'Bollycao Leche 1€', 2, 0.91, 10, 1.0, 14,'bollycaoL',1),
('8410099573675', 'Weikis', 2, 0.91, 10, 1.0, 100,'weikis',1),
('', 'Caña Dalmata', 2, 0.91, 10, 1.0, 35,'canaD',1),
('', 'Caña XL', 2, 0.91, 10, 1.0, 35,'canaXL',1),
('', 'Cacahuetes Dorado', 2, 0.91, 10, 1.0, 72,'cacahD',1),
('', 'Patatas fritas', 2, 0.91, 10, 1.0, 72,'patatasF',1),
('', 'Chaskis', 2, 0.91, 10, 1.0, 72,'chaskis',1),
('', 'Frutos Secos', 2, 0.91, 10, 1.0, 72,'frutosS',1),
('', 'Golosinas Acidas', 2, 0.91, 10, 1.0, 72,'goloA',1),
('', 'Golosinas Dulces', 2, 0.91, 10, 1.0, 72,'goloD',1),
/*ropa*/
('0000000000004', 'Camiseta S', 3, 8.26, 21, 10.0, 50,'camS',1),
('0000000000005', 'Camiseta M', 3, 8.26, 21, 10.0, 50,'camM',1),
('0000000000006', 'Camiseta L', 3, 8.26, 21, 10.0, 50,'camL',1),
('8410732050011', 'Camiseta XL', 3, 8.26, 21, 10.0, 20,'camXL',1),
('0000000000007', 'Camiseta XXL', 3, 8.26, 21, 10.0, 50,'camXXL',1),
('0000000000008', 'Cam Vieja', 3, 1.81, 10, 2.0, 1000,'camV',1),
('0000000000009', 'Cam 2018', 3, 7.27, 10, 8.0, 1000,'camV',1),
/*otros*/
('0000000000000', 'Ticket Cesta', 4, 1.0, 10, 1.0, 1000,'Ticket',1),
('0000000000001', 'Comida', 4, 18.18, 10, 20.0, 50,'Comida',1);
GO