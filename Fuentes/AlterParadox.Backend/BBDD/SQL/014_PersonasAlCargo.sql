﻿CREATE PROCEDURE [dbo].[GetUsersOnCharge]
	@Id int
AS
	SELECT id, number, dni, name, lastname, association, email, phone, cp, birthday, shirt, lunch, sleep, tutorpermission, comments ,wantshirt,bringactivity,secondlunch,sleeplist, useroncharge
	FROM USERS
	WHERE useroncharge = @Id
GO