﻿
CREATE PROCEDURE [dbo].[GetActivityUsers]
	@IdActivity int = NULL
AS
BEGIN
	DECLARE @NumParticipants int = 0
	DECLARE @MaxParticipants int = 0

	 SELECT @NumParticipants = COALESCE((Select Count(*) from activity_users where activity_id = activities.id and enabled = 1),0), @MaxParticipants = maxParticipants
			FROM activities
			WHERE Id = @IdActivity

	
	SELECT TOP (@MaxParticipants) activity_users.id as Id into #UsuariosInscritos
		FROM activity_users 
		LEFT JOIN users on activity_users.user_id = users.id
		WHERE activity_users.activity_id = @IdActivity AND enabled = 1 and reserve = 0
		ORDER BY activity_users.inscriptiondate

		SELECT activity_users.id, activity_users.activity_id, activity_users.user_id, 
			activity_users.reserve, activity_users.inscriptiondate,
			activity_users.comments, 
			users.name, users.lastname, users.number, users.aviso, 1 as inscrito
			FROM activity_users 
			LEFT JOIN users on activity_users.user_id = users.id
			WHERE activity_users.activity_id = @IdActivity AND enabled = 1 and reserve = 0 And activity_users.id in (select Id from #UsuariosInscritos)
	UNION ALL 
		SELECT activity_users.id, activity_users.activity_id, activity_users.user_id, 
			activity_users.reserve, activity_users.inscriptiondate,
			activity_users.comments, 
			users.name, users.lastname, users.number, users.aviso, 0 as inscrito
			FROM activity_users 
			LEFT JOIN users on activity_users.user_id = users.id
			WHERE activity_users.activity_id = @IdActivity AND enabled = 1 And activity_users.id NOT in (select Id from #UsuariosInscritos)
	ORDER BY inscrito DESC, activity_users.inscriptiondate

	DROP TABLE #UsuariosInscritos
END 
	
GO

CREATE PROCEDURE [dbo].[GetActivityUser]
	@IdActivity int = NULL,
	@IdUser int = NULL
AS
	SELECT activity_users.id, activity_users.activity_id, activity_users.user_id, 
    activity_users.reserve, activity_users.inscriptiondate,
    activity_users.comments, 
    users.name, users.lastname, users.number, users.aviso
    FROM activity_users LEFT JOIN users on activity_users.user_id = users.id 
    WHERE  activity_users.id >0
	AND activity_users.activity_id = @IdActivity
	AND activity_users.user_id = @IdUser
	AND enabled = 1 
	ORDER BY  activity_users.id asc, users.name
GO

CREATE PROCEDURE [dbo].[SaveActivityUser]
	@Id int = NULL,
	@IdActivity int = NULL,
	@IdUser int = NULL,
	@InscriptionDate datetime = NULL,
	@Reserve bit = 0,
	@Comments text
AS

	DECLARE @Action VARCHAR(10)

	IF (@Id IS NULL)
	BEGIN
        
		SET @Action = 'INSERT'

		INSERT INTO activity_users(activity_id, user_id, inscriptiondate, reserve, comments ) VALUES
			( @IdActivity, @IdUser, @InscriptionDate, @Reserve, @Comments)
		SELECT @Id = @@IDENTITY ; 
	END
	ELSE
	BEGIN
		SET @Action = 'UPDATE'
		UPDATE activity_users SET
			reserve = @Reserve,
			comments = @Comments
		WHERE id = @Id
		AND enabled = 1 
		
	END

	Select @Action as Action, @Id as Id, reserve as Reserve
		FROM activity_users Where id = @Id

GO

CREATE PROCEDURE [dbo].[DeleteActivityUser]
	@Id int = NULL
AS
	UPDATE activity_users SET enabled = 0 
	WHERE id =@Id
GO
