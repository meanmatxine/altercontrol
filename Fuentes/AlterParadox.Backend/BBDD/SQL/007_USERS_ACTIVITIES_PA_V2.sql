
CREATE PROCEDURE [dbo].[GetActivities]

AS
	SELECT id, organizer, email, phone, association, name, activitydate, place, type, enabled, participants, duration, winner,
		COALESCE((Select Count(*) from activity_users where activity_id = activities.id and enabled = 1),0) as numparticipants, maxparticipants, minparticipants, summary, comments, needs, modifydate 
    FROM activities
	WHERE enabled = 1
	ORDER BY activityDate ASC, name
GO

CREATE PROCEDURE [dbo].[GetActivity]
	@Id int = NULL
AS
	SELECT id, organizer, email, phone, association, name, activitydate, place, type, enabled, participants, duration, winner,
		COALESCE((Select Count(*) from activity_users where activity_id = activities.id and enabled = 1),0) as numparticipants, maxparticipants, minparticipants, summary, comments, needs, modifydate 
    FROM activities
	WHERE id =@Id
	AND enabled = 1
	ORDER BY activityDate ASC, name
GO

CREATE PROCEDURE [dbo].[SaveActivity]
	@Id int = NULL,
	@Organizer varchar(100),
	@Email varchar(100),
	@Phone varchar(100),
	@Association varchar(100),
	@Name varchar(100),
	@ActivityDate datetime = null,
	@Place varchar(100),
	@Type varchar(100),
	@Participants varchar(10),
	@MaxParticipants int = 0, 
	@MinParticipants int = 0,
	@Duration int = 0,
	@Enabled bit = 1,
	@Summary text,
	@Comments text,
	@Needs text,
	@Winner text = null

AS

	DECLARE @Action VARCHAR(10)
	
	IF (@Id IS NULL)
	BEGIN
		SET @Action = 'INSERT'
		INSERT INTO activities(organizer ,email ,phone ,association ,name ,activityDate ,place ,type ,participants ,maxparticipants, minparticipants, duration, enabled, summary ,comments ,needs, winner) VALUES
			( @Organizer, @Email, @Phone, @Association, @Name, @ActivityDate, @Place, @Type, @Participants, @MaxParticipants, @MinParticipants, @Duration, 1, @Summary, @Comments, @Needs, @Winner)
		SELECT @Id = @@IDENTITY ; 
	END
	ELSE
	BEGIN
		SET @Action = 'UPDATE'
		UPDATE activities SET
			organizer = @Organizer,
			email = @Email,
			phone = @Phone,
			association = @Association,
			name = @Name,
			activityDate = @ActivityDate,
			place = @Place,
			type = @Type,
			participants = @Participants,
			maxparticipants = @MaxParticipants,
			minparticipants = @MinParticipants,
			duration = @Duration,
			summary = @Summary,
			comments = @Comments,
			needs = @Needs,
			winner = @Winner
		WHERE id = @Id
		
	END

	Select @Action as Action, @Id as Id, name
		FROM activities Where id = @Id

GO

CREATE PROCEDURE [dbo].[DeleteActivity]
	@Id int = NULL
AS
	UPDATE activities SET enabled = 0 
	WHERE id =@Id
GO

CREATE PROCEDURE [dbo].[GetNumParticipantsActivity]
	@Id int = NULL
AS

	SELECT COALESCE((Select Count(*) from activity_users where activity_id = activities.id and enabled = 1),0) as NumParticipants 
	FROM activities
	WHERE id =@id
	ORDER BY id ASC

GO