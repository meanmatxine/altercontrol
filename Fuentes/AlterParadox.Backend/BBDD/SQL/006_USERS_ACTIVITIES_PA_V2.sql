
ALTER PROCEDURE [dbo].[SaveUser]
	@Id int = NULL,
	@Number int = NULL,
	@Dni varchar(20),
	@Name varchar(100),
	@LastName varchar(150),
	@Association varchar(100),
	@Email varchar(100),
	@Phone varchar(100),
	@CP varchar(10),
	@Birthday datetime = null,
	@Shirt varchar(10),
	@Lunch bit, 
	@Sleep bit,
	@TutorPermission bit,
	@Comments text

AS

	DECLARE @Action VARCHAR(10)
	
	IF (@Id IS NULL)
	BEGIN
		SET @Action = 'INSERT'
		INSERT INTO users( number, dni, name, lastname, email, phone, cp, comments, association, shirt, lunch, sleep, tutorpermission, birthday) VALUES
			( (SELECT COALESCE(MAX(number)+1,1) FROM users), @Dni, @Name, @LastName, @Email, @Phone, @CP, @Comments, @Association, @Shirt, @Lunch, @Sleep, @TutorPermission, @Birthday)
		SELECT @Id = @@IDENTITY ; 
	END
	ELSE
	BEGIN
		SET @Action = 'UPDATE'
		UPDATE users SET
			number = @Number,
			dni = @Dni,
			name = @Name,
			lastname = @LastName,
			email = @Email,
			phone = @Phone,
			cp = @CP,
			comments = @Comments,
			association = @Association,
			shirt = @Shirt,
			lunch = @Lunch,
			sleep = @Sleep,
			tutorpermission = @TutorPermission,
			birthday = @Birthday
		WHERE id = @Id
		
	END

	Select @Action as Action, @Id as Id, number, name, lastname, dni
		FROM users Where id = @Id
GO

CREATE PROCEDURE [dbo].[GetActivities]

AS
	SELECT id, organizer, email, phone, association, name, activitydate, place, type, enabled, participants, 
		CONCAT('' , (Select Count(*) from activity_users where activity_id = activities.id)) as numparticipants, maxparticipants, summary, comments, needs, modifydate 
    FROM activities
	ORDER BY activityDate ASC, name
GO

CREATE PROCEDURE [dbo].[GetActivity]
	@Id int = NULL
AS
	SELECT id, organizer, email, phone, association, name, activitydate, place, type, enabled, participants,
		CONCAT('' , (Select Count(*) from activity_users where activity_id = activities.id)) as numparticipants, maxparticipants, summary, comments, needs, modifydate 
    FROM activities
	WHERE id =@Id
	ORDER BY activityDate ASC, name
GO

CREATE PROCEDURE [dbo].[SaveActivity]
	@Id int = NULL,
	@Organizer varchar(100),
	@Email varchar(100),
	@Phone varchar(100),
	@Association varchar(100),
	@Name varchar(100),
	@ActivityDate datetime = null,
	@Place varchar(100),
	@Type varchar(100),
	@Participants varchar(10),
	@MaxParticipants int = null, 
	@Enabled bit = 1,
	@Summary text,
	@Comments text,
	@Needs text

AS

	DECLARE @Action VARCHAR(10)
	
	IF (@Id IS NULL)
	BEGIN
		SET @Action = 'INSERT'
		INSERT INTO activities(organizer ,email ,phone ,association ,name ,activityDate ,place ,type ,participants ,maxparticipants, enabled, summary ,comments ,needs) VALUES
			( @Organizer, @Email, @Phone, @Association, @Name, @ActivityDate, @Place, @Type, @Participants, @MaxParticipants, @Enabled, @Summary, @Comments, @Needs)
		SELECT @Id = @@IDENTITY ; 
	END
	ELSE
	BEGIN
		SET @Action = 'UPDATE'
		UPDATE activities SET
			organizer = @Organizer,
			email = @Email,
			phone = @Phone,
			association = @Association,
			name = @Name,
			activityDate = @ActivityDate,
			place = @Place,
			type = @Type,
			participants = @Participants,
			maxparticipants = @MaxParticipants,
			enabled = @Enabled,
			summary = @Summary,
			comments = @Comments,
			needs = @Needs
		WHERE id = @Id
		
	END

	Select @Action as Action, @Id as Id, name
		FROM activities Where id = @Id

GO

CREATE PROCEDURE [dbo].[DeleteActivity]
	@Id int = NULL
AS
	UPDATE activities SET enabled = 0 
	WHERE id =@Id
GO

CREATE PROCEDURE [dbo].[GetNumParticipantsActivity]
	@Id int = NULL
AS

	SELECT COALESCE((Select Count(*) from activity_users where activity_id = activities.id),0) as NumParticipants 
	FROM activities
	WHERE id =@id
	ORDER BY id ASC

GO