﻿Delete from TPV_Productos;
GO
DBCC CHECKIDENT (TPV_Productos, RESEED, 0)
GO
INSERT INTO [dbo].[TPV_Productos] (productoCB, productoName, productoTipo, productoPrecioSinIva, productoIVA, productoPrecio, productoCantidad,productoImage, productoActivo) 
VALUES 
/*bebida*/
('5449000000996','Cocacola',1,0.91,10,1.0,100,'coke',1),
('5449000131805', 'Cocacola Zero', 1, 0.91, 10, 1.0, 100,'coke0',1),
('5449000006004', 'Fanta Limon', 1, 0.91, 10, 1.0, 100,'fantaL',1),
('5449000011527', 'FANTA NARANJA', 1, 0.91, 10, 1.0, 100,'fantaN',1),
('5449000121059', 'Agua Mineral', 1, 0.54, 10, 0.6, 100,'agua',1),
('5449000012913', 'Nestea', 1, 0.91, 10, 1.0, 100,'nestea',1),
('5449000205520', 'Sprite', 1, 0.91, 10, 1.0, 100,'sprite',1),
('5449000058560', 'Aquarius Límon', 1, 0.91, 10, 1.0, 100,'aquL',1),
('5449000033819', 'Aquarius Naranja', 1, 0.91, 10, 1.0, 100,'aquN',1),
('5060166690380', 'MONSTER', 1, 1.81, 10, 2.0, 100,'monster',1),
('54491090','Minute Maid Piña', 1, 0.72, 10, 0.8, 100,'mmP',1),
('40822716','Minute Maid Melocoton', 1, 0.72, 10, 0.8, 1000,'mmM',1),
/*comida*/
('8410022108219', 'Donuts Glace 1€', 2, 0.91, 10, 1.0, 44,'donut',1),
('8410022108226', 'Donuts Bombón 1€', 2, 0.91, 10, 1.0, 44,'donutB',1),
('8410022109094', 'Donettes Clasicos 1€', 2, 0.91, 10, 1.0, 20,'donetteC',1),
('8410022109117', 'Donettes Rayados 1€', 2, 0.91, 10, 1.0, 20,'donetteR',1),
('0000000000003', 'Palmera Hojaldre', 2, 0.91, 10, 1.0, 35,'palmeraH',1),
('8410022012035', 'Palmera Bollo Rellena 1€', 2, 0.91, 10, 1.0, 30,'palmeraB',1),
('8410022110717', 'Bollycao Clasicos 1€', 2, 0.91, 10, 1.0, 14,'bollycaoC',1),
('8410022110724', 'Bollycao Leche 1€', 2, 0.91, 10, 1.0, 14,'bollycaoL',1),
('8410099573675', 'Weikis', 2, 0.91, 10, 1.0, 100,'weikis',1),
('8410022012110', 'Caña Crema 1€', 2, 0.91, 10, 1.0, 35,'canaC',1),
('8412600028629', 'Takis Fuego', 2, 0.91, 10, 1.0, 72,'takisF',1),
('8412600028605', 'Takis Queso', 2, 1.36, 10, 1.5, 36,'takisQ',1),
('8412600028599', 'Takis Extrahot', 2, 1.36, 10, 1.5, 36,'takisE',1),
('8412600029015', 'Takis Burguer', 2, 1.36, 10, 1.5, 36, 'takisB',1),
('8412600025925', 'Cacahuetes Miel', 2, 0.91, 10, 1.0, 72,'cacahM',1),
('8412600028377', 'Cacahuetes Barbacoa', 2, 1.36, 10, 1.5, 24,'cacahB',1),
('8412600028384', 'Cacahuetes Chili', 2, 1.36, 10, 1.5, 24,'cacahC',1),
/*ropa*/
('0000000000004', 'Camiseta S', 3, 8.26, 21, 10.0, 50,'camS',1),
('0000000000005', 'Camiseta M', 3, 8.26, 21, 10.0, 50,'camM',1),
('0000000000006', 'Camiseta L', 3, 8.26, 21, 10.0, 50,'camL',1),
('8410732050011', 'Camiseta XL', 3, 8.26, 21, 10.0, 20,'camXL',1),
('0000000000007', 'Camiseta XXL', 3, 8.26, 21, 10.0, 50,'camXXL',1),
('0000000000008', 'Cam Vieja', 3, 1.81, 10, 2.0, 1000,'camV',1),
/*otros*/
('0000000000000', 'Ticket Cesta', 4, 1.0, 10, 1.0, 1000,'Ticket',1),
('0000000000001', 'Comida', 4, 18.18, 10, 20.0, 50,'Comida',1);
GO