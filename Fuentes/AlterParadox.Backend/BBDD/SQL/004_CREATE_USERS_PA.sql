﻿
CREATE PROCEDURE [dbo].[GetUsers]

AS
	SELECT id, number, dni, name, lastname, association, email, phone, cp, birthday, shirt, lunch, sleep, tutorpermission, comments,wantshirt,bringactivity,secondlunch,sleeplist, useroncharge, aviso
    FROM users 
GO


CREATE PROCEDURE [dbo].[GetUser]
	@Id int
AS
	SELECT id, number, dni, name, lastname, association, email, phone, cp, birthday, shirt, lunch, sleep, tutorpermission, comments ,wantshirt,bringactivity,secondlunch,sleeplist, useroncharge, aviso
	FROM USERS
	WHERE id = @Id
GO

CREATE PROCEDURE [dbo].[GetUserByNumber]
	@Number int
AS
	SELECT id, number, dni, name, lastname, association, email, phone, cp, birthday, shirt, lunch, sleep, tutorpermission, comments ,wantshirt,bringactivity,secondlunch,sleeplist, useroncharge, aviso
	FROM USERS
	WHERE number = @Number
GO

CREATE PROCEDURE [dbo].[GetWebUsers]

AS
	SELECT id, dni, name, lastname, association, email, phone, cp, birthday, shirt, lunch, sleep, tutorpermission, comments ,wantshirt,bringactivity,secondlunch,sleeplist
	FROM web_users

GO

CREATE PROCEDURE [dbo].[SaveWebUser]
	@Id int = NULL,
	@Dni varchar(20) = '',
	@Name varchar(100) = '',
	@LastName varchar(150) = '',
	@Association varchar(100) = '',
	@Email varchar(100) = '',
	@Phone varchar(100) = '',
	@CP varchar(10) = '',
	@Birthday date = NULL,
	@WantShirt bit = 0,
	@Shirt varchar(10) = '',
	@BringActivity bit = 0,
	@Lunch bit = 0,
	@SecondLunch text = '',
	@Sleep bit = 0,
	@SleepList bit = 0,
	@TutorPermission bit = 0,
	@Comments text = ''

AS

	IF (@Id IS NULL)
		INSERT INTO web_users( dni, name, lastname, email, phone, cp, comments, association, shirt, lunch, sleep, tutorpermission, birthday,wantshirt,bringactivity,secondlunch,sleeplist) VALUES
			( @Dni, @Name, @LastName, @Email, @Phone, @CP, @Comments, @Association, @Shirt, @Lunch, @Sleep, @TutorPermission, @Birthday, @WantShirt, @BringActivity, @SecondLunch, @SleepList)
	ELSE
		UPDATE web_users SET
			dni = @Dni,
			name = @Name,
			lastname = @LastName,
			email = @Email,
			phone = @Phone,
			cp = @CP,
			comments = @Comments,
			association = @Association,
			shirt = @Shirt,
			lunch = @Lunch,
			sleep = @Sleep,
			tutorpermission = @TutorPermission,
			birthday = @Birthday,
			wantshirt = @WantShirt,
			bringactivity = @BringActivity,
			secondlunch = @SecondLunch,
			sleeplist = @SleepList
		WHERE id = @Id
		
GO



CREATE  PROCEDURE [dbo].[SaveUser]
	@Id int = NULL,
	@Number int = NULL,
	@Dni varchar(20),
	@Name varchar(100),
	@LastName varchar(150),
	@Association varchar(100),
	@Email varchar(100),
	@Phone varchar(100),
	@CP varchar(10),
	@Birthday date = null,
	@WantShirt bit,
	@Shirt varchar(10),
	@BringActivity bit,
	@Lunch bit, 
	@SecondLunch text,
	@Sleep bit,
	@SleepList bit,
	@TutorPermission bit,
	@UserOnCharge int = null,
	@Comments text,
	@Aviso text
AS
BEGIN
	DECLARE @Action VARCHAR(10)
	
	IF (@Id IS NULL)
	BEGIN
		SET @Action = 'INSERT'
		INSERT INTO users( number, dni, name, lastname, email, phone, cp, comments, association, shirt, lunch, sleep, tutorpermission, birthday,wantshirt,bringactivity,secondlunch,sleeplist, useroncharge, aviso) VALUES
			( (SELECT COALESCE(MAX(number)+1,1) FROM users), @Dni, @Name, @LastName, @Email, @Phone, @CP, @Comments, @Association, @Shirt, @Lunch, @Sleep, @TutorPermission, @Birthday, @WantShirt, @BringActivity, @SecondLunch, @SleepList, @UserOnCharge, @Aviso)
		SELECT @Id = @@IDENTITY ; 
		Select @Number = Number from users where id = @Id;
	END
	ELSE
	BEGIN
		SET @Action = 'UPDATE'
		UPDATE users SET
			number = @Number,
			dni = @Dni,
			name = @Name,
			lastname = @LastName,
			email = @Email,
			phone = @Phone,
			cp = @CP,
			comments = @Comments,
			association = @Association,
			shirt = @Shirt,
			lunch = @Lunch,
			sleep = @Sleep,
			tutorpermission = @TutorPermission,
			birthday = @Birthday,
			wantshirt = @WantShirt,
			bringactivity = @BringActivity,
			secondlunch = @SecondLunch,
			sleeplist = @SleepList,
			useroncharge = @UserOnCharge,
			aviso = @Aviso
		WHERE id = @Id
		
	END

	Select @Action as Action, @Id as Id, @Name as Name, @LastName as LastName, @Dni as Dni, @Number as number
END
GO

CREATE PROCEDURE [dbo].[DeleteUser]
	@Id int = NULL
AS

	IF @Id IS NOT NULL
	BEGIN
		DELETE FROM activity_users WHERE user_id = @Id
		DELETE FROM users WHERE id = @Id
	END 
		
		
GO
