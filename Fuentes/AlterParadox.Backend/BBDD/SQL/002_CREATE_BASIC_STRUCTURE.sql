﻿--CREAR TABLA USUARIOS
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number] [int] NOT NULL DEFAULT (0),
	[dni] [varchar](20) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[lastname] [varchar](150) NOT NULL,
	[association] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[cp] [varchar](10) NOT NULL,
	[birthday] [date] NOT NULL,
	[wantshirt] [bit] NOT NULL DEFAULT (0),
	[bringactivity] [bit] NOT NULL DEFAULT (0),
	[shirt] [varchar](10) NOT NULL,
	[lunch] [bit] NOT NULL DEFAULT (0),
	[secondlunch] [text] NOT NULL,
	[sleep] [bit] NOT NULL DEFAULT (0),
	[sleeplist] [bit] NOT NULL DEFAULT (0),
	[tutorpermission] [bit] NOT NULL DEFAULT (0),
	[comments] [text] NOT NULL,
	[aviso] [text] NOT NULL,
	[useroncharge] int null,
	[createdate] [datetime] NOT NULL DEFAULT (getdate()),
	[modifydate] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


--CREAR TRIGGER USUARIOS
CREATE TRIGGER trg_users_Update
ON dbo.users
AFTER UPDATE
AS
    UPDATE dbo.users
    SET modifydate = GETDATE()
    WHERE ID IN (SELECT DISTINCT ID FROM INSERTED)
GO

--CREAR TABLA USUARIOS WEB
CREATE TABLE [dbo].[web_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dni] [varchar](20) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[lastname] [varchar](150) NOT NULL,
	[association] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[cp] [varchar](10) NOT NULL,
	[birthday] [date] NOT NULL,
	[shirt] [varchar](10) NOT NULL,
	[lunch] [bit] NOT NULL DEFAULT (0),
	[sleep] [bit] NOT NULL DEFAULT (0),
	[wantshirt] [bit] NOT NULL DEFAULT (0),
	[bringactivity] [bit] NOT NULL DEFAULT (0),
	[secondlunch] [text] NOT NULL,
	[sleeplist] [bit] NOT NULL DEFAULT (0),
	[tutorpermission] [bit] NOT NULL DEFAULT (0),
	[comments] [text] NOT NULL,
	[createdate] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_web_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

insert into web_users (dni, name, lastname, association, email, phone, cp, birthday, shirt, lunch, sleep, wantshirt, bringactivity, secondlunch, sleeplist, tutorpermission, comments)
Values
('11111111H', 'Sr. Umbras', '', 'Alter Paradox', 'test@test.es', '000000000', '31001', '2005-08-30', 'XL', 1, 1, 1, 1, 'Carne', 1, 1, 'Comentarios');
GO

--CREAR TABLA Actividades
CREATE TABLE [dbo].[activities](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[organizer] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[association] [varchar](100) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[activitydate] [datetime] NOT NULL,
	[place] [varchar](100) NOT NULL,
	[type] [varchar](100) NOT NULL,
	[participants] [varchar](100) NOT NULL,
	[maxparticipants] [int] NOT NULL DEFAULT (0),
	[minparticipants] [int] NOT NULL DEFAULT (0),
	[duration] [int] NOT NULL DEFAULT (0),
	[enabled] [bit] NOT NULL DEFAULT (1),
	[summary] [text] NOT NULL,
	[comments] [text] NOT NULL,
	[needs] [text] NOT NULL,
	[winner] [text] NOT NULL,
	[createdate] [datetime] NOT NULL DEFAULT (getdate()) ,
	[modifydate] [datetime] NOT NULL DEFAULT (getdate()) ,
 CONSTRAINT [PK_activities] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

--CREAR TRIGGER Actividades
CREATE TRIGGER trg_Actividades_Update
ON dbo.activities
AFTER UPDATE
AS
    UPDATE dbo.activities
    SET modifydate = GETDATE()
    WHERE ID IN (SELECT DISTINCT ID FROM INSERTED)
GO

--CREAR TABLA activity_users
CREATE TABLE [dbo].[activity_users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[activity_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[inscriptiondate] [datetime] NOT NULL DEFAULT (getdate()),
	[reserve] [bit] NOT NULL DEFAULT (0),
	[comments] [text] NOT NULL,
    [enabled] [bit] NOT NULL DEFAULT (1)
 CONSTRAINT [PK_activity_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[activity_users]  WITH CHECK ADD  CONSTRAINT [FK_activity_users_activities] FOREIGN KEY([activity_id])
REFERENCES [dbo].[activities] ([id])
GO

ALTER TABLE [dbo].[activity_users] CHECK CONSTRAINT [FK_activity_users_activities]
GO

ALTER TABLE [dbo].[activity_users]  WITH CHECK ADD  CONSTRAINT [FK_activity_users_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO

ALTER TABLE [dbo].[activity_users] CHECK CONSTRAINT [FK_activity_users_users]
GO

--CREAR TABLA games
CREATE TABLE [dbo].[games](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](200) NOT NULL,
	[editorial] [varchar](200) NOT NULL,
	[family] [int] NOT NULL DEFAULT (0),
	[type] [int] NOT NULL DEFAULT (0),
	[years] [int] NOT NULL DEFAULT (0),
	[player_min] [int] NOT NULL DEFAULT (0),
	[player_max] [int] NOT NULL DEFAULT (0),
	[difficulty] [int] NOT NULL DEFAULT (0),
	[expansion] [int] NOT NULL DEFAULT (0),
	[duration] [int] NOT NULL DEFAULT (0),
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_games] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

--CREATE TABLA associations
CREATE TABLE [dbo].[associations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](200) NOT NULL,
	[contact] [varchar](100) NOT NULL,
	[email] [varchar](100) NOT NULL,
	[phone] [varchar](100) NOT NULL,
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_associations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


--CREAR TABLA localgames
CREATE TABLE [dbo].[localgames](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[game_id] [int] NOT NULL,
	[association_id] [int] NOT NULL,
	[comments] [text] NOT NULL,
 CONSTRAINT [PK_localgames] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[localgames]  WITH CHECK ADD  CONSTRAINT [FK_localgames_associations] FOREIGN KEY([association_id])
REFERENCES [dbo].[associations] ([id])
GO

ALTER TABLE [dbo].[localgames] CHECK CONSTRAINT [FK_localgames_associations]
GO

ALTER TABLE [dbo].[localgames]  WITH CHECK ADD  CONSTRAINT [FK_localgames_games] FOREIGN KEY([game_id])
REFERENCES [dbo].[games] ([id])
GO

ALTER TABLE [dbo].[localgames] CHECK CONSTRAINT [FK_localgames_games]
GO

--crear tabla games_lending
CREATE TABLE [dbo].[games_lending](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[localgame_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[modifydate] [datetime] NOT NULL DEFAULT (getdate()),
	[comments] [text] NOT NULL,
	[status] [bit] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_games_lending] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[games_lending]  WITH CHECK ADD  CONSTRAINT [FK_games_lending_localgames] FOREIGN KEY([localgame_id])
REFERENCES [dbo].[localgames] ([id])
GO

ALTER TABLE [dbo].[games_lending] CHECK CONSTRAINT [FK_games_lending_localgames]
GO

ALTER TABLE [dbo].[games_lending]  WITH CHECK ADD  CONSTRAINT [FK_games_lending_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO

ALTER TABLE [dbo].[games_lending] CHECK CONSTRAINT [FK_games_lending_users]
GO

