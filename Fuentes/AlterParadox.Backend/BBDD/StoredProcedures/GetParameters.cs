﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core.Models;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetParameters", Provider = "System.Data.SqlClient")]
    public interface GetParameters
    {
    }

    public interface GetParameters_Output
    {
        [Field]
        int SQLVersion { get; set; }
        [Field]
        bool activitiesenabled_revsthirtyminutes { get; set; }

    }

    public static class GetParameters_Mapper
    {
        public static Parameter RegistroToDTO(GetParameters_Output row)
        {
            return new Parameter()
            {
                SQLVersion = row.SQLVersion,
                REVsCheckThirtyMinutes = row.activitiesenabled_revsthirtyminutes
            };
        }
    }

}