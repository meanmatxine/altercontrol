﻿using AlterParadox.Backend.Infrastructure.Data;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{

    [StoredProcedure(Name = "SaveParameters", Provider = "System.Data.SqlClient")]
    public interface SaveParameters
    {
        [Parameter(Name = "SQLVersion", Size = 0, Direction = ParameterDirection.Input)]
        int SQLVersion { get; set; }
    }

}