﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "SaveUser", Provider = "System.Data.SqlClient")]
    public interface SaveUser
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int? Id { get; set; }

        [Parameter(Name = "Number", Size = 0, Direction = ParameterDirection.Input)]
        int? Number { get; set; }

        [Parameter(Name = "Dni", Size = 20, Direction = ParameterDirection.Input)]
        string Dni { get; set; }

        [Parameter(Name = "Name", Size = 100, Direction = ParameterDirection.Input)]
        string Name { get; set; }

        [Parameter(Name = "LastName", Size = 150, Direction = ParameterDirection.Input)]
        string LastName { get; set; }

        [Parameter(Name = "Association", Size = 100, Direction = ParameterDirection.Input)]
        string Association { get; set; }

        [Parameter(Name = "Email", Size = 100, Direction = ParameterDirection.Input)]
        string Email { get; set; }

        [Parameter(Name = "Phone", Size = 100, Direction = ParameterDirection.Input)]
        string Phone { get; set; }

        [Parameter(Name = "CP", Size = 10, Direction = ParameterDirection.Input)]
        string CP { get; set; }

        [Parameter(Name = "Birthday", Size = 0, Direction = ParameterDirection.Input)]
        DateTime Birthday { get; set; }

        [Parameter(Name = "WantShirt", Size = 0, Direction = ParameterDirection.Input)]
        bool WantShirt { get; set; }

        [Parameter(Name = "Shirt", Size = 10, Direction = ParameterDirection.Input)]
        string Shirt { get; set; }

        [Parameter(Name = "BringActivity", Size = 0, Direction = ParameterDirection.Input)]
        bool BringActivity { get; set; }

        [Parameter(Name = "Lunch", Size = 0, Direction = ParameterDirection.Input)]
        bool Lunch { get; set; }

        [Parameter(Name = "SecondLunch", Size = 0, Direction = ParameterDirection.Input)]
        string SecondLunch { get; set; }

        [Parameter(Name = "Sleep", Size = 0, Direction = ParameterDirection.Input)]
        bool Sleep { get; set; }

        [Parameter(Name = "SleepList", Size = 0, Direction = ParameterDirection.Input)]
        bool SleepList { get; set; }

        [Parameter(Name = "TutorPermission", Size = 0, Direction = ParameterDirection.Input)]
        bool TutorPermission { get; set; }

        [Parameter(Name = "UserOnCharge", Size = 0, Direction = ParameterDirection.Input)]
        int? UserOnCharge { get; set; }

        [Parameter(Name = "Comments", Size = 0, Direction = ParameterDirection.Input)]
        string Comments { get; set; }
        [Parameter(Name = "Aviso", Size = 0, Direction = ParameterDirection.Input)]
        string Aviso { get; set; }

        
    }

    public interface SaveUser_Output
    {
        [Field]
        string Action { get; set; }
        [Field]
        int Id      { get; set; }
        [Field]
        int number { get; set; }
        [Field]
        string name { get; set; }
        [Field]
        string lastname { get; set; }
        [Field]
        string Dni { get; set; }
    }

    public static class SaveUser_Mapper
    {
        public static UserResumen RegistroToDTO(SaveUser_Output row)
        {
            return new UserResumen()
            {
                Action = row.Action,
                Id = row.Id,
                Number = row.number,
                Name = row.name,
                LastName = row.lastname,
                Dni = row.Dni,
                HumanoDependiente = false
            };
        }
    }

}