﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "SaveWebUser", Provider = "System.Data.SqlClient")]
    public interface SaveWebUser
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int? Id { get; set; }

        [Parameter(Name = "Dni", Size = 20, Direction = ParameterDirection.Input)]
        string Dni { get; set; }

        [Parameter(Name = "Name", Size = 100, Direction = ParameterDirection.Input)]
        string Name { get; set; }

        [Parameter(Name = "LastName", Size = 150, Direction = ParameterDirection.Input)]
        string LastName { get; set; }

        [Parameter(Name = "Association", Size = 100, Direction = ParameterDirection.Input)]
        string Association { get; set; }

        [Parameter(Name = "Email", Size = 100, Direction = ParameterDirection.Input)]
        string Email { get; set; }

        [Parameter(Name = "Phone", Size = 100, Direction = ParameterDirection.Input)]
        string Phone { get; set; }

        [Parameter(Name = "CP", Size = 10, Direction = ParameterDirection.Input)]
        string CP { get; set; }

        [Parameter(Name = "Birthday", Size = 0, Direction = ParameterDirection.Input)]
        DateTime Birthday { get; set; }

        [Parameter(Name = "WantShirt", Size = 0, Direction = ParameterDirection.Input)]
        bool WantShirt { get; set; }

        [Parameter(Name = "Shirt", Size = 10, Direction = ParameterDirection.Input)]
        string Shirt { get; set; }

        [Parameter(Name = "BringActivity", Size = 0, Direction = ParameterDirection.Input)]
        bool BringActivity { get; set; }

        [Parameter(Name = "Lunch", Size = 0, Direction = ParameterDirection.Input)]
        bool Lunch { get; set; }

        [Parameter(Name = "SecondLunch", Size = 0, Direction = ParameterDirection.Input)]
        string SecondLunch { get; set; }

        [Parameter(Name = "Sleep", Size = 0, Direction = ParameterDirection.Input)]
        bool Sleep { get; set; }

        [Parameter(Name = "SleepList", Size = 0, Direction = ParameterDirection.Input)]
        bool SleepList { get; set; }

        [Parameter(Name = "TutorPermission", Size = 0, Direction = ParameterDirection.Input)]
        bool TutorPermission { get; set; }

        [Parameter(Name = "Comments", Size = 0, Direction = ParameterDirection.Input)]
        string Comments { get; set; }
    }

}