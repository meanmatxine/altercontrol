﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetUsersOnCharge", Provider = "System.Data.SqlClient")]
    public interface GetUsersOnCharge
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int Id { get; set; }
    }

    public interface GetUsersOnCharge_Output
    {
        [Field]
        int id { get; set; }
        [Field]
        int number { get; set; }
        [Field]
        string name { get; set; }
        [Field]
        string lastname { get; set; }
        [Field]
        DateTime birthday { get; set; }
    }

    public static class GetUsersOnCharge_Mapper
    {
        public static PersonaAlCargo RegistroToDTO(GetUsersOnCharge_Output row)
        {
            return new PersonaAlCargo()
            {
                Id = row.id,
                Number = row.number,
                Name = row.name,
                LastName = row.lastname,
                Birthday = row.birthday,
            };
        }
    }

}