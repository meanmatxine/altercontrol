﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetUsers", Provider = "System.Data.SqlClient")]
    public interface GetUsers
    {
    }

    public interface GetUsers_Output
    {
        [Field]
        int id { get; set; }
        [Field]
        int number { get; set; }
        [Field]
        string dni { get; set; }
        [Field]
        string name { get; set; }
        [Field]
        string lastname { get; set; }
        [Field]
        string association { get; set; }
        [Field]
        string email { get; set; }
        [Field]
        string phone { get; set; }
        [Field]
        string cp { get; set; }
        [Field]
        DateTime birthday { get; set; }
        [Field]
        bool wantshirt { get; set; }
        [Field]
        string shirt { get; set; }
        [Field]
        bool bringactivity { get; set; }
        [Field]
        bool lunch { get; set; }
        [Field]
        string secondlunch { get; set; }
        [Field]
        bool sleep { get; set; }
        [Field]
        bool sleeplist { get; set; }
        [Field]
        bool tutorpermission { get; set; }
        [Field]
        int? useroncharge { get; set; }
        [Field]
        string comments { get; set; }
        [Field]
        string aviso { get; set; }

    }

    public static class GetUsers_Mapper
    {
        public static User RegistroToDTO(GetUsers_Output row)
        {
            return new User()
            {
                Id = row.id,
                Number = row.number,
                Dni = row.dni, 
                Name = row.name,
                LastName = row.lastname,
                Association = row.association, 
                Email = row.email,
                Phone = row.phone,
                Birthday = row.birthday,
                Camiseta = row.wantshirt,
                Shirt = row.shirt,
                TraeActividades = row.bringactivity,
                Lunch = row.lunch,
                SegundoLunch = row.secondlunch,
                Sleep = row.sleep,
                DormirListaEspera = row.sleeplist,
                TutorPermission = row.tutorpermission,
                UserOnCharge = row.useroncharge,
                Comments = row.comments,
                Aviso = row.aviso,
                CP = row.cp
            };
        }
    }

}