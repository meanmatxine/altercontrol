﻿using AlterParadox.Backend.Infrastructure.Data;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{

    [StoredProcedure(Name = "DeleteActivity", Provider = "System.Data.SqlClient")]
    public interface DeleteActivity
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int Id { get; set; }
    }

}