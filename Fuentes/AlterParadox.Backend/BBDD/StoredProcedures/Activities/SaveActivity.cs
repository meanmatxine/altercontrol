﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "SaveActivity", Provider = "System.Data.SqlClient")]
    public interface SaveActivity
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int? Id { get; set; }
        [Parameter(Name = "Organizer", Size = 100, Direction = ParameterDirection.Input)]
        string Organizer { get; set; }
        [Parameter(Name = "Email", Size = 100, Direction = ParameterDirection.Input)]
        string Email { get; set; }
        [Parameter(Name = "Phone", Size = 100, Direction = ParameterDirection.Input)]
        string Phone { get; set; }
        [Parameter(Name = "Association", Size = 100, Direction = ParameterDirection.Input)]
        string Association { get; set; }
        [Parameter(Name = "Name", Size = 100, Direction = ParameterDirection.Input)]
        string Name { get; set; }
        [Parameter(Name = "ActivityDate", Size = 0, Direction = ParameterDirection.Input)]
        DateTime ActivityDate { get; set; }
        [Parameter(Name = "Place", Size = 100, Direction = ParameterDirection.Input)]
        string Place { get; set; }
        [Parameter(Name = "Type", Size = 100, Direction = ParameterDirection.Input)]
        string Type { get; set; }
        [Parameter(Name = "Participants", Size = 10, Direction = ParameterDirection.Input)]
        string Participants { get; set; }
        [Parameter(Name = "MaxParticipants", Size = 0, Direction = ParameterDirection.Input)]
        int MaxParticipants { get; set; }
        [Parameter(Name = "MinParticipants", Size = 0, Direction = ParameterDirection.Input)]
        int MinParticipants { get; set; }
        [Parameter(Name = "Duration", Size = 0, Direction = ParameterDirection.Input)]
        int Duration { get; set; }
        [Parameter(Name = "Enabled", Size = 0, Direction = ParameterDirection.Input)]
        bool Enabled { get; set; }
        [Parameter(Name = "Summary", Size = 0, Direction = ParameterDirection.Input)]
        string Summary { get; set; }
        [Parameter(Name = "Comments", Size = 0, Direction = ParameterDirection.Input)]
        string Comments { get; set; }
        [Parameter(Name = "Needs", Size = 0, Direction = ParameterDirection.Input)]
        string Needs { get; set; }
        [Parameter(Name = "Winner", Size = 0, Direction = ParameterDirection.Input)]
        string Winner { get; set; }
    }

    public interface SaveActivity_Output
    {
        [Field]
        string Action { get; set; }
        [Field]
        int Id { get; set; }
        [Field]
        string name { get; set; }
    }

    public static class SaveActivity_Mapper
    {
        public static ActivityResumen RegistroToDTO(SaveActivity_Output row)
        {
            return new ActivityResumen()
            {
                Action = row.Action,
                Id = row.Id,
                Name = row.name
            };
        }
    }

}