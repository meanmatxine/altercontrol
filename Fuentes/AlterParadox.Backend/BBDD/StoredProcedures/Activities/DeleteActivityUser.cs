﻿using AlterParadox.Backend.Infrastructure.Data;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{

    [StoredProcedure(Name = "DeleteActivityUser", Provider = "System.Data.SqlClient")]
    public interface DeleteActivityUser
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int Id { get; set; }
    }

}