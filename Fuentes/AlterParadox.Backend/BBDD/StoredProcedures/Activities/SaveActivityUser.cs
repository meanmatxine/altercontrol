﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "SaveActivityUser", Provider = "System.Data.SqlClient")]
    public interface SaveActivityUser
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int? Id { get; set; }
        [Parameter(Name = "IdActivity", Size = 0, Direction = ParameterDirection.Input)]
        int IdActivity { get; set; }
        [Parameter(Name = "IdUser", Size = 0, Direction = ParameterDirection.Input)]
        int IdUser { get; set; }
        [Parameter(Name = "InscriptionDate", Size = 0, Direction = ParameterDirection.Input)]
        DateTime InscriptionDate { get; set; }
        [Parameter(Name = "Reserve", Size = 0, Direction = ParameterDirection.Input)]
        bool Reserve { get; set; }
        [Parameter(Name = "Comments", Size = 0, Direction = ParameterDirection.Input)]
        string Comments { get; set; }
    }

    public interface SaveActivityUser_Output
    {
        [Field]
        string Action { get; set; }
        [Field]
        int Id { get; set; }
    }

    public static class SaveActivityUser_Mapper
    {
        public static ActivityUserResumen RegistroToDTO(SaveActivityUser_Output row)
        {
            return new ActivityUserResumen()
            {
                Action = row.Action,
                Id = row.Id
            };
        }
    }

}