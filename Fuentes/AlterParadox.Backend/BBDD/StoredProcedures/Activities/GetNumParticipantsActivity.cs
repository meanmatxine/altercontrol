﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetNumParticipantsActivity", Provider = "System.Data.SqlClient")]
    public interface GetNumParticipantsActivity
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int Id { get; set; }
    }

    public interface GetNumParticipantsActivity_Output
    {
        [Field]
        int NumParticipants { get; set; }
    }

    public static class GetNumParticipantsActivity_Mapper
    {
        public static int RegistroToDTO(GetNumParticipantsActivity_Output row)
        {
            return row.NumParticipants;
        }
    }

}
