﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetActivities", Provider = "System.Data.SqlClient")]
    public interface GetActivities
    {
    }

    public interface GetActivities_Output
    {
        [Field]
        int id { get; set; }
        [Field]
        string organizer { get; set; }
        [Field]
        string email { get; set; }
        [Field]
        string phone { get; set; }
        [Field]
        string association { get; set; }
        [Field]
        string name { get; set; }
        [Field]
        DateTime activitydate { get; set; }
        [Field]
        string place { get; set; }
        [Field]
        string type { get; set; }
        [Field]
        bool enabled { get; set; }
        [Field]
        string participants { get; set; }
        [Field]
        int numparticipants { get; set; }
        [Field]
        int maxparticipants { get; set; }
        [Field]
        int minparticipants { get; set; }
        [Field]
        int duration { get; set; }
        [Field]
        string summary { get; set; }
        [Field]
        string comments { get; set; }
        [Field]
        string needs { get; set; }
        [Field]
        string winner { get; set; }
        [Field]
        DateTime modifydate { get; set; }
    }

    public static class GetActivities_Mapper
    {
        public static Activity RegistroToDTO(GetActivities_Output row)
        {

            return new Activity()
            {
                Id = row.id,
                Organizer = row.organizer,
                Email = row.email,
                Phone = row.phone,
                Association = row.association,
                Name = row.name,
                ActivityDate = row.activitydate,
                Place = row.place,
                Type = row.type,
                Participants = row.participants,
                MaxParticipants = row.maxparticipants,
                MinParticipants = row.minparticipants,
                NumParticipants = row.numparticipants,
                Duration = row.duration,
                Summary = row.summary,
                Comments = row.comments,
                Needs = row.needs,
                Winner = row.winner,
                LastModify = row.modifydate
            };
        }
    }

}
