﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetActivityUsers", Provider = "System.Data.SqlClient")]
    public interface GetActivityUsers
    {
        [Parameter(Name = "IdActivity", Size = 0, Direction = ParameterDirection.Input)]
        int IdActivity { get; set; }
    }

    public interface GetActivityUsers_Output
    {
        [Field]
        int id { get; set; }
        [Field]
        int activity_id { get; set; }
        [Field]
        int user_id { get; set; }
        [Field]
        bool reserve { get; set; }
        [Field]
        DateTime inscriptiondate { get; set; }
        [Field]
        string comments { get; set; }
        [Field]
        string name { get; set; }
        [Field]
        string lastname { get; set; }
        [Field]
        int number { get; set; }
        [Field]
        string aviso { get; set; }

        [Field]
        bool inscrito { get; set; }

    }

    public static class GetActivityUsers_Mapper
    {
        public static ActivityUser RegistroToDTO(GetActivityUsers_Output row)
        {

            return new ActivityUser()
            {
                Id = row.id,
                Activity_Id = row.activity_id,
                User_Id = row.user_id,
                Reserve = row.reserve,
                InscriptionDate = row.inscriptiondate,
                Comments = row.comments,
                Name = string.Format("{0} {1}",row.name,row.lastname),
                Number = row.number,
                Inscrito = row.inscrito,
                Aviso = row.aviso
            };
        }
    }

}
