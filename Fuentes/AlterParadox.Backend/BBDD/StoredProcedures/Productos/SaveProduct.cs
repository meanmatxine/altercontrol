﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "SaveProduct", Provider = "System.Data.SqlClient")]
    public interface SaveProduct
    {
        [Parameter(Name = "productoID", Size = 0, Direction = ParameterDirection.Input)]
        int? productoID { get; set; }
        [Parameter(Name = "productoCB", Size = 100, Direction = ParameterDirection.Input)]
        string productoCB { get; set; }
        [Parameter(Name = "productoName", Size = 100, Direction = ParameterDirection.Input)]
        string productoName { get; set; }
        [Parameter(Name = "productoTipo", Size = 0, Direction = ParameterDirection.Input)]
        int productoTipo { get; set; }
        [Parameter(Name = "productoPrecioSinIva", Size = 0, Direction = ParameterDirection.Input)]
        decimal productoPrecioSinIva { get; set; }
        [Parameter(Name = "productoIVA", Size = 0, Direction = ParameterDirection.Input)]
        int productoIVA { get; set; }
        [Parameter(Name = "productoPrecio", Size = 0, Direction = ParameterDirection.Input)]
        decimal productoPrecio { get; set; }
        [Parameter(Name = "productoCantidad", Size = 0, Direction = ParameterDirection.Input)]
        int productoCantidad { get; set; }
        [Parameter(Name = "productoImage", Size = 50, Direction = ParameterDirection.Input)]
        string productoImage { get; set; }
        [Parameter(Name = "productoActivo", Size = 0, Direction = ParameterDirection.Input)]
        int productoActivo { get; set; }
    }

    public interface SaveProduct_Output
    {
        [Field]
        string Action { get; set; }
        [Field]
        int productoID { get; set; }
        [Field]
        string productoName { get; set; }
    }

    public static class SaveProduct_Mapper
    {
        public static ProductResumen RegistroToDTO(SaveProduct_Output row)
        {
            return new ProductResumen()
            {
                Action = row.Action,
                productoID = row.productoID,
                productoName = row.productoName
            };
        }
    }

}