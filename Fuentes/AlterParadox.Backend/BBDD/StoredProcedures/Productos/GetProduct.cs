﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetProduct", Provider = "System.Data.SqlClient")]
    public interface GetProduct
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int Id { get; set; }
    }

    public interface GetProduct_Output
    {
        [Field]
        int productoID { get; set; }
        [Field]
        string productoCB { get; set; }
        [Field]
        string productoName { get; set; }
        [Field]
        int productoTipo { get; set; }
        [Field]
        decimal productoPrecioSinIva { get; set; }
        [Field]
        int productoIVA { get; set; }
        [Field]
        decimal productoPrecio { get; set; }
        [Field]
        int productoCantidad { get; set; }
        [Field]
        string productoImage { get; set; }
        [Field]
        int productoActivo { get; set; }
    }

    public static class GetProduct_Mapper
    {
        public static Product RegistroToDTO(GetProduct_Output row)
        {
            return new Product()
            {
                productoID = row.productoID,
                productoCB = row.productoCB,
                productoName = row.productoName,
                productoTipo = row.productoTipo,
                productoPrecioSinIva = row.productoPrecioSinIva,
                productoIVA = row.productoIVA,
                productoPrecio = row.productoPrecio,
                productoCantidad = row.productoCantidad,
                productoImage = row.productoImage,
                productoActivo=row.productoActivo
            };
        }
    }

}