﻿using AlterParadox.Backend.Infrastructure.Data;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "DeleteProduct", Provider = "System.Data.SqlClient")]
    public interface DeleteProduct
    {
        [Parameter(Name = "productoID", Size = 0, Direction = ParameterDirection.Input)]
        int productoID { get; set; }
    }
}