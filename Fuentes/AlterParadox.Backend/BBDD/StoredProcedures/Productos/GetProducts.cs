﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetProducts", Provider = "System.Data.SqlClient")]
    public interface GetProducts
    {

    }

    public interface GetProducts_Output
    {
        [Field]
        int productoID { get; set; }
        [Field]
        string productoCB { get; set; }
        [Field]
        string productoName { get; set; }
        [Field]
        int productoTipo { get; set; }
        [Field]
        decimal productoPrecioSinIva { get; set; }
        [Field]
        int productoIVA { get; set; }
        [Field]
        decimal productoPrecio { get; set; }
        [Field]
        int productoCantidad { get; set; }
        [Field]
        string productoImage { get; set; }
        [Field]
        int productoActivo { get; set; }
    }

    public static class GetProducts_Mapper
    {
        public static Product RegistroToDTO(GetProducts_Output row)
        {
            return new Product()
            {
                productoID = row.productoID,
                productoCB = row.productoCB,
                productoName = row.productoName,
                productoTipo = row.productoTipo,
                productoPrecioSinIva = row.productoPrecioSinIva,
                productoIVA = row.productoIVA,
                productoPrecio = row.productoPrecio,
                productoCantidad = row.productoCantidad,
                productoImage = row.productoImage,
                productoActivo = row.productoActivo
            };
        }
    }

}