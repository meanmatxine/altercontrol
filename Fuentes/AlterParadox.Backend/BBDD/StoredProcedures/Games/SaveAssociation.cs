﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "SaveAssociation", Provider = "System.Data.SqlClient")]
    public interface SaveAssociation
    {
        [Parameter(Name = "Id", Size = 0, Direction = ParameterDirection.Input)]
        int? Id { get; set; }
        [Parameter(Name = "Name", Size = 200, Direction = ParameterDirection.Input)]
        string Name { get; set; }
        [Parameter(Name = "Contact", Size = 100, Direction = ParameterDirection.Input)]
        string Contact { get; set; }
        [Parameter(Name = "Email", Size = 100, Direction = ParameterDirection.Input)]
        string Email { get; set; }
        [Parameter(Name = "Phone", Size = 100, Direction = ParameterDirection.Input)]
        string Phone { get; set; }
        [Parameter(Name = "Comments", Size = 0, Direction = ParameterDirection.Input)]
        string Comments { get; set; }

    }

    public interface SaveAssociation_Output
    {
        [Field]
        string Action { get; set; }
        [Field]
        int Id { get; set; }
        [Field]
        string Name { get; set; }
    }

    public static class SaveAssociation_Mapper
    {
        public static AssociationResumen RegistroToDTO(SaveAssociation_Output row)
        {
            return new AssociationResumen()
            {
                Action = row.Action,
                Id = row.Id,
                Name = row.Name
            };
        }
    }

}