﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetLocalGames", Provider = "System.Data.SqlClient")]
    public interface GetLocalGames
    {
    }

    public interface GetLocalGames_Output
    {
        [Field]
        int id { get; set; }

        [Field]
        int game_id { get; set; }

        [Field]
        string gamename { get; set; }

        [Field]
        int association_id { get; set; }

        [Field]
        string associationname { get; set; }

        [Field]
        string editorial { get; set; }

        [Field]
        int family { get; set; }

        [Field]
        int type { get; set; }

        [Field]
        int years { get; set; }

        [Field]
        int player_min { get; set; }

        [Field]
        int player_max { get; set; }

        [Field]
        int difficulty { get; set; }

        [Field]
        bool expansion { get; set; }

        [Field]
        int duration { get; set; }

        [Field]
        string comments { get; set; }

        [Field]
        bool status { get; set; }
    }

    public static class GetLocalGames_Mapper
    {
        public static LocalGame RegistroToDTO(GetLocalGames_Output row)
        {

            return new LocalGame()
            {
                Id = row.id,
                Association = new Association()
                {
                    Id = row.association_id,
                    Name = row.associationname
                },
                TheGame = new Game()
                {
                    Id = row.game_id,
                    Name = row.gamename,
                    Family = row.family,
                    Type = row.type,
                    Editorial = row.editorial,
                    Difficulty = row.difficulty,
                    Comments = row.comments,
                    Duration = row.duration,
                    IsExpansion = row.expansion ? 1 : 0,
                    PlayerMax = row.player_max,
                    PlayerMin = row.player_min,
                    Years = row.years
                },
                Status = row.status ? 1 : 0

            };
        }
    }

}
