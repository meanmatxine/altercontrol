﻿using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using AlterParadox.Core.Models;
using System;
using System.Data;

namespace AlterParadox.Backend.BBDD.StoredProcedures
{
    [StoredProcedure(Name = "GetAssociations", Provider = "System.Data.SqlClient")]
    public interface GetAssociations
    {
      
    }

    public interface GetAssociations_Output
    {
        [Field]
        int Id { get; set; }
        [Field]
        string Name { get; set; }
        [Field]
        string Contact { get; set; }
        [Field]
        string Email { get; set; }
        [Field]
        string Phone { get; set; }
        [Field]
        string Comments { get; set; }

    }

    public static class GetAssociations_Mapper
    {
        public static Association RegistroToDTO(GetAssociations_Output row)
        {
            return new Association()
            {
                Id = row.Id,
                Name = row.Name,
                Contact = row.Contact,
                Email = row.Email,
                Phone = row.Phone,
                Comments = row.Comments
            };
        }
    }

}