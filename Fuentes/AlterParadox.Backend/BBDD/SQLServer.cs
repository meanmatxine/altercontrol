﻿using AlterParadox.Backend.BBDD;
using AlterParadox.Backend.BBDD.StoredProcedures;
using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AlterParadox.Backend
{
    public interface ISQLServer
    {
        void saveParameter(Parameter p);
        Parameter getParameters();
        bool executeSQL(string SQL);
        DataSet Query(string SQL);
        void Init();
        string GetConnectionString();
    }

    public class SQLServer : AbstractDatabaseRepository, ISQLServer
    {
        private readonly IConnectionManager _connectionManager;

        public SQLServer(
           IConnectionManager connectionManager,
           IDbCommandFactory commandFactory) : base(connectionManager, commandFactory)
        {
            if (connectionManager == null)
                throw new ArgumentNullException("IConnectionManager no puede ser nulo en DBManager");

            _connectionManager = connectionManager;
        }

        public void Init()
        {

            //string SQL = "";
            int version = 0;

            if (!checkVersionExists())
            {
                executeScript("001_CREATE_TABLE_parameters.sql");//Crea tabla y PAs necesarios para saber en que versión estamos.
            }
                

            var parametro = getParameters();
            if (parametro == null)
                return; //Esto no debería ocurrir nunca

            version = parametro.SQLVersion;

            if (version < 2)
            {//Creo la estructura inicial
                executeScript("002_CREATE_BASIC_STRUCTURE.sql");
                parametro.SQLVersion = 2;
            }

            if (version < 3)
            {//Creo la estructura inicial
                executeScript("003_CREATE_BASIC_STRUCTURE_TPV.sql");
                parametro.SQLVersion = 3;
            }

            if (version < 4)
            {//Creo la estructura inicial
                executeScript("004_CREATE_USERS_PA.sql");
                parametro.SQLVersion = 4;
            }

            //if (version < 5)
            //{//Creo la estructura inicial
            //    executeScript("005_MODIFY_PRODUCTS_TABLE.sql");
            //    parametro.SQLVersion = 5;
            //}

            if (version < 6)
            {//Creo la estructura inicial
                executeScript("006_CREATE_PRODUCTS_PA.sql");
                parametro.SQLVersion = 6;
            }


            if (version < 7)
            {//Creo la estructura inicial
                executeScript("007_USERS_ACTIVITIES_PA_V2.sql");
                
                parametro.SQLVersion = 7;
            }

            if (version < 8)
            {//Creo la estructura inicial
                executeScript("008_UPDATE_PRODUCTS.sql");
                parametro.SQLVersion = 8;
            }

            if (version < 9)
            {//Creo la estructura inicial
                executeScript("009_INSERT_GAMES.sql");
                parametro.SQLVersion = 9;
            }

            if (version < 10)
            {//Creo la estructura inicial
                executeScript("010_ACTIVITYUSERS_PA.sql");
                parametro.SQLVersion = 10;
            }

            if (version < 11)
            {//Creo la estructura inicial
                executeScript("011_TABLE_UPGRADES_UMBRAS2019.sql");
                parametro.SQLVersion = 11;
            }

            if (version < 12)
            {//Creo la estructura inicial
                executeScript("012_GAMES_PA.sql");
                parametro.SQLVersion = 12;
            }
            //

            if (version < 13)
            {//Creo la estructura inicial
                executeScript("013_GAMES_V2_PA.sql");
                parametro.SQLVersion = 13;
            }

            if (version < 14)
            {//Creo la estructura inicial
                if(executeScript("014_PersonasAlCargo.sql"))
                    parametro.SQLVersion = 14;
            }

            //}
            //if (productosCambiados)
            //{
            //    //Creo la estructura inicial
            //    executeScript("099_UPDATE_PRODUCTS.sql");
            //}

            //Si se ha actualizado la versión de SQL, guardo la nueva versión
            saveParameter(parametro);
            
        }

        public string getResourceContent(string name)
        {// Open the text file using a stream reader.
            string fileContents = string.Empty;
            try
            {
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AlterParadox.Backend.BBDD.SQL."+name))
                {
                    TextReader tr = new StreamReader(stream);
                    fileContents = tr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
           
            return fileContents;
        }

        public bool checkVersionExists()
        {
            var respuesta = false;
            using (SqlConnection conexion = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand comando = new SqlCommand())
                {
                    Assembly _assemblytexto1 = Assembly.GetExecutingAssembly();


                    comando.CommandText = getResourceContent("000_Exists_BBDDVersion.sql");//File.ReadAllText();// );

                    comando.CommandTimeout = 5;
                    comando.Connection = conexion;
                    comando.CommandType = System.Data.CommandType.Text;

                    conexion.Open();

                    // ejecuto sentencia y obtengo resultado
                    var reader = null as SqlDataReader;
                    reader = comando.ExecuteReader();
                    while (reader.Read())
                    {
                        respuesta = Convert.ToBoolean(reader["existe"]);
                    }

                    conexion.Close();
                }
            }

            return respuesta;
        }

        public bool createVersion()
        {
            var respuesta = true;
            string SQL = string.Empty;

            

            respuesta = executeScript(SQL);
            if (!respuesta) return respuesta;

            return true;


        }

        public Parameter getParameters()
        {
            var result = CommandFactory
                .Create<GetParameters>()
                .SetConnection(ConnectionManager.GetConnectionDefault())
                .Execute()
                .ToList<GetParameters_Output>();

            if (!result.Any())
                return null;

            var parameter = GetParameters_Mapper.RegistroToDTO(result.FirstOrDefault());

            return parameter;

        }

        public void saveParameter(Parameter parameter)
        {
            CommandFactory
                    .Create<SaveParameters>()
                    .SetParameter(p => p.SQLVersion = parameter.SQLVersion)
                    .SetConnection(ConnectionManager.GetConnectionDefault())
                    .Execute();

        }

        public bool executeScript(string scriptPath)
        {
            bool respuesta = false;
            string SQL = string.Empty;
            //Creo tabla de versión
            SQL = getResourceContent(scriptPath);
            if (string.IsNullOrEmpty(SQL))
            {
                //Añade el fichero SQL como recurso incrutado en las propiedades del fichero
               //return false;
                throw new Exception("Añade el fichero SQL como recurso incrutado en las propiedades del fichero");
            }

            try
            {
                using (SqlConnection conexion = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand comando = new SqlCommand())
                    {
                        comando.CommandTimeout = 5;
                        comando.Connection = conexion;
                        comando.CommandType = System.Data.CommandType.Text;
                        conexion.Open();

                        foreach (var sqlBatch in SQL.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            comando.CommandText = sqlBatch;
                            comando.ExecuteNonQuery();
                        }

                        conexion.Close();
                        respuesta = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }

            return respuesta;
        }

        public bool executeSQL(string SQL)
        {
            bool respuesta = true;

            try
            {
                using (SqlConnection conexion = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand comando = new SqlCommand())
                    {
                        comando.CommandText = SQL;
                        comando.CommandTimeout = 5;
                        comando.Connection = conexion;
                        comando.CommandType = System.Data.CommandType.Text;
                        conexion.Open();
                        comando.ExecuteNonQuery();
                        conexion.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                
            }
            

            return respuesta;
        }

        public DataSet Query(string SQL)
        {
            DataSet respuesta = new DataSet();
            DataTable table = new DataTable("Table");
            try
            {
                using (SqlConnection conexion = new SqlConnection(GetConnectionString()))
                {
                    using (SqlCommand comando = new SqlCommand())
                    {
                        comando.CommandText = SQL;
                        comando.CommandTimeout = 5;
                        comando.Connection = conexion;
                        comando.CommandType = System.Data.CommandType.Text;
                        conexion.Open();

                        table.Load(comando.ExecuteReader());
                        respuesta.Tables.Add(table);

                        conexion.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return respuesta;
        }

        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["BBDD"].ConnectionString.Replace("**hostname**", Environment.MachineName.ToString());
        }

    }

   
}