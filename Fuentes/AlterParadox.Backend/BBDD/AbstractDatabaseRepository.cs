﻿using System;
using System.Data;
using AlterParadox.Backend.Infrastructure.Data;

namespace AlterParadox.Backend.BBDD
{
    public abstract class AbstractDatabaseRepository
    {
        public AbstractDatabaseRepository(IConnectionManager connectionManager, IDbCommandFactory commandFactory)
        {
            if (null == connectionManager || null == commandFactory)
                throw new ArgumentNullException();
            ConnectionManager = connectionManager;
            CommandFactory = commandFactory;
            // TODO: Esto tiene que estar inicializado de antemano. Sino va a llamarse por cada objeto de datos.
            //ConnectionManager.LoadConfigurationFile();
        }

        protected IConnectionManager ConnectionManager { get; private set; }
        protected IDbCommandFactory CommandFactory { get; private set; }
    }
}