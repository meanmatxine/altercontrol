﻿using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System.Collections.Generic;
using System.ServiceModel;
using System.Linq;
using System;
using Newtonsoft.Json;

namespace AlterParadox.Backend.DAL
{
    [ServiceContract]
    public interface IUserServiceRepository
    {
        [OperationContract]
        DeleteUserResponse DeleteUser(DeleteUserRequest request);
        [OperationContract]
        GetUserResponse GetUser(GetUserRequest request);
        [OperationContract]
        GetUserByNumberResponse GetUserByNumber(GetUserByNumberRequest request);
        [OperationContract]
        GetUsersResponse GetUsers();
        [OperationContract]
        GetWebUsersResponse GetWebUsers();
        [OperationContract]
        SaveUserResponse SaveUser(SaveUserRequest request);
        [OperationContract]
        SaveWebUserResponse SaveWebUser(SaveWebUserRequest request);

        [OperationContract]
        GetLocalGamesResponse GetLocalGames();


    }

    public class UserServiceRepository : IUserServiceRepository
    {
        private readonly IUsers _users;

        public UserServiceRepository(
            IUsers users)
        {
            _users = users;
        }

        public GetUserResponse GetUser(GetUserRequest request)
        {
            var usuario = _users.GetUser(request.IdUser);

            if (usuario == null)
            {
                return GetUserResponse.RespuestaFallo(
                   Result<GetUserRequest>.CreateFailed(string.Format("No se han encontrado el usuario vinculado al ID {0}.", request.IdUser))
                );
            }
            else
            {
                var lista = _users.GetUsuariosAlCargo(request.IdUser);
                usuario.PersonasAlCargo = JsonConvert.SerializeObject(lista);

            }
            return GetUserResponse.RespuestaExito(usuario);
        }

        public GetUserByNumberResponse GetUserByNumber(GetUserByNumberRequest request)
        {
            var usuario = _users.GetUserByNumber(request.Number);

            if (usuario == null)
                return GetUserByNumberResponse.RespuestaFallo(
                   Result<GetUserByNumberRequest>.CreateFailed(string.Format("No se han encontrado el usuario vinculado al número {0}.", request.Number))
                );

            return GetUserByNumberResponse.RespuestaExito(usuario);
        }

        public GetUsersResponse GetUsers()
        {
            var listaUsuarios = _users.GetUsers();

            if (!listaUsuarios.Any())
                return GetUsersResponse.RespuestaFallo(
                   Result<GetUsersRequest>.CreateFailed(string.Format("No se han encontrado usuarios.").ToString())
                );

            return GetUsersResponse.RespuestaExito(listaUsuarios);
        }

        public GetWebUsersResponse GetWebUsers()
        {
            var listaUsuarios = _users.GetWebUsers();

            if (!listaUsuarios.Any())
                return GetWebUsersResponse.RespuestaFallo(
                   Result<GetWebUsersRequest>.CreateFailed(string.Format("No se han encontrado usuarios.").ToString())
                );

            return GetWebUsersResponse.RespuestaExito(listaUsuarios);
        }

        public SaveUserResponse SaveUser(SaveUserRequest request)
        {
            UserResumen usuarioResultante = null;
            try
            {
                usuarioResultante = _users.SaveUser(request.Usuario);
            }
            catch (Exception ex)
            {
                return SaveUserResponse.RespuestaFallo(
                  Result<SaveUserRequest>.CreateFailed(string.Format("Error guardando el usuario: {0}", ex.ToString()))
                );
            }

            if(usuarioResultante == null)
                return SaveUserResponse.RespuestaFallo(
                   Result<SaveUserRequest>.CreateFailed(string.Format("No se ha podido recuperar el usuario final.", request.ToString()))
                 );

            return SaveUserResponse.RespuestaExito(usuarioResultante);
        }

        public SaveWebUserResponse SaveWebUser(SaveWebUserRequest request)
        {
            try
            {
                _users.SaveWebUser(request.Usuario);
            }
            catch (Exception ex)
            {
                return SaveWebUserResponse.RespuestaFallo(
                  Result<SaveWebUserRequest>.CreateFailed(string.Format("Error guardando el usuario web: {0}", ex.ToString()))
                );
            }

            return SaveWebUserResponse.RespuestaExito();
        }

        public DeleteUserResponse DeleteUser(DeleteUserRequest request)
        {
            try
            {
                _users.DeleteUser(request.IdUser);
            }
            catch (Exception ex)
            {
                return DeleteUserResponse.RespuestaFallo(
                   Result<DeleteUserRequest>.CreateFailed(string.Format("Error borrando el usuario: {0}", ex.ToString()))
                );
            }

            return DeleteUserResponse.RespuestaExito();
        }

        public GetLocalGamesResponse GetLocalGames()
        {
            var listaGames = _users.GetLocalGames();

            if (!listaGames.Any())
                return GetLocalGamesResponse.RespuestaFallo(
                   Result<GetLocalGamesRequest>.CreateFailed(string.Format("No se han encontrado usuarios.").ToString())
                );

            return GetLocalGamesResponse.RespuestaExito(listaGames);
        }

    }
}