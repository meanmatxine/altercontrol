﻿using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System.Collections.Generic;
using System.ServiceModel;
using System.Linq;
using System;

namespace AlterParadox.Backend.DAL
{
    [ServiceContract]
    public interface IActivityServiceRepository
    {
        #region actividades
        

        [OperationContract]
        GetActivityResponse GetActivity(GetActivityRequest request);

        [OperationContract]
        GetActivitiesResponse GetActivities();

        [OperationContract]
        SaveActivityResponse SaveActivity(SaveActivityRequest request);

        [OperationContract]
        DeleteActivityResponse DeleteActivity(DeleteActivityRequest request);

        [OperationContract]
        int CheckParticipants(Activity activity);
        #endregion

        #region actividadesUsuario
        
        [OperationContract]
        GetActivityUserResponse GetActivityUser(GetActivityUserRequest request);

        [OperationContract]
        GetActivityUsersResponse GetActivityUsers(GetActivityUsersRequest request);

        [OperationContract]
        SaveActivityUserResponse SaveActivityUser(SaveActivityUserRequest request);

        [OperationContract]
        DeleteActivityUserResponse DeleteActivityUser(DeleteActivityUserRequest request);
        #endregion

    }

    public class ActivityServiceRepository : IActivityServiceRepository
    {
        private readonly IActivities _activities;

        public ActivityServiceRepository(
            IActivities activities)
        {
            _activities = activities;
        }
        #region actividades
        public GetActivityResponse GetActivity(GetActivityRequest request)
        {
            var actividad = _activities.GetActivity(request.IdActivity);

            if (actividad == null)
                return GetActivityResponse.RespuestaFallo(
                   Result<GetActivityRequest>.CreateFailed(string.Format("No se han encontrado la actividad vinculada al ID {0}.", request.IdActivity))
                );

            return GetActivityResponse.RespuestaExito(actividad);
        }

        public GetActivitiesResponse GetActivities()
        {
            var listaActividades = _activities.GetActivities();

            if (!listaActividades.Any())
                return GetActivitiesResponse.RespuestaFallo(
                   Result<GetActivitiesRequest>.CreateFailed(string.Format("No se han encontrado actividades.").ToString())
                );

            return GetActivitiesResponse.RespuestaExito(listaActividades);
        }

        public SaveActivityResponse SaveActivity(SaveActivityRequest request)
        {
            ActivityResumen actividadResultante = null;
            try
            {
                actividadResultante = _activities.SaveActivity(request.Actividad);
            }
            catch (Exception ex)
            {
                return SaveActivityResponse.RespuestaFallo(
                  Result<SaveActivityRequest>.CreateFailed(string.Format("Error guardando la actividad: {0}", ex.ToString()))
                );
            }

            if (actividadResultante == null)
                return SaveActivityResponse.RespuestaFallo(
                   Result<SaveActivityRequest>.CreateFailed(string.Format("No se ha podido recuperar la actividad final.", request.ToString()))
                 );

            return SaveActivityResponse.RespuestaExito(actividadResultante);
        }

        public DeleteActivityResponse DeleteActivity(DeleteActivityRequest request)
        {
            try
            {
                _activities.DeleteActivity(request.IdActivity);
            }
            catch (Exception ex)
            {
                return DeleteActivityResponse.RespuestaFallo(
                   Result<DeleteActivityRequest>.CreateFailed(string.Format("Error borrando la actividad: {0}", ex.ToString()))
                );
            }

            return DeleteActivityResponse.RespuestaExito();
        }

        public int CheckParticipants(Activity activity)
        {
            if (activity == null)
                return 0;
            if (activity.Id == null)
                return 0;
            return _activities.CheckParticipants((int)activity.Id);
        }
        #endregion

        #region actividadesUsuario
        public GetActivityUserResponse GetActivityUser(GetActivityUserRequest request)
        {
            var actividadUsuario = _activities.GetActivityUser(request.IdActivity, request.IdUser);

            if (actividadUsuario == null)
                return GetActivityUserResponse.RespuestaFallo(
                   Result<GetActivityUserRequest>.CreateFailed(string.Format("No se han encontrado la actividad vinculada al ID {0}.", request.IdActivity))
                );

            return GetActivityUserResponse.RespuestaExito(actividadUsuario);
        }

        public GetActivityUsersResponse GetActivityUsers(GetActivityUsersRequest request)
        {
            var listaActividadUsuarios = _activities.GetActivityUsers(request.IdActivity);

            if (!listaActividadUsuarios.Any())
                return GetActivityUsersResponse.RespuestaFallo(
                   Result<GetActivityUsersRequest>.CreateFailed(string.Format("No se han encontrado actividades.").ToString())
                );

            return GetActivityUsersResponse.RespuestaExito(listaActividadUsuarios);
        }

        public SaveActivityUserResponse SaveActivityUser(SaveActivityUserRequest request)
        {
            ActivityUserResumen actividadUsuarioResultante = null;
            try
            {
                actividadUsuarioResultante = _activities.SaveActivityUser(request.ActividadUsuario);
            }
            catch (Exception ex)
            {
                return SaveActivityUserResponse.RespuestaFallo(
                  Result<SaveActivityUserRequest>.CreateFailed(string.Format("Error guardando la actividad: {0}", ex.ToString()))
                );
            }

            if (actividadUsuarioResultante == null)
                return SaveActivityUserResponse.RespuestaFallo(
                   Result<SaveActivityUserRequest>.CreateFailed(string.Format("No se ha podido recuperar la actividad final.", request.ToString()))
                 );

            return SaveActivityUserResponse.RespuestaExito(actividadUsuarioResultante);
        }

        public DeleteActivityUserResponse DeleteActivityUser(DeleteActivityUserRequest request)
        {
            try
            {
                _activities.DeleteActivityUser(request.Id);
            }
            catch (Exception ex)
            {
                return DeleteActivityUserResponse.RespuestaFallo(
                   Result<DeleteActivityUserRequest>.CreateFailed(string.Format("Error borrando la actividad: {0}", ex.ToString()))
                );
            }

            return DeleteActivityUserResponse.RespuestaExito();
        }

        #endregion
    }
}