﻿using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System.Collections.Generic;
using System.ServiceModel;
using System.Linq;
using System;

namespace AlterParadox.Backend.DAL
{
    [ServiceContract]
    public interface IProductServiceRepository
    {
        [OperationContract]
        DeleteProductResponse DeleteProduct(DeleteProductRequest request);
        [OperationContract]
        GetProductResponse GetProduct(GetProductRequest request);
        [OperationContract]
        GetProductsResponse GetProducts();
        [OperationContract]
        SaveProductResponse SaveProduct(SaveProductRequest request);

    }

    public class ProductServiceRepository : IProductServiceRepository
    {
        private readonly IProducts _products;

        public ProductServiceRepository(
            IProducts products)
        {
            _products = products;
        }
        //by ID
        public GetProductResponse GetProduct(GetProductRequest request)
        {
            var producto = _products.GetProduct(request.IdProduct);

            if (producto == null)
                return GetProductResponse.RespuestaFallo(
                   Result<GetProductRequest>.CreateFailed(string.Format("No se han encontrado el producto vinculado al ID {0}.",request.IdProduct))
                );

            return GetProductResponse.RespuestaExito(producto);
        }

        //todos
        public GetProductsResponse GetProducts()
        {
            var listaProductos = _products.GetProducts();

            if (!listaProductos.Any())
                return GetProductsResponse.RespuestaFallo(
                   Result<GetProductsRequest>.CreateFailed(string.Format("No se han encontrado productos.").ToString())
                );

            return GetProductsResponse.RespuestaExito(listaProductos);
        }

        //guardar
        public SaveProductResponse SaveProduct(SaveProductRequest request)
        {
            ProductResumen productoResultante = null;
            try
            {
                productoResultante = _products.SaveProduct(request.Product);
            }
            catch (Exception ex)
            {
                return SaveProductResponse.RespuestaFallo(
                  Result<SaveProductRequest>.CreateFailed(string.Format("Error borrando el producto: {0}", ex.ToString()))
                );
            }

            if(productoResultante == null)
                return SaveProductResponse.RespuestaFallo(
                   Result<SaveProductRequest>.CreateFailed(string.Format("No se ha podido recuperar el producto final.", request.ToString()))
                 );

            return SaveProductResponse.RespuestaExito(productoResultante);
        }

        //Borrar
        public DeleteProductResponse DeleteProduct(DeleteProductRequest request)
        {
            try
            {
                _products.DeleteProduct(request.productoID);
            }
            catch (Exception ex)
            {
                return DeleteProductResponse.RespuestaFallo(
                   Result<DeleteProductRequest>.CreateFailed(string.Format("Error borrando el producto: {0}", ex.ToString()))
                );
            }

            return DeleteProductResponse.RespuestaExito();
        }
    }
}