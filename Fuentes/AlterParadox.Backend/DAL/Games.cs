﻿using AlterParadox.Backend.BBDD;
using AlterParadox.Backend.BBDD.StoredProcedures;
using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;

namespace AlterParadox.Backend.DAL
{
    public interface IGames
    {
        Association GetAssociation(int id);
        List<Association> GetAssociations();
        AssociationResumen SaveAssociation(Association usuario);
        List<LocalGame> GetLocalGames();
    }

    public class Games : AbstractDatabaseRepository, IGames
    {
        private readonly IConnectionManager _connectionManager;

        public Games(IConnectionManager connectionManager,
           IDbCommandFactory commandFactory) : base(connectionManager, commandFactory)
        {
            if (connectionManager == null)
                throw new ArgumentNullException("IConnectionManager no puede ser nulo en Games");

            _connectionManager = connectionManager;
        }

        #region Association

        #endregion

        public Association GetAssociation(int id)
        {
            var resultado = CommandFactory
               .Create<GetAssociation>()
               .SetParameter(p => p.Id = id)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetAssociation_Output>();

            if (!resultado.Any())
                return null;

            var asociacion = GetAssociation_Mapper.RegistroToDTO(resultado.FirstOrDefault());

            return asociacion;

        }

        public List<Association> GetAssociations()
        {
            var asociaciones = new List<Association>();

            CommandFactory
               .Create<GetAssociations>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetAssociations_Output>()
               .ForEach(x =>
               {
                   asociaciones.Add(GetAssociations_Mapper.RegistroToDTO(x));
               });

            return asociaciones;
        }

        public AssociationResumen SaveAssociation(Association asociacion)
        {
            AssociationResumen asociacionFinal = null;

            try
            {
                CommandFactory
                   .Create<SaveAssociation>()
                   .SetParameter(p => p.Id = asociacion.Id)
                   .SetParameter(p => p.Name = asociacion.Name)
                   .SetParameter(p => p.Contact = asociacion.Contact)
                   .SetParameter(p => p.Email = asociacion.Email)
                   .SetParameter(p => p.Phone = asociacion.Phone)
                   .SetParameter(p => p.Comments = asociacion.Comments)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute()
                   .ToList<SaveAssociation_Output>()
                   .ForEach(x =>
                   {
                       asociacionFinal = (SaveAssociation_Mapper.RegistroToDTO(x));
                   });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            //Incorporar lista para recuperar el asociación creado
            return asociacionFinal;
        }
      
        public List<LocalGame> GetLocalGames()
        {
            var games = new List<LocalGame>();

            CommandFactory
               .Create<GetLocalGames>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetLocalGames_Output>()
               .ForEach(x =>
               {
                   games.Add(GetLocalGames_Mapper.RegistroToDTO(x));
               });

            return games;
        }
    }
}