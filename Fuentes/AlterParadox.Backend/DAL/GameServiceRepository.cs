﻿using AlterParadox.Backend.DTO;
using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System.Collections.Generic;
using System.ServiceModel;
using System.Linq;
using System;

namespace AlterParadox.Backend.DAL
{
    [ServiceContract]
    public interface IGameServiceRepository
    {
        [OperationContract]
        GetAssociationResponse GetAssociation(GetAssociationRequest request);

        [OperationContract]
        GetAssociationsResponse GetAssociations();

        [OperationContract]
        SaveAssociationResponse SaveAssociation(SaveAssociationRequest request);

        [OperationContract]
        GetLocalGamesResponse GetLocalGames();
    }

    public class GameServiceRepository : IGameServiceRepository
    {
        private readonly IGames _games;

        public GameServiceRepository(
            IGames games)
        {
            _games = games;
        }

        public GetAssociationResponse GetAssociation(GetAssociationRequest request)
        {
            var asociacion = _games.GetAssociation(request.IdAsociacion);

            if (asociacion == null)
                return GetAssociationResponse.RespuestaFallo(
                   Result<GetAssociationRequest>.CreateFailed(string.Format("No se han encontrado la asociación vinculada al ID {0}.", request.IdAsociacion))
                );

            return GetAssociationResponse.RespuestaExito(asociacion);
        }

        public GetAssociationsResponse GetAssociations()
        {
            var listaAsociaciones = _games.GetAssociations();

            if (!listaAsociaciones.Any())
                return GetAssociationsResponse.RespuestaFallo(
                   Result<GetAssociationsRequest>.CreateFailed(string.Format("No se han encontrado asociaciones.").ToString())
                );

            return GetAssociationsResponse.RespuestaExito(listaAsociaciones);
        }

        public SaveAssociationResponse SaveAssociation(SaveAssociationRequest request)
        {
            AssociationResumen asociacionResultante = null;
            try
            {
                asociacionResultante = _games.SaveAssociation(request.Asociacion);
            }
            catch (Exception ex)
            {
                return SaveAssociationResponse.RespuestaFallo(
                  Result<SaveAssociationRequest>.CreateFailed(string.Format("Error guardando el asociación: {0}", ex.ToString()))
                );
            }

            if (asociacionResultante == null)
                return SaveAssociationResponse.RespuestaFallo(
                   Result<SaveAssociationRequest>.CreateFailed(string.Format("No se ha podido recuperar el asociación final.", request.ToString()))
                 );

            return SaveAssociationResponse.RespuestaExito(asociacionResultante);
        }

        public GetLocalGamesResponse GetLocalGames()
        {
            var listaGames = _games.GetLocalGames();

            if (!listaGames.Any())
                return GetLocalGamesResponse.RespuestaFallo(
                   Result<GetLocalGamesRequest>.CreateFailed(string.Format("No se han encontrado usuarios.").ToString())
                );

            return GetLocalGamesResponse.RespuestaExito(listaGames);
        }

    }
}