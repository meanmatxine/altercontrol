﻿using AlterParadox.Backend.BBDD;
using AlterParadox.Backend.BBDD.StoredProcedures;
using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;

namespace AlterParadox.Backend.DAL
{
    public interface IActivities
    {
        Activity GetActivity(int id);
        List<Activity> GetActivities();
        ActivityResumen SaveActivity(Activity actividad);
        void DeleteActivity(int id);
        int CheckParticipants(int activity);
        
        ActivityUser GetActivityUser(int idActividad, int idUsuario);
        List<ActivityUser> GetActivityUsers(int idActividad);
        ActivityUserResumen SaveActivityUser(ActivityUser actividadUsuario);
        void DeleteActivityUser(int id);

    }

    public class Activities : AbstractDatabaseRepository, IActivities
    {
        private readonly IConnectionManager _connectionManager;

        public Activities(IConnectionManager connectionManager,
           IDbCommandFactory commandFactory) : base(connectionManager, commandFactory)
        {
            if (connectionManager == null)
                throw new ArgumentNullException("IConnectionManager no puede ser nulo en DBManager");

            _connectionManager = connectionManager;
        }

        public Activity GetActivity(int id)
        {
            var resultado = CommandFactory
               .Create<GetActivity>()
               .SetParameter(p => p.Id = id)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetActivity_Output>();

            if (!resultado.Any())
                return null;

            var actividad = GetActivity_Mapper.RegistroToDTO(resultado.FirstOrDefault());

            return actividad;

        }

        public List<Activity> GetActivities()
        {
            var actividades = new List<Activity>();

            CommandFactory
               .Create<GetActivities>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetActivities_Output>()
               .ForEach(x =>
               {
                   actividades.Add(GetActivities_Mapper.RegistroToDTO(x));
               });

            return actividades;
        }

        public ActivityResumen SaveActivity(Activity actividad)
        {
            ActivityResumen activity = null;

            try
            {
                CommandFactory
                   .Create<SaveActivity>()
                   .SetParameter(p => p.Id = actividad.Id)
                   .SetParameter(p => p.Organizer = actividad.Organizer)
                   .SetParameter(p => p.Email = actividad.Email)
                   .SetParameter(p => p.Phone = actividad.Phone)
                   .SetParameter(p => p.Association = actividad.Association)
                   .SetParameter(p => p.Name = actividad.Name)
                   .SetParameter(p => p.ActivityDate = actividad.ActivityDate)
                   .SetParameter(p => p.Place = actividad.Place)
                   .SetParameter(p => p.Type = actividad.Type)
                   .SetParameter(p => p.Participants = actividad.Participants)
                   .SetParameter(p => p.MaxParticipants = actividad.MaxParticipants)
                   .SetParameter(p => p.MinParticipants = actividad.MinParticipants)
                   .SetParameter(p => p.Duration = actividad.Duration)
                   .SetParameter(p => p.Enabled = actividad.Enabled)
                   .SetParameter(p => p.Summary = actividad.Summary)
                   .SetParameter(p => p.Comments = actividad.Comments)
                   .SetParameter(p => p.Needs = actividad.Needs)
                   .SetParameter(p => p.Winner = actividad.Winner)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute()
                   .ToList<SaveActivity_Output>()
                   .ForEach(x =>
                   {
                       activity = (SaveActivity_Mapper.RegistroToDTO(x));
                   });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            //Incorporar lista para recuperar el usuario creado
            return activity;
        }

        public void DeleteActivity(int id)
        {
            CommandFactory
                   .Create<DeleteActivity>()
                   .SetParameter(p => p.Id = id)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute();
        }

        public int CheckParticipants(int activity)
        {
            int i = 0;
            CommandFactory
                   .Create<GetNumParticipantsActivity>()
                   .SetParameter(p => p.Id = activity)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute()
                   .ToList<GetNumParticipantsActivity_Output>()
                   .ForEach(x =>
                   {
                       i = (GetNumParticipantsActivity_Mapper.RegistroToDTO(x));
                   });

            return i;
        }

        public ActivityUser GetActivityUser(int idActividad, int idUsuario) {

            var resultado = CommandFactory
               .Create<GetActivityUser>()
               .SetParameter(p => p.IdActivity = idActividad)
               .SetParameter(p => p.IdUser = idUsuario)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetActivityUser_Output>();

            if (!resultado.Any())
                return null;

            var actividadUsuario = GetActivityUser_Mapper.RegistroToDTO(resultado.FirstOrDefault());

            return actividadUsuario;

        }
        
        public List<ActivityUser> GetActivityUsers(int idActividad) {

            var actividadusuarios = new List<ActivityUser>();

            CommandFactory
               .Create<GetActivityUsers>()
               .SetParameter(p => p.IdActivity = idActividad)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetActivityUsers_Output>()
               .ForEach(x =>
               {
                   actividadusuarios.Add(GetActivityUsers_Mapper.RegistroToDTO(x));
               });

            return actividadusuarios;

        }
        public ActivityUserResumen SaveActivityUser(ActivityUser actividadUsuario) {

            ActivityUserResumen activityUserResume = null;

            try
            {
                CommandFactory
                   .Create<SaveActivityUser>()
                   .SetParameter(p => p.Id = actividadUsuario.Id)
                   .SetParameter(p => p.IdActivity = actividadUsuario.Activity_Id)
                   .SetParameter(p => p.IdUser = actividadUsuario.User_Id)
                   .SetParameter(p => p.InscriptionDate = actividadUsuario.InscriptionDate)
                   .SetParameter(p => p.Reserve = actividadUsuario.Reserve)
                   .SetParameter(p => p.Comments = actividadUsuario.Comments)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute()
                   .ToList<SaveActivityUser_Output>()
                   .ForEach(x =>
                   {
                       activityUserResume = (SaveActivityUser_Mapper.RegistroToDTO(x));
                   });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            //Incorporar lista para recuperar el usuario creado
            return activityUserResume;

        }
        public void DeleteActivityUser(int id) {

            CommandFactory
                  .Create<DeleteActivityUser>()
                  .SetParameter(p => p.Id = id)
                  .SetConnection(ConnectionManager.GetConnectionDefault())
                  .Execute();

        }
    }
}