﻿using AlterParadox.Backend.BBDD;
using AlterParadox.Backend.BBDD.StoredProcedures;
using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using AlterParadox.Core.Models;

namespace AlterParadox.Backend.DAL
{
    public interface IUsers
    {
        User GetUser(int id);
        List<PersonaAlCargo> GetUsuariosAlCargo(int id);
        User GetUserByNumber(int number);
        List<User> GetUsers();
        List<User> GetWebUsers();
        UserResumen SaveUser(User usuario);
        void SaveWebUser(User usuario);
        void DeleteUser(int id);

        //TO DELETE
        List<LocalGame> GetLocalGames();

    }

    public class Users : AbstractDatabaseRepository, IUsers
    {
        private readonly IConnectionManager _connectionManager;

        public Users(IConnectionManager connectionManager,
           IDbCommandFactory commandFactory) : base(connectionManager, commandFactory)
        {
            if (connectionManager == null)
                throw new ArgumentNullException("IConnectionManager no puede ser nulo en DBManager");

            _connectionManager = connectionManager;
        }

        public User GetUser(int id)
        {
            var resultado = CommandFactory
               .Create<GetUser>()
               .SetParameter(p => p.Id = id)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetUser_Output>();

            if (!resultado.Any())
                return null;

            var usuario = GetUser_Mapper.RegistroToDTO(resultado.FirstOrDefault());

            return usuario;

        }

        public List<PersonaAlCargo> GetUsuariosAlCargo(int id)
        {
            var users = new List<PersonaAlCargo>();

            CommandFactory
               .Create<GetUsersOnCharge>()
               .SetParameter(p => p.Id = id)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetUsersOnCharge_Output>()
               .ForEach(x =>
               {
                   users.Add(GetUsersOnCharge_Mapper.RegistroToDTO(x));
               });

            return users;
        }

        public User GetUserByNumber(int number)
        {
            var resultado = CommandFactory
               .Create<GetUserByNumber>()
               .SetParameter(p => p.Number = number)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetUserByNumber_Output>();

            if (!resultado.Any())
                return null;

            var usuario = GetUserByNumber_Mapper.RegistroToDTO(resultado.FirstOrDefault());

            return usuario;

        }

        public List<User> GetUsers()
        {
            var users = new List<User>();

            CommandFactory
               .Create<GetUsers>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetUsers_Output>()
               .ForEach(x =>
               {
                   users.Add(GetUsers_Mapper.RegistroToDTO(x));
               });

            return users;
        }

        public List<User> GetWebUsers()
        {
            var users = new List<User>();

            CommandFactory
               .Create<GetWebUsers>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetWebUsers_Output>()
               .ForEach(x =>
               {
                   users.Add(GetWebUsers_Mapper.RegistroToDTO(x));
               });

            return users;

        }
        
        public UserResumen SaveUser(User usuario)
        {
            UserResumen user = null;

            try
            {
                CommandFactory
                   .Create<SaveUser>()
                   .SetParameter(p => p.Id = usuario.Id)
                   .SetParameter(p => p.Dni = usuario.Dni)
                   .SetParameter(p => p.Number = usuario.Number)
                   .SetParameter(p => p.Name = usuario.Name)
                   .SetParameter(p => p.LastName = usuario.LastName)
                   .SetParameter(p => p.Association = usuario.Association)
                   .SetParameter(p => p.Email = usuario.Email)
                   .SetParameter(p => p.Phone = usuario.Phone)
                   .SetParameter(p => p.CP = usuario.CP)
                   .SetParameter(p => p.Birthday = usuario.Birthday)
                   .SetParameter(p => p.WantShirt = usuario.Camiseta)
                   .SetParameter(p => p.Shirt = usuario.Shirt)
                   .SetParameter(p => p.BringActivity = usuario.TraeActividades)

                   .SetParameter(p => p.Lunch = usuario.Lunch)
                   .SetParameter(p => p.SecondLunch = usuario.SegundoLunch)

                   .SetParameter(p => p.Sleep = usuario.Sleep)
                   .SetParameter(p => p.SleepList = usuario.DormirListaEspera)
                   .SetParameter(p => p.TutorPermission = usuario.TutorPermission)
                   .SetParameter(p => p.UserOnCharge = usuario.UserOnCharge)
                   .SetParameter(p => p.Comments = usuario.Comments)
                   .SetParameter(p => p.Aviso = usuario.Aviso)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute()
                   .ToList<SaveUser_Output>()
                   .ForEach(x =>
                   {
                       user = (SaveUser_Mapper.RegistroToDTO(x));
                   });
                }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            

            //Incorporar lista para recuperar el usuario creado
            return user;
        }

        public void SaveWebUser(User usuario)
        {
  
                CommandFactory
                   .Create<SaveWebUser>()
                   .SetParameter(p => p.Dni = usuario.Dni)
                   .SetParameter(p => p.Name = usuario.Name)
                   .SetParameter(p => p.LastName = usuario.LastName)
                   .SetParameter(p => p.Association = usuario.Association)
                   .SetParameter(p => p.Email = usuario.Email)
                   .SetParameter(p => p.Phone = usuario.Phone)
                   .SetParameter(p => p.CP = usuario.CP)
                   .SetParameter(p => p.Birthday = usuario.Birthday)
                   .SetParameter(p => p.WantShirt = usuario.Camiseta)
                   .SetParameter(p => p.Shirt = usuario.Shirt)
                   .SetParameter(p => p.BringActivity = usuario.TraeActividades)

                   .SetParameter(p => p.Lunch = usuario.Lunch)
                   .SetParameter(p => p.SecondLunch = usuario.SegundoLunch)

                   .SetParameter(p => p.Sleep = usuario.Sleep)
                   .SetParameter(p => p.SleepList = usuario.DormirListaEspera)
                   .SetParameter(p => p.TutorPermission = usuario.TutorPermission)
                   .SetParameter(p => p.Comments = usuario.Comments)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute();

            
        }

        public void DeleteUser(int id)
        {
            CommandFactory
                   .Create<DeleteUser>()
                   .SetParameter(p => p.Id = id)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute();
        }


        public List<LocalGame> GetLocalGames()
        {
            var games = new List<LocalGame>();

            CommandFactory
               .Create<GetLocalGames>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetLocalGames_Output>()
               .ForEach(x =>
               {
                   games.Add(GetLocalGames_Mapper.RegistroToDTO(x));
               });

            return games;
        }

    }
}