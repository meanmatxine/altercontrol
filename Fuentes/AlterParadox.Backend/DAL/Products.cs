﻿using AlterParadox.Backend.BBDD;
using AlterParadox.Backend.BBDD.StoredProcedures;
using AlterParadox.Backend.Infrastructure.Data;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;

namespace AlterParadox.Backend.DAL
{
    public interface IProducts
    {
        Product GetProduct(int id);
        List<Product> GetProducts();
        ProductResumen SaveProduct(Product product);
        void DeleteProduct(int productoID);


    }

    public class Products : AbstractDatabaseRepository, IProducts
    {
        private readonly IConnectionManager _connectionManager;

        public Products(IConnectionManager connectionManager,
           IDbCommandFactory commandFactory) : base(connectionManager, commandFactory)
        {
            if (connectionManager == null)
                throw new ArgumentNullException("IConnectionManager no puede ser nulo en DBManager");

            _connectionManager = connectionManager;
        }

        //BY ID
        public Product GetProduct(int id)
        {
            var resultado = CommandFactory
               .Create<GetProduct>()
               .SetParameter(p => p.Id = id)
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetProduct_Output>();

            if (!resultado.Any())
                return null;

            var producto = GetProduct_Mapper.RegistroToDTO(resultado.FirstOrDefault());

            return producto;

        }

        //all
        public List<Product> GetProducts()
        {
            var products = new List<Product>();

            CommandFactory
               .Create<GetProducts>()
               .SetConnection(ConnectionManager.GetConnectionDefault())
               .Execute()
               .ToList<GetProducts_Output>()
               .ForEach(x =>
               {
                   products.Add(GetProducts_Mapper.RegistroToDTO(x));
               });

            return products;
        }


        //save Producto
        public ProductResumen SaveProduct(Product producto)
        {
            ProductResumen product = null;

            try
            {
                CommandFactory
                   .Create<SaveProduct>()
                   .SetParameter(p => p.productoID = producto.productoID)
                   .SetParameter(p => p.productoName = producto.productoName)
                   .SetParameter(p => p.productoCB = producto.productoCB)
                   .SetParameter(p => p.productoTipo = producto.productoTipo)
                   .SetParameter(p => p.productoPrecioSinIva = producto.productoPrecioSinIva)
                   .SetParameter(p => p.productoIVA = producto.productoIVA)
                   .SetParameter(p => p.productoPrecio = producto.productoPrecio)
                   .SetParameter(p => p.productoCantidad = producto.productoCantidad)
                   .SetParameter(p => p.productoImage = producto.productoImage)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute()
                   .ToList<SaveProduct_Output>()
                   .ForEach(x =>
                   {
                       product = (SaveProduct_Mapper.RegistroToDTO(x));
                   });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


            //Incorporar lista para recuperar el usuario creado
            return product;
        }

        //Delete Product
        public void DeleteProduct(int id)
        {
            CommandFactory
                   .Create<DeleteProduct>()
                   .SetParameter(p => p.productoID = id)
                   .SetConnection(ConnectionManager.GetConnectionDefault())
                   .Execute();
        }

    }
}