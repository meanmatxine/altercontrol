﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using AlterParadox.Backend.Infrastructure;
using System.ServiceModel;
using AlterParadox.Backend.DAL;
using Castle.Facilities.WcfIntegration;
using System;
using System.ServiceModel.Activation;
using AlterParadox.Backend.Services;
using AlterParadox.Backend.Infrastructure.Services;
using AlterParadox.Backend.Infrastructure.Data;

namespace AlterParadox.Backend
{
    public class BackendHostFactory : DIServiceHostFactory
    {
        public BackendHostFactory()
        {
            //Conexión BBDD
            Container.Register(Component.For<IConnectionManager>().ImplementedBy<ConnectionManagerFromConfigurationFile>().OnlyNewServices());
            Container.Register(Component.For<IDbCommandFactory>().ImplementedBy<StoredProcedureRepository>().OnlyNewServices());

            //// manejo de excepciones
            //Container.Register(Component.For<IErrorHandler>().ImplementedBy<GenericExceptionHandler>());
            //Container.Register(Component.For<IExceptionHandler>().ImplementedBy<CatalogosDeCodificacionExceptionHandler>());


            //// servicios WCF
            //Container.Register(Component.For<ICatalogosDeCodificacionService>().ImplementedBy<CatalogosDeCodificacionService>());


            //Añadimos los Servicios Propios que va a exponer el IC
            Container.Register(Component.For<ISQLServer>().ImplementedBy<SQLServer>());
            Container.Register(Component.For<ISQLService>().ImplementedBy<SQLService>());

            Container.Register(Component.For<IUserService>().ImplementedBy<UserService>());
            Container.Register(Component.For<IUsers>().ImplementedBy<Users>());
            Container.Register(Component.For<IUserServiceRepository>().ImplementedBy<UserServiceRepository>());

            Container.Register(Component.For<IActivityService>().ImplementedBy<ActivityService>());
            Container.Register(Component.For<IActivities>().ImplementedBy<Activities>());
            Container.Register(Component.For<IActivityServiceRepository>().ImplementedBy<ActivityServiceRepository>());

            Container.Register(Component.For<IGameService>().ImplementedBy<GameService>());
            Container.Register(Component.For<IGames>().ImplementedBy<Games>());
            Container.Register(Component.For<IGameServiceRepository>().ImplementedBy<GameServiceRepository>());

            Container.Register(Component.For<IProductService>().ImplementedBy<ProductService>());
            Container.Register(Component.For<IProducts>().ImplementedBy<Products>());
            Container.Register(Component.For<IProductServiceRepository>().ImplementedBy<ProductServiceRepository>());

        }

    }
}