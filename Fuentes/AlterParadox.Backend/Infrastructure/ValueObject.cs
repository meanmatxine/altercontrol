﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AlterParadox.Backend.Infrastructure
{
    /// <summary>
    /// Objeto-Valor
    /// Concepto del dominio (negocio) que representa un aspecto que no tiene, conceptualmente, identidad. 
    /// Es un estupendo lugar en el que modelar funcionalidades y reglas de negocio alrededor de ese concepto y esos datos.
    /// 
    /// Los creamos para representar elementos del diseño de los que nos importa qué es lo que son, no quién son.
    /// Por tanto, un objeto-valor se diferencia de otro porque cambia alguna característica del mismo, no su identidad.
    /// Son inmutables: cambiar una propiedad de los mismos conlleva crear una nueva instancia del objeto.
    /// Por ejemplo: Color, Moneda, DateTime, String ...
    /// https://dddsamplenet.codeplex.com/SourceControl/latest#DDDSample-ModelLayers/DomainModel.Infrastructure/ValueObject.cs --> IdentityProperties
    /// http://enterprisecraftsmanship.com/2015/01/03/value-objects-explained/ -> operadores == y !=
    /// </summary>
    [Serializable]
    public abstract class ValueObject
    {
        public ValueObject()
        {
            if (GetIdentityProperties() == null || GetIdentityProperties().Count() == 0)
                throw new ArgumentNullException("Un ValueObject debe tener alguna propiedad que describa su identidad.");
        }

        /// <summary>
        /// Propiedades que se comparan a la hora de determinar si un objeto es igual a otro.
        /// CUIDADO: si este método devuelve una lista vacía o nula se lanzará una DDDConceptException al construir el objeto.
        /// </summary>
        /// <returns>Colección de propiedades que definen la identidad del objeto</returns>
        protected abstract IEnumerable<object> GetIdentityProperties();

        /// <summary>
        /// Compares two Value Objects according to atomic values returned by <see cref="GetIdentityProperties"/>.
        /// </summary>
        /// <param name="obj">Object to compare to.</param>
        /// <returns>True if objects are considered equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
                return false;

            ValueObject other = (ValueObject)obj;
            var thisIdentityProperties = RemoveInnerEnumerations(GetIdentityProperties());
            var otherIdentityProperties = RemoveInnerEnumerations(other.GetIdentityProperties());
            if (thisIdentityProperties.Count() != otherIdentityProperties.Count())
                return false;

            var thisValuesEnum = thisIdentityProperties.GetEnumerator();
            var otherValuesEnum = otherIdentityProperties.GetEnumerator();

            while (thisValuesEnum.MoveNext() && otherValuesEnum.MoveNext())
            {
                if (ReferenceEquals(thisValuesEnum.Current, null) ^ ReferenceEquals(otherValuesEnum.Current, null))
                    return false;

                if (thisValuesEnum.Current != null && !thisValuesEnum.Current.Equals(otherValuesEnum.Current))
                    return false;
            }
            return !thisValuesEnum.MoveNext() && !otherValuesEnum.MoveNext();
        }

        private IEnumerable<object> RemoveInnerEnumerations(IEnumerable enumeration)
        {
            if (enumeration == null)
                return null;

            var result = new List<object>();

            foreach (object x in enumeration)
            {
                var innerEnum = x as IEnumerable;
                if (innerEnum != null)
                    result.AddRange(RemoveInnerEnumerations(innerEnum)); // the current element is an enumeration
                else
                    result.Add(x);
            }

            return result;
        }

        /// <summary>
        /// Returns hashcode value calculated according to a collection of atomic values
        /// returned by <see cref="GetIdentityProperties"/>.
        /// </summary>
        /// <returns>Hashcode value.</returns>
        public override int GetHashCode()
        {
            return GetIdentityProperties()
               .Select(x => x != null ? x.GetHashCode() : 0)
               .Aggregate((x, y) => x ^ y);
        }


        public static bool operator ==(ValueObject a, ValueObject b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(ValueObject a, ValueObject b)
        {
            return !(a == b);
        }
    }
}