﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlterParadox.Backend.Infrastructure.Exceptions
{
    public class PropertyNotDefinedAsParameterException : Exception
    {
        public PropertyNotDefinedAsParameterException(string propertyName)
            : base("The member [" + propertyName + "]is not defined as stored procedure parameter")
        {

        }
    }

    public class TypeNotDefinedAsStoredProcedure : Exception
    {
        public TypeNotDefinedAsStoredProcedure(Type type) :
            base("The type [" + type + "] is not defined as StoredProcedure")
        {

        }
    }

    public class MalformedOutputInterfaceException : Exception
    {
        public MalformedOutputInterfaceException(string outputInterface, Exception innerException)
            : base(outputInterface, innerException)
        {

        }
    }

    public class DatabaseMappingException : Exception
    {
        public DatabaseMappingException(string fieldName, Exception innerException) : base(string.Format("Missing field {0}", fieldName), innerException)
        {

        }
    }

    public class CommandRetrieveResultsException : Exception
    {
        public CommandRetrieveResultsException(string commandName, Exception innerException) : base(commandName, innerException)
        {

        }
    }

    public class ConnectionRequieredException : Exception
    {
    }

    public class RaiseErrorDatabaseException : Exception
    {
        public RaiseErrorDatabaseException(string Message, Exception innerException) : base(Message, innerException)
        {

        }

    }
    public class ConnectionNotFoundException : Exception
    {
        public ConnectionNotFoundException(string connectionName) : base("Connection named as " + connectionName + " not found")
        {

        }
    }
    public class RegistrationException : Exception
    {
        public RegistrationException(string reason) : base(reason)
        {

        }
    }

}