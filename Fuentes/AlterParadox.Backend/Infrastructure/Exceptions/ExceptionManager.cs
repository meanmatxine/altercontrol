﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlterParadox.Backend.Infrastructure.Exceptions
{
    public static class ExceptionManager
    {
        /// <summary>Lanza una excepción generica System.Exception</summary>
        /// <param name="message">Mensaje</param>
        public static void Throw(string message)
        {
            throw new System.Exception(message);
        }

        public static void Throw<TEx>() where TEx : System.Exception
        {
            throw System.Activator.CreateInstance(typeof(TEx)) as System.Exception;
        }

        /// <summary>Lanza un tipo concreto de excepción</summary>
        /// <typeparam name="TEx">Tipo de excepción a lanzar</typeparam>
        /// <param name="message">Mensaje</param>
        /// <remarks>Los tipos a emitir tienen que tener un constructor que admita una cadena conteniendo el mensaje a incluir en la excepción</remarks>
        public static void Throw<TEx>(string message) where TEx : System.Exception
        {
            throw (TEx)System.Activator.CreateInstance(typeof(TEx));
        }

        public static void ThrowIf<TEx>(string message, Func<bool> executionCondition) where TEx : System.Exception
        {
            if (executionCondition())
                Throw<TEx>(message);
        }

        /// <summary>
        /// Lanza una excepción del tipo dado en cas de que obj sea nulo
        /// </summary>
        /// <typeparam name="TEx">Tipo de excepción a lanzar</typeparam>
        /// <param name="message">Texto descriptivo</param>
        /// <param name="obj">Objeto a comprobar con null</param>
        public static void ThrowIfNull<TEx>(string message, object obj) where TEx : Exception
        {
            ThrowIf<TEx>(message, () => null == obj);
        }

        public static TEx GetInnerException<TEx>(Exception ex) where TEx : Exception
        {
            Exception e = ex;
            while (e != null)
            {
                if (e.GetType() == typeof(TEx))
                {
                    return (TEx)e;
                }
                else
                {
                    e = e.InnerException;
                }
            }
            return null;
        }

        public static T Guard<T>(string message, T obj)
        {
            ThrowIf<ArgumentNullException>(message, () => null == obj);
            return obj;
        }
    }
}