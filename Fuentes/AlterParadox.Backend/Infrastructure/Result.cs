﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AlterParadox.Backend.Infrastructure
{
    public class Result
    {
        private readonly bool _succeeded;
        private readonly string _errorMessage;
        private List<string> _warningMessages;

        public Result()
        {
            _succeeded = true;
            _warningMessages = new List<string>();
        }
        public Result(string errorMessage)
        {
            _errorMessage = errorMessage;
            _succeeded = false;
        }

        public bool Succeeded { get { return _succeeded; } }
        public string ErrorMessage { get { return _errorMessage; } }
        public bool HasWarnings { get { return (_warningMessages != null && _warningMessages.Count > 0); } }
        public string[] WarningMessages { get { return _warningMessages.ToArray(); } }
        public string AllWarningMessages
        {
            get
            {
                var buff = new StringBuilder();
                _warningMessages.ForEach(m => buff.AppendLine(m));
                return buff.ToString();
            }
        }

        public Result AddWarningMessage(string message)
        {
            _warningMessages.Add(message);
            return this;
        }
        public Result AddWarningMessages(string[] messages)
        {
            foreach (var msg in messages)
                _warningMessages.Add(msg);
            return this;
        }

        public static Result CreateSucceeded()
        {
            return new Result();
        }
        public static Result CreateFailed(string errorMessage)
        {
            return new Result(errorMessage);
        }
        public static Result CreateFailed(Exception ex)
        {
            // obtengo mensaje de error completo
            var buffer = string.Empty;
            var iex = ex;
            while (iex != null)
            {
                buffer = string.Format("{0}{1}{2}",
                                       buffer, string.IsNullOrEmpty(buffer) ? string.Empty : Environment.NewLine,
                                       iex.Message);
                iex = iex.InnerException;
            }
            return new Result(buffer);
        }
    }
    public class Result<T> : ValueObject
    {
        private readonly bool _succeeded;
        private readonly string _errorMessage;
        private readonly T _value;
        private readonly BaseError _errorInfo;

        internal Result(T value)
        {
            _value = value;
            _succeeded = true;
        }
        internal Result(string errorMessage)
        {
            _errorMessage = errorMessage;
            _succeeded = false;
        }
        internal Result(BaseError errorInfo)
        {
            _errorMessage = errorInfo.Description;
            _errorInfo = errorInfo;
            _succeeded = false;
        }

        public bool Succeeded { get { return _succeeded; } }
        public BaseError Error { get { return _errorInfo; } }
        public string ErrorMessage { get { return _errorMessage; } }
        public T Value { get { return _value; } }

        public static Result<T> CreateSucceeded(T value)
        {
            return new Result<T>(value);
        }
        public static Result<T> CreateFailed(string errorMessage)
        {
            return new Result<T>(errorMessage);
        }
        public static Result<T> CreateFailed(ResultErrorMsgs resultErrorMsgs)
        {
            return new Result<T>(resultErrorMsgs.ToString());
        }
        public static Result<T> CreateFailed(BaseError errorInfo)
        {
            return new Result<T>(errorInfo);
        }
        public static Result<T> CreateFailed(Exception ex)
        {
            // obtengo mensaje de error completo
            var buffer = string.Empty;
            var iex = ex;
            while (iex != null)
            {
                buffer = string.Format("{0}{1}{2}",
                                       buffer, string.IsNullOrEmpty(buffer) ? string.Empty : Environment.NewLine,
                                       iex.Message);
                iex = iex.InnerException;
            }
            return new Result<T>(buffer);
        }

        public ResultDTO<T> ToResultDTO()
        {
            return new ResultDTO<T>()
            {
                Succeeded = _succeeded,
                ErrorMessage = _errorMessage,
                Value = _value
            };
        }
        public static Result<T> FromResultDTO(ResultDTO<T> result)
        {
            if (result.Succeeded)
                return CreateSucceeded(result.Value);
            else
                return CreateFailed(result.ErrorMessage);
        }

        protected override IEnumerable<object> GetIdentityProperties()
        {
            yield return _succeeded;
            yield return _errorMessage;
            yield return _value;
        }
    }
    [DataContract]
    public class ResultDTO<T> : ValueObject
    {
        public ResultDTO() { }

        [DataMember]
        public bool Succeeded { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
        [DataMember]
        public T Value { get; set; }

        protected override IEnumerable<object> GetIdentityProperties()
        {
            yield return Succeeded;
            yield return ErrorMessage;
            yield return Value;
        }
    }
    public class ResultErrorMsgs
    {
        private List<string> _errorMsgs;
        public ResultErrorMsgs()
        {
            _errorMsgs = new List<string>();
        }

        public ResultErrorMsgs Append(string msg)
        {
            _errorMsgs.Add(msg);
            return this;
        }

        /// <summary>
        /// Devuelve todos los mensajes de error separados por un salto de línea
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (_errorMsgs.Count == 1)
                return _errorMsgs[0];

            string msg = string.Empty;
            _errorMsgs.ForEach(x => msg += '\n' + x);

            return msg;
        }

        public bool HasErrMessages()
        {
            return _errorMsgs.Count > 0;
        }
    }
    public abstract class BaseError : ValueObject
    {
        private readonly int _code;
        private readonly string _description;

        public BaseError(int code, string description)
        {
            _code = code;
            _description = description;
        }

        public int Code { get { return _code; } }
        public string Description { get { return _description; } }

        protected override IEnumerable<object> GetIdentityProperties()
        {
            yield return Code;
            yield return Description;
        }
    }
}