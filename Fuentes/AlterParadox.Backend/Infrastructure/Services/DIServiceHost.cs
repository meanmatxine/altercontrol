﻿using System;
using System.ServiceModel;
using Castle.MicroKernel;
using Castle.Windsor;
using System.ServiceModel.Description;

namespace AlterParadox.Backend.Infrastructure.Services
{
    public class DIServiceHost : ServiceHost
    {
        private readonly IWindsorContainer _container;
        private readonly IServiceBehavior _errorHandlerBehavior;

        public DIServiceHost(
            Type serviceType,
            IWindsorContainer container,
            IServiceBehavior errorHandlerBehavior,
            params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            _container = container;
            _errorHandlerBehavior = errorHandlerBehavior;
        }

        protected override void OnOpening()
        {
            Description.Behaviors.Add(new DIServiceBehavior(_container));
            //Description.Behaviors.Add(_errorHandlerBehavior);

            base.OnOpening();
        }
    }
}