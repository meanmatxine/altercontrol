﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Castle.MicroKernel;
using Castle.Windsor;

namespace AlterParadox.Backend.Infrastructure.Services
{
    public class DIInstanceProvider : IInstanceProvider
    {
        private readonly Type _serviceType;
        private readonly Type _contractType;
        private readonly IWindsorContainer _container;

        public DIInstanceProvider(Type serviceType, IWindsorContainer container)
        {
            _container = container;
            _serviceType = serviceType;
            _contractType = ContractDescription.GetContract(_serviceType).ContractType;
        }

        public object GetInstance(InstanceContext instanceContext)
        {
            return GetInstance(instanceContext, null);
        }

        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            return _container.Resolve(_contractType);
        }

        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {

        }
    }
}