﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace AlterParadox.Backend.Infrastructure.Services
{
    public class DIServiceHostFactory : ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            Container.Register(Component.For<IWindsorContainer>().Instance(Container));

            //Container.Register(Component.For<IMessageBus>().ImplementedBy<MessageBus>());
            return new DIServiceHost(serviceType, Container, GetErrorBehavior(), baseAddresses);
        }

        public readonly IWindsorContainer Container = new WindsorContainer();

        protected virtual IServiceBehavior GetErrorBehavior()
        {
            //var bus = Container.Resolve<IMessageBus>();
            var errorHandler = null as IErrorHandler;

            if (Container.Kernel.HasComponent(typeof(IErrorHandler)))
                errorHandler = Container.Resolve<IErrorHandler>();
            else
                return null;

            return null;
            //    errorHandler = new ExceptionErrorHandlerWCF(bus);

            //return new ErrorServiceBehavior(bus, errorHandler);
        }
    }
}