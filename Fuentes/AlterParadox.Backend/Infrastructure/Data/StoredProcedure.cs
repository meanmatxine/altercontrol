﻿using AlterParadox.Backend.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
/*
 * http://msdn.microsoft.com/es-es/library/dd0w4a2z%28v=vs.90%29.aspx
 * http://developer.teradata.com/blog/netfx/2010/12/dbproviderfactories-demystified
 * 
 */

namespace AlterParadox.Backend.Infrastructure.Data
{
    public interface IStoredProcedure<TInput>
    {
        TInput Parameters { get; }
        IStoredProcedure<TInput> SetParameter(Action<TInput> setter);
        IStoredProcedure<TInput> SetConnection(IDbConnection connection);
        IStoredProcedure<TInput> SetCommandTimeout(int timeout);
        IStoredProcedure<TInput> Execute();
        List<T> ToList<T>();
        DataSet ToDataSet();
    }

    public class StoredProcedure<TInput> :
       IStoredProcedure<TInput>,
       IDisposable
    {
        /// <summary>Evento antes de la ejecución del comando</summary>
        public event ObservableCommandEventHandler BeforeExecute;
        /// <summary>Evento post-ejecución del comando</summary>
        public event ObservableCommandEventHandler AfterExecute;
        /// <summary>Evento lanzado al establecer la propiedad Connection</summary>
        public event ObservableCommandEventHandler ConnectionChanged;

        public StoredProcedure()
            : this(DbProviderFactories.GetFactory(ModelInspector.GetProviderTypeName<TInput>()))
        {
            // --
        }

        public StoredProcedure(DbProviderFactory factory)
        {
            DbFactory = factory;
            ParameterAccesor = ParameterAccesorFactory.Create<TInput>();
            CreateCommand();
        }

        protected DbProviderFactory DbFactory
        {
            get
            {
                return _dbFactory;
            }
            set
            {
                if (null == value)
                    throw new ArgumentNullException("DbProvider factory must not be null");
                _dbFactory = value;
            }
        }

        public List<T> ToList<T>()
        {
            IDataReader reader = null;
            var items = new List<T>();
            if (dataset.Tables.Count == 0)
                return items;
            reader = dataset.CreateDataReader();
            try
            {
                while (reader.Read())
                    items.Add(ParameterAccesorFactory.CreateOutput<T>(reader));
            }
            catch (DatabaseMappingException ex)
            {
                throw new CommandRetrieveResultsException(Name, ex);
            }
            finally
            {
                reader.Close();
            }
            return items;
        }

        public DataSet ToDataSet()
        {
            return dataset;
        }

        public IStoredProcedure<TInput> SetConnection(IDbConnection connection)
        {
            Command.Connection = connection;
            return this;
        }

        public IStoredProcedure<TInput> SetCommandTimeout(int timeout)
        {
            Command.CommandTimeout = timeout;
            return this;
        }

        public void Dispose()
        {
            // Esto desconecta el reader en caso de estar abierto.
            //this.ToList<>().Dispose();
        }

        /// <summary>
        /// Permite establecer los parámetros del procedimiento de la siguiente forma:
        /// procedureInstance
        ///    .SetParameter(p => p.FirstProperty  = value1)
        ///    .SetParameter(p => p.SecondProperty = value2);
        /// </summary>
        /// <param name="setter"></param>
        /// <returns></returns>
        public IStoredProcedure<TInput> SetParameter(Action<TInput> setter)
        {
            setter(ParameterAccesor);
            return this;
        }

        /// <summary>Nombre del procedimiento almacenado.</summary>
        public string Name
        {
            get { return ModelInspector.GetCommandName(typeof(TInput)); }
        }

        private DataSet dataset = null;

        public IStoredProcedure<TInput> Execute()
        {
            // Carga de los parámetros para el procedimiento a partir de la metainformación 
            // que se ha definido.
            foreach (var propertyInfo in ModelInspector.MembersDefinedAsParameters<TInput>())
            {
                var parameter = Command.Parameters[GetDbParameterName(propertyInfo)] as IDbDataParameter;
                parameter.Value = propertyInfo.GetValue(ParameterAccesor, new object[] { }) ?? DBNull.Value;
            }
            //if (null != BeforeExecute)
            //    BeforeExecute(this, EventArgs.Empty);
            if (null == Command.Connection)
                throw new ConnectionRequieredException();

            Debug.WriteLine("SP: " + Command.CommandText);

            try
            {
                if (Command.Connection.State == ConnectionState.Closed)
                    Command.Connection.Open();
                // NOTA: El DataSet es necesario para poder obtener el reader de una forma desconectada.
                IDbDataAdapter dataAdapter = _dbFactory.CreateDataAdapter();
                dataAdapter.SelectCommand = Command;

                dataset = new DataSet();
                dataAdapter.Fill(dataset);
                // Actualizar los parámetros de salida.
                foreach (var propertyInfo in ModelInspector.MembersDefinedAsOutputParameters<TInput>())
                {
                    var param = ((IDbDataParameter)Command.Parameters[GetDbParameterName(propertyInfo)]);
                    propertyInfo.SetValue
                        (
                        ParameterAccesor,
                        param.Value is DBNull ? null : param.Value
                        , new object[] { });
                }
            }
            catch (SqlException ExSql)
            {
                if (ExSql.Number == 50000) //Excepcion de tipo RaiseError
                {
                    throw new RaiseErrorDatabaseException(ExSql.Message, ExSql);
                }
                else
                {
                    throw ExSql;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                Command.Connection.Close();
            }
            //if (null != AfterExecute)
            //    AfterExecute(this, EventArgs.Empty);
            return this;
        }

        public TInput Parameters { get { return ParameterAccesor; } }

        /// <summary>
        /// Crea el IDbCommand requerido por ADO para ejecutar sobre la base de datos.
        /// Esta llamada se realiza cuando se establece la conexión al procedimiento.
        /// </summary>
        private void CreateCommand()
        {
            Command = _dbFactory.CreateCommand();
            Command.CommandText = Name;
            Command.CommandType = CommandType.StoredProcedure;
            foreach (PropertyInfo propertyInfo in ModelInspector.MembersDefinedAsParameters<TInput>())
            {
                IDbDataParameter parameter = _dbFactory.CreateParameter();
                var parameterMetainfo = ModelInspector.ParameterDefinition(propertyInfo);
                parameter.ParameterName = GetDbParameterName(propertyInfo);
                parameter.Direction = parameterMetainfo.Direction;
                parameter.Size = parameterMetainfo.Size;
                parameter.Value = parameterMetainfo.DefaultValue;
                if (parameterMetainfo.Scale > 0 || parameterMetainfo.Precision > 0)
                {
                    parameter.Precision = parameterMetainfo.Precision;
                    parameter.Scale = parameterMetainfo.Scale;
                }
                Command.Parameters.Add(parameter);
            }
        }

        private string GetDbParameterName(PropertyInfo propertyInfo)
        {
            return String.Format("@{0}", ModelInspector.GetParameterName(propertyInfo));
        }

        /// <summary>Commando de base de datos utilizado por el procedimiento</summary>
        private IDbCommand Command { set; get; }
        /// <summary>Interface de acceso a los parametros de cabecera del procedimiento</summary>
        private TInput ParameterAccesor { get; set; }
        /// <summary>Factoria del proveedor de datos utilizado</summary>
        private DbProviderFactory _dbFactory;
    }
}