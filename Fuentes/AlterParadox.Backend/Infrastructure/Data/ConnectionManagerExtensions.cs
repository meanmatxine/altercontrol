﻿using System.Data;

/*
 * Sobre carga de dataproviders 
 * http://www.codeproject.com/Articles/55890/Don-t-hard-code-your-DataProviders
 * 
 * 
 */

namespace AlterParadox.Backend.Infrastructure.Data
{
    public static class ConnectionManagerExtensions
    {
        public static IDbConnection GetConnectionDefault(this IConnectionManager cm)
        {
            return cm.GetNamedConnection("BBDD");
        }
    }
    /// <summary>Construye la conexión desde la configuración de la aplicación.</summary>
    public class ConnectionManagerFromConfigurationFile : ConnectionManager
    {
        public ConnectionManagerFromConfigurationFile()
        {
            LoadConfigurationFile();
        }
    }
}