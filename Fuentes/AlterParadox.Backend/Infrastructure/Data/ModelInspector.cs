﻿using AlterParadox.Backend.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace AlterParadox.Backend.Infrastructure.Data
{
    // See the attribute guidelines at 
    //  http://go.microsoft.com/fwlink/?LinkId=85236        
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class, Inherited = true)]
    public class StoredProcedureAttribute : System.Attribute
    {
        /// <summary>Nombre del procedimiento almacendado</summary>
        public string Name { get; set; }
        /// <summary>Descripcion del procedimiento</summary>
        public string Description { get; set; }
        /// <summary>Tipo de IDbCommand a instanciar</summary>
        [Obsolete]
        public Type CommandType { get; set; }

        public string Provider { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    public class FieldAttribute : System.Attribute
    {
        public FieldAttribute()
        {
            //--
        }

        public FieldAttribute(string mapToName)
        {
            MapTo = mapToName;
        }

        public string MapTo { get; set; }
    }

    public class ModelInspector
    {
        #region Interfaz para tipo instanciado
        private readonly Type _typeToInspect;
        private readonly ParameterInspector[] _parameterInspectors;

        protected ModelInspector(Type inspect)
        {
            _typeToInspect = inspect;
            var inspectors = new List<ParameterInspector>();
            foreach (PropertyInfo property in ModelInspector.MembersDefinedAsParameters(_typeToInspect))
            {
                ParameterInspector.IsDefinedAsParameter(property);
                inspectors.Add(new ParameterInspector(property));
            }
            _parameterInspectors = inspectors.ToArray();
        }

        public static ModelInspector CreateFor<TCommand>()
        {
            if (!IsDefinedAsStoredProcedure<TCommand>())
                throw new TypeNotDefinedAsStoredProcedure(typeof(TCommand));
            return new ModelInspector(typeof(TCommand));
        }

        public string CommandName { get { return ModelInspector.GetCommandName(_typeToInspect); } }

        public string ProviderTypeName { get { return ModelInspector.GetProviderTypeName(_typeToInspect); } }

        public IEnumerable<ParameterInspector> Parameters { get { return _parameterInspectors; } }

        //public IEnumerable<OutputInspector> OutputFields { get { return null; } }

        #endregion
        /// <summary>
        /// Obtiene el nombre del comando definido en la base de datos
        /// </summary>
        /// <param name="type">Tipo de objeto de comando</param>
        /// <returns>Nombre correspondiente del comando en la base de datos</returns>
        /// <remarks>Utiliza el nombre de la clase como nombre si no se especifica uno en su atributo de clase.</remarks>
        public static string GetCommandName(Type type)
        {
            if (!IsDefinedAsStoredProcedure(type))
                throw new TypeNotDefinedAsStoredProcedure(type);

            StoredProcedureAttribute[] atts = (StoredProcedureAttribute[])type.GetCustomAttributes(typeof(StoredProcedureAttribute), false);
            if (String.IsNullOrEmpty(atts[0].Name))
                return type.Name;
            return atts[0].Name;
        }

        public static string GetProviderTypeName<T>()
        {
            StoredProcedureAttribute[] atts = (StoredProcedureAttribute[])typeof(T).GetCustomAttributes(typeof(StoredProcedureAttribute), false);
            return atts[0].Provider;
        }

        public static string GetProviderTypeName(Type type)
        {
            StoredProcedureAttribute[] atts = (StoredProcedureAttribute[])type.GetCustomAttributes(typeof(StoredProcedureAttribute), false);
            return atts[0].Provider;
        }

        public static bool IsDefinedAsStoredProcedure(Type type)
        {
            return type.GetCustomAttributes(typeof(StoredProcedureAttribute), false).Length > 0;
        }

        public static bool IsDefinedAsStoredProcedure<TCommand>()
        {
            return IsDefinedAsStoredProcedure(typeof(TCommand));
        }

        /// <summary>Obtiene el nombre del parametro en la base de datos</summary>
        /// <param name="property">propiedad a inspeccionar</param>
        /// <returns>Nombre de la propiedad de la clase si no se ha especificado un nombre en el atributo Parameter.</returns>
        public static string GetParameterName(PropertyInfo property)
        {
            if (!ParameterInspector.IsDefinedAsParameter(property))
                throw new Exception("The member is not defined as stored procedure parameter");

            ParameterAttribute att = ParameterDefinition(property);
            return String.IsNullOrEmpty(att.Name) ? property.Name : att.Name;
        }

        public static ParameterDirection GetParameterDirection(PropertyInfo property)
        {
            if (!ParameterInspector.IsDefinedAsParameter(property))
                throw new PropertyNotDefinedAsParameterException(property.Name);
            ParameterAttribute att = ParameterDefinition(property);
            return att.Direction;
        }

        public static object GetDefaultValue(PropertyInfo property)
        {
            if (!ParameterInspector.IsDefinedAsParameter(property))
                throw new PropertyNotDefinedAsParameterException(property.Name);
            ParameterAttribute att = ParameterDefinition(property);
            return att.DefaultValue;
        }

        /// <summary>
        /// Lista las propiedades de la clase definidas como parametros.
        /// </summary>
        /// <typeparam name="T">Tipo a inspeccionar</typeparam>
        /// <returns>Información de las propiedades </returns>
        public static IEnumerable<PropertyInfo> MembersDefinedAsParameters<T>()
        {
            return MembersDefinedAsParameters(typeof(T));
        }

        public static IEnumerable<PropertyInfo> MembersDefinedAsOutputParameters<T>()
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                ParameterAttribute[] att = (ParameterAttribute[])pi.GetCustomAttributes(typeof(ParameterAttribute), false);
                if (ParameterInspector.IsDefinedAsParameter(pi) && (att[0].Direction == ParameterDirection.Output || att[0].Direction == ParameterDirection.InputOutput
                    || att[0].Direction == ParameterDirection.ReturnValue))
                {
                    yield return pi;
                }
            }
        }

        public static IEnumerable<PropertyInfo> MembersDefinedAsParameters(Type type)
        {
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                ParameterAttribute[] att = (ParameterAttribute[])pi.GetCustomAttributes(typeof(ParameterAttribute), false);
                if (ParameterInspector.IsDefinedAsParameter(pi))
                {
                    yield return pi;
                }
            }
        }

        public static IEnumerable<PropertyInfo> MembersDefinedAsFields<T>()
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (pi.GetCustomAttributes(typeof(FieldAttribute), false).Any())
                    yield return pi;
            }
        }

        /// <summary>
        /// Devuelve el atributo con los valores del parámetro
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static ParameterAttribute ParameterDefinition(PropertyInfo property)
        {
            return ParameterInspector.IsDefinedAsParameter(property) ?
                (ParameterAttribute)(property.GetCustomAttributes(typeof(ParameterAttribute), false)[0])
            :
                null
            ;
        }
    }
}