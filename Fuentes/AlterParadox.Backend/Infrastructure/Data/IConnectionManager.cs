﻿using AlterParadox.Backend.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace AlterParadox.Backend.Infrastructure.Data
{
    public interface IConnectionManager
    {
        IDbConnection[] GetAll();
        IDbConnection GetDefaultConnection();
        IConnectionManager SetDefaultConnection(string connectionName);
        IDbConnection GetNamedConnection(string name);
        IConnectionManager Register(string connectionName, IDbConnection connection);
        IConnectionManager ReleaseConnection(string name);
        ExecutionHandler CreateExecutionContext();
        IConnectionManager LoadConfigurationFile();
    }

    /// <summary>
    /// Contiene y gestiona las conexiones disponibles para el sistema.
    /// </summary>
    public class ConnectionManager : IConnectionManager
    {
        /// <summary>Devuelve una nueva instancia de objeto conexion</summary>
        /// <param name="name">Nombre del model de conexión a crear</param>
        /// <returns></returns>
        public IDbConnection GetNamedConnection(string name)
        {
            try
            {
                return _connections[name]();
            }
            catch (KeyNotFoundException)
            {
                throw new ConnectionNotFoundException(name);
            }
        }
        /// <summary>Obtiene la conexión marcada como conexión por defecto</summary>
        /// <returns>La conexión establecida para usarse por defecto</returns>
        public IDbConnection GetDefaultConnection()
        {
            return GetNamedConnection(_defaultConnection);
        }
        /// <summary>Establece una conexion registrada para funcionar como conexion por defecto.</summary>
        /// <param name="connectionName"></param>
        public IConnectionManager SetDefaultConnection(string connectionName)
        {
            _defaultConnection = connectionName;
            return this;
        }

        /// <summary>Obtiene todas las conexiones registradas</summary>
        public IDbConnection[] GetAll()
        {
            throw new NotImplementedException();
            //return new List<IDbConnection>(_connections.Values).ToArray();            
        }

        /// <summary>Registra una conexión</summary>
        /// <param name="connectionName">Clave de la conexión registrada</param>
        /// <param name="connection">Objeto conexión</param>
        public IConnectionManager Register(string connectionName, IDbConnection connection)
        {
            if (connection == null) throw new RegistrationException(CONNECTION_OBJECT_CANNOT_BE_NULL_);
            try
            {
                throw new NotImplementedException();
                //_connections.Add(connectionName, connection);
            }
            catch (ArgumentException)
            {
                throw new RegistrationException(string.Format(CONNECTION_ALREADY_REGISTERED_, connectionName));
            }
        }

        /// <summary>
        /// Elimina una conexión registrada y libera todos sus recursos. 
        /// La conexión no estará disponible en ningún otro contexto.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public IConnectionManager ReleaseConnection(string name)
        {
            var cn = GetNamedConnection(name);
            _connections.Remove(name);
            cn.Dispose();
            return this;
        }

        private const string CONNECTION_OBJECT_CANNOT_BE_NULL_ = "El objeto de conexión no puede ser nulo";
        private const string CONNECTION_ALREADY_REGISTERED_ = "Se ha agregado una conexión con un nombre previamente registrado. Clave: {0}";
        private string _defaultConnection = string.Empty;
        private readonly Dictionary<string, Func<IDbConnection>> _connections = new Dictionary<string, Func<IDbConnection>>();


        public ExecutionHandler CreateExecutionContext()
        {
            return new ExecutionHandler(this);
        }

        public IConnectionManager LoadConfigurationFile()
        {
            // Sobre carga de dataproviders 
            // http://www.codeproject.com/Articles/55890/Don-t-hard-code-your-DataProviders
            foreach (ConnectionStringSettings settings in ConfigurationManager.ConnectionStrings)
            {
                // CUIDADO:
                //          No meter settings.ConnectionString dentro del delegado. Es una referencia
                //          a una iteración. Por tanto, cuando se ejecute el delegado, settings estará 
                //          apuntando siempre al último elemento de la colección.
                var cn = settings.ConnectionString.Replace("**hostname**", Environment.MachineName.ToString());
                var name = settings.Name;
                var providerName = settings.ProviderName;
                _connections.Add
                    (
                    name,
                    () =>
                    {
                        IDbConnection con = DbProviderFactories.GetFactory(providerName).CreateConnection();
                        con.ConnectionString = cn;
                        return con;
                    });
            }
            return this;
        }
    }

   
}