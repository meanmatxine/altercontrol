﻿using System;
using System.Collections.Generic;
using System.Data;

namespace AlterParadox.Backend.Infrastructure.Data
{
    public class ExecutionHandler
    {
        public event EventHandler Cancelled = delegate { };

        private readonly List<IObservableCommand> _commands = new List<IObservableCommand>();
        private readonly List<IDbConnection> _connections = new List<IDbConnection>();
        private readonly IConnectionManager _connectionManager;

        public ExecutionHandler(IConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }

        private void command_AfterExecute(IObservableCommand sender, EventArgs e)
        {
            if (_cancelPredicate != null)
                if (!_cancelPredicate(sender))
                    Cancel(sender);
        }

        #region Registrar commandos

        public ExecutionHandler Register(IObservableCommand command)
        {
            if (command == null)
                throw new ArgumentNullException();

            command.AfterExecute += command_AfterExecute;
            _commands.Add(command);
            return this;
        }

        public ExecutionHandler Register(IObservableCommand command, IDbConnection[] useConnection)
        {
            throw new NotImplementedException();
        }
        #endregion

        public ExecutionHandler On(IDbConnection useConnection)
        {
            _connections.Add(useConnection);
            return this;
        }

        public ExecutionHandler On(string connectionName)
        {
            _connections.Add(_connectionManager.GetNamedConnection(connectionName));
            return this;
        }

        public ExecutionHandler On(IDbConnection[] useConnections)
        {
            _connections.AddRange(useConnections);
            return this;
        }

        public ExecutionHandler OnAll()
        {
            throw new NotImplementedException();
        }

        public ExecutionHandler OnDefault()
        {
            throw new NotImplementedException();
        }

        protected ExecutionHandler Cancel(IObservableCommand from)
        {
            _executionCancelled = true;
            this.Cancelled(from, EventArgs.Empty);
            return this;
        }

        private bool _executionCancelled = false;
        private Predicate<IObservableCommand> _cancelPredicate = null;

        public ExecutionHandler CancelIf(Predicate<IObservableCommand> pred)
        {
            _cancelPredicate = pred;
            return this;
        }

        //
        // TODO: Hacer este funcionamiento más flexible, cada comando tiene que 
        //       registrarse explicitamente con 1 o más conexiones sobre la que se ejecutará.
        //       Evitando que todos tengan que compartir la misma.
        //
        /// <summary>
        /// Ejecuta para cada conexión todos los comandos registrados en el 
        /// handler
        /// </summary>
        public void Execute()
        {
            try
            {
                foreach (IDbConnection con in _connections)
                {
                    foreach (IObservableCommand sp in _commands)
                    {
                        if (_executionCancelled)
                            break;
                        sp.Connection = con;
                        sp.Execute();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}