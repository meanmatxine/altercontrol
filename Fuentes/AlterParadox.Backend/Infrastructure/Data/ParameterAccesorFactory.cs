﻿using AlterParadox.Backend.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace AlterParadox.Backend.Infrastructure.Data
{
    public static class ParameterAccesorFactory
    {
        static ParameterAccesorFactory()
        {
            AppDomain domain = AppDomain.CurrentDomain;
            AssemblyName asmName = new AssemblyName(Guid.NewGuid().ToString());
            AssemblyBuilder asmBuilder = domain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.RunAndSave);
            ModuleBuilder = asmBuilder.DefineDynamicModule(asmName.Name, asmName.Name + ".dll");
        }

        // http://msdn.microsoft.com/en-us/library/3y322t50.aspx
        // http://msdn.microsoft.com/en-us/library/4xxf1410%28v=vs.90%29.aspx
        // http://geekswithblogs.net/rgupta/archive/2008/12/01/dynamically-creating-types-using-reflection-and-setting-properties-using-reflection.emit.aspx
        // http://stackoverflow.com/questions/1558866/casting-object-to-a-specific-class-in-il
        public static T Create<T>()
        {
            try
            {
                Type newType = null;
                Type instanceType = typeof(T);

                if (!_stored.TryGetValue(instanceType, out newType))
                {
                    // Assembly
                    AppDomain domain = AppDomain.CurrentDomain;
                    AssemblyName asmName = new AssemblyName(Guid.NewGuid().ToString());
                    AssemblyBuilder asmBuilder = domain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.RunAndSave);
                    // modulo
                    ModuleBuilder modBuilder = asmBuilder.DefineDynamicModule(asmName.Name, asmName.Name + ".dll");
                    // tipos
                    TypeBuilder tb = modBuilder.DefineType(instanceType.Name + "_dynProx", TypeAttributes.Public);
                    tb.AddInterfaceImplementation(instanceType);
                    {
                        foreach (PropertyInfo pi in ModelInspector.MembersDefinedAsParameters<T>())
                        {
                            string name = pi.Name;
                            string field = "_" + name.ToLower();
                            TypeBuilder t = tb;
                            Type typ = pi.PropertyType;

                            FieldBuilder fieldBldr = t.DefineField(field, typ, FieldAttributes.Private);
                            PropertyBuilder getPropertyBuilder = t.DefineProperty(name, System.Reflection.PropertyAttributes.HasDefault, typ, null);
                            const MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;
                            MethodBuilder getPropBldr = t.DefineMethod("get_" + name, getSetAttr, typ, Type.EmptyTypes);
                            ILGenerator getIL = getPropBldr.GetILGenerator();
                            getIL.Emit(OpCodes.Ldarg_0);
                            getIL.Emit(OpCodes.Ldfld, fieldBldr);
                            getIL.Emit(OpCodes.Ret);
                            MethodBuilder setPropBldr = t.DefineMethod("set_" + name, getSetAttr, null, new Type[] { typ });
                            ILGenerator setIL = setPropBldr.GetILGenerator();
                            setIL.Emit(OpCodes.Ldarg_0);
                            setIL.Emit(OpCodes.Ldarg_1);
                            setIL.Emit(OpCodes.Stfld, fieldBldr);
                            setIL.Emit(OpCodes.Ret);
                            getPropertyBuilder.SetGetMethod(getPropBldr);
                            getPropertyBuilder.SetSetMethod(setPropBldr);
                            t.DefineMethodOverride(setPropBldr, typeof(T).GetProperty(name).GetSetMethod());
                            t.DefineMethodOverride(getPropBldr, typeof(T).GetProperty(name).GetGetMethod());
                        }
                    }
                    newType = tb.CreateType();
                    lock (_storedTypesSyncRoot2)
                    {
                        if (!_stored.ContainsKey(instanceType))
                            _stored.Add(instanceType, newType);
                    }
                }
                return (T)Activator.CreateInstance(newType);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Excepción desde 'ParameterAccesorFactory' > 'Create<T>'", ex);
            }
        }

        //
        // Fuente: http://es.softuses.com/8681
        //
        public static T CreateOutput<T>(IDataReader reader)
        {
            try
            {
                Type newType = null;
                Type instanceType = typeof(T);

                if (instanceType.GetProperties().Length != ModelInspector.MembersDefinedAsFields<T>().Count())
                {
                    throw new MalformedOutputInterfaceException(instanceType.FullName, new Exception("Clase de Salida Malformada!!!"));
                }

                if (!_stored.TryGetValue(instanceType, out newType))
                {
                    TypeBuilder tb = ModuleBuilder.DefineType(instanceType.Name + "_dynProx", TypeAttributes.Public);
                    tb.AddInterfaceImplementation(instanceType);
                    {
                        foreach (PropertyInfo pi in ModelInspector.MembersDefinedAsFields<T>())
                        {
                            var atts = (FieldAttribute[])pi.GetCustomAttributes(typeof(FieldAttribute), false);
                            // Si no tiene asignado campo de mapeo coger el nombre de la propiedad
                            if (atts[0].MapTo == null)
                            {
                                atts[0].MapTo = pi.Name;
                            }
                            string name = pi.Name;
                            string field = "_" + name.ToLower();
                            TypeBuilder t = tb;
                            Type typ = pi.PropertyType;
                            FieldBuilder fieldBldr = t.DefineField(field, typ, FieldAttributes.Private);

                            #region Cargar Atributo de valor por defecto
                            PropertyBuilder property = tb.DefineProperty(
                                            pi.Name,
                                            System.Reflection.PropertyAttributes.None,
                                            typ,
                                            new Type[0]);
                            //Create the custom attribute to set the description.
                            var ctorParams = new[] { typeof(string) };
                            ConstructorInfo classCtorInfo =
                                typeof(DescriptionAttribute).GetConstructor(ctorParams);

                            CustomAttributeBuilder myCABuilder = new CustomAttributeBuilder(
                                classCtorInfo,
                                new object[] { "This is the long description of this property." });

                            property.SetCustomAttribute(myCABuilder);

                            //Set the default value.
                            ctorParams = new[] { typeof(Type), typeof(string) };
                            classCtorInfo = typeof(DefaultValueAttribute).GetConstructor(ctorParams);

                            if (typ.IsEnum)
                            {
                                ////val contains the text version of the enum. Parse it to the enumeration value.
                                //object o = Enum.Parse(tb, val.ToString(), true);
                                //myCABuilder = new CustomAttributeBuilder(
                                //    classCtorInfo,
                                //    new object[] { o });
                            }
                            else
                            {
                                myCABuilder = new CustomAttributeBuilder
                                    (
                                    classCtorInfo,
                                    new[] { typ, null }
                                    );
                            }

                            property.SetCustomAttribute(myCABuilder);
                            #endregion

                            //PropertyBuilder getPropertyBuilder = t.DefineProperty(name, System.Reflection.PropertyAttributes.HasDefault, typ, null);
                            const MethodAttributes getSetAttr = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual;
                            MethodBuilder getPropBldr = t.DefineMethod("get_" + name, getSetAttr, typ, Type.EmptyTypes);
                            ILGenerator getIL = getPropBldr.GetILGenerator();
                            getIL.Emit(OpCodes.Ldarg_0);
                            getIL.Emit(OpCodes.Ldfld, fieldBldr);
                            getIL.Emit(OpCodes.Ret);
                            MethodBuilder setPropBldr = t.DefineMethod("set_" + name, getSetAttr, null, new Type[] { typ });
                            ILGenerator setIL = setPropBldr.GetILGenerator();
                            setIL.Emit(OpCodes.Ldarg_0);
                            setIL.Emit(OpCodes.Ldarg_1);
                            setIL.Emit(OpCodes.Stfld, fieldBldr);
                            setIL.Emit(OpCodes.Ret);

                            property.SetGetMethod(getPropBldr);
                            property.SetSetMethod(setPropBldr);
                            t.DefineMethodOverride(setPropBldr, typeof(T).GetProperty(name).GetSetMethod());
                            t.DefineMethodOverride(getPropBldr, typeof(T).GetProperty(name).GetGetMethod());

                            // Injecta el valor contenido en el reader
                            //fieldBldr.SetValue(setPropBldr, reader[atts[0].MapTo]);                    
                        }
                    }
                    newType = tb.CreateType();

                    lock (_storedTypesSyncRoot1)
                    {
                        if (!_stored.ContainsKey(instanceType))
                            _stored.Add(instanceType, newType);
                    }
                }

                var generatedObject = (T)System.Activator.CreateInstance(newType);

                // Loop over all the generated properties, and assign the default values
                var properties = instanceType.GetProperties();
                var props = TypeDescriptor.GetProperties(newType);

                for (var i = 0; i < properties.Length; i++)
                {
                    var field = properties[i].Name;
                    var pType = properties[i].PropertyType;

                    var atts = (FieldAttribute[])properties[i].GetCustomAttributes(typeof(FieldAttribute), true);
                    if (atts[0].MapTo == null)
                    {
                        atts[0].MapTo = properties[i].Name;
                    }

                    object value;
                    try
                    {
                        value = reader[atts[0].MapTo];
                    }
                    catch (Exception ex)
                    {
                        throw new DatabaseMappingException(atts[0].MapTo, ex);
                    }

                    if (DBNull.Value != value)
                    {
                        if (Nullable.GetUnderlyingType(pType) != null)
                            properties[i].SetValue(generatedObject, new NullableConverter(pType).ConvertFrom(value), null);
                        else
                            properties[i].SetValue(generatedObject, Convert.ChangeType(value, pType), null);
                    }
                }
                return (T)generatedObject;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Excepción desde 'ParameterAccesorFactory' > 'CreateOutput<T>'", ex);
            }
        }

        private static object _storedTypesSyncRoot1 = new object();
        private static object _storedTypesSyncRoot2 = new object();

        private static Dictionary<Type, Type> _stored = new Dictionary<Type, Type>();
        private static ModuleBuilder ModuleBuilder { get; set; }
    }
}