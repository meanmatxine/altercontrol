﻿using AlterParadox.Backend.Infrastructure.Exceptions;
using System;
using System.Data;
using System.Reflection;

namespace AlterParadox.Backend.Infrastructure.Data
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ParameterAttribute : System.Attribute
    {
        public ParameterAttribute()
        {
            Direction = ParameterDirection.Input;
        }

        public string Name { get; set; }
        public SqlDbType Type { get; set; }
        public object DefaultValue { get; set; }
        public ParameterDirection Direction { get; set; }
        /// <summary>Tamaño del parametro</summary>
        public int Size { get; set; }

        public byte Precision { get; set; }

        public byte Scale { get; set; }
    }

    public class ParameterInspector
    {
        private readonly ParameterAttribute _attribute;

        public ParameterInspector(PropertyInfo property)
        {
            if (!IsDefinedAsParameter(property))
                throw new PropertyNotDefinedAsParameterException(property.Name);
            _attribute = (ParameterAttribute)(property.GetCustomAttributes(typeof(ParameterAttribute), false)[0]);
        }

        public string Name { get { return _attribute.Name; } }

        public bool IsMapped { get { return !string.IsNullOrEmpty(_attribute.Name); } }

        public bool HasDefaultValue { get { return _attribute.DefaultValue != null; } }

        public bool HasSize { get { return _attribute.Size > 0; } }

        public int Size { get { return _attribute.Size; } }

        public ParameterDirection Direction { get { return _attribute.Direction; } }

        public static bool IsDefinedAsParameter(PropertyInfo property)
        {
            if (property == null)
                throw new ArgumentNullException("property");
            return property.GetCustomAttributes(typeof(ParameterAttribute), false).Length > 0;
        }
    }
}