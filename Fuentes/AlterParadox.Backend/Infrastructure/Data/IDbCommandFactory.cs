﻿using System.Data.Common;

namespace AlterParadox.Backend.Infrastructure.Data
{
    public interface IDbCommandFactory
    {
        IStoredProcedure<TInput> Create<TInput>() where TInput : class;
    }

    public class StoredProcedureRepository : IDbCommandFactory
    {
        //
        //  TODO: <David López Gutiérrez, xxxx-xx-xx> Repositorio mal planteado.
        //       Este repositorio está mal planteado. Debería únicamente funcionar como
        //       un contenedor donde se hayan registrado los procedimientos disponibles
        //       Ahora mismo es una mera factoría.
        //
        public StoredProcedureRepository(IConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }

        public IStoredProcedure<TInput> Create<TInput>()
            where TInput : class
        {
            var instance =
                new StoredProcedure<TInput>(DbProviderFactories.GetFactory(ModelInspector.GetProviderTypeName<TInput>()));
            instance.BeforeExecute += StoredProcedure_BeforeExecute;
            instance.AfterExecute += StoredProcedure_AfterExecute;
            return instance;
        }

        #region REFACTORIZAR
        //
        // TODO: <David López Gutiérrez, xxxx-xx-xx> No es responsabilidad del repositorio establecer, abrir y cerrar las 
        //       conexiones. Esto tiene que recaer sobre el manejador de ejecución de
        //       conexión. Recordar que tampoco es responsabilidad del PA, abrirlas y
        //       cerrarlas.
        //
        void StoredProcedure_AfterExecute(IObservableCommand sender, System.EventArgs e)
        {
            sender.Connection.Close();
        }

        void StoredProcedure_BeforeExecute(IObservableCommand sender, System.EventArgs e)
        {
            // HACK: <David López Gutiérrez, xxxx-xx-xx> Execution Handler
            // Esto es un problema cuando se utiliza el execution handler. ExecutionHandler
            // tiene las conexiones a utilizar. Si ahora pasa por aqui sobreescribe la conexión
            // que ha incluido el execution handler
            //
            if (sender.Connection == null)
                sender.Connection = _connectionManager.GetDefaultConnection();
            if (sender.Connection.State == System.Data.ConnectionState.Closed)
                sender.Connection.Open();
        }
        #endregion

        private readonly IConnectionManager _connectionManager = null;
    }

}