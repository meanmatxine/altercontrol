﻿using System.Data;
using System;

namespace AlterParadox.Backend.Infrastructure.Data
{
    public delegate void ObservableCommandEventHandler(IObservableCommand command, EventArgs e);

    /// <summary>Interface para el manejo de ejecución de comandos de la base de datos</summary>
    public interface IObservableCommand
    {
        /// <summary>Evento que se dispara antes de la ejecución contra la base de datos</summary>
        event ObservableCommandEventHandler BeforeExecute;
        /// <summary>Evento disparado después de recibir los resultados de la base de datos</summary>
        event ObservableCommandEventHandler AfterExecute;
        /// <summary>Nombre del procedimiento almacenado</summary>
        string Name { get; }
        /// <summary>Ejecución del comando de Base de datos</summary>
        /// <returns>Data reader</returns>
        IObservableCommand Execute();
        IDbConnection Connection { get; set; }
    }
}