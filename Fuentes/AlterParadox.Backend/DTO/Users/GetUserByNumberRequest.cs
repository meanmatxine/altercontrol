﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetUserByNumberRequest
    {
        [DataMember(IsRequired = true)]
        public int Number { get; set; }

        public Result<GetUserByNumberRequest> Validar()
        {
            if (Number <= 0)
                return Result<GetUserByNumberRequest>.CreateFailed("El objecto 'Number' no puede ser menor o igual a 0.");

            return Result<GetUserByNumberRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetUserByNumberResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public User Usuario { get; set; }

        public static GetUserByNumberResponse RespuestaFallo(Result<GetUserByNumberRequest> requestResult)
        {
            return new GetUserByNumberResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Usuario = null
            };
        }

        public static GetUserByNumberResponse RespuestaExito(User usuario)
        {
            return new GetUserByNumberResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Usuario = usuario
            };
        }

    }
}