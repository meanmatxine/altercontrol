﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class SaveUserRequest
    {
        [DataMember(IsRequired = true)]
        public User Usuario { get; set; }

        public Result<SaveUserRequest> Validar()
        {
            if (Usuario == null)
                return Result<SaveUserRequest>.CreateFailed("El objecto 'Usuario' no puede ser nulo.");

            return Result<SaveUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class SaveUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public UserResumen Usuario { get; set; }

        public static SaveUserResponse RespuestaFallo(Result<SaveUserRequest> requestResult)
        {
            return new SaveUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Usuario = null
            };
        }

        public static SaveUserResponse RespuestaExito(UserResumen usuario)
        {
            return new SaveUserResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Usuario = usuario
            };
        }

    }
}