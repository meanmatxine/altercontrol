﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetUserRequest
    {
        [DataMember(IsRequired = true)]
        public int IdUser { get; set; }

        public Result<GetUserRequest> Validar()
        {
            if (IdUser <= 0)
                return Result<GetUserRequest>.CreateFailed("El objecto 'IdUser' no puede ser menor o igual a 0.");

            return Result<GetUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public User Usuario { get; set; }

        public static GetUserResponse RespuestaFallo(Result<GetUserRequest> requestResult)
        {
            return new GetUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Usuario = null
            };
        }

        public static GetUserResponse RespuestaExito(User usuario)
        {
            return new GetUserResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Usuario = usuario
            };
        }

    }
}