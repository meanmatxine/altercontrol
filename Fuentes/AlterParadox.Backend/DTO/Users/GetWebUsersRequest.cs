﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetWebUsersRequest
    {
        public Result<GetWebUsersRequest> Validar()
        {

            return Result<GetWebUsersRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetWebUsersResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<User> ListaUsuarios { get; set; }

        public static GetWebUsersResponse RespuestaFallo(Result<GetWebUsersRequest> requestResult)
        {
            return new GetWebUsersResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ListaUsuarios = null
            };
        }

        public static GetWebUsersResponse RespuestaExito(List<User> request)
        {
            return new GetWebUsersResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ListaUsuarios = request
            };
        }

    }
}