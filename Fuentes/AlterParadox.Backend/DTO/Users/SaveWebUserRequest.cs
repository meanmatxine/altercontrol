﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class SaveWebUserRequest
    {
        [DataMember(IsRequired = true)]
        public User Usuario { get; set; }

        public Result<SaveWebUserRequest> Validar()
        {
            if (Usuario == null)
                return Result<SaveWebUserRequest>.CreateFailed("El objecto 'Usuario' no puede ser nulo.");

            return Result<SaveWebUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class SaveWebUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        public static SaveWebUserResponse RespuestaFallo(Result<SaveWebUserRequest> requestResult)
        {
            return new SaveWebUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage
            };
        }

        public static SaveWebUserResponse RespuestaExito()
        {
            return new SaveWebUserResponse()
            {
                EsCorrecto = true,
                MensajeError = ""
            };
        }

    }
}