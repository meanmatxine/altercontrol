﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetUsersRequest
    {
        public Result<GetUsersRequest> Validar()
        {

            return Result<GetUsersRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetUsersResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<User> ListaUsuarios { get; set; }

        public static GetUsersResponse RespuestaFallo(Result<GetUsersRequest> requestResult)
        {
            return new GetUsersResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ListaUsuarios = null
            };
        }

        public static GetUsersResponse RespuestaExito(List<User> listaUsuarios)
        {
            return new GetUsersResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ListaUsuarios = listaUsuarios
            };
        }

    }
}