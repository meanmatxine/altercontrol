﻿using AlterParadox.Backend.Infrastructure;
using System.Runtime.Serialization;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class DeleteUserRequest
    {
        [DataMember(IsRequired = true)]
        public int IdUser { get; set; }
     
        public Result<DeleteUserRequest> Validar()
        {
            if (IdUser <=0)
                return Result<DeleteUserRequest>.CreateFailed("El objecto 'IdUser' no puede ser menor o igual a 0.");

            return Result<DeleteUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class DeleteUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        public static DeleteUserResponse RespuestaFallo(Result<DeleteUserRequest> requestResult)
        {
            return new DeleteUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage
            };
        }

        public static DeleteUserResponse RespuestaExito()
        {
            return new DeleteUserResponse()
            {
                EsCorrecto = true,
                MensajeError = ""
            };
        }

    }
}