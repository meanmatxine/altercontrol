﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class SaveActivityRequest
    {
        [DataMember(IsRequired = true)]
        public Activity Actividad { get; set; }

        public Result<SaveActivityRequest> Validar()
        {
            if (Actividad == null)
                return Result<SaveActivityRequest>.CreateFailed("El objecto 'Actividad' no puede ser nulo.");

            return Result<SaveActivityRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class SaveActivityResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public ActivityResumen Actividad { get; set; }

        public static SaveActivityResponse RespuestaFallo(Result<SaveActivityRequest> requestResult)
        {
            return new SaveActivityResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Actividad = null
            };
        }

        public static SaveActivityResponse RespuestaExito(ActivityResumen actividad)
        {
            return new SaveActivityResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Actividad = actividad
            };
        }

    }
}