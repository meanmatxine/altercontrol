﻿using AlterParadox.Backend.Infrastructure;
using System.Runtime.Serialization;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class DeleteActivityUserRequest
    {
        [DataMember(IsRequired = true)]
        public int Id { get; set; }

        public Result<DeleteActivityUserRequest> Validar()
        {
            if (Id <= 0)
                return Result<DeleteActivityUserRequest>.CreateFailed("El objecto 'IdActivity' no puede ser menor o igual a 0.");

            return Result<DeleteActivityUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class DeleteActivityUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        public static DeleteActivityUserResponse RespuestaFallo(Result<DeleteActivityUserRequest> requestResult)
        {
            return new DeleteActivityUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage
            };
        }

        public static DeleteActivityUserResponse RespuestaExito()
        {
            return new DeleteActivityUserResponse()
            {
                EsCorrecto = true,
                MensajeError = ""
            };
        }

    }

}