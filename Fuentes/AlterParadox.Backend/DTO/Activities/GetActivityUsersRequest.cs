﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetActivityUsersRequest
    {
        [DataMember(IsRequired = true)]
        public int IdActivity { get; set; }

        public Result<GetActivityUsersRequest> Validar()
        {
            if (IdActivity <= 0)
                return Result<GetActivityUsersRequest>.CreateFailed("El objecto 'IdActivity' no puede ser menor o igual a 0.");

            return Result<GetActivityUsersRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetActivityUsersResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<ActivityUser> ListaActividadUsuarios { get; set; }

        public static GetActivityUsersResponse RespuestaFallo(Result<GetActivityUsersRequest> requestResult)
        {
            return new GetActivityUsersResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ListaActividadUsuarios = null
            };
        }

        public static GetActivityUsersResponse RespuestaExito(List<ActivityUser> listaActividadUsuarios)
        {
            return new GetActivityUsersResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ListaActividadUsuarios = listaActividadUsuarios
            };
        }

    }
}