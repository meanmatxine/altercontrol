﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetActivityUserRequest
    {
        [DataMember(IsRequired = true)]
        public int IdActivity { get; set; }
        [DataMember(IsRequired = true)]
        public int IdUser { get; set; }

        public Result<GetActivityUserRequest> Validar()
        {
            if (IdActivity <= 0)
                return Result<GetActivityUserRequest>.CreateFailed("El objecto 'IdActivity' no puede ser menor o igual a 0.");
            if (IdUser <= 0)
                return Result<GetActivityUserRequest>.CreateFailed("El objecto 'IdActivity' no puede ser menor o igual a 0.");

            return Result<GetActivityUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetActivityUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public ActivityUser ActividadUsuario { get; set; }

        public static GetActivityUserResponse RespuestaFallo(Result<GetActivityUserRequest> requestResult)
        {
            return new GetActivityUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ActividadUsuario = null
            };
        }

        public static GetActivityUserResponse RespuestaExito(ActivityUser actividadUsuario)
        {
            return new GetActivityUserResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ActividadUsuario = actividadUsuario
            };
        }

    }
}