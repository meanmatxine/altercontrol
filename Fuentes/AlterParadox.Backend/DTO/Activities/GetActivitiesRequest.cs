﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetActivitiesRequest
    {
        public Result<GetActivitiesRequest> Validar()
        {

            return Result<GetActivitiesRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetActivitiesResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<Activity> ListaActividades { get; set; }

        public static GetActivitiesResponse RespuestaFallo(Result<GetActivitiesRequest> requestResult)
        {
            return new GetActivitiesResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ListaActividades = null
            };
        }

        public static GetActivitiesResponse RespuestaExito(List<Activity> listaActividades)
        {
            return new GetActivitiesResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ListaActividades = listaActividades
            };
        }

    }
}