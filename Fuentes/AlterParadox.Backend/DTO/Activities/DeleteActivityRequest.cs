﻿using AlterParadox.Backend.Infrastructure;
using System.Runtime.Serialization;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class DeleteActivityRequest
    {
        [DataMember(IsRequired = true)]
        public int IdActivity { get; set; }

        public Result<DeleteActivityRequest> Validar()
        {
            if (IdActivity <= 0)
                return Result<DeleteActivityRequest>.CreateFailed("El objecto 'IdActivity' no puede ser menor o igual a 0.");

            return Result<DeleteActivityRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class DeleteActivityResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        public static DeleteActivityResponse RespuestaFallo(Result<DeleteActivityRequest> requestResult)
        {
            return new DeleteActivityResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage
            };
        }

        public static DeleteActivityResponse RespuestaExito()
        {
            return new DeleteActivityResponse()
            {
                EsCorrecto = true,
                MensajeError = ""
            };
        }

    }

}