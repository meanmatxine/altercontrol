﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class SaveActivityUserRequest
    {
        [DataMember(IsRequired = true)]
        public ActivityUser ActividadUsuario { get; set; }

        public Result<SaveActivityUserRequest> Validar()
        {
            if (ActividadUsuario == null)
                return Result<SaveActivityUserRequest>.CreateFailed("El objecto 'ActividadUsuario' no puede ser nulo.");

            return Result<SaveActivityUserRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class SaveActivityUserResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public ActivityUserResumen ActividadUsuario { get; set; }

        public static SaveActivityUserResponse RespuestaFallo(Result<SaveActivityUserRequest> requestResult)
        {
            return new SaveActivityUserResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ActividadUsuario = null
            };
        }

        public static SaveActivityUserResponse RespuestaExito(ActivityUserResumen actividadUsuario)
        {
            return new SaveActivityUserResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ActividadUsuario = actividadUsuario
            };
        }

    }
}