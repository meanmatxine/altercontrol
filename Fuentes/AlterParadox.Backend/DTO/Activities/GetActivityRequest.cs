﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetActivityRequest
    {
        [DataMember(IsRequired = true)]
        public int IdActivity { get; set; }

        public Result<GetActivityRequest> Validar()
        {
            if (IdActivity <= 0)
                return Result<GetActivityRequest>.CreateFailed("El objecto 'IdActivity' no puede ser menor o igual a 0.");

            return Result<GetActivityRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetActivityResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public Activity Actividad { get; set; }

        public static GetActivityResponse RespuestaFallo(Result<GetActivityRequest> requestResult)
        {
            return new GetActivityResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Actividad = null
            };
        }

        public static GetActivityResponse RespuestaExito(Activity actividad)
        {
            return new GetActivityResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Actividad = actividad
            };
        }

    }
}