﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetAssociationsRequest
    {
        public Result<GetAssociationsRequest> Validar()
        {
            return Result<GetAssociationsRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetAssociationsResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<Association> Asociaciones { get; set; }

        public static GetAssociationsResponse RespuestaFallo(Result<GetAssociationsRequest> requestResult)
        {
            return new GetAssociationsResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Asociaciones = null
            };
        }

        public static GetAssociationsResponse RespuestaExito(List<Association> listaAsociaciones)
        {
            return new GetAssociationsResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Asociaciones = listaAsociaciones
            };
        }
    }
}