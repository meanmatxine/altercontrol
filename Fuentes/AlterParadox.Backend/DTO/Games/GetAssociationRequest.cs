﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetAssociationRequest
    {
        [DataMember(IsRequired = true)]
        public int IdAsociacion { get; set; }

        public Result<GetAssociationRequest> Validar()
        {
            if (IdAsociacion <= 0)
                return Result<GetAssociationRequest>.CreateFailed("El objecto 'IdAsociacion' no puede ser menor o igual a 0.");

            return Result<GetAssociationRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetAssociationResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public Association Asociacion { get; set; }

        public static GetAssociationResponse RespuestaFallo(Result<GetAssociationRequest> requestResult)
        {
            return new GetAssociationResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Asociacion = null
            };
        }

        public static GetAssociationResponse RespuestaExito(Association asociacion)
        {
            return new GetAssociationResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Asociacion = asociacion
            };
        }

    }
}