﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class SaveAssociationRequest
    {
        [DataMember(IsRequired = true)]
        public Association Asociacion { get; set; }

        public Result<SaveAssociationRequest> Validar()
        {
            if (Asociacion == null)
                return Result<SaveAssociationRequest>.CreateFailed("El objecto 'Asociacion' no puede ser nulo.");

            return Result<SaveAssociationRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class SaveAssociationResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public AssociationResumen Asociacion { get; set; }

        public static SaveAssociationResponse RespuestaFallo(Result<SaveAssociationRequest> requestResult)
        {
            return new SaveAssociationResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Asociacion = null
            };
        }

        public static SaveAssociationResponse RespuestaExito(AssociationResumen asociacion)
        {
            return new SaveAssociationResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Asociacion = asociacion
            };
        }

    }
}