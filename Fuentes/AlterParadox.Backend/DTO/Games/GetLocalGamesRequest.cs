﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetLocalGamesRequest
    {
        public Result<GetLocalGamesRequest> Validar()
        {
            return Result<GetLocalGamesRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetLocalGamesResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<LocalGame> ListaJuegos { get; set; }

        public static GetLocalGamesResponse RespuestaFallo(Result<GetLocalGamesRequest> requestResult)
        {
            return new GetLocalGamesResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ListaJuegos = null
            };
        }

        public static GetLocalGamesResponse RespuestaExito(List<LocalGame> listaJuegos)
        {
            return new GetLocalGamesResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ListaJuegos = listaJuegos
            };
        }

    }
}