﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetProductRequest
    {
        [DataMember(IsRequired = true)]
        public int IdProduct { get; set; }

        public Result<GetProductRequest> Validar()
        {
            if (IdProduct <= 0)
                return Result<GetProductRequest>.CreateFailed("El objecto 'IdProduct' no puede ser menor o igual a 0.");

            return Result<GetProductRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetProductResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public Product Producto { get; set; }

        public static GetProductResponse RespuestaFallo(Result<GetProductRequest> requestResult)
        {
            return new GetProductResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Producto = null
            };
        }

        public static GetProductResponse RespuestaExito(Product producto)
        {
            return new GetProductResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Producto = producto
            };
        }

    }
}