﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class GetProductsRequest
    {

        public Result<GetProductsRequest> Validar()
        {

            return Result<GetProductsRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class GetProductsResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public List<Product> ListaProductos { get; set; }

        public static GetProductsResponse RespuestaFallo(Result<GetProductsRequest> requestResult)
        {
            return new GetProductsResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                ListaProductos = null
            };
        }

        public static GetProductsResponse RespuestaExito(List<Product> listaproductos)
        {
            return new GetProductsResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                ListaProductos = listaproductos
            };
        }
    }
}