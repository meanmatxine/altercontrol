﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class SaveProductRequest
    {
        [DataMember(IsRequired = true)]
        public Product Product { get; set; }

        public Result<SaveProductRequest> Validar()
        {
            if (Product == null)
                return Result<SaveProductRequest>.CreateFailed("El objecto 'Producto' no puede ser nulo.");

            return Result<SaveProductRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class SaveProductResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        [DataMember]
        public ProductResumen Producto { get; set; }

        public static SaveProductResponse RespuestaFallo(Result<SaveProductRequest> requestResult)
        {
            return new SaveProductResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage,
                Producto = null
            };
        }

        public static SaveProductResponse RespuestaExito(ProductResumen producto)
        {
            return new SaveProductResponse()
            {
                EsCorrecto = true,
                MensajeError = "",
                Producto = producto
            };
        }

    }
}