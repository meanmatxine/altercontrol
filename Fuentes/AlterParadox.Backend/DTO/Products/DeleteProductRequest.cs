﻿using AlterParadox.Backend.Infrastructure;
using AlterParadox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AlterParadox.Backend.DTO
{
    [DataContract]
    public class DeleteProductRequest
    {
        [DataMember(IsRequired = true)]
        public int productoID { get; set; }

        public Result<DeleteProductRequest> Validar()
        {
            if (productoID <= 0)
                return Result<DeleteProductRequest>.CreateFailed("El objecto 'productoID' no puede ser menor o igual a 0.");

            return Result<DeleteProductRequest>.CreateSucceeded(this);
        }
    }

    [DataContract]
    public class DeleteProductResponse
    {
        [DataMember]
        public bool EsCorrecto { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MensajeError { get; set; }

        public static DeleteProductResponse RespuestaFallo(Result<DeleteProductRequest> requestResult)
        {
            return new DeleteProductResponse()
            {
                EsCorrecto = false,
                MensajeError = requestResult.ErrorMessage
            };
        }

        public static DeleteProductResponse RespuestaExito()
        {
            return new DeleteProductResponse()
            {
                EsCorrecto = true,
                MensajeError = ""
            };
        }

    }
}