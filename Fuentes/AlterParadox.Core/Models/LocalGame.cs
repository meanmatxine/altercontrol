﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    public class LocalGame_Filter
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public bool All { get; set; }
        public int Status { get; set; }

        public bool FilterActivate { get; set; }
        public int? Family { get; set; }
        public int? Type { get; set; }
        public int? MinPlayers { get; set; }
        public int? MaxPlayers { get; set; }
        public int? Difficulty { get; set; }
        public int? Association { get; set; }

        public LocalGame_Filter()
        {
            this.Filter = "";
            this.Id = 0;
            this.All = true;
            this.Status = 99;

            this.FilterActivate = false;
            this.Family = null;
            this.Type = null;
            this.MinPlayers = null;
            this.MaxPlayers = null;
            this.Difficulty = null;
            this.Association = null;

        }
    }

    public class LocalGame
    {
        public int Id { get; set; }
        public Game TheGame { get; set; }
        public Association Association { get; set; }
        public string Comments { get; set; }
        public int Status { get; set; }

        public override string ToString()
        {
            return TheGame.Name + " (" + Association.Name+")";
        }
    }
}
