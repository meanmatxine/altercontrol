﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{

    public class Game_Filter
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }

        public Game_Filter()
        {
            this.Filter = "";
            this.Id = 0;
            this.Name = "";
        }
    }

    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Editorial { get; set; }

        public int Family { get; set; }
        public int Type { get; set; }
        public int Years { get; set; }

        public int PlayerMin { get; set; }
        public int PlayerMax { get; set; }
        public int Difficulty { get; set; }
        public int Duration { get; set; }
        public int IsExpansion { get; set; }

        public string Comments { get; set; }

        public override string ToString()
        {
            return Name;
        }
    
    }
}
