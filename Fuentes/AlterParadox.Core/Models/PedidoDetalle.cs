﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    class PedidoDetalle
    {
        public int pedidoID { get; set; }
        public int productoID { get; set; }
        public int pdCantidad { get; set; }

        private static string tabla = "TPV_PedidosDetalle";

    }
}
