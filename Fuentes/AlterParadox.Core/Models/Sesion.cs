﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    class Sesion
    {
        public int sesionID { get; set; }
        public string sesionStart { get; set; }//"DD/MM/YYYY HH:MM:SS"
        public string sesionEnd { get; set; }//"DD/MM/YYYY HH:MM:SS"
        public decimal sesionDineroInicial { get; set; }
        public decimal sesionDineroFinal { get; set; }
        public int sesionActiva { get; set; }//0-1
        public decimal sesionDescuadre { get; set; }

        private static string tabla = "tpv_sesiones";

    }
}
