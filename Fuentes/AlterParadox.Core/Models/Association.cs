﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    [DataContract]
    public class Association
    {
        public Association()
        {
            Id = null;
            Name = string.Empty;
            Contact = string.Empty;
            Email = string.Empty;
            Phone = string.Empty;
            Comments = string.Empty;
        }
        [DataMember] public int? Id { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string Contact { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public string Phone { get; set; }
        [DataMember] public string Comments { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class AssociationResumen
    {
        [DataMember] public string Action { get; set; }
        [DataMember] public int? Id { get; set; }
        [DataMember] public string Name { get; set; }
    }

}
