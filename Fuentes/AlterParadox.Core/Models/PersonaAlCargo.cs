﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core.Models
{
    [DataContract]
    public class PersonaAlCargo
    {
        public PersonaAlCargo()
        {
        }

        public PersonaAlCargo(string nombre, string apellidos, DateTime birthday)
        {
            this.Id = null;
            this.Number = null;
            this.Birthday = birthday;
            this.Name = nombre;
            this.LastName = apellidos;
        }

        [DataMember] public int? Id { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string LastName { get; set; }
        [DataMember] public DateTime Birthday { get; set; }

        [DataMember]
        public string Edad
        {
            get
            {

                var today = DateTime.Now;
                var dateOfB = Birthday;

                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (dateOfB.Year * 100 + dateOfB.Month) * 100 + dateOfB.Day;

                var years =  (a - b) / 10000;

                return $"{years} años.{((years < 18) ? " menor." : "")}";

            }
        }

        [DataMember] public int? Number { get; set; }
    }
}