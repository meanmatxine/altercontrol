﻿

namespace AlterParadox.Core.Models
{
    public class Mejora
    {
        public int? Id { get; set; }
        public string Usuario { get; set; }
        public string MejoraText { get; set; }
    }
}
