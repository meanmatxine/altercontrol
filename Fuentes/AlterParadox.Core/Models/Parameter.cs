﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core.Models
{
    public class Parameter
    {
        public int SQLVersion { get; set; }
        public bool REVsCheckThirtyMinutes { get; set; }

    }
}
