﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    public class ActivityUser
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int Activity_Id { get; set; }
        [DataMember]
        public int User_Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Number { get; set; }

        [DataMember]
        public DateTime InscriptionDate { get; set; }
        [DataMember]
        public bool Reserve { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string Aviso { get; set; }
        [DataMember]
        public bool Inscrito { get; set; }
    }

    [DataContract]
    public class ActivityUserResumen
    {
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public bool Reserve { get; set; }

    }

}
