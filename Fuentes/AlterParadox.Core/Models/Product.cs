﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.Serialization;

namespace AlterParadox.Core
{
    [DataContract]
    public class Product
    {
        [DataMember] public int productoID { get; set; }
        [DataMember] public string productoCB { get; set; }
        [DataMember] public string productoName { get; set; }
        [DataMember] public int productoTipo { get; set; }
        [DataMember] public decimal productoPrecioSinIva { get; set; }
        [DataMember] public int productoIVA { get; set; }
        [DataMember] public decimal productoPrecio { get; set; }
        [DataMember] public int productoCantidad { get; set; }
        [DataMember] public string productoImage { get; set; }
        [DataMember] public int productoActivo {get; set;}


    }

    [DataContract]
    public class ProductResumen
    {
        [DataMember] public string Action { get; set; }
        [DataMember] public int? productoID { get; set; }
        [DataMember] public string productoName { get; set; }
    }
}
