﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    class ControlCaja
    {
        public int cajaMovID { get; set; }
        public string cajaMovFecha { get; set; }//"DD/MM/YYYY HH:MM:SS"
        public string cajaPersona { get; set; }
        public decimal cajaIngreso { get; set; }
        public decimal cajaExtracto { get; set; }
        public string cajaConcepto { get; set; }
        public string cajaObservaciones { get; set; }
        public decimal cajaDineroActual { get; set; }
        public decimal cajaDescuadre { get; set; }


        private static readonly string tabla = "TPV_ControlCaja";
    }
}
