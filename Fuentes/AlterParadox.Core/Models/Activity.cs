﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{

    public class Activity_JSON
    {
        public string actividadID { get; set; }
        public string actividadNombre { get; set; }
        public string actividadShortDesc { get; set; }
        public string actividadLongDesc { get; set; }
        public string actividadOrganizador { get; set; }
        public string actividadPlazasMin { get; set; }
        public string actividadPlazasMax { get; set; }
        public string actividadInscritos { get; set; }
        public string actividadFecha { get; set; }
        public string actividadLugar { get; set; }
        public string actividadCancelada { get; set; }
        public string actividadLastUpdate { get; set; }
        public string actividadType { get; set; }
    }

    [DataContract]
    public class Activity
    {
        [DataMember] public int? Id { get; set; }

        [DataMember] public string Organizer { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public string Phone { get; set; }
        [DataMember] public string Association { get; set; }

        [DataMember] public string Name { get; set; }
        [DataMember] public DateTime ActivityDate { get; set; }
        [DataMember] public string Place { get; set; }
        [DataMember] public string Type { get; set; }

        [DataMember] public string Participants { get; set; }
        [DataMember] public int MaxParticipants { get; set; }
        [DataMember] public int MinParticipants { get; set; }
        [DataMember] public int NumParticipants { get; set; }

        [DataMember] public int Duration { get; set; }

        [DataMember] public string Summary { get; set; }
        [DataMember] public string Comments { get; set; }

        [DataMember] public string Winner { get; set; }


        [DataMember] public bool Enabled { get; set; }

        public string Participantes { get {
                if(MinParticipants != 0)
                    return $"{NumParticipants}/{MaxParticipants}m{MinParticipants}";
                else
                    return $"{NumParticipants}/{MaxParticipants}";
            } }
        public bool IsAbleToJoin { get
            {
                //if (this.Type == "REV")
                //{
                //    if (this.ActivityDate.AddMinutes(-30) < DateTime.Now)
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}


                //Actividad cerda de comidas
                if (this.Id == 138)
                    return true;

                //Actividad cerda de comidas
                if (this.Id == 127)
                    return true;

                int turno = DateTime.Now.Hour < 16 ? 0 : 1;
                if (turno == 0) //Turno de 10 a 16
                {
                    if (this.ActivityDate.Date == DateTime.Now.Date)
                    {
                        if (this.ActivityDate.Hour < 16)
                        {
                            return true;
                        }
                        if (this.ActivityDate.Hour == 16 && this.ActivityDate.Minute == 0)
                        {
                            return true;
                        }
                    }
                }
                else //Turno de 16 a 23
                {
                    if (this.ActivityDate.Date == DateTime.Now.Date)
                    {
                        return true;
                    }
                    else if (this.ActivityDate.Date == DateTime.Now.Date.AddDays(1))
                    {
                        if (this.ActivityDate.Hour < 12)
                        {
                            return true;
                        }
                        if (this.ActivityDate.Hour == 12 && this.ActivityDate.Minute == 0)
                        {
                            return true;
                        }
                    }

                }


                

                
                return true;
            }
        }

        [DataMember] public string Needs { get; set; }

        [DataMember] public DateTime LastModify { get; set; }

    }

    [DataContract]
    public class ActivityResumen
    {
        [DataMember] public string Action { get; set; }
        [DataMember] public int? Id { get; set; }
        [DataMember] public string Name { get; set; }

    }
}
