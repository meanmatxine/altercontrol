﻿using AlterParadox.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{

    [DataContract]
    public class User
    {

        [DataMember] public int? Id { get; set; }
        [DataMember] public int? Number { get; set; }
        [DataMember] public string Dni { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string LastName { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public string Phone { get; set; }
        [DataMember] public string CP { get; set; }
        [DataMember] public string Comments { get; set; }

        [DataMember] public string Association { get; set; }
        
        [DataMember] public bool Sleep { get; set; }
        [DataMember] public bool TraeActividades { get; set; }
        [DataMember] public bool DormirListaEspera { get; set; }
        [DataMember] public bool Camiseta { get; set; }
        [DataMember] public string Shirt { get; set; }
        [DataMember] public bool Lunch { get; set; }
        [DataMember] public string SegundoLunch { get; set; }

        [DataMember] public bool TutorPermission { get; set; }
        [DataMember] public int? UserOnCharge { get; set; }

        [DataMember] public string Aviso { get; set; }


        [DataMember] public DateTime Birthday { get; set; }

        [DataMember]
        public string PersonasAlCargo { get; set; }

        public override string ToString()
        {
            return Number + " - " + Name + " " + LastName;
        }

    }


    [DataContract]
    public class UserResumen
    {
        [DataMember] public string Action { get; set; }
        [DataMember] public int? Id { get; set; }
        [DataMember] public int? Number { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public string LastName { get; set; }
        [DataMember] public string Dni { get; set; }
        [DataMember] public bool HumanoDependiente { get; set; }

    }
}
