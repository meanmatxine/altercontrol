﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AlterParadox.Core
{
    public class Producto
    {
        public int productoID { get; set; }
        public string productoCB { get; set; }
        public string productoName { get; set; }
        public int productoTipo { get; set; }
        public decimal productoPrecioSinIva { get; set; }
        public int productoIva { get; set; }
        public decimal productoPrecio { get; set; }
        public int productoCantidad { get; set; }

        private static string tabla = "TPV_Productos";

    }
}
