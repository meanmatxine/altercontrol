﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlterParadox.Core
{
    class ProductoTipo
    {
        public int tipoID { get; set; }
        public string tipoName { get; set; }

        private static string tabla = "TPV_ProductoTipo";

    }
}
