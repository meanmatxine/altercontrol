﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlterParadox.Core.Controls
{
    public partial class Fecha : UserControl
    {
        private DateTime? fechaControl;
        public Fecha()
        {
            InitializeComponent();
            fechaControl = null;
        }

        public void SetFecha(DateTime fecha)
        {
            fechaControl = fecha;
            txtFecha.Text = fecha.ToShortDateString();
            Validate();
        }

        public void CleanControl()
        {
            fechaControl = null;
            txtFecha.Text = String.Empty;
            Validate();
        }

        public DateTime? GetFecha()
        {
            return fechaControl;
        }
        public DateTime GetFechaForzada()
        {
            //Si no tiene fecha esto petará
            return fechaControl.Value;
        }

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when textbox Date is Validated")]
        public event EventHandler Validated;
        private void txtFecha_Validated(object sender, EventArgs e)
        {
            Validate();
            //bubble the event up to the parent
            if (this.Validated != null)
                this.Validated(this, e);

        }
        private void Validate()
        {
            DateTime fechaFinal;
            var textoFormateado = string.Empty;
            if (txtFecha.Text.Length > 0)
            {
                textoFormateado = txtFecha.Text.Replace("/", "-");
                if (textoFormateado.Length == 8)//ddmmaaaa ej 15012022
                {
                    textoFormateado = $"{textoFormateado.Substring(0, 2)}-{textoFormateado.Substring(2, 2)}-{textoFormateado.Substring(4, 4)}";
                }
            }
            var test = DateTime.TryParse(textoFormateado, out fechaFinal);
            if (test)
            {
                txtFecha.Text = fechaFinal.ToShortDateString();
                fechaControl = fechaFinal;
                txtFecha.ForeColor = Color.Black;
            }
            else
            {
                fechaControl = null;
                txtFecha.ForeColor = Color.Red;
            }
        }

        private void txtFecha_Enter(object sender, EventArgs e)
        {
            txtFecha.SelectAll();
        }

        private void txtFecha_TextChanged(object sender, EventArgs e)
        {
            if (fechaControl.HasValue)
            {
                fechaControl = null;
                //bubble the event up to the parent
                if (this.Validated != null)
                    this.Validated(this, e);
            }
        }
    }
}
